export const state = () => ({
    token:'',
    userName:'',
    user:'',

    systemConfig:'',    // 系统配置信息
    title:'资交网-淘宝虚拟资源买卖网',   // 平台名称
    titleAbbr:'资交网',   // 平台名称
    platform:'资交网 淘宝虚拟资源买卖网_网页模板_设计素材_办公范文_软件下载_视频教程在线学习网站',   // 网站标题
    // platform:'NETSS系统-自主研发的虚拟资源网站系统',   // 网站标题
    keywords:'绿色软件、软件下载、办公范文、PPT、Excel、Word、课程视频、视频教程、在线学习、在线课堂、网站源码、网站模板、html模板、视频素材、设计素材、素材下载',    // 关键词
    description:'资交网让你的资源变得更有价值，是一个优质的互联网资源买卖平台，其中包含网页模板、设计素材、办公范文、软件应用、视频素材和视频课程的分享。除了作品创意与美感，资交网更注重资源的实用性，深入研究每一类资源的使用人群，使用场景、受众、需要资源传递的信息。资源买卖就上资交网www.zijiao.cn',  // 描述
    icp:'',
    logo:'',
})

export const mutations = {
    setToken(state, token){
        state.token = token
    },

    setUser(state, user){
        state.user = user
    },

    setUserName(state, userName){
        state.userName = userName
    },

    systemConfig(state, data){
        state.systemConfig = data
        if(data.title){
            state.title = data.title.value
        }
        if(data.platform){
            state.platform = data.platform.value
        }
        if(data.keywords){
            state.keywords = data.keywords.value
        }
        if(data.description){
            state.description = data.description.value
        }
        if(data.icp){
            state.icp = data.icp.value
        }
        if(data.title_abbr){
            state.titleAbbr = data.title_abbr.value
        }
        if(data.logo){
            this.$cookies.set('logo', data.logo.value)
            state.logo = data.logo.value
        }
    }
}


export const actions = {
    // nuxtServerInit 服务端渲染自动调用，仅服务端，初始化数据
    // nuxtServerInit(store, {app:{$cookies}}) {
    nuxtServerInit(store) {
        let user = this.$cookies.get('gouziyuanUser') ? this.$cookies.get('gouziyuanUser') : '';
        store.commit('setUser', user)
        if(user && user.token){
            store.commit('setToken', user.token)
        }
        if(user && user.username){
            store.commit('setUserName', user.username)
        }
    },

    setUser(store){ // 登录时调用
        let user = this.$cookies.get('gouziyuanUser') ? this.$cookies.get('gouziyuanUser') : '';
        store.commit('setUser', user)
        if(user && user.token){
            store.commit('setToken', user.token)
        }else{
            store.commit('setToken', '')
        }
        if(user && user.username){
            store.commit('setUserName', user.username)
        }
    },

    systemConfig(store){ // 获取系统配置信息
        let data = sessionStorage.getItem('systemConfig') ? sessionStorage.getItem('systemConfig') : '';
        if(data){
            store.commit('systemConfig', JSON.parse(data))
        }
    }
}
