import Mock from "mockjs"




Mock.mock('/api/user/sheyingshi', {
	"status":1,
	"message":"SUCCESS",
	"result|10":[
		{
			"touxiang":'/images/user/',
			"name":"@cname()",
			"zuopin|100-9999":115,
			"follow|50-999":60,
			"is_renzheng":true
		},
	]
})


// 生成作品
Mock.mock('/api/content/zuopin', {
	"status":1,
	"message":"SUCCESS",
	"result|5":[
		{
			"cover":'/images/image/',
			"title":"@cparagraph",
			"status|0-2":0,
			"yuedu|0-99999":0,
			"xiazai|0-99999":0,
			"dianzan|0-99999":0,
			"shoucang|0-99999":0,
		},
	]
})

// 生成专辑
Mock.mock('/api/content/zhuanji', {
	"status":1,
	"message":"SUCCESS",
	"result|5":[
		{
			"cover":'/images/image/',
			"title":"@csentence",
			"status|0-2":0,
			"yuedu|0-99999":0,
			"xiazai|0-99999":0,
			"dianzan|0-99999":0,
			"shoucang|0-99999":0,
            "num|1-999":1
		},
	]
})


// 生成下载数据
Mock.mock('/api/content/xiazai', {
	"status":1,
	"message":"SUCCESS",
	"result|8":[
		{
            "key":"@id",
			"title":"@csentence",
			"price|0-99":0,
			"date":"@date"
		},
	]
})


// 生成提现记录数据
Mock.mock('/api/content/tixianLog', {
	"status":1,
	"message":"SUCCESS",
	"result|10":[
		{

			"date":"@date",
            "totalPrice|10-1000":10,
            "tax":"@float(0.5, 100, 1, 2)",
            "price|10-1000":10,
            "status|0-2":0
		},
	]
})
