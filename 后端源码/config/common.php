<?php 

return [
	'common_prefix' => 'lcs_',	// 公共缓存前缀
	'password_prefix' => 'lcus_',  // 用户密码前缀, 谨慎修改
	'password_system_prefix' => 'lcus_netsystem_',  // 用户密码前缀, 谨慎修改
	'page_size' => 20, // 分页，每页显示条数
	'user_code_host' => 'https://netss.cn/', // 推广二维码跳转域名
	'upload_path' => 'http://api.xxxx.com/v1/imgUpload', // 上传地址
	'static_path' => 'https://cccccc.cn/', // 静态资源地址，七牛云地址


	// 七牛云配置项
	'qiniu' => [
		'accessKey' => '',
		'secretKey' => '',
		'bucket' => '', // 空间名称
		'host' => ''
	],

	// 短信配置
	'sms' => [
		'provider' => 'ali', // 短信服务商，wangyi或ali
		'wangyi' => [ // 网易云短信
			'AppKey' => '',
			'AppSecret' => '',
			// 模板id
			'templateids' => [
				'common' => '', //通用
				'register' => '', // 注册
				'login' => '', // 登录
				'findPwd' => '', // 找回密码
				'notice' => '', // 通知
				'editPhone' => '', // 修改手机号
				'editPwd' => '', // 修改密码
				'formlc_login' => '', // 后台登录
			]
		],
		'ali' => [ // 阿里云短信
			'AppKey' => '',
			'AppSecret' => '',
			'signName' => 'NETSS', // 签名
			// 模板CODE
			'templateids' => [
				'common' => '', //通用
				'register' => '', // 注册
				'login' => '', // 登录
				'findPwd' => '', // 找回密码
				'notice' => '', // 通知
				'editPhone' => '', // 修改手机号
				'editPwd' => '', // 修改密码
				'formlc_login' => '', // 后台登录
			]
		],

		'type' => ['login','register','findPwd','real','common','editPhone','editPwd'], // 自定义发送验证码类型
		'platform_type' => ['formlc_login','formlc_action'], // 自定义后台发送验证码类型, formlc_action 常规操作
		'platform_login_pre' => ['form_login','form_action'], // 后台登录前缀
		'login_prefix' => 'api_login', // 前缀
		'register_prefix' => 'api_register', // 前缀
		'findPwd_prefix' => 'api_findPwd',
		'real_prefix' => 'api_real',
		'sms_code_time' => 300, // 短信验证码有效时间
	],

	// 邮件配置
	'email' => [
		'host' => 'smtp.163.com',  // 邮箱smtp邮箱
		'name' => 'NETSS', // 邮件标题
		'username' => '',  // 发送方邮箱
		'password' => '',  // 发送方smtp密码

		'type' => ['editEmail'], // 自定义发送验证码类型

		'email_time' => 300, // 短信验证码有效时间
	],

	
	'ali_ip_config' => [
		'AppKey' => '',
		'AppSecret' => '',
		'AppCode' => '',
		'url' => ''
	],

	// 微信相关配置
	'WeChat' => [
		'web' => [ // 开放平台网站应用
			'Appid' => '',
			'AppSecret' => '',
			'redirect_uri' => 'https://www.xxxxx.cn/user/wx-login' // 回调地址，前端处理登录状态页面
		],

		'common' => [ // 公众号
			'appID' => '',
        	'appsecret' => '',
        	'token' => '',
        	'host' => 'https://wx.xxxxx.cn'
		]
	]

	
];