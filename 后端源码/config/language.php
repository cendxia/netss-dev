<?php 

return [
    'success' => 'SUCCESS',
    'error' => 'Error',
    'textSuccess' => '操作成功',
    'textError' => '操作失败',
    'request_error' => '非法请求',
    'mysql_error' => '数据操作失败',
    'param_error' => '参数错误',
    'login_error' => '登录失败',
    'login_success' => '登录成功',
    'login_wrong' => '用户名或密码错误',
    'not_login' => '未登录',
    'token_error' => '请登录', // token获取失败
    'token_cache_error' => '请登录',  // token获取失败
    'request_error' => '请求类型错误',
    'startTime_endTime' => '开始时间需小于结束时间',
    'upload_error' => '上传失败',
    'upload_success' => '上传成功',
    'config_error' => '配置信息错误',
    'sms_success' => '短信发送成功',
    'sms_error' => '短信发送失败',
    'email_success' => '邮件发送成功',
    'email_error' => '邮件发送失败',
    'code_error' => '验证码错误'
];