<?php 

/**
 * 
 */


return [
	'success' => 1, // 成功
	'error' => 0, // 通用错误
	'not_login' => -1, // 未登录
	'user_is_register' => -2, // 用户已被注册
	'action_not_found' => -3, // 方法不存在
	'controller_not_found' => -4, // 控制器不存在


	// mysql相关状态配置
	'mysql' => [
		'table_normal' => 1, // 正常
		'table_pedding' => 0, //待审
		'table_delete' => -1,  //删除
	],

	
];

