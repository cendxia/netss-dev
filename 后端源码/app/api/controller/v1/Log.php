<?php 

namespace app\api\controller\v1;

use app\api\business\Log as logBus;

/**
 * 用户日志
 */
class Log extends ApiBase
{
	
	/**
	 * 用户登录日志
	 * @Author   cendxia
	 * @DateTime 2022-10-15T11:41:32+0800
	 * @return   [type]                   [description]
	 */
	public function loginLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$logBus = new logBus();
		$result = $logBus->loginLog($this->user_id, $page, $pageSize);
		return show($result);
	}


	/**
	 * 获取用户操作日志
	 * @Author   cendxia
	 * @DateTime 2022-10-15T11:52:38+0800
	 * @return   [type]                   [description]
	 */
	public function getActionLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$logBus = new logBus();
		$result = $logBus->getActionLog($this->user_id, $page, $pageSize);
		return show($result);
	}
	
}