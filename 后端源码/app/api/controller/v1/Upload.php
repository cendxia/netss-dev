<?php 

namespace app\api\controller\v1;

use app\common\lib\Qiniu;
use app\common\lib\Upload as uploadLib;

/**
 *  上传
 */
class Upload extends ApiBase
{

	private $config;
	
	public function __construct()
	{
		$this->config = new Qiniu();
	}


	/**
	 * 单文件上传到七牛云
	 * @Author   cendxia
	 * @DateTime 2022-02-13T14:15:59+0800
	 * @return   [type]                   [description]
	 */
	public function imgUploadQiniu(){
		if(!request()->isPost()){
			return requestError();
		}

		$type = !empty(input('post.type')) ? input('post.type') : '';
		$bucket = !empty(input('post.bucket')) ? input('post.bucket') : '';

		if(empty($_FILES['file'])){
			return resError(config('language.param_error'));
		}
		
		$result = (new uploadLib())->fileUploadQiniu($_FILES['file'], $type, $bucket);

		if($type == 'wangeditor'){
			$url = $result['url'];
			if($_FILES['file']['type'] == 'image/jpeg' || $_FILES['file']['type'] == 'image/png'){
				$url = $result['url'].'?watermark/4/text/6LWE5Lqk572RIHd3dy56aWppYW8uY24/fontsize/400/fill/Z3JheQ==/dissolve/15/rotate/30/uw/280/uh/280/resize/1'; // 加上水印 gou资源：Z2916LWE5rqQ，资交网：6LWE5Lqk572R，资交网 www.zijiao.cn：6LWE5Lqk572RIHd3dy56aWppYW8uY24
				// "{$signed_url}?watermark/1/image/{$watermark_url}/dissolve/{$opacity}/gravity/{$position}"
			}
			$res = [
				'errno' => 0,
				'data' => [
					'url' => $url,
					'cover' => $result['url'],
					'alt' => '上传成功',
					'href' => ''
				]
			];
			return json($res);
		}
		return show($result);
	}

	

	/**
	 * 上传文件到七牛云
	 * @Author   cendxia
	 * @DateTime 2022-09-28T17:02:24+0800
	 * @return   [type]                   [description]
	 */
	public function fileUploadQiniu(){
		if(!request()->isPost()){
			return requestError();
		}
		if(empty($_FILES['file'])){
			return resError(config('language.param_error'));
		}
		$result = (new uploadLib())->fileUploadQiniu($_FILES['file'], 'file');
		return show($result);
	}

	
}