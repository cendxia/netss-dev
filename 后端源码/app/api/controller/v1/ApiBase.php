<?php 

namespace app\api\controller\v1;

use app\BaseController;

use think\facade\Request;
use think\facade\Cache;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;



/**
 * 
 */
class ApiBase extends BaseController
{
	
	protected $user_id;
	protected $user;

	public function initialize(){
		parent::initialize();
		$this->getUser();
	}


	private function getUser(){
		$token = Request::header('token');

		// 获取公共缓存前缀
        $prefix = config('common.common_prefix');

        $jwtToken = cache(config('redis.token_pre').$token);
        if($jwtToken){
        	$this->user = JwT::decode($jwtToken, new Key($prefix.'login_lc_', 'HS256')); //decode解码

			if(isset($this->user->user_id)){
				$this->user_id = $this->user->user_id;
			}
        }
	}
}