<?php 

namespace app\api\controller\v1;

use app\api\business\MpTongji as tongjiBus;

/**
 * 创作中心数据统计
 */
class MpTongji extends ApiBase
{
	
	/**
	 * 首页数据统计
	 * @Author   cendxia
	 * @DateTime 2022-08-19T16:32:59+0800
	 * @return   [type]                   [description]
	 */
	public function index(){
		$tongjiBus = new tongjiBus();
		$result = $tongjiBus->index($this->user_id);
		return show($result);
	}

	/**
	 * 内容数据
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:18:53+0800
	 * @return   [type]                   [description]
	 */
	public function contentData(){
		$tongjiBus = new tongjiBus();
		$result = $tongjiBus->contentData($this->user_id);
		return show($result);
	}

	/**
	 * 收益数据
	 * @Author   cendxia
	 * @DateTime 2022-08-20T17:46:01+0800
	 * @return   [type]                   [description]
	 */
	public function getProfit(){
		$tongjiBus = new tongjiBus();
		$result = $tongjiBus->getProfit($this->user_id);
		return show($result);
	}

	/**
	 * 收益明细
	 * @Author   cendxia
	 * @DateTime 2022-08-28T11:06:55+0800
	 * @return   [type]                   [description]
	 */
	public function incomeDetails(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$tongjiBus = new tongjiBus();
		$result = $tongjiBus->incomeDetails($this->user_id, $page, $pageSize);
		return show($result);
	}

	
}