<?php 

namespace app\api\controller\v1;

use app\api\business\Ziyuan as ziyuanBus;

/**
 * 
 */
class Ziyuan extends ApiBase
{

	/**
	 * 获取分类
	 * @Author   cendxia
	 * @DateTime 2022-07-27T15:52:35+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys(){
		$subclass = $this->request->param('subclass', false, 'boolean');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getCategorys($subclass);
		return show($result);
	}


	/**
	 * 获取分类详情
	 * @Author   cendxia
	 * @DateTime 2024-04-23T23:15:03+0800
	 * @return   [type]                   [description]
	 */
	public function getCategoryInfo(){
		$category_id = $this->request->param('category_id', null, 'intval');
		$ziyuan = new ziyuanBus();
		$result = $ziyuan->getCategoryInfo($category_id);
		return show($result);
	}

	/**
	 * 根据分类id获取兄弟分类信息
	 * @Author   cendxia
	 * @DateTime 2022-09-01T12:33:28+0800
	 * @return   [type]                   [description]
	 */
	public function getBrotherCategorys(){
		$category_id = $this->request->param('category_id', null, 'intval');
		$ziyuan = new ziyuanBus();
		$result = $ziyuan->getBrotherCategorys($category_id);
		return show($result);
	}


	/**
	 * 随机获取资源列表，在首页使用
	 * @Author   cendxia
	 * @DateTime 2023-11-28T13:26:47+0800
	 * @return   [type]                   [description]
	 */
	public function getRandomList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$ziyuan = new ziyuanBus();
		$result = $ziyuan->getRandomList($page, $pageSize);
		return show($result);
	}


	/**
	 * 获取浏览量最高的资源列表
	 * @Author   cendxia
	 * @DateTime 2023-11-28T14:03:36+0800
	 * @return   [type]                   [description]
	 */
	public function getZiyuanListBrowse(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$ziyuan = new ziyuanBus();
		$result = $ziyuan->getBrowseList($page, $pageSize);
		return show($result);
	}

	/**
	 * 获取资源列表、加上category_id是获取推荐列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T14:07:46+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$category_id = $this->request->param('category_id', null, 'intval');
		$user_id = $this->request->param('user_id', '', 'intval');
		$all = $this->request->param('all', false, 'boolean');

		$ziyuan = new ziyuanBus();
		$result = $ziyuan->getList($page, $pageSize, $user_id, $category_id, $all, true, true);
		return show($result);
	}
	
	/**
	 * 资源详情
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:07:16+0800
	 * @return   [type]                   [description]
	 */
	public function getInfo(){
		$id = $this->request->param('id', null, 'intval');
		$info = $this->request->param('info', null);
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getInfo($this->user_id, $id, $info);
		return show($result);
	}

	/**
	 * 推荐资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:28:19+0800
	 * @return   [type]                   [description]
	 */
	public function getTuijian(){
		$category_id = $this->request->param('category_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getTuijian($category_id);
		return show($result);
	}

	/**
	 * 获取收藏记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:36:49+0800
	 * @return   [type]                   [description]
	 */
	public function collectList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getCollect($this->user_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 收藏/取消收藏资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:04:13+0800
	 */
	public function setCollect(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$ziyuan_id = $this->request->param('ziyuan_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->setCollect($this->user_id, $ziyuan_id);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 专辑列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T09:51:59+0800
	 * @return   [type]                   [description]
	 */
	public function zhuanji(){
		$user_id = $this->request->param('user_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->zhuanji($this->user_id, $user_id);
		return show($result);
	}

	/**
	 * 添加浏览记录
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:32:59+0800
	 * @return   [type]                   [description]
	 */
	public function read(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$ziyuanBus->read($this->user_id, $id); // 不返回任何数据
	}

	/**
	 * 获取当前账号和资源信息，如：是否收藏、购买
	 * @Author   cendxia
	 * @DateTime 2022-08-21T21:21:53+0800
	 * @return   [type]                   [description]
	 */
	public function getUserInfo(){
		$ziyuan_id = $this->request->param('ziyuan_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getUserInfo($this->user_id, $ziyuan_id);
		return show($result);
	}

	/**
	 * 搜索
	 * @Author   cendxia
	 * @DateTime 2022-10-08T21:37:12+0800
	 * @return   [type]                   [description]
	 */
	public function search(){
		$keyWord = $this->request->param('keyWord', '', 'trim');
		$category_id = $this->request->param('category_id', 1, 'intval');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->search($keyWord, $category_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 获取分类所有一级分类下的资源
	 * @Author   cendxia
	 * @DateTime 2022-11-07T23:07:49+0800
	 * @return   [type]                   [description]
	 */
	public function getCategoryZiyuan(){
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$auto = $this->request->param('auto', false, 'boolean');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getCategoryZiyuan($pageSize, $auto);
		if(empty($auto)) return show($result);
	}


	/**
	 * 获取Ta的资源
	 * @Author   cendxia
	 * @DateTime 2022-12-26T17:35:45+0800
	 * @return   [type]                   [description]
	 */
	public function getUserZiyuan(){
		$ziyuan_id = $this->request->param('ziyuan_id', null, 'intval');
		$user_id = $this->request->param('user_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getUserZiyuan($user_id, $ziyuan_id);
		return show($result);
	}


	/**
	 * 获取专辑信息
	 * @Author   cendxia
	 * @DateTime 2023-10-19T20:09:18+0800
	 * @return   [type]                   [description]
	 */
	public function getZhuanjiInfo(){
		$zhuanji = $this->request->param('zhuanji', null, 'trim');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getZhuanjiInfo($zhuanji);
		return show($result);
	}


	/**
	 * 获取标签列表
	 * @Author   cendxia
	 * @DateTime 2024-04-09T19:02:10+0800
	 * @return   [type]                   [description]
	 */
	public function getLabelList(){
		$num = $this->request->param('num', 0, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getLabelList($num);
		return show($result);
	}
}