<?php 

namespace app\api\controller\v1;

use app\BaseController;
use app\api\business\Common as commonBus;
use app\common\lib\Qiniu;


/**
 * 
 */
class Common extends BaseController
{
	
	/**
	 * 发送短信验证码
	 * @Author   cendxia
	 * @DateTime 2022-10-03T00:55:07+0800
	 * @return   [type]                   [description]
	 */
	public function smsSend(){
		if(!request()->isPost()){
			return requestError();
		}

		$result = commonBus::smsSend(input('post.'));
		if($result){
			return R(config('language.sms_success'));
		}
		return resError(config('language.sms_error'));
	}

	/**
	 * 发送邮件
	 * @Author   cendxia
	 * @DateTime 2022-10-14T19:28:57+0800
	 * @return   [type]                   [description]
	 */
	public function emailSend(){
		if(!request()->isPost()){
			return requestError();
		}

		$result = commonBus::emailSend(input('post.'));
		if($result){
			return R(config('language.email_success'));
		}
		return resError(config('language.email_error'));
	}


	/**
	 * 获取七牛云上传token
	 * @Author   cendxia
	 * @DateTime 2022-12-15T12:22:39+0800
	 * @return   [type]                   [description]
	 */
	public function getQiniuToekn(){
		$bucket = $this->request->param('bucket', null, 'trim');
		$token = (new Qiniu)->getToken($bucket);
		return show($token);
	}

	/**
	 * 获取系统配置信息
	 * @Author   cendxia
	 * @DateTime 2022-10-26T21:23:02+0800
	 * @return   [type]                   [description]
	 */
	public function getSystemConfig(){
		$commonBus = new commonBus();
		$result = $commonBus->getSystemConfig();
		return show($result);
	}


	/**
	 * 平台数据统计，平台流量、资源总数、下载次数
	 * @Author   cendxia
	 * @DateTime 2023-11-28T14:20:59+0800
	 * @return   [type]                   [description]
	 */
	public function getIndexTongji(){
		$commonBus = new commonBus();
		$result = $commonBus->getIndexTongji();
		return show($result);
	}

	/**
	 * 获取上传地址
	 * @Author   cendxia
	 * @DateTime 2024-05-04T10:04:29+0800
	 * @return   [type]                   [description]
	 */
	public function getUploadPath(){
		$config = config('common');
		$result = [
			'upload_path' => $config['upload_path'],
			'static_path' => $config['static_path']
		];
		return show($result);
	}

	/**
	 * 获取微信小程序支持平台
	 * @Author   cendxia
	 * @DateTime 2023-04-09T19:05:12+0800
	 * @return   [type]                   [description]
	 */
	public function getToolxcx(){
		$result = [
			'抖音',
			'小红书',
			'皮皮虾',
			'微博',
			'度小视',
			'西瓜视频',
			'陌陌',
			'哔哩哔哩',
			'美拍',
			'逗拍',
			'6间房',
			'虎牙',
			'新片场',
			'更多功能待你解锁'
		];

		return show($result);
	}

	
}