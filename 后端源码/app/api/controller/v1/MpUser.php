<?php 

namespace app\api\controller\v1;

use app\api\business\UserWallet;
use app\api\business\MpUser as muser;

/**
 * 
 */
class MpUser extends ApiBase
{
	
	/**
	 * 获取钱包信息
	 * @Author   cendxia
	 * @DateTime 2022-08-28T15:40:54+0800
	 * @return   [type]                   [description]
	 */
	public function getWallet(){
		$userWallet = new UserWallet();
		$result = $userWallet->getWallet($this->user_id);
		return show($result);
	}


	/**
	 * 用户提现
	 * @Author   cendxia
	 * @DateTime 2024-01-17T11:54:22+0800
	 * @return   [type]                   [description]
	 */
	public function tixian(){
		if(!request()->isPost()){
			return requestError();
		}
		$money = $this->request->param('money', null, 'intval');
		$accountType = $this->request->param('accountType', null, 'trim');
		$muser = new muser();
		$result = $muser->tixian($this->user_id, $money, $accountType);

		$result = true;
		if(empty($result)){
			return resError('提现失败');
		}
		return R('提现成功，请等待审核！');
	}

	/**
	 * 获取提现记录
	 * @Author   cendxia
	 * @DateTime 2022-08-29T23:20:07+0800
	 * @return   [type]                   [description]
	 */
	public function getTixianLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$muser = new muser();
		$result = $muser->getTixianLog($this->user_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 申请创作者
	 * @Author   cendxia
	 * @DateTime 2022-10-10T15:47:45+0800
	 * @return   [type]                   [description]
	 */
	public function registerMp(){
		if(!request()->isPost()){
			return requestError();
		}
		$muser = new muser();
		$result = $muser->registerMp($this->user_id, input('post.'));
		if(empty($result)){
			return resError('申请失败');
		}
		return R('提交成功，请等待审核！');
	}

	/**
	 * 获取申请创作者状态
	 * @Author   cendxia
	 * @DateTime 2022-10-12T13:06:34+0800
	 * @return   [type]                   [description]
	 */
	public function getMpStatus(){
		$muser = new muser();
		$result = $muser->getMpStatus($this->user_id);
		return show($result);
	}

	/**
	 * 获取收款账户列表信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T14:14:37+0800
	 * @return   [type]                   [description]
	 */
	public function getUserAccount(){
		$muser = new muser();
		$result = $muser->getUserAccount($this->user_id);
		return show($result);
	}

	/**
	 * 添加收款信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T14:44:43+0800
	 */
	public function setUserAccount(){
		if(!request()->isPost()){
			return requestError();
		}
		$muser = new muser();
		$result = $muser->setUserAccount($this->user_id, input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	public function delUserAccount(){
		if(!request()->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$muser = new muser();
		$result = $muser->delUserAccount($this->user_id, $id);
		if(empty($result)){
			return resError('删除失败');
		}
		return R('删除成功');
	}

	/**
	 * 获取用户最新状态
	 * @Author   cendxia
	 * @DateTime 2022-11-12T00:32:24+0800
	 * @return   [type]                   [description]
	 */
	public function upUserStatus(){
		$token = request()->header('token');
		$muser = new muser();
		$result = $muser->upUserStatus($this->user_id, $token);
		return show($result);
	}
}