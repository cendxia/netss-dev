<?php 

namespace app\api\controller\v1;

use app\BaseController;

use app\api\business\LogRegister as userBus;
use think\facade\Request;

/**
 * 用户登录与注册
 */
class LogRegister extends BaseController
{

	private $userBus;

	public function __construct(userBus $userBus){
		$this->initialize();
		$this->userBus = $userBus;
	}
	
	/**
	 * 用户登录
	 * @Author   cendxia
	 * @DateTime 2022-09-18T01:55:22+0800
	 * @return   [type]                   [description]
	 */
	public function login(){
		if(!request()->isPost()){
			return requestError();
		}
		$result = $this->userBus->login(input('post.'));
		return show($result);
	}


	/**
	 * 用户注册
	 * @Author   cendxia
	 * @DateTime 2022-09-18T01:55:42+0800
	 * @return   [type]                   [description]
	 */
	public function register(){
		if(!request()->isPost()){
			return requestError();
		}
		$result = $this->userBus->register(input('post.'));
		if(empty($result)){
			return resError('注册失败');
		}
		return R('注册成功');
	}


	/**
	 * 退出登录
	 * @Author   cendxia
	 * @DateTime 2022-10-10T21:11:18+0800
	 * @return   [type]                   [description]
	 */
	public function loginOut(){
		if(!request()->isPost()){
			return requestError();
		}
		$token = Request::header('token');
		$result = $this->userBus->loginOut($token);
		if(empty($result)){
			return resError('退出登录失败');
		}
		return R('已退出登录');
	}
}