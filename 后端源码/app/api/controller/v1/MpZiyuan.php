<?php 

namespace app\api\controller\v1;

use app\api\business\MpZiyuan as ziyuanBus;
use app\api\business\Ziyuan;

/**
 * 
 */
class MpZiyuan extends ApiBase
{
	/**
	 * 获取资源列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T14:07:46+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');


		// 0待审核 1审核通过 2审核未通过 3草稿箱

		$category_id = $this->request->param('category_id', null, 'intval');
		$status = $this->request->param('status', null, 'trim'); // 一定要设置为trim，否则为空是得到的是0
		$start_time = $this->request->param('start_time', null, 'trim');
		$end_time = $this->request->param('end_time', null, 'trim');
		$title = $this->request->param('title', null, 'trim');

		$ziyuan = new ziyuanBus();
		$result = $ziyuan->getList($this->user_id, $page, $pageSize, $category_id, $status, $start_time, $end_time, $title);
		return show($result);
	}

	/**
	 * 发布资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T15:39:25+0800
	 * @return   [type]                   [description]
	 */
	public function fabu(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->fabu($this->user_id, input('post.'));
		if(empty($result)){
			return resError('发布失败');
		}
		return R('发布成功');
	}

	/**
	 * 创作中心-获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-08-20T15:37:06+0800
	 * @return   [type]                   [description]
	 */
	public function getDownloadLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getDownloadLog($this->user_id, $page, $pageSize);
		return show($result);
	}


	/**
	 * 获取已发布资源类别
	 * @Author   cendxia
	 * @DateTime 2022-09-25T16:55:47+0800
	 * @return   [type]                   [description]
	 */
	public function getUserZiyuanCategory(){
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getUserZiyuanCategory($this->user_id);
		return show($result);
	}


	// 资源操作，置顶、仅自己可见
	public function operation(){
		$ziyuan_id = $this->request->param('ziyuan_id', null, 'intval');
		$type = $this->request->param('type', null, 'trim');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->operation($this->user_id, $ziyuan_id, $type);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 获取资源详情，修改时使用
	 * @Author   cendxia
	 * @DateTime 2022-09-26T16:09:54+0800
	 * @return   [type]                   [description]
	 */
	public function getZiyuanInfo(){
		$ziyuan_id = $this->request->param('ziyuan_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getZiyuanInfo($this->user_id, $ziyuan_id);
		return show($result);
	}

	/**
	 * 删除资源
	 * @Author   cendxia
	 * @DateTime 2022-10-10T11:12:46+0800
	 * @return   [type]                   [description]
	 */
	public function delZiyuan(){
		$ziyuan_id = $this->request->param('ziyuan_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->delZiyuan($this->user_id, $ziyuan_id);
		if(empty($result)){
			return resError('删除失败');
		}
		return R('删除成功');
	}
}