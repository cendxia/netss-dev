<?php 

namespace app\api\controller\v1;

use app\api\business\WeChat as service;
use think\facade\Request;

/**
 * 
 */
class WeChat extends ApiBase
{

	/**
	 * 公众号接入入口
	 * @Author   cendxia
	 * @DateTime 2022-10-21T19:58:11+0800
	 * @return   [type]                   [description]
	 */
	public function interface(){
		(new service)->interface();
	}


	/**
	 * 获取登录二维码
	 * @Author   cendxia
	 * @DateTime 2022-10-21T17:27:30+0800
	 * @return   [type]                   [description]
	 */
	public function getLoginCode(){
		$config = config('common.WeChat.web');
		// 回调地址
		$redirect_uri = urlencode($config['redirect_uri']);
		// 第一步，获取Code
		$result = 'https://open.weixin.qq.com/connect/qrconnect?appid='.$config['Appid'].'&redirect_uri='.$redirect_uri.'&response_type=code&scope=snsapi_login&state=STATE#wechat_redirect';
		return show($result);
	}

	/**
	 * 微信登录回调
	 * @Author   cendxia
	 * @DateTime 2022-10-21T17:35:50+0800
	 * @return   [type]                   [description]
	 */
	public function login(){
		if(!request()->isPost()){
			return requestError();
		}
		if(empty(input('post.code'))){
			return resError('登录失败');
		}
		$service = new service();
		$result = $service->login(input('post.code'));
		if(empty($result)){
			return resError('登录失败');
		}
		return show($result);
	}


	/**
	 * 通过token获取登录信息
	 * @Author   cendxia
	 * @DateTime 2022-10-27T22:17:44+0800
	 * @return   [type]                   [description]
	 */
	public function getLoginData(){
		if(!request()->isPost()){
			return requestError();
		}
		$token = $this->request->param('token', null, 'trim');;
		$service = new service();
		$result = $service->getLoginData($token);
		if(empty($result)){
			return resError('登录失败');
		}
		return show($result);
	}

	/**
	 * 获取微信带参数的二维码
	 * @Author   cendxia
	 * @DateTime 2022-10-24T12:50:25+0800
	 * @return   [type]                   [description]
	 */
	public function getQrCode(){
		$type = $this->request->param('type', null, 'trim');
		$service = new service();
		$result = $service->getQrCode($this->user_id, $type);
		return show($result);
	}


	/**
	 * 微信公众号受权登录
	 * @Author   cendxia
	 * @DateTime 2022-10-25T17:46:02+0800
	 * @return   [type]                   [description]
	 */
	public function wxlogin(){
		$result = (new service)->wxlogin();
		return show($result);
	}

	/**
	 * 微信公众号获取用户详情
	 * @Author   cendxia
	 * @DateTime 2022-10-25T17:46:41+0800
	 * @return   [type]                   [description]
	 */
	public function userInfo(){
		if(empty($_GET['code'])){
        	throw new \think\Exception(config('language.param_error'));
        }
        $param = Request::param();
		$result = (new service)->userInfo($_GET['code'], $param);
		if($result && isset($result['type']) && isset($result['url'])){
			return redirect($result['url']);
		}
		if($result){
			return redirect($result);
		}
	}

}