<?php 

namespace app\api\controller\v1;

use app\api\business\Article as service;

/**
 * 
 */
class Article extends ApiBase
{


	/**
	 * 获取文章栏目
	 * @Author   cendxia
	 * @DateTime 2023-11-29T13:17:16+0800
	 * @return   [type]                   [description]
	 */
	public function getArticleColumn(){
		$service = new service();
		$result = $service->getArticleColumn();
		return show($result);
	}


	/**
	 * 文章搜索
	 * @Author   cendxia
	 * @DateTime 2023-11-29T13:45:24+0800
	 * @return   [type]                   [description]
	 */
	public function getArticleSearch(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$keyWord = $this->request->param('keyWord', null, 'trim');
		$service = new service();
		$result = $service->getArticleSearch($page, $pageSize, $keyWord);
		return show($result);
	}
	
	/**
	 * 获取文章详情
	 * @Author   cendxia
	 * @DateTime 2022-10-20T21:58:54+0800
	 * @return   [type]                   [description]
	 */
	public function getArticleInfo(){
		$id = $this->request->param('id', null, 'intval');
		$service = new service();
		$result = $service->getArticleInfo($id);
		return show($result);
	}

	/**
	 * 获取栏目文章
	 * @Author   cendxia
	 * @DateTime 2022-10-20T22:28:42+0800
	 * @return   [type]                   [description]
	 */
	public function getColumnArticleList(){
		$category_id = $this->request->param('category_id', null, 'intval');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');

		$service = new service();
		$result = $service->getColumnArticleList($category_id, $page, $pageSize);
		return show($result);
	}
}