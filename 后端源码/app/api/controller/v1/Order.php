<?php 

namespace app\api\controller\v1;

use app\api\business\Order as orderBus;

/**
 * 
 */
class Order extends ApiBase
{

	/**
	 * 提交订单
	 * @Author   cendxia
	 * @DateTime 2022-09-15T22:55:49+0800
	 * @return   [type]                   [description]
	 */
	public function submitOrder(){
		if(!request()->isPost()){
			return requestError();
		}
		$ziyuan_id = $this->request->param('ziyuan_id', null, 'intval');
		$orderBus = new orderBus();
		$result = $orderBus->submitOrder($this->user_id, $ziyuan_id);
		if(empty($result)){
			return resError('订单创建失败');
		}
		return show($result, '订单创建成功');
	}


	/**
	 * 订单列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:16:33+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$orderBus = new orderBus();
		$result = $orderBus->getList($this->user_id, $page, $pageSize, 'user_id');
		return show($result);
	}

	/**
	 * 获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:30:40+0800
	 * @return   [type]                   [description]
	 */
	public function getDownloadLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$orderBus = new orderBus();
		$result = $orderBus->getDownloadLog($this->user_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 获取卖家订单列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:04:57+0800
	 * @return   [type]                   [description]
	 */
	public function getSellList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$orderBus = new orderBus();
		$result = $orderBus->getList($this->user_id, $page, $pageSize, 'sell_user_id');
		return show($result);
	}


	/**
	 * 资源下载
	 * @Author   cendxia
	 * @DateTime 2022-10-20T16:06:23+0800
	 */
	public function ziyuanDownload(){
		if(!request()->isPost()){
			return requestError();
		}
		$order_id = $this->request->param('order_id', null, 'trim');
		$orderBus = new orderBus();
		$result = $orderBus->ziyuanDownload($this->user_id, $order_id);
		if(empty($result)){
			return resError('未获取到订单信息');
		}
		return show($result);
	}


	/**
	 * 获取订单详情
	 * @Author   cendxia
	 * @DateTime 2022-11-06T18:45:09+0800
	 * @return   [type]                   [description]
	 */
	public function getOrderInfo(){
		$order_id = $this->request->param('order_id', null, 'trim');
		$orderBus = new orderBus();
		$result = $orderBus->getOrderInfo($order_id, $this->user_id);
		return show($result);
	}

	/**
	 * 获取资源/课程订单详情
	 * @Author   cendxia
	 * @DateTime 2023-04-20T11:58:00+0800
	 * @return   [type]                   [description]
	 */
	public function getOrderDetails(){
		$order_id = $this->request->param('order_id', null, 'trim');
		$orderBus = new orderBus();
		$result = $orderBus->getOrderDetails($order_id, $this->user_id);
		return show($result);
	}

	/**
	 * 查询订单状态
	 * @Author   cendxia
	 * @DateTime 2022-11-30T19:31:41+0800
	 * @return   [type]                   [description]
	 */
	public function getOrderStatus(){
		$order_id = $this->request->param('order_id', null, 'trim');
		$orderBus = new orderBus();
		$result = $orderBus->getOrderStatus($order_id, $this->user_id);
		return show($result);
	}
}