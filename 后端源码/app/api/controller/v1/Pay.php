<?php 

namespace app\api\controller\v1;

use app\api\business\Pay as service;


use app\common\lib\Str;
use think\facade\Db;


/**
 * 支付
 */
class Pay extends ApiBase
{
    
    
    /**
     * 微信扫码支付 官方
     * @Author   cendxia
     * @DateTime 2022-09-14T22:54:36+0800
     * @return   [type]                   [description]
     */
    public function nativePay(){
        $order_id = $this->request->param('order_id', null, 'trim');
        $type = $this->request->param('type', 'ziyuan', 'trim');
        $service = new service();
        $result = $service->nativePay($this->user_id, $order_id, $type);
        return show($result);
    }

    /**
     * 微信支付回调
     * @Author   cendxia
     * @DateTime 2022-11-03T13:31:49+0800
     * @return   [type]                   [description]
     */
    public function wxPayCallback(){
        // 接收数据
        $data = file_get_contents('php://input');
        $service = new service();
        $result = $service->wxPayCallback($data);
        Db::name('test')->insert(['data'=>$data,'title'=>'0412微信支付']);
        if($result){
            $returnParams = [
                'return_code'=>'SUCCESS',
                'return_msg'=>'OK'
            ];
            echo Str::ArrToXml($returnParams); exit;
        }
    }


    /////////////////支//付//宝/////////////////////////

    // 开发版无支付宝支付


    /////////////////虎//皮//椒/////////////////////////

    // 开发版无虎皮椒支付


    /////////////////////////  V I P  /////////////////////////


    /**
     * VIP微信支付
     * @Author   cendxia
     * @DateTime 2024-03-22T18:19:04+0800
     * @return   [type]                   [description]
     */
    public function vipNativePay(){
        $order_id = $this->request->param('order_id', null, 'trim');
        $service = new service();
        $result = $service->nativePay($this->user_id, $order_id, 'vip');
        return show($result);
    }

}