<?php 

namespace app\api\controller\v1;
use app\api\business\MpKecheng as keBus;

/**
 * 课程
 */
class MpKecheng extends ApiBase
{

	/**
	 * 获取课程列表
	 * @Author   cendxia
	 * @DateTime 2023-03-16T19:56:05+0800
	 * @return   [type]                   [description]
	 */
	public function getKeList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');

		$keBus = new keBus();
		$result = $keBus->getKeList($this->user_id, $page, $pageSize);
		return show($result);
	}
	
	/**
	 * 创建/编辑课程
	 * @Author   cendxia
	 * @DateTime 2023-03-16T18:17:58+0800
	 * @return   [type]                   [description]
	 */
	public function kecheng(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$keBus = new keBus();
		$result = $keBus->kecheng($this->user_id, input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 添加/编辑课程章节
	 * @Author   cendxia
	 * @DateTime 2023-03-17T14:22:07+0800
	 * @return   [type]                   [description]
	 */
	public function chapters(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$keBus = new keBus();
		$result = $keBus->chapters($this->user_id, input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}


	/**
	 * 获取课程章节
	 * @Author   cendxia
	 * @DateTime 2023-03-17T23:32:34+0800
	 * @return   [type]                   [description]
	 */
	public function getChaptersList(){
		$ke_id = $this->request->param('ke_id', null, 'intval');
		$keBus = new keBus();
		$result = $keBus->getChaptersList($this->user_id, $ke_id);
		return show($result);
	}


	/**
	 * 添加/编辑课程视频内容
	 * @Author   cendxia
	 * @DateTime 2023-03-20T15:46:06+0800
	 * @return   [type]                   [description]
	 */
	public function directory(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$keBus = new keBus();
		$result = $keBus->directory($this->user_id, input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 获取课程详情
	 * @Author   cendxia
	 * @DateTime 2023-03-24T23:46:40+0800
	 * @return   [type]                   [description]
	 */
	public function getKechengInfo(){
		$ke_id = $this->request->param('ke_id', null, 'intval');
		$keBus = new keBus();
		$result = $keBus->getKechengInfo($this->user_id, $ke_id);
		return show($result);
	}

	/**
	 * 删除章节
	 * @Author   cendxia
	 * @DateTime 2023-04-22T14:53:49+0800
	 * @return   [type]                   [description]
	 */
	public function delChapters(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$ke_id = $this->request->param('ke_id', null, 'intval');
		$chapters_id = $this->request->param('chapters_id', null, 'intval');
		$keBus = new keBus();
		$result = $keBus->delChapters($ke_id, $chapters_id, $this->user_id);
		if(empty($result)){
			return resError('删除失败');
		}
		return R('删除成功');
	}


	/**
	 * 删除课程内容
	 * @Author   cendxia
	 * @DateTime 2023-04-22T15:30:01+0800
	 * @return   [type]                   [description]
	 */
	public function delDirectory(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$chapters_id = $this->request->param('chapters_id', null, 'intval');
		$directory_id = $this->request->param('directory_id', null, 'intval');
		$keBus = new keBus();
		$result = $keBus->delDirectory($chapters_id, $directory_id, $this->user_id);
		if(empty($result)){
			return resError('删除失败');
		}
		return R('删除成功');
	}

	/**
	 * 删除课程
	 * @Author   cendxia
	 * @DateTime 2023-04-23T13:26:45+0800
	 * @return   [type]                   [description]
	 */
	public function delKecheng(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$ke_id = $this->request->param('ke_id', null, 'intval');
		$keBus = new keBus();
		$result = $keBus->delDirectory($ke_id, $this->user_id);
		// if(empty($result)){
		// 	return resError('删除失败');
		// }
		// return R('删除成功');
	}
}