<?php 

namespace app\api\controller\v1;

use app\api\business\User as userBus;
use app\api\business\Vip;

/**
 * 
 */
class User extends ApiBase
{
	/**
	 * 获取卖家信息
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:20:54+0800
	 * @return   [type]                   [description]
	 */
	public function sellUser(){
		$user_id = $this->request->param('user_id', null, 'intval');
		$info = $this->request->param('info', false, 'boolean');
		$userBus = new userBus();
		$result = $userBus->sellUser($this->user_id, $user_id, $info);
		return show($result);
	}

	/**
	 * 个人中心，获取粉丝列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T14:42:45+0800
	 * @return   [type]                   [description]
	 */
	public function getFansList(){
		$user_id = $this->request->param('user_id', null, 'intval');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$userBus = new userBus();
		$result = $userBus->getFansList($user_id, $this->user_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 个人中心，获取关注列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T11:00:30+0800
	 * @return   [type]                   [description]
	 */
	public function getFollowList(){
		$user_id = $this->request->param('user_id', null, 'intval');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$userBus = new userBus();
		$result = $userBus->getFollowList($user_id, $this->user_id, $page, $pageSize);
		return show($result);
	}


	/**
	 * 关注
	 * @Author   cendxia
	 * @DateTime 2022-08-19T15:01:41+0800
	 * @return   [type]                   [description]
	 */
	public function follow(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$user_id = $this->request->param('user_id', null, 'intval');
		$userBus = new userBus();
		$result = $userBus->follow($user_id, $this->user_id);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T18:49:03+0800
	 * @return   [type]                   [description]
	 */
	public function getUserInfo(){
		$userBus = new userBus();
		$result = $userBus->getUserInfo($this->user_id);
		return show($result);
	}

	/**
	 * 	修改用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T21:52:26+0800
	 * @return   [type]                   [description]
	 */
	public function editUser(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$userBus = new userBus();
		$result = $userBus->editUser($this->user_id, input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}


	/**
	 * 解绑微信
	 * @Author   cendxia
	 * @DateTime 2022-10-26T11:31:15+0800
	 * @return   [type]                   [description]
	 */
	public function unBinding(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$userBus = new userBus();
		$result = $userBus->unBinding($this->user_id);
		if(empty($result)){
			return resError('解绑失败');
		}
		return R('解绑成功');
	}


	/**
	 * 找回密码
	 * @Author   cendxia
	 * @DateTime 2022-10-26T16:54:07+0800
	 * @return   [type]                   [description]
	 */
	public function findPwd(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$userBus = new userBus();
		$result = $userBus->findPwd(input('post.'));
		if(empty($result)){
			return resError('密码重置失败');
		}
		return R('密码重置成功');
	}

	/**
	 * 用户反馈
	 * @Author   cendxia
	 * @DateTime 2022-12-03T12:46:34+0800
	 * @return   [type]                   [description]
	 */
	public function feedback(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$userBus = new userBus();
		$result = $userBus->feedback($this->user_id, input('post.'));
		if(empty($result)){
			return resError('提交失败');
		}
		return R('提交成功');
	}

	/**
	 * 获取绑定微信需要的参数
	 * @Author   cendxia
	 * @DateTime 2022-12-03T22:26:15+0800
	 * @return   [type]                   [description]
	 */
	public function getWeChatBindingParams(){
		$service = new userBus();
		$result = $service->getWeChatBindingParams($this->user_id);
		return show($result);
	}

	/**
	 * 获取带参数的二维码进行登录
	 * @Author   cendxia
	 * @DateTime 2023-09-22T16:42:38+0800
	 * @return   [type]                   [description]
	 */
	public function getQrCodeLogin(){
		$service = new userBus();
		$result = $service->getQrCodeLogin();
		return show($result);
	}


	/**
	 * 前端通过api查询微信扫码关注登录信息
	 * @Author   cendxia
	 * @DateTime 2023-09-23T16:42:57+0800
	 * @return   [type]                   [description]
	 */
	public function getWxLoginData(){
		$code = $this->request->param('code', null, 'trim');
		$service = new userBus();
		$result = $service->getWxLoginData($code);
		if(empty($result)) return R('fail');
		return show($result);
	}




	/////////////////////  VIP  ////////////////////////


	public function getVipList(){
		$service = new Vip();
		$result = $service->getVipList();
		return show($result);
	}


	/**
	 * 提交VIP订单
	 * @Author   cendxia
	 * @DateTime 2024-03-22T11:37:26+0800
	 * @return   [type]                   [description]
	 */
	public function submitVipOrder(){
		$id = $this->request->param('id', null, 'intval');
		$service = new Vip();
		$result = $service->submitVipOrder($id, $this->user_id);
		return show($result);
	}


	/**
	 * 查询VIP订单状态
	 * @Author   cendxia
	 * @DateTime 2024-03-23T17:07:29+0800
	 * @return   [type]                   [description]
	 */
	public function getVipOrderStatus(){
		$order_id = $this->request->param('order_id', null, 'trim');
		$service = new Vip();
		$result = $service->getOrderStatus($order_id, $this->user_id);
		return show($result);
	}


	/**
	 * 查询我的VIP记录
	 * @Author   cendxia
	 * @DateTime 2024-03-23T18:08:37+0800
	 * @return   [type]                   [description]
	 */
	public function getUserVipList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new Vip();
		$result = $service->getUserVipList($page, $pageSize, $this->user_id);
		return show($result);
	}


	/**
	 * 获取百度网盘下载工具
	 * @Author   cendxia
	 * @DateTime 2024-04-26T08:55:15+0800
	 * @return   [type]                   [description]
	 */
	public function getPandownload(){
		$userBus = new userBus();
		$result = $userBus->getPandownload($this->user_id);
		return show($result);
	}
}