<?php 

namespace app\api\controller;

use app\api\business\Ziyuan as ziyuanBus;

/**
 * 
 */
class Ziyuan extends AuthBase
{

	/**
	 * 获取分类
	 * @Author   cendxia
	 * @DateTime 2022-07-27T15:52:35+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys(){
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getCategorys();
		return show($result);
	}

	/**
	 * 获取资源列表、加上category_id是获取推荐列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T14:07:46+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$category_id = $this->request->param('category_id', null, 'intval');

		$ziyuan = new ziyuanBus();
		$result = $ziyuan->getList($page, $pageSize, '', $category_id);
		return show($result);		
	}
	
	/**
	 * 资源详情
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:07:16+0800
	 * @return   [type]                   [description]
	 */
	public function getInfo(){
		$id = $this->request->param('id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getInfo($id);
		return show($result);
	}

	/**
	 * 推荐资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:28:19+0800
	 * @return   [type]                   [description]
	 */
	public function getTuijian(){
		$category_id = $this->request->param('category_id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getTuijian($category_id);
		return show($result);
	}

	/**
	 * 获取收藏记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:36:49+0800
	 * @return   [type]                   [description]
	 */
	public function collectList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getCollect($this->user_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 收藏/取消收藏资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:04:13+0800
	 */
	public function setCollect(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->setCollect($this->user_id, $id);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 专辑列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T09:51:59+0800
	 * @return   [type]                   [description]
	 */
	public function zhuanji(){
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->zhuanji($this->user_id);
		return show($result);
	}

	/**
	 * 添加浏览记录
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:32:59+0800
	 * @return   [type]                   [description]
	 */
	public function read(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$ziyuanBus = new ziyuanBus();
		$ziyuanBus->read($this->user_id, $id); // 不返回任何数据
	}

	
}