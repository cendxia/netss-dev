<?php 

namespace app\api\controller;

use app\common\lib\Upload;
use app\api\business\Common as commonBus;

/**
 * 
 */
class CommonAuth extends AuthBase
{
	
	/**
	 * 单文件上传到七牛云
	 * @Author   cendxia
	 * @DateTime 2022-02-13T14:15:59+0800
	 * @return   [type]                   [description]
	 */
	public function imgUploadQiniu(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$type = '';
		if(!empty(input('post.type'))){
			$type = input('post.type');
		}
		if(empty($_FILES['file'])){
			return resError(config('language.param_error'));
		}
		$result = (new Upload())->imgUploadQiniu($_FILES['file']);
		if($type == 'wangeditor'){
			$res = [
				'errno' => 0,
				'data' => [
					'url' => $result['url'],
					'alt' => '上传成功',
					'href' => ''
				]
			];
			return json($res);
		}
		return show($result, '上传成功');
	}

}