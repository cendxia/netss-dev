<?php 

namespace app\api\controller;

use app\api\business\User as userBus;


/**
 * 
 */
class UserBase extends ApiBase
{
	
	/**
	 * 登录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T12:08:06+0800
	 * @return   [type]                   [description]
	 */
	public function login(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$userBus = new userBus();
		$result = $userBus->login(input('post.'));
		return show($result);
	}
}