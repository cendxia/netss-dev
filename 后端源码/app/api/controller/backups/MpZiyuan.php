<?php 

namespace app\api\controller;

use app\api\business\MpZiyuan as ziyuanBus;
use app\api\business\Ziyuan;

/**
 * 
 */
class MpZiyuan extends AuthBase
{
	/**
	 * 获取资源列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T14:07:46+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');

		$ziyuan = new Ziyuan();
		$result = $ziyuan->getList($page, $pageSize, $this->user_id,);
		return show($result);		
	}

	/**
	 * 发布资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T15:39:25+0800
	 * @return   [type]                   [description]
	 */
	public function fabu(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->fabu($this->user_id, input('post.'));
		if(empty($result)){
			return resError('发布失败');
		}
		return R('发布成功');
	}

	/**
	 * 创作中心-获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-08-20T15:37:06+0800
	 * @return   [type]                   [description]
	 */
	public function getDownloadLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$ziyuanBus = new ziyuanBus();
		$result = $ziyuanBus->getDownloadLog($this->user_id, $page, $pageSize);
		return show($result);
	}

	
}