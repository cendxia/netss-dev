<?php 

namespace app\api\controller;

use app\api\business\Order as orderBus;

/**
 * 
 */
class Order extends AuthBase
{
	/**
	 * 订单列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:16:33+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$orderBus = new orderBus();
		$result = $orderBus->getList($this->user_id, $page, $pageSize, 'user_id');
		return show($result);
	}

	/**
	 * 获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:30:40+0800
	 * @return   [type]                   [description]
	 */
	public function getDownloadLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$orderBus = new orderBus();
		$result = $orderBus->getDownloadLog($this->user_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 获取卖家订单列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:04:57+0800
	 * @return   [type]                   [description]
	 */
	public function getSellList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$orderBus = new orderBus();
		$result = $orderBus->getList($this->user_id, $page, $pageSize, 'sell_user_id');
		return show($result);
	}
}