<?php 

namespace app\api\controller;

use app\api\business\User as userBus;

/**
 * 
 */
class User extends AuthBase
{
	/**
	 * 获取卖家信息
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:20:54+0800
	 * @return   [type]                   [description]
	 */
	public function sellUser(){
		$user_id = $this->request->param('user_id', null, 'intval');
		$info = $this->request->param('info', false);
		$userBus = new userBus();
		$result = $userBus->sellUser($this->user_id, $user_id, $info);
		return show($result);
	}

	/**
	 * 个人中心，获取粉丝列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T14:42:45+0800
	 * @return   [type]                   [description]
	 */
	public function getFansList(){
		$user_id = $this->request->param('user_id', null, 'intval');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$userBus = new userBus();
		$result = $userBus->getFansList($user_id, $this->user_id, $page, $pageSize);
		return show($result);
	}

	/**
	 * 个人中心，获取关注列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T11:00:30+0800
	 * @return   [type]                   [description]
	 */
	public function getFollowList(){
		$user_id = $this->request->param('user_id', null, 'intval');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$userBus = new userBus();
		$result = $userBus->getFollowList($user_id, $this->user_id, $page, $pageSize);
		return show($result);
	}


	/**
	 * 关注
	 * @Author   cendxia
	 * @DateTime 2022-08-19T15:01:41+0800
	 * @return   [type]                   [description]
	 */
	public function follow(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$user_id = $this->request->param('user_id', null, 'intval');
		$userBus = new userBus();
		$result = $userBus->follow($user_id, $this->user_id);
		if(empty($result)){
			return resError('关注失败');
		}
		return R('关注成功');
	}
}