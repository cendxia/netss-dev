<?php 

namespace app\api\controller;

use app\BaseController;
use app\api\business\Common as commonBus;
use app\common\lib\Sms;

/**
 * 
 */
class Common extends BaseController
{
	
	/**
	 * 发送短信验证码
	 * @Author   cendxia
	 * @DateTime 2022-02-21T15:42:45+0800
	 * @return   [type]                   [description]
	 */
	public function smsSendLckj(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$data = input('post.');
		$region_id = request()->header('regionId');
		if(empty($region_id)){
			$region_id = 3;
		}

		$result = Sms::smsSend($region_id, $data);
		if($result){
			return R(config('language.sms_success'));
		}
		return resError(config('language.sms_error'));
	}

	/**
	 * 获取app信息
	 * @Author   cendxia
	 * @DateTime 2022-04-25T16:04:39+0800
	 * @param    [type]                   $region_id [description]
	 * @return   [type]                              [description]
	 */
	public function getAppInfo(){
		$region_id = $this->request->param('region_id', null, 'intval');
		if(empty($region_id)){
			return resError(config('language.param_error'));
		}
		$commonBus = new commonBus();
		$result = $commonBus->getAppInfo($region_id);
		return show($result);
	}

	/**
	 * app检测更新
	 * @Author   cendxia
	 * @DateTime 2022-03-21T16:52:24+0800
	 * @return   [type]                   [description]
	 */
	public function appUpdate(){
		// 版本号
		$version = $this->request->param('version', null, 'intval');
		$region_id = $this->request->param('region_id', 3, 'intval');
		$commonModel = new commonBus();
		$result = $commonModel->appUpdate($region_id, $version);
		return show($result);
	}
}