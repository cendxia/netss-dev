<?php 

use think\facade\Route;

// 不需要验证token
Route::group('/:version/', function(){
	// 注册
	Route::post('register', 'api/:version.LogRegister/register');
	// 登录
	Route::post('login', 'api/:version.LogRegister/login');
	// 前端通过api查询微信扫码关注登录信息
	Route::post('getWxLoginData', 'api/:version.User/getWxLoginData');
	// 发送短信
	Route::post('smsSend', 'api/:version.Common/smsSend');
	// 发送邮件
	Route::post('emailSend', 'api/:version.Common/emailSend');
	// 找回密码
	Route::post('findPwd', 'api/:version.User/findPwd');

	// 获取资源分类
	Route::get('getCategorys', 'api/:version.Ziyuan/getCategorys');
	// 获取分类详情
	Route::get('getCategoryInfo', 'api/:version.Ziyuan/getCategoryInfo');

	// 获取分类所有一级分类下所有的资源
	Route::get('getCategoryZiyuan', 'api/:version.Ziyuan/getCategoryZiyuan');

	// 资源详情
	Route::get('getZiyuanInfo', 'api/:version.Ziyuan/getInfo');
	// 添加资源浏览记录
	Route::post('ziyuanRead', 'api/:version.Ziyuan/read');
	// 获取推荐资源
	Route::get('getTuijianZiyuan', 'api/:version.Ziyuan/getTuijian');
	// 获取资源列表-随机
	Route::get('getZiyuanListRandom', 'api/:version.Ziyuan/getRandomList');
	// 获取浏览量最高的资源列表
	Route::get('getZiyuanListBrowse', 'api/:version.Ziyuan/getZiyuanListBrowse');
	// 获取资源列表
	Route::get('getZiyuanList', 'api/:version.Ziyuan/getList');
	// 获取专辑列表
	Route::get('getZhuanjiList', 'api/:version.Ziyuan/zhuanji');
	// 获取创作者用户基础信息
	Route::get('sellUser', 'api/:version.User/sellUser');
	// 粉丝列表
	Route::get('getFansList', 'api/:version.User/getFansList');
	// 关注列表
	Route::get('getFollowList', 'api/:version.User/getFollowList');
	// 获取当前账号和的信息，如：是否收藏、购买、、
	Route::get('getUserZiyuanInfo', 'api/:version.Ziyuan/getUserInfo');
	// 根据分类id获取兄弟分类信息
	Route::get('getBrotherCategorys', 'api/:version.Ziyuan/getBrotherCategorys');
	// 搜索
	Route::get('search', 'api/:version.Ziyuan/search');


	// 获取文章栏目
	Route::get('getArticleColumn', 'api/:version.Article/getArticleColumn');
	// 获取文章详情
	Route::get('getArticleInfo', 'api/:version.Article/getArticleInfo');
	// 获取栏目文章
	Route::get('getColumnArticleList', 'api/:version.Article/getColumnArticleList');
	// 文章搜索
	Route::get('getArticleSearch', 'api/:version.Article/getArticleSearch');
	/** 微信相关接口start */
	// 获取登录二维码
	Route::get('weChat/getLoginCode', 'api/:version.WeChat/getLoginCode');
	// 微信登录回调地址
	Route::post('weChat/login', 'api/:version.WeChat/login');
	// 通过token获取登录信息
	Route::post('weChat/getLoginData', 'api/:version.WeChat/getLoginData');

	// 公众号接入
	Route::rule('weChat/interface', 'api/:version.WeChat/interface');
	// 微信公众号登录授权地址
	Route::get('weChat/wxlogin', 'api/:version.WeChat/wxlogin');
	// 微信公众号获取用户信息
	Route::get('weChat/userInfo', 'api/:version.WeChat/userInfo');
	// 微信用户绑定操作
	Route::post('weChat/binding', 'api/:version.WeChat/wxBinding');
	// 微信支付回调
	Route::rule('weChat/payCallback', 'api/:version.Pay/wxPayCallback');
	// 获取带参数的二维码进行登录
	Route::get('getQrCodeLogin', 'api/:version.User/getQrCodeLogin');
	/** 微信相关接口end */

	// 获取系统配置信息
	Route::get('getSystemConfig', 'api/:version.Common/getSystemConfig');

	// 支付宝支付异步通知
	Route::post('alipay/notify', 'api/:version.pay/aliNotify');
	// 支付宝支付同步通知
	Route::get('alipay/return', 'api/:version.pay/aliReturn');

	// 虎皮椒扫码支付回调
	Route::rule('hppayNotify', 'api/:version.pay/hppayNotify');


	// Ta的资源
	Route::get('getUserZiyuan', 'api/:version.Ziyuan/getUserZiyuan');

	// 获取专辑信息
	Route::get('getZhuanjiInfo', 'api/:version.Ziyuan/getZhuanjiInfo');
	// 平台数据统计，平台流量、资源总数、下载次数
	Route::get('getIndexTongji', 'api/:version.Common/getIndexTongji');


	// 获取VIP列表
	Route::get('getVipList', 'api/:version.User/getVipList');

	// 获取标签
	Route::get('getLabelList', 'api/:version.ziyuan/getLabelList');
});


// 需要验证token
Route::group('/:version/', function(){
	// 退出登录
	Route::post('loginOut', 'api/:version.LogRegister/loginOut');
	// 获取用户信息
	Route::get('getUserInfo', 'api/:version.User/getUserInfo');
	// 修改用户信息
	Route::post('setUser', 'api/:version.User/editUser');
	// 根据token获取用户最新状态
	Route::get('upUserStatus', 'api/:version.MpUser/upUserStatus');
	// 获取订单列表
	Route::get('getOrderList', 'api/:version.Order/getList');
	// 获取订单详情
	Route::get('getOrderInfo', 'api/:version.Order/getOrderInfo');
	// 查询订单状态
	Route::get('getOrderStatus', 'api/:version.Order/getOrderStatus');
	// 获取下载记录
	Route::get('getDownloadLog', 'api/:version.Order/getDownloadLog');
	// 收藏列表
	Route::get('getCollectList', 'api/:version.Ziyuan/collectList');
	// 收藏/取消收藏
	Route::post('setCollect', 'api/:version.Ziyuan/setCollect');
	// 关注/取消关注
	Route::post('follow', 'api/:version.User/follow');
	// 提交订单
	Route::post('submitOrder', 'api/:version.Order/submitOrder');
	// 获取上传地址
	Route::get('getUploadPath', 'api/:version.Common/getUploadPath');
	// 图片上传
	Route::post('imgUpload', 'api/:version.Upload/imgUploadQiniu');
	// 上传文件
	Route::post('fileUpload', 'api/:version.Upload/fileUploadQiniu');
	// 申请创作者
	Route::post('registerMp', 'api/:version.MpUser/registerMp');
	// 获取申请创作者状态
	Route::get('getMpStatus', 'api/:version.MpUser/getMpStatus');
	// 资源下载
	Route::post('ziyuanDownload', 'api/:version.Order/ziyuanDownload');
	// 获取登录日志
	Route::get('loginLog', 'api/:version.Log/loginLog');
	// 获取用户操作日志
	Route::get('actionLog', 'api/:version.Log/getActionLog');
	// 用户反馈
	Route::post('feedback', 'api/:version.User/feedback');
	// 获取绑定微信需要的参数,h5绑定微信，即将停用
	Route::get('getWeChatBindingParams', 'api/:version.User/getWeChatBindingParams');

	/** 微信相关接口start */	
	// 微信带参数的二维码
	Route::get('weChat/getQrCode', 'api/:version.WeChat/getQrCode');
	// 微信解绑
	Route::post('weChat/unBinding', 'api/:version.User/unBinding');
	// 微信扫码支付
	Route::post('nativePay', 'api/:version.Pay/nativePay');
	/** 微信相关接口end */

	// 获取支付宝支付url
	Route::post('alipay/pay', 'api/:version.Pay/alipay');

	// 虎皮椒微信扫码支付
	Route::post('hpjWxPay', 'api/:version.pay/hpjWxPay');

	// 获取七牛云上传token
	Route::get('getQiniuToekn', 'api/:version.Common/getQiniuToekn');

	// 提交课程订单
	Route::post('submitKechengOrder', 'api/:version.Kecheng/submitOrder');
	// 查询课程订单状态
	Route::get('getKechengOrderStatus', 'api/:version.Kecheng/getOrderStatus');
	// 获取课程订单列表
	Route::get('getKechengOrderList', 'api/:version.Kecheng/getOrderList');
	// 获取课程订单详情
	Route::get('getKechengOrderInfo', 'api/:version.Kecheng/getOrderInfo');
	// 获取资源/课程订单详情
	Route::get('getOrderDetails', 'api/:version.Order/getOrderDetails');

	
	// 提交VIP订单
	Route::post('submitVipOrder', 'api/:version.User/submitVipOrder');
	// VIP支付宝支付
	Route::post('vipAlipay', 'api/:version.Pay/vipAlipay');
	// VIP微信扫码支付
	Route::post('vipNativePay', 'api/:version.Pay/vipNativePay');
	// VIP虎皮椒微信扫码支付
	Route::post('vipHpjWxPay', 'api/:version.Pay/vipHpjWxPay');
	// 查询VIP订单状态
	Route::get('getVipOrderStatus', 'api/:version.User/getVipOrderStatus');
	// 查询我的VIP记录
	Route::get('getUserVipList', 'api/:version.User/getUserVipList');

	// 获取百度网盘下载工具
	Route::get('getPandownload', 'api/:version.User/getPandownload');

})->middleware(\app\api\middleware\AuthTokenMiddleware::class);


// 需要验证token, 创作中心接口
Route::group('/:version/mp/', function(){
	// 订单列表
	Route::get('getOrderList', 'api/:version.Order/getSellList');
	// 获取资源列表
	Route::get('getZiyuanList', 'api/:version.MpZiyuan/getList');
	// 发布资源
	Route::post('fabu', 'api/:version.MpZiyuan/fabu');
	// 下载记录
	Route::get('getDownloadLog', 'api/:version.MpZiyuan/getDownloadLog');
	// 首页信息统计
	Route::get('tongji/index', 'api/:version.MpTongji/index');
	// 内容数据统计
	Route::get('tongji/contentData', 'api/:version.MpTongji/contentData');
	// 收益统计
	Route::get('tongji/getprofit', 'api/:version.MpTongji/getprofit');
	// 收益明细
	Route::get('incomeDetails', 'api/:version.MpTongji/incomeDetails');
	// 获取钱包信息
	Route::get('getWallet', 'api/:version.MpUser/getWallet');
	// 用户提现
	Route::post('userTixian', 'api/:version.MpUser/tixian');
	// 提现记录
	Route::get('getTixianLog', 'api/:version.MpUser/getTixianLog');
	// 获取已发布资源类别
	Route::get('getUserZiyuanCategory', 'api/:version.MpZiyuan/getUserZiyuanCategory');
	// 资源操作，置顶、仅自己可见
	Route::post('ziyuanOperation', 'api/:version.MpZiyuan/operation');
	// 获取资源详情，修改时使用
	Route::get('getZiyuanInfo', 'api/:version.MpZiyuan/getZiyuanInfo');
	// 删除资源
	Route::post('delZiyuan', 'api/:version.MpZiyuan/delZiyuan');
	// 获取收款账户列表信息
	Route::get('getUserAccount', 'api/:version.MpUser/getUserAccount');
	// 添加收款信息
	Route::post('setUserAccount', 'api/:version.MpUser/setUserAccount');
	// 删除收款信息
	Route::post('delUserAccount', 'api/:version.MpUser/delUserAccount');

	// 创建/编辑课程
	Route::post('kecheng', 'api/:version.MpKecheng/kecheng');
	// 获取课程列表
	Route::get('getKeList', 'api/:version.MpKecheng/getKeList');
	// 获取课程详情
	Route::get('getKechengInfo', 'api/:version.MpKecheng/getKechengInfo');
	// 添加/编辑课程章节
	Route::post('chapters', 'api/:version.MpKecheng/chapters');
	// 获取课程章节列表
	Route::get('getChaptersList', 'api/:version.MpKecheng/getChaptersList');
	// 添加/编辑课程内容
	Route::post('keDirectory', 'api/:version.MpKecheng/directory');
	// 删除课程章节
	Route::post('delKechengChapters', 'api/:version.MpKecheng/delChapters');
	// 删除课程内容
	Route::post('delKechengDirectory', 'api/:version.MpKecheng/delDirectory');
	// 删除课程
	Route::post('delKecheng', 'api/:version.MpKecheng/delKecheng');
	
})->middleware(\app\api\middleware\AuthTokenMiddleware::class)->middleware(\app\api\middleware\AuthMpMiddleware::class);