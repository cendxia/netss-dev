<?php 

namespace app\api\validate;

use think\Validate;


/**
 * 
 */
class Kecheng extends Validate
{
	
	
	// 验证规则
	protected $rule = [
		'user_id' => 'require|number',
		'title' => 'require|min:3',
		'cover' => 'require',
		'price' => 'require|float|>=:0',
		'cover' => 'require',
		'desc' => 'require',
		'content' => 'require',
		'grade' => 'require|number',
		'ke_id' => 'require|number',
		'sort' => 'require|number',
		'ke_chapters_id' => 'require|number',
		'is_free' => 'require|in:0,1',
		'type' => 'require|in:1,2'
	];


	// 提示
	protected $message = [
		'user_id.require' => '参数错误',
		'user_id.number' => '参数错误',
		'title.require' => '名称必填',
		'title.min' => '名称长度需大于等于3个字符',
		'price.require' => '售价必填',
		'price.float' => '参数错误',
		'price.egt' => '最小金额为0',
		'cover.require' => '请上传封面',
		'desc.require' => '请填写描述',
		'content.require' => '请添加内容',
		'grade.require' => '参数错误',
		'grade.number' => '参数错误',
		'ke_chapters_id.require' => '参数错误',
		'ke_chapters_id.number' => '参数错误',
		'is_free.require' => '参数错误',
		'is_free.in' => '参数错误',
		'type.require' => '参数错误',
		'type.in' => '参数错误'

	];

	// 验证场景
	protected $scene = [
		'kecheng' => ['user_id','title','desc','cover','price','content','grade'],
		'chapters' => ['user_id','ke_id','title','sort'],
		'directory' => ['user_id','title','ke_chapters_id','is_free','type','content','sort']
	];
}