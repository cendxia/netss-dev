<?php 

namespace app\api\validate;

use think\Validate;


/**
 * 
 */
class Ziyuan extends Validate
{
	
	
	// 验证规则
	protected $rule = [
		'user_id' => 'require|number',
		'category_id' => 'require|number',
		'title' => 'require|min:3',
		'cover' => 'require',
		'price' => 'require|float|>=:0',
		'download_url' => 'require',
		'is_kami' => 'require|in:0,1'
	];


	// 提示
	protected $message = [
		'user_id.require' => '参数错误',
		'user_id.number' => '参数错误',
		'category_id.require' => '请选择分类',
		'category_id.number' => '参数错误',
		'title.require' => '名称必填',
		'title.min' => '名称长度需大于等于3个字符',
		'price.require' => '售价必填',
		'price.float' => '参数错误',
		'price.egt' => '最小金额为0',
		'download_url.require' => '下载地址必填',
		'is_kami.require' => '参数错误',
		'is_kami.in' => '参数错误'
	];

	// 验证场景
	protected $scene = [
		'fabu' => ['user_id','category_id','title','cover','price','is_kami'],
	];
}