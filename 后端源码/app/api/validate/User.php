<?php 

namespace app\api\validate;

use think\Validate;

/**
 * 
 */
class User extends Validate
{
	
	// 验证规则
	protected $rule = [
		'user_id' => 'require|number',
		'login_name' => 'require',
		'user_name' => 'require|min:2',
		'phone' => 'require|mobile',
		'type' => 'require',
		'password' => 'require|min:6',
		'rpassword' => 'require|confirm:password',
		'sms_code' => 'require|number|min:4',
		'province' => 'require|min:2',
		'city' => 'require|min:2',
		'area' => 'require|min:2',
		'address' => 'require|min:3',
		'id_card' => 'require|idCard',
		'email' => 'require|email',
		'qq' => 'require|length:6,10',
		'account_name' => 'require',
		'account_holder' => 'require',
		'opening_bank' => 'require',
		'account_type' => 'require',
		'email_type' => 'require',
	];

	// 提示
	protected $message = [
		'user_id.require' => '用户id错误',
		'user_id.number' => '用户id错误',
		'login_name.require' => '请输入用户名',
		'user_name.require' => '请填写姓名',
		'user_name.min' => '请填写正确的姓名',
		'phone.require' => '请填写手机号',
		'phone.mobile' => '手机号错误',
		'password.require' => '请输入密码',
		'password.min' => '密码长度必须大于登录6个字符',
		'rpassword.require' => '确认密码必填',
		'rpassword.confirm' => '两次密码不一致',
		'sms_code.require' => '请输入验证码',
		'sms_code.number' => '验证码错误',
		'sms_code.min' => '验证码错误',
		'province.require' => '省份错误',
		'province.min' => '省份错误',
		'city.require' => '地址错误',
		'city.min' => '地址错误',
		'area.require' => '地址错误',
		'area.min' => '地址错误',
		'address.require' => '地址错误',
		'address.min' => '地址错误',
		'id_card.require' => '身份证号必填',
		'id_card.idCard' => '身份证号格式错误',
		'email.require' => '邮箱号必填',
		'email.email' => '邮箱号格式错误',
		'qq.require' => 'QQ号必填',
		'qq.length' => 'QQ号错误',
		'type.require' => '验证码类型错误',
		'account_name.require' => '请填写卡号',
		'account_holder.require' => '请填写姓名',
		'opening_bank.require' => '请填写开户行',
		'account_type.require' => '类型错误',
		'email_type.require' => '邮件类型错误',
	];

	// 验证场景
	protected $scene = [
		'register' => ['login_name','password','rpassword','phone','sms_code'],
		'login' => ['login_name','password'],
		'sms_send' => ['phone','type'], // 发送验证码
		'email_send' => ['email','email_type'], // 发送邮件
		'address' => ['user_id','user_name','phone','province','city','area','address'],
		'findPwd' => ['phone','sms_code','password','rpassword'],
		'registerMp' => ['user_name','phone','id_card','email','qq','sms_code'], // 注册创作者
		'userAccount' => ['account_name','account_holder','opening_bank','account_type']
	];
}