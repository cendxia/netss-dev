<?php 


// 验证码前缀
function smsPrefix($type){
	$prefix = '';
    if($type == 'login'){
        $prefix = config('common.sms.login_prefix');
    }else if($type == 'register'){
        $prefix = config('common.sms.register_prefix');
    }else if($type == 'findPwd'){
        $prefix = config('common.sms.findPwd_prefix');
    }else if($type == 'real'){
        $prefix = config('common.sms.real_prefix');
    }
    return $prefix;
}

// 获取当月最后一天时间戳
function monthEndTime($date, $info=true){
    if($info){
        $str = 'Y-m-d 23:59:59';
    }else{
        $str = 'Y-m-d';
    }
    return date($str, strtotime("$date +1 month -1 day"));
}

// 获取一年内的每个月份
function oneYearDate(){
    $start_date = date('Y-m', strtotime('-12 months') );

    $begin = new \DateTime($start_date);
    $end = new \DateTime(date('Y-m'));
    $end = $end->modify('+1 month');
    $interval = new \DateInterval('P1M');
    $daterange = new \DatePeriod($begin, $interval, $end);
    $month = array();
    foreach ($daterange as $key => $date) {
        array_push($month, $date->format("Y-m"));
    }

    // 倒序
    return array_reverse($month);
}


// 封装用户操作日志
function logData($user_id, $title, $value=[]){
    $result = [
        'user_id' => $user_id,
        'title' => $title,
        'ip' => getUserIp(),
        'value' => serialize($value)
    ];
    return $result;
}


// 过滤带表情的中文字符，一般用户微信昵称过滤
function filterEmoji($str){
    $str = preg_replace_callback('/./u', function (array $match) {
        return strlen($match[0]) >= 4 ? '' : $match[0];
    }, $str);
    return $str;
}