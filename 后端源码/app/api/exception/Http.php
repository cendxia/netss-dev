<?php
namespace app\api\exception;
use think\exception\Handle;
use think\Response;
use Throwable;
use app\platform\model\ErrorLog;

class Http extends Handle {

    public $httpStatus = 200;


    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response{

        // 封闭错误信息
        $list = array(
            'mode' => 'api', // 所属模块
            'file'  => $e->getFile(),
            'message'  =>  $e->getMessage(),
            'code'  =>  $e->getCode(),
            'line'  =>  $e->getLine(),
            'ip'    =>  getUserIp(),
            'url'  =>  request()->url(),
        );
        

        // dump($list); exit;

        if($list['url'] != '/v1/imgUpload'){
            (new ErrorLog)->insertLog($list);
        }
        

        if($e instanceof  \think\Exception) {
            return show([], $e->getMessage(),  $e->getCode());
        }
        if($e instanceof \think\exception\HttpResponseException) {
            return parent::render($request, $e);
        }

        if(method_exists($e, "getStatusCode")) {
            $httpStatus = $e->getStatusCode();
        } else {
            $httpStatus = $this->httpStatus;
        }
        // 添加自定义异常处理机制
        return show([], $e->getMessage(), config("status.error"), $httpStatus);
    }
}