<?php 

namespace app\api\lib;

/**
 * 
 */
class Redis
{
	private $redis;

	public function __construct(){
		$this->redis = new \Redis();
		$this->redis->connect('127.0.0.1', 6379);
		// $this->redis->auth('LC605A_9940C1_37EEC7'); // 密码
	}


	/**
	 * 添加队列
	 * @Author   cendxia
	 * @DateTime 2022-03-01T16:33:05+0800
	 * @param    [type]                   $goods_id [description]
	 * @param    [type]                   $data     [description]
	 * @param    string                   $prefix   [description]
	 * @return   [type]                             [description]
	 */
	public function lPush($goods_id, $data, $prefix='app_jingpai_'){
		$this->redis->lPush($prefix.$goods_id, $data);
		return true;
	}

	/**
	 * 获取队列长度
	 * @Author   cendxia
	 * @DateTime 2022-03-01T16:47:13+0800
	 * @param    [type]                   $goods_id [description]
	 * @param    string                   $prefix   [description]
	 * @return   [type]                             [description]
	 */
	public function lLen($goods_id, $prefix='app_jingpai_'){
		return $this->redis->lLen($prefix.$goods_id);
	}


	/**
	 * 取队列数据并删除
	 * @Author   cendxia
	 * @DateTime 2022-03-01T17:02:22+0800
	 * @param    [type]                   $goods_id [description]
	 * @param    string                   $prefix   [description]
	 * @return   [type]                             [description]
	 */
	public function rPop($goods_id, $prefix='app_jingpai_'){
		return $this->redis->rPop($prefix.$goods_id);
	}


	/**
	 * 删除redis
	 * @Author   cendxia
	 * @DateTime 2022-03-31T17:45:49+0800
	 * @param    [type]                   $key [description]
	 * @return   [type]                        [description]
	 */
	public function delete($key){
		return $this->delete($key);
	}

}