<?php 

namespace app\api\middleware;

use think\facade\Request;
use think\facade\Cache;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * 
 */
class AuthTokenMiddleware{


	private $user_id;
	private $user;
	private $TOKEN;

    public function handle($request, \Closure $next){
        // 判断是否有token
		$this->getHeaderToken();

		// 判断是否登录 取消，使用中是键实现该功能
		if(empty($this->isLogin())){
			throw new \think\Exception(config('language.not_login'), config('status.not_login'));
		}
		if($this->user_id <= 0){
			throw new \think\Exception(config('language.not_login'), config('status.not_login'));
		}
		$this->tokenTime();
		return $next($request);
    }



    /**
	 * 判断是否登录
	 * @Author   cendxia
	 * @DateTime 2022-02-20T14:26:04+0800
	 * @return   boolean                  [description]
	 */
	protected function isLogin(){
		$jwtToken = cache(config('redis.token_pre').$this->TOKEN);
		
		if(empty($jwtToken)){
			throw new \think\Exception(config('language.not_login'), config('status.not_login'));
		}

		// 获取公共缓存前缀
        $prefix = config('common.common_prefix');

		$this->user = JwT::decode($jwtToken, new Key($prefix.'login_lc_', 'HS256')); //decode解码
		if(isset($this->user->user_id)){
			$this->user_id = $this->user->user_id;
		}
		return $this->user ? true : false;
	}


	/**
	 * 获取前端用户传过来的token
	 * @Author   cendxia
	 * @DateTime 2022-02-20T14:26:11+0800
	 * @return   [type]                   [description]
	 */
	private function getHeaderToken(){
		$token = Request::header('token');
		if(empty($token)){
			throw new \think\Exception(config('language.token_error'), config('status.not_login'));
		}
		$this->TOKEN = $token;
	}

	/**
	 * 判断token有效时间，更新token
	 * @Author   cendxia
	 * @DateTime 2022-04-25T12:34:14+0800
	 * @return   [type]                   [description]
	 */
	private function tokenTime(){
		$user = $this->user;
		// 获取公共缓存前缀
        $prefix = config('common.common_prefix');


		if($user->token_time - time() <= config('common.user_login_token_update')){
			// // 单点登录使用
	        // Cache($prefix.'login_lc_'.$user->id, $user->token, config('common.user_login_expires_time'));
	        // // 存储登录token
	        // Cache(config('redis.token_pre').$user->token, $user, config('common.user_login_expires_time'));


	        // // 单点登录使用
	        // cache($prefix.'login_lc_'.$user->user_id, $this->TOKEN, config('common.user_login_expires_time'));
	        // // 存储登录token
	        // cache(config('redis.token_pre').$this->TOKEN, $jwtToken, config('common.user_login_expires_time'));
		}
	}
}