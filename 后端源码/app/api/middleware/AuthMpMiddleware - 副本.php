<?php 

namespace app\api\middleware;

use think\facade\Request;
use think\facade\Cache;

/**
 * 
 */
class AuthMpMiddleware
{
	
	public function handle($request, \Closure $next){
		$token = Request::header('token');
        $user = cache(config('redis.token_pre').$token);
        if(empty($user) || $user['type'] != 2){
        	throw new \think\Exception('无权限', 2);
        }
        return $next($request);
    }
}