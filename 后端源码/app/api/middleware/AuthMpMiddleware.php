<?php 

namespace app\api\middleware;

use think\facade\Request;
use think\facade\Cache;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * 
 */
class AuthMpMiddleware
{
	
	public function handle($request, \Closure $next){
		$token = Request::header('token');
        $jwtToken = cache(config('redis.token_pre').$token);
        if(empty($jwtToken)){
            // throw new \think\Exception(config('language.not_login'), config('status.not_login'));
            dump('未登录');

            exit;
        }

        // 获取公共缓存前缀
        $prefix = config('common.common_prefix');
        $user = JwT::decode($jwtToken, new Key($prefix.'login_lc_', 'HS256')); //decode解码
        if(empty($user) || $user->type != 2){
        	throw new \think\Exception('无权限', 2);
        }
        return $next($request);
    }
}