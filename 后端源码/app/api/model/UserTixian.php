<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class UserTixian extends Model
{

	/**
	 * 添加记录
	 * @Author   cendxia
	 * @DateTime 2024-01-17T21:32:32+0800
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
	/**
	 * 提现记录
	 * @Author   cendxia
	 * @DateTime 2022-08-29T23:14:27+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize){
		try {
			return $this->where(['user_id'=>$user_id])->order('create_time','DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-08-29T23:16:50+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getCount($user_id){
		try {
			return $this->where(['user_id'=>$user_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}