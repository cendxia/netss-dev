<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class WxBinding extends Model
{
	/**
	 * 添加记录
	 * @Author   cendxia
	 * @DateTime 2022-10-26T11:11:18+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'))
		}
	}
}