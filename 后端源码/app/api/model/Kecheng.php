<?php 

namespace app\api\model;

use think\Model;

/**
 * 课程
 */
class Kecheng extends Model
{
	
	/**
	 * 创建/编辑课程
	 * @Author   cendxia
	 * @DateTime 2023-03-16T18:51:42+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function kecheng($data){
		if(empty($data['id'])){
			// 添加
			return $this->insertGetId($data);
		}
		// 修改
		$id = $data['id'];
		unset($data['id']);
		if(isset($data['create_time'])) unset($data['create_time']);
		if(isset($data['update_time'])) unset($data['update_time']);
		return $this->where(['id'=>$id,'user_id'=>$data['user_id']])->save($data);
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2023-03-26T22:56:58+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCount($where=[]){
		try {
			return $this->where($where)->where('status', '<>', -1)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取课程列表
	 * @Author   cendxia
	 * @DateTime 2023-03-26T22:51:05+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function getKeList($page, $pageSize, $where=[]){
		try {
			return $this->where($where)->where(['status'=>1])->order('create_time','DESC')->page($page, $pageSize)->field('id,title,desc,study_num,grade,cover,price,vip_dis')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 创作中心获取课程列表
	 * @Author   cendxia
	 * @DateTime 2023-03-16T19:59:02+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize){
		try {
			return $this->where(['user_id'=>$user_id,'is_show'=>1])->where('status', '<>', -1)->order('create_time', 'DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 根基id查询课程信息
	 * @Author   cendxia
	 * @DateTime 2023-03-17T23:10:49+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $id      [description]
	 * @return   [type]                            [description]
	 */
	public function getKechengById($user_id, $id){
		try {
			$result = $this->where(['id'=>$id, 'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 获取课程详情
	 * @Author   cendxia
	 * @DateTime 2023-03-29T19:07:35+0800
	 * @param    [type]                   $id    [description]
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getInfo($id, $where=[]){
		try {
			$result = $this->where(['id'=>$id])->where($where)->find();
		} catch (\think\Exception $e) {
			throw new Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}
}


