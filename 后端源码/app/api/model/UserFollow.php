<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class UserFollow extends Model
{
	
	/**
	 * 模型关联
	 * @Author   cendxia
	 * @DateTime 2022-08-19T10:51:53+0800
	 * @return   [type]                   [description]
	 */
	public function user(){
    	return $this->hasOne(Users::class, 'user_id', 'user_id')->field('user_id,avatar,nickname');
    }


    /**
	 * 模型关联
	 * @Author   cendxia
	 * @DateTime 2022-08-19T10:51:53+0800
	 * @return   [type]                   [description]
	 */
	public function followUser(){
    	return $this->hasOne(Users::class, 'user_id', 'follow_user_id')->field('user_id,avatar,nickname');
    }

    /**
     * 添加
     * @Author   cendxia
     * @DateTime 2022-08-19T15:03:45+0800
     * @param    [type]                   $data [description]
     */
    public function add($data){
    	try {
    		return $this->insertGetId($data);
    	} catch (\think\Exception $e) {
    		throw new \think\Exception(config('language.mysql_error'));
    	}
    }

    /**
     * 查询是否关注
     * @Author   cendxia
     * @DateTime 2022-08-19T15:06:05+0800
     * @param    [type]                   $user_id      [description]
     * @param    [type]                   $this_user_id [description]
     * @return   [type]                                 [description]
     */
    public function getByUserId($user_id, $this_user_id){
    	try {
    		return $this->where(['user_id'=>$this_user_id,'follow_user_id'=>$user_id])->find();
    	} catch (\think\Exception $e) {
    		throw new \think\Exception(config('language.mysql_error'));
    	}
    }

    /**
     * 取消关注
     * @Author   cendxia
     * @DateTime 2022-10-09T22:34:32+0800
     * @param    [type]                   $user_id      [description]
     * @param    [type]                   $this_user_id [description]
     * @return   [type]                                 [description]
     */
    public function delData($user_id, $this_user_id){
        try {
            return $this->where(['user_id'=>$this_user_id,'follow_user_id'=>$user_id])->delete();
        } catch (\think\Exception $e) {
            throw new \think\Exception(config('language.mysql_error'));
        }
    }
}