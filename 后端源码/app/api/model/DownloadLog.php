<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class DownloadLog extends Model
{

	/**
	 * 添加记录
	 * @Author   cendxia
	 * @DateTime 2022-10-20T16:27:43+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function insertLog($data){
		// try {
			return $this->insertGetId($data);
		// } catch (\think\Exception $e) {
		// 	throw new \think\Exception(config('language.mysql_error'));
		// }
	}

	/**
	 * 获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:29:56+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize){
		return $this->where(['user_id'=>$user_id])->order('create_time','DESC')->page($page, $pageSize)->select()->toArray();
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-09-23T01:40:15+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getCount($user_id){
		return $this->where(['user_id'=>$user_id])->count();
	}

	/**
	 * 创作中心下载记录
	 * @Author   cendxia
	 * @DateTime 2022-08-20T15:43:20+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getSellList($sell_user_id, $page, $pageSize){
		return $this->where(['sell_user_id'=>$sell_user_id])->order('create_time','DESC')->page($page, $pageSize)->select()->toArray();
	}

	/**
	 * 根据资源id获取下载数量
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:00:10+0800
	 * @param    [type]                   $ids   [description]
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCountByZiyuanId($ids, $where=[]){
		try {
			return $this->where('ziyuan_id', 'in', $ids)->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 根据订单号获取下载数量
	 * @Author   cendxia
	 * @DateTime 2022-08-20T10:58:26+0800
	 * @param    [type]                   $ids   [description]
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCountByOrderId($ids, $where=[]){
		try {
			return $this->where('order_id', 'in', $ids)->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}