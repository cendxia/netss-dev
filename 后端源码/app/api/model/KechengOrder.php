<?php 

namespace app\api\model;

use think\Model;

/**
 * 课程订单
 */
class KechengOrder extends Model
{

	/**
	 * 创建订单
	 * @Author   cendxia
	 * @DateTime 2023-04-11T11:33:38+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
	/**
	 * 获取订单详情
	 * @Author   cendxia
	 * @DateTime 2023-03-29T19:59:33+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderByOrderId($user_id, $order_id){
		try {
			$result = $this->where(['order_id'=>$order_id, 'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 根据课程id获取订单数据
	 * @Author   cendxia
	 * @DateTime 2023-03-29T20:13:20+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $ke_id   [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function getOrderByKeId($user_id, $ke_id, $where=[]){
		try {
			$result = $this->where(['ke_id'=>$ke_id, 'user_id'=>$user_id])->where($where)->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 根据订单号获取订单信息
	 * @Author   cendxia
	 * @DateTime 2023-04-12T21:10:12+0800
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderInfoByOrderId($order_id){
		try {
			$result = $this->where(['order_id'=>$order_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 获取订单列表
	 * @Author   cendxia
	 * @DateTime 2023-04-15T16:52:54+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize, $where=[]){
		try {
			return $this->where(['user_id'=>$user_id])->where($where)->order('create_time','DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2023-04-15T16:58:06+0800
	 * @param    [type]                   $user_id [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function getCount($user_id, $where=[]){
		try {
			return $this->where(['user_id'=>$user_id])->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}