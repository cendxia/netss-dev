<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class UserRegisterMp extends Model
{
	/**
	 * 添加记录
	 * @Author   cendxia
	 * @DateTime 2022-10-10T16:34:30+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	
}