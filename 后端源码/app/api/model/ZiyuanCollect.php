<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class ZiyuanCollect extends Model
{
	/**
	 * 收藏记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:41:04+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize){
		return $this->where(['user_id'=>$user_id])->order('create_time','DESC')->page($page, $pageSize)->select();
	}

	/**
	 * 根据id获取收藏信息
	 * @Author   cendxia
	 * @DateTime 2022-09-25T11:57:52+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function getCollectFind($user_id, $ziyuan_id){
		try {
			$result = $this->where(['ziyuan_id'=>$ziyuan_id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-09-23T02:08:22+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getCount($user_id){
		try {
			return $this->where(['user_id'=>$user_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 添加收藏
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:16:59+0800
	 * @param    [type]                   $data [description]
	 */
	public function addCollect($data){
		return $this->insertGetId($data);
	}

	/**
	 * 删除收藏
	 * @Author   cendxia
	 * @DateTime 2022-09-25T12:02:25+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function delCollect($user_id, $ziyuan_id){
		return $this->where(['ziyuan_id'=>$ziyuan_id,'user_id'=>$user_id])->delete();
	}

	/**
	 * 根据资源id获取收藏信息
	 * @Author   cendxia
	 * @DateTime 2022-08-21T21:31:10+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function getByZiyuanId($user_id, $ziyuan_id){
		try {
			return $this->where(['user_id'=>$user_id,'ziyuan_id'=>$ziyuan_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}