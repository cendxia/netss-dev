<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class SystemConfig extends Model
{
	
	public function getConfig(){
		try {
			return $this->where(['is_frontend'=>1])->field('id,field,value,type,note')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}