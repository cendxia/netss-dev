<?php 


namespace app\api\model;

use think\Model;

/**
 * 
 */
class Vip extends Model
{
	
	/**
	 * 根据id查询数据
	 * @Author   cendxia
	 * @DateTime 2024-03-22T17:02:50+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getVipById($id){
		try {
			$result = $this->where(['id'=>$id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 获取VIP列表
	 * @Author   cendxia
	 * @DateTime 2024-03-22T18:37:12+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		try {
			return $this->where(['status'=>1])->field('id,title,desc,day,price')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}