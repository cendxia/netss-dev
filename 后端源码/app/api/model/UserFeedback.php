<?php 

namespace app\api\model;

use think\Model;

/**
 * 用户反馈表
 */
class UserFeedback extends Model
{
	
	/**
	 * 添加
	 * @Author   cendxia
	 * @DateTime 2022-12-03T12:54:24+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
}