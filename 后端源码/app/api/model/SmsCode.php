<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class SmsCode extends Model
{
	
	public function add($data){
		try {
    		return $this->insertGetId($data);
    	} catch (\think\Exception $e) {
    		throw new \think\Exception(config('language.mysql_error'));
    	}
	}
}