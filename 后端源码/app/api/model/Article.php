<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class Article extends Model
{
	
	/**
	 * 根据id获取文章详情
	 * @Author   cendxia
	 * @DateTime 2022-10-20T22:13:20+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getById($id){
		try {
			$result = $this->where(['id'=>$id])->find();
			// 自增阅读量
			$this->where(['id'=>$id])->inc('read_num')->update();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 获取列表
	 * @Author   cendxia
	 * @DateTime 2022-10-20T22:31:35+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize, $where=[]){
		try {
			return $this->where($where)->order('create_time','DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-10-20T22:33:11+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCount($where=[]){
		try {
			return $this->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}