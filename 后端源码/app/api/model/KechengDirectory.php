<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class KechengDirectory extends Model
{

	/**
	 * 添加/编辑课程内容
	 * @Author   cendxia
	 * @DateTime 2023-03-20T16:24:22+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function directory($data){
		if(isset($data['ke_id'])) unset($data['ke_id']);
		if(empty($data['id'])){
			// 添加
			return $this->insertGetId($data);
		}
		// 修改
		$id = $data['id'];
		unset($data['id']);
		if(isset($data['create_time'])) unset($data['create_time']);
		if(isset($data['update_time'])) unset($data['update_time']);
		$this->where(['id'=>$id,'user_id'=>$data['user_id']])->save($data);
		return $id;
	}
	
	/**
	 * 获取课程目录列表
	 * @Author   cendxia
	 * @DateTime 2023-03-29T20:49:44+0800
	 * @param    [type]                   $chapters_id [description]
	 * @param    array                    $where       [description]
	 * @return   [type]                                [description]
	 */
	public function getList($chapters_id, $where=[]){
		try {
			return $this->where(['ke_chapters_id'=>$chapters_id])->where($where)->order('sort', 'ASC')->order('id', 'ASC')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


}