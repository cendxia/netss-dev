<?php 

namespace app\api\model;

use think\Model;

/**
 * 支付回调表
 */
class PayCallback extends Model
{
	
	/**
	 * 创建记录
	 * @Author   cendxia
	 * @DateTime 2022-11-03T15:56:42+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function insertLog($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception('language.mysql_error');
		}
	}
}