<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class UserAccount extends Model
{
	/**
	 * 获取列表
	 * @Author   cendxia
	 * @DateTime 2022-10-13T14:18:10+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getList($user_id){
		try {
			return $this->where(['user_id'=>$user_id])->order('update_time','DESC')->field('id,account_name,account_holder,opening_bank,account_type')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 根据类型查询收款信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T15:01:22+0800
	 * @param    [type]                   $user_id      [description]
	 * @param    [type]                   $account_type [description]
	 * @return   [type]                                 [description]
	 */
	public function getByAccountType($user_id, $account_type){
		try {
			$result = $this->where(['user_id'=>$user_id,'account_type'=>$account_type])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 添加
	 * @Author   cendxia
	 * @DateTime 2022-10-13T15:03:53+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 修改
	 * @Author   cendxia
	 * @DateTime 2022-10-13T15:04:32+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function edit($user_id, $id, $data){
		try {
			return $this->where(['user_id'=>$user_id,'id'=>$id])->save($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 删除收款信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T17:40:29+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $id      [description]
	 * @return   [type]                            [description]
	 */
	public function del($user_id, $id){
		try {
			return $this->where(['user_id'=>$user_id,'id'=>$id])->delete();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}