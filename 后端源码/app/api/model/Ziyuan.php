<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class Ziyuan extends Model
{

	/**
	 * 获取资源详情
	 * @Author   cendxia
	 * @DateTime 2022-09-15T23:14:19+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getInfo($id){
		try {
			$result = $this->where(['id'=>$id])->field('id,user_id,category_id,title,cover,price,vip_dis,status,download_url,extract,is_kami,kami')->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		if(empty($result)) return [];
		return $result->toArray();
	}
	
	/**
	 * 获取资源列表
	 * @Author   cendxia
	 * @DateTime 2024-02-18T13:06:43+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    array                    $where    [description]
	 * @param    boolean                  $rand     [description]
	 * @param    string                   $field    [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize, $where=[], $rand=false, $field=''){
		if(empty($field)){
			$field = 'id,title,category_id,cover,price,vip_dis,status,collect_num,download_num,report_num,read_num,praise_num,is_top,self_visible,audio,is_kami,fail_reason,create_time';
		}
		if($rand){ // 获取数据
			return $this->where($where)->page($page, $pageSize)->field($field)->orderRaw('RAND()')->select()->toArray();
		}
		return $this->where($where)->page($page, $pageSize)->field($field)->order('is_top','DESC')->order('ziyuan_update_time','DESC')->select()->toArray();
	}

	/**
	 * 获取资源总条数
	 * @Author   cendxia
	 * @DateTime 2022-09-23T01:25:20+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCount($where=[]){
		return $this->where($where)->count();
	}

	/**
	 * 资源搜索
	 * @Author   cendxia
	 * @DateTime 2023-01-05T17:20:28+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    [type]                   $keyword  [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function search($page, $pageSize, $keyword, $where=[]){
		return $this->where($where)->where('title|label|abstract|content', 'like', "%{$keyword}%")->page($page, $pageSize)->field('id,title,category_id,cover,price,vip_dis,status,collect_num,download_num,report_num,read_num,praise_num,is_top,self_visible,is_kami,create_time')->order('update_time','DESC')->select()->toArray();
	}

	/**
	 * 获取搜索时总条数
	 * @Author   cendxia
	 * @DateTime 2023-01-05T17:24:45+0800
	 * @param    [type]                   $keyword [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function searchCount($keyword, $where=[]){
		return $this->where($where)->where('title|label|abstract|content', 'like', "%{$keyword}%")->count();
	}

	/**
	 * 添加/修改资源信息
	 * @Author   cendxia
	 * @DateTime 2022-07-27T16:36:54+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function ziyuan($data){
		if(empty($data['id'])){
			// 添加
			return $this->insertGetId($data);
		}
		// 修改
		$id = $data['id'];
		unset($data['id']);
		if(isset($data['create_time'])) unset($data['create_time']);
		if(isset($data['update_time'])) unset($data['update_time']);
		return $this->where(['id'=>$id,'user_id'=>$data['user_id']])->save($data);
	}


	/**
	 * 分组查询
	 * @Author   cendxia
	 * @DateTime 2022-08-19T09:56:19+0800
	 * @return   [type]                   [description]
	 */
	public function getCategoryGroup($user_id){
		try {
			return $this->where(['user_id'=>$user_id,'status'=>1])->group('category_id')->field('category_id')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取某分类下的总条数
	 * @Author   cendxia
	 * @DateTime 2022-08-19T10:14:19+0800
	 * @param    [type]                   $user_id     [description]
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getCategoryCount($user_id, $category_id){
		try {
			return $this->where(['user_id'=>$user_id,'category_id'=>$category_id,'status'=>1])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取资源id
	 * @Author   cendxia
	 * @DateTime 2022-08-19T22:51:27+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getZiyuanIds($user_id){
		try {
			return $this->where(['user_id'=>$user_id])->column('id');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 修改资源
	 * @Author   cendxia
	 * @DateTime 2022-09-26T00:35:14+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @param    [type]                   $data      [description]
	 * @return   [type]                              [description]
	 */
	public function editZiayuan($user_id, $ziyuan_id, $data){
		try {
			return $this->where(['user_id'=>$user_id,'id'=>$ziyuan_id])->save($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 删除资源
	 * @Author   cendxia
	 * @DateTime 2022-10-10T11:15:33+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function delZiyuan($user_id, $ziyuan_id){
		try {
			return $this->where(['user_id'=>$user_id,'id'=>$ziyuan_id])->save(['status'=>-1]);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取所有标签
	 * @Author   cendxia
	 * @DateTime 2024-04-09T21:28:46+0800
	 * @param    [type]                   $num [description]
	 * @return   [type]                        [description]
	 */
	public function getLabelList($num){
		try {
			if(empty($num)){
				return $this->where(['status'=>1])->where('label', '<>', '')->where('biaoshi', '=', null)->field('label')->orderRaw('RAND()')->select()->toArray();
			}else{
				return $this->where(['status'=>1])->where('label', '<>', '')->where('biaoshi', '=', null)->orderRaw('RAND()')->page(1, $num)->field('label')->select()->toArray();
			}
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}