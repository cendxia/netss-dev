<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class ZiyuanCategory extends Model
{
	/**
	 * 获取分类信息
	 * @Author   cendxia
	 * @DateTime 2022-07-27T15:55:22+0800
	 * @param    [type]                   $where [description]
	 * @return   [type]                          [description]
	 */
	public function getList($where){
		try {
			return $this->where($where)->where(['show'=>1])->order('sort','ASC')->field('id,pid,title,home_show,icon')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 根据id获取详情
	 * @Author   cendxia
	 * @DateTime 2022-08-19T10:08:57+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getById($category_id){
		try {
			$result = $this->where(['id'=>$category_id,'show'=>1])->field('id,pid,title,home_show')->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 查询子分类数据
	 * @Author   cendxia
	 * @DateTime 2022-10-08T18:47:29+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getByIdAll($category_id){
		try {
			return $this->where(['pid'=>$category_id])->order('sort','ASC')->field('id,title')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取详情
	 * @Author   cendxia
	 * @DateTime 2024-04-23T23:19:48+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getInfo($category_id){
		try {
			$result = $this->where(['id'=>$category_id,'show'=>1])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}
}