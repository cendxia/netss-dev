<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class KechengChapters extends Model
{
	
	/**
	 * 添加/编辑课程章节
	 * @Author   cendxia
	 * @DateTime 2023-03-17T23:27:35+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function chapters($data){
		if(empty($data['id'])){
			// 添加
			return $this->insertGetId($data);
		}
		// 修改
		$id = $data['id'];
		unset($data['id']);
		if(isset($data['create_time'])) unset($data['create_time']);
		if(isset($data['update_time'])) unset($data['update_time']);
		return $this->where(['id'=>$id,'user_id'=>$data['user_id']])->save($data);
	}

	/**
	 * 获取课程章节列表
	 * @Author   cendxia
	 * @DateTime 2023-03-17T23:33:52+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $ke_id   [description]
	 * @return   [type]                            [description]
	 */
	public function getList($ke_id, $where=[]){
		try {
			return $this->where(['ke_id'=>$ke_id])->where($where)->order('sort','ASC')->field('id,user_id,title,ke_id,sort')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取课程章节数量
	 * @Author   cendxia
	 * @DateTime 2023-03-26T21:19:41+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $ke_id   [description]
	 * @return   [type]                            [description]
	 */
	public function getChaptersNum($user_id, $ke_id){
		try {
			return $this->where(['user_id'=>$user_id, 'ke_id'=>$ke_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}