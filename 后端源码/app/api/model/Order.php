<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class Order extends Model
{


	/**
	 * 创建订单
	 * @Author   cendxia
	 * @DateTime 2022-09-15T23:44:20+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
	/**
	 * 获取订单列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:19:16+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize, $field='user_id'){
		try {
			return $this->where([$field=>$user_id])->order('create_time','DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-09-23T01:00:07+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    string                   $field    [description]
	 * @return   [type]                             [description]
	 */
	public function getCount($user_id, $page, $pageSize, $field='user_id'){
		try {
			return $this->where([$field=>$user_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 根据资源id获取订单号
	 * @Author   cendxia
	 * @DateTime 2022-08-20T10:47:27+0800
	 * @param    [type]                   $ids   [description]
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getOrderIdsByZiyuanId($ids, $where=[]){
		try {
			return $this->where('ziyuan_id', 'in', $ids)->where($where)->column('order_id');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 根据资源id获取购买信息
	 * @Author   cendxia
	 * @DateTime 2022-08-21T21:34:23+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function getByZiyuanId($user_id, $ziyuan_id, $where=[]){
		try {
			return $this->where(['user_id'=>$user_id,'ziyuan_id'=>$ziyuan_id])->where($where)->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取订单详情
	 * @Author   cendxia
	 * @DateTime 2022-10-20T10:41:08+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $user_id  [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderInfo($order_id, $user_id){
		try {
			$result = $this->where(['order_id'=>$order_id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 根据资源id获取订单详情
	 * @Author   cendxia
	 * @DateTime 2022-10-20T15:13:49+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function getOrderByZiyuanId($user_id, $ziyuan_id){
		try {
			$result = $this->where(['ziyuan_id'=>$ziyuan_id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 根据订单号获取订单信息
	 * @Author   cendxia
	 * @DateTime 2022-11-03T16:04:33+0800
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderByOrderId($order_id){
		try {
			$result = $this->where(['order_id'=>$order_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 关闭订单
	 * @Author   cendxia
	 * @DateTime 2024-02-27T11:48:21+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function closeOrder($user_id, $order_id){
		try {
			return $this->where(['user_id'=>$user_id,'order_id'=>$order_id,'status'=>0])->save(['status'=>2]);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 查询今日VIP订单是数
	 * @Author   cendxia
	 * @DateTime 2024-03-24T12:58:25+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getVipOrderCount($user_id){
		$start_time = date('Y-m-d 00:00:00');
		try {
			return $this->where(['user_id'=>$user_id,'vip'=>1,'status'=>1])->where('pay_time','>=',$start_time)->where('pay_time','<',date('Y-m-d H:i:s'))->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取今日百度网盘地址订单
	 * @Author   cendxia
	 * @DateTime 2024-04-26T09:01:37+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function gerTodayOrderBaiduPan($user_id){
		$start_time = date('Y-m-d 00:00:00');
		try {
			return $this->where(['user_id'=>$user_id,'status'=>1])->where('price','>',0)->where('pay_time','>=',$start_time)->where('pay_time','<',date('Y-m-d H:i:s'))->where('download_url','like','%https://pan.baidu.com%')->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}