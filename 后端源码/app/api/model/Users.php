<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class Users extends Model
{
	/**
	 * 根据登录名或手机号获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-07-27T13:21:27+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function getUserByLoginnamePhone($data){
		$result = $this->where('login_name|phone',$data)->find();
		if($result) return $result->toArray();
		return [];
	}

	/**
	 * 根据手机号获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-09-30T11:59:32+0800
	 * @param    [type]                   $phone [description]
	 * @return   [type]                          [description]
	 */
	public function getUserByPhone($phone){
		try {
			$result = $this->where('phone', $phone)->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if($result) return $result->toArray();
		return [];
	}

	/**
	 * 根据用户名获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-11T14:06:24+0800
	 * @param    [type]                   $login_name [description]
	 * @return   [type]                               [description]
	 */
	public function getUserByLoginName($login_name){
		try {
			$result = $this->where('login_name', $login_name)->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if($result) return $result->toArray();
		return [];
	}

	/**
	 * 获取创作者用户基础信息
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:23:41+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getSellUserBasics($user_id){
		try {
			$result = $this->where(['user_id'=>$user_id,'state'=>0,'type'=>2])->field('user_id,nickname,phone,qq,avatar,describe')->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}		
		if($result) return $result->toArray();
		return [];
	}

	/**
	 * 根据用户id获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-08-19T15:11:05+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getByUserId($user_id){
		$result = $this->where(['user_id'=>$user_id])->find();
		if($result) return $result->toArray();
		return [];
	}


	/**
	 * 根据身份证号查询用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-10T17:45:17+0800
	 * @param    [type]                   $id_card [description]
	 * @return   [type]                            [description]
	 */
	public function getByIdCard($id_card){
		$result = $this->where(['id_card'=>$id_card])->find();
		if($result) return $result->toArray();
		return [];
	}

	/**
	 * 修改
	 * @Author   cendxia
	 * @DateTime 2022-10-13T22:20:26+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function edit($user_id, $data, $where=[]){
		try {
			return $this->where(['user_id'=>$user_id])->where($where)->save($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 根据unionid获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-25T18:46:40+0800
	 * @param    [type]                   $unionid [description]
	 * @return   [type]                            [description]
	 */
	public function getUserByUnionid($unionid){
		try {
			$result = $this->where(['unionid'=>$unionid])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));			
		}
		if($result) return $result->toArray();
		return [];
	}
}