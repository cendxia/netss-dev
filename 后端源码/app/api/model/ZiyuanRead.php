<?php 

namespace app\api\model;

use think\Model;

/**
 *  浏览记录可定期清除
 */
class ZiyuanRead extends Model
{
	
	/**
	 * 添加
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:22:11+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		return $this->insertGetId($data);
	}

	/**
	 * 获取浏览记录
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:23:00+0800
	 * @param    [type]                   $ids   [description]
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCountByZiyuanId($ids, $where=[]){
		try {
			return $this->where('ziyuan_id', 'in', $ids)->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}