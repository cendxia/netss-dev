<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class UserVip extends Model
{
	
	/**
	 * 创建订单
	 * @Author   cendxia
	 * @DateTime 2024-03-22T17:19:12+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 查询未支付订单
	 * @Author   cendxia
	 * @DateTime 2024-03-22T17:23:36+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getUnpaidOrder($id, $user_id){
		try {
			return $this->where(['user_id'=>$user_id,'vip_id'=>$id,'pay_status'=>0])->order('id', 'DESC')->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 根据订单号获取订单详情
	 * @Author   cendxia
	 * @DateTime 2024-03-22T20:56:35+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderByUserOrderId($user_id, $order_id){
		try {
			$result = $this->where(['order_id'=>$order_id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 根据订单号获取订单详情
	 * @Author   cendxia
	 * @DateTime 2024-03-22T21:20:55+0800
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderInfo($order_id){
		try {
			$result = $this->where(['order_id'=>$order_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 查询订单状态
	 * @Author   cendxia
	 * @DateTime 2024-03-23T17:45:40+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $user_id  [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderStatus($order_id, $user_id){
		try {
			return $this->where(['order_id'=>$order_id,'user_id'=>$user_id])->value('pay_status');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * [getUserVipList description]
	 * @Author   cendxia
	 * @DateTime 2024-03-23T18:22:38+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    [type]                   $user_id  [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function getUserVipList($page, $pageSize, $user_id, $where=[]){
		try {
			return $this->where(['user_id'=>$user_id])->where($where)->order('pay_time', 'DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取我的VIP总条数
	 * @Author   cendxia
	 * @DateTime 2024-03-23T18:19:43+0800
	 * @param    [type]                   $user_id [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function getUserVipCount($user_id, $where=[]){
		try {
			return $this->where(['user_id'=>$user_id])->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取我的VIP信息
	 * @Author   cendxia
	 * @DateTime 2024-03-24T10:09:44+0800
	 * @param    [type]                   $user_id [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function getUserVip($user_id, $where=[]){

		// try {
			$result = $this->where(['user_id'=>$user_id,'pay_status'=>1,'status'=>0])->where($where)->where('end_time', '>', date('Y-m-d H:i:s'))->find();
		// } catch (\think\Exception $e) {
		// 	throw new \think\Exception(config('language.mysql_error'));
		// }
		if(empty($result)) return [];
		return $result->toArray();
	}
}