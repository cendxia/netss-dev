<?php 

namespace app\api\model;

use think\Model;
use app\api\model\UserWallet;

/**
 * 
 */
class UserWalletLog extends Model
{

	/**
	 * 添加记录
	 * @Author   cendxia
	 * @DateTime 2022-11-03T17:15:51+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function insertLog($data){
		try {
			$res = (new UserWallet())->getByUserId($data['user_id']);
			if($res){
				$data['after'] = $res['money'];
			}
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
	/**
	 * 获取收益
	 * @Author   cendxia
	 * @DateTime 2022-08-20T17:53:30+0800
	 * @param    [type]                   $user_id [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function getProfit($user_id, $where=[]){
		try {
			return $this->where(['user_id'=>$user_id,'type'=>'+'])->where($where)->sum('money');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 收益明细
	 * @Author   cendxia
	 * @DateTime 2022-08-28T11:15:31+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize){
		try {
			return $this->where(['user_id'=>$user_id,'type'=>'+'])->order('create_time','DESC')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-08-28T11:16:36+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getCount($user_id){
		try {
			return $this->where(['user_id'=>$user_id,'type'=>'+'])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}