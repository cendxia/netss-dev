<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class ArticleCategory extends Model
{
	
	
	/**
	 * 获取详情
	 * @Author   cendxia
	 * @DateTime 2022-10-20T22:22:12+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getById($category_id){
		try {
			$result = $this->where(['id'=>$category_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 获取文章栏目
	 * @Author   cendxia
	 * @DateTime 2023-11-29T13:20:39+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		try {
			return $this->where(['show'=>1])->order('sort', 'ASC')->field('id,name')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}