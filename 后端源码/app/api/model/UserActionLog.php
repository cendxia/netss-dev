<?php 

namespace app\api\model;

use think\Model;

/**
 * 用户操作日志表
 */
class UserActionLog extends Model
{
	/**
	 * 添加日志
	 * @Author   cendxia
	 * @DateTime 2022-10-14T15:41:56+0800
	 * @param    [type]                   $data [description]
	 */
	public function insertLog($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取用户操作日志
	 * @Author   cendxia
	 * @DateTime 2022-10-15T11:54:08+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize){
		try {
			return $this->where(['user_id'=>$user_id])->order('create_time','DESC')->page($page, $pageSize)->field('title,value,ip,create_time')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}