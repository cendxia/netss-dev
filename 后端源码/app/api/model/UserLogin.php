<?php 

namespace app\api\model;

use think\Model;

/**
 * 
 */
class UserLogin extends Model
{
	
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取列表
	 * @Author   cendxia
	 * @DateTime 2022-10-15T11:45:16+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize){
		try {
			return $this->where(['user_id'=>$user_id])->order('create_time','DESC')->page($page, $pageSize)->field('mode,platform,ip,create_time')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}