<?php 

return [
	'common_prefix' => 'lcs_',		// 公共缓存前缀
	'user_login_expires_time' => 36000,  // token有效期，单位为秒
	'user_login_token_update' => 600, // token在多少分钟内重新更新，防止突然退出登录
	'wallet' => [
		'min_tixian' => 10,  // 最小提现金额
		'tx_free' => 0.1 // 提现手续费
	],
	// 收款账号类型
	'account_types' => [
		'alipay',
		'wxpay',
		'bank'
	],

	'user' => [
		'rate' => 0.05 // 用户收入税率
	],

	'vip' => [
		'max_order_num' => 10, // VIP每天最多订单次数
	]
];