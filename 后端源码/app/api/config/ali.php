<?php 

/**
 * 阿里相关配置
 */
return [
	'pay' => [  // 支付宝配置
		'pay_url' => 'https://openapi.alipay.com/gateway.do', // 支付调用地址
		'app_id' => '',
		'notify_url' => 'https://api.xxxxx.cn/v1/alipay/notify', // 异步通知地址
		'return_url' => 'https://www.xxxx.cn/payment/alipay', // 同步通知地址,写前端地址，然后查询订单状态即可
		// 支付宝公钥，并非应用公钥
		'public_key' => '',
		// 私钥
		'private_key' => '', 
	]
];