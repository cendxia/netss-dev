<?php 

namespace app\api\business;

use app\api\model\UserWallet as wallet;

/**
 * 
 */
class UserWallet
{

	private $wallet;
	
	public function __construct()
	{
		$this->wallet = new wallet();
	}

	/**
	 * 获取钱包信息
	 * @Author   cendxia
	 * @DateTime 2022-08-28T15:42:37+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getWallet($user_id){
		$result = $this->wallet->getByUserId($user_id);
		return $result;
	}


	


	
}