<?php 

namespace app\api\business;
use app\api\validate\Kecheng as kechengValidate;
use app\api\model\Kecheng;
use app\api\model\KechengChapters;
use app\api\model\KechengDirectory;
use app\common\lib\SubmitSeo;


/**
 * 课程
 */
class MpKecheng
{

	private $model;
	private $chapters;
	private $directory;
	
	public function __construct()
	{
		$this->model = new Kecheng();
		$this->chapters = new KechengChapters();
		$this->directory = new KechengDirectory();
	}


	/**
	 * 获取课程列表
	 * @Author   cendxia
	 * @DateTime 2023-03-16T19:58:29+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getKeList($user_id, $page, $pageSize){
		$result = $this->model->getList($user_id, $page, $pageSize);
		$count = $this->model->getCount(['user_id'=>$user_id]);

		foreach($result as $key => $value){
			if(!empty($value['cover'])){
				$result[$key]['cover'] = unserialize($value['cover']);
			}
			$result[$key]['chaptersNum'] = $this->chapters->getChaptersNum($user_id, $value['id']);
			if($value['grade'])

			switch ($value['grade']) {
				case 1:
					$result[$key]['gradeText'] = '入门';
					break;
				case 2:
					$result[$key]['gradeText'] = '中级';
					break;
				case 3:
					$result[$key]['gradeText'] = '高级';
					break;
				case 4:
					$result[$key]['gradeText'] = '进阶';
					break;
				default:
					$result[$key]['gradeText'] = '';
					break;
			}
		}
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 创建/编辑课程
	 * @Author   cendxia
	 * @DateTime 2023-03-16T18:19:25+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function kecheng($user_id, $data){
		$data['user_id'] = $user_id;
		$data['ke_update_time'] = date('Y-m-d');
		// 使用验证器
		try {
	    	validate(kechengValidate::class)->scene('kecheng')->check($data);
	    } catch (ValidateException $e) {
	    	throw new \think\Exception($e->getError());
	    }
	    $data['cover'] = serialize($data['cover']);


	    if(!empty($data['kejian']) && !is_array($data['kejian'])){
	    	throw new \think\Exception(config('language.param_error'));
	    }
	    if(!empty($data['tool']) && !is_array($data['tool'])){
	    	throw new \think\Exception(config('language.param_error'));
	    }
	    if($data['vip_dis'] > 1 || $data['vip_dis'] < 0){
        	$data['vip_dis'] = 1;
        }

	    $data['kejian'] = serialize($data['kejian']);
	    $data['tool'] = serialize($data['tool']);

		try {
			return $this->model->kecheng($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 添加/编辑课程章节
	 * @Author   cendxia
	 * @DateTime 2023-03-17T14:22:52+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function chapters($user_id, $data){
		$data['user_id'] = $user_id;
		$data['sort'] = empty($data['sort']) ? 0 : $data['sort'];
		// 使用验证器
		try {
	    	validate(kechengValidate::class)->scene('chapters')->check($data);
	    } catch (ValidateException $e) {
	    	throw new \think\Exception($e->getError());
	    }

		$kecheng = $this->model->getKechengById($user_id, $data['ke_id']);

		if(empty($kecheng) || $kecheng['status'] == -1){
			throw new \think\Exception('当前课程不存在');
		}
		if($kecheng['status'] == 2){
			throw new \think\Exception('当前课程未通过审核');
		}

		try {
			return $this->chapters->chapters($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取课程章节
	 * @Author   cendxia
	 * @DateTime 2023-03-17T23:38:09+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $ke_id   [description]
	 * @return   [type]                            [description]
	 */
	public function getChaptersList($user_id, $ke_id){

		$result = $this->chapters->getList($ke_id, ['user_id'=>$user_id]);

		foreach($result as $key => $value){
			$res = $this->directory->getList($value['id'], ['user_id'=>$user_id]);
			$result[$key]['list'] = $res;

			foreach($res as $k => $v){
				if($v['type'] == 2 && !empty($v['content'])){
					$result[$key]['list'][$k]['content'] = htmlspecialchars_decode($v['content']);
				}
			}
			
		}

		return $result;
	}

	/**
	 * 添加/编辑课程内容
	 * @Author   cendxia
	 * @DateTime 2023-03-20T15:25:00+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function directory($user_id, $data){
		$data['user_id'] = $user_id;
		$data['sort'] = empty($data['sort']) ? 0 : $data['sort'];

		// 使用验证器
		try {
	    	validate(kechengValidate::class)->scene('directory')->check($data);
	    } catch (ValidateException $e) {
	    	throw new \think\Exception($e->getError());
	    }

	    try {
	    	$id = $this->directory->directory($data);
	    	if(!empty($data['ke_id']) && $data['is_free'] == 1){
	    		SubmitSeo::baidu('keTry', ['ke_id'=>$data['ke_id'],'chapters_id'=>$data['ke_chapters_id'],'directory_id'=>$id]);
	    		unset($data['ke_id']);
	    	}
	    	return true;
	    } catch (\think\Exception $e) {
	    	throw new \think\Exception(config('language.textError'));
	    }
	}


	/**
	 * 获取课程详情
	 * @Author   cendxia
	 * @DateTime 2023-03-24T23:50:10+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $ke_id   [description]
	 * @return   [type]                            [description]
	 */
	public function getKechengInfo($user_id, $ke_id){
		if(empty($ke_id)){
			throw new \think\Exception(config('language.param_error'));
		}

		try {
			$result = $this->model->where(['id'=>$ke_id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		if(empty($result)) return [];

		$result['cover'] = unserialize($result['cover']);
		$result['content'] = htmlspecialchars_decode($result['content']);
		if(!empty($result['kejian'])){
			$result['kejian'] = unserialize($result['kejian']);
		}else{
			$result['kejian'] = [];
		}
		if(!empty($result['tool'])){
			$result['tool'] = unserialize($result['tool']);
		}else{
			$result['tool'] = [];
		}

		return $result;
	}

	/**
	 * 删除章节
	 * @Author   cendxia
	 * @DateTime 2023-04-22T14:52:30+0800
	 * @param    [type]                   $ke_id       [description]
	 * @param    [type]                   $chapters_id [description]
	 * @param    [type]                   $user_id     [description]
	 * @return   [type]                                [description]
	 */
	public function delChapters($ke_id, $chapters_id, $user_id){
		if(empty($ke_id) || empty($chapters_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		try {
			$result = $this->chapters->where(['ke_id'=>$ke_id,'id'=>$chapters_id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return false;
		try {
			$directory = $this->directory->where(['ke_chapters_id'=>$chapters_id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(!empty($directory)){
			throw new \think\Exception('删除失败，请先删除章节内的课程内容');
		}

		try {
			return $this->chapters->where(['ke_id'=>$ke_id,'id'=>$chapters_id,'user_id'=>$user_id])->delete();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 删除课程内容
	 * @Author   cendxia
	 * @DateTime 2023-04-23T13:11:52+0800
	 * @param    [type]                   $chapters_id  [description]
	 * @param    [type]                   $directory_id [description]
	 * @param    [type]                   $user_id      [description]
	 * @return   [type]                                 [description]
	 */
	public function delDirectory($chapters_id, $directory_id, $user_id){
		if(empty($chapters_id) || empty($directory_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		try {
			return $this->directory->where(['ke_chapters_id'=>$chapters_id,'id'=>$directory_id,'user_id'=>$user_id])->delete();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 删除课程
	 * @Author   cendxia
	 * @DateTime 2023-04-23T13:27:27+0800
	 * @param    [type]                   $ke_id   [description]
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function delKecheng($ke_id, $user_id){
		if(empty($ke_id)){
			throw new \think\Exception(config('language.param_error'));
		}
	}
}