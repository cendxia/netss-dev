<?php 

namespace app\api\business;

use app\api\model\Ziyuan;
use app\api\model\Order;
use app\api\model\DownloadLog;
use app\api\model\UserFollow;
use app\api\model\UserWallet;
use app\api\model\UserWalletLog;

use app\common\lib\Str;

/**
 * 创作中心数据统计
 */
class MpTongji
{

	private $ziyuan;
	private $order;
	private $downloadLog;
	private $userFollow;

	public function __construct(){
		$this->ziyuan = new Ziyuan();
		$this->order = new Order();
		$this->downloadLog = new DownloadLog();
		$this->userFollow = new UserFollow();
	}


	/**
	 * 获取总浏览量
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:20:22+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	private function getReadNum($user_id){
		try {
			return $this->ziyuan->where(['user_id'=>$user_id])->sum('read_num');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总下载量
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:20:22+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	private function getDownloadNum($user_id){
		try {
			return $this->ziyuan->where(['user_id'=>$user_id])->sum('download_num');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总粉丝量
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:20:22+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	private function getFansNum($user_id){
		try {
			return $this->userFollow->where(['follow_user_id'=>$user_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总粉丝量
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:20:22+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	private function getCollectNum($user_id){
		try {
			return $this->ziyuan->where(['user_id'=>$user_id])->sum('collect_num');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取订单总数
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:26:34+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	private function getOrderNum($user_id){
		try {
			return $this->order->where(['sell_user_id'=>$user_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取资源总数
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:27:48+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	private function getZiyuanNum($user_id, $where=[]){
		try {
			return $this->ziyuan->where(['user_id'=>$user_id])->where($where)->where('status','<>',-1)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取近日下载数量
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:01:27+0800
	 * @param    [type]                   $user_id [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	private function getDownloadLogNum($user_id, $where=[]){
		$ids = $this->ziyuan->getZiyuanIds($user_id);
		return $this->downloadLog->getCountByZiyuanId($ids, $where);
	}

	/**
	 * 获取近日收藏量
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:06:48+0800
	 * @param    [type]                   $user_id [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	private function getCollectLogNum($user_id, $where=[]){
		$ids = $this->ziyuan->getZiyuanIds($user_id);
		$collectModel = new \app\api\model\ZiyuanCollect;
		try {
			return $collectModel->where('ziyuan_id', 'in', $ids)->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取近日浏览记录
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:24:20+0800
	 * @param    [type]                   $user_id [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	private function getReadLogNum($user_id, $where=[]){
		$ids = $this->ziyuan->getZiyuanIds($user_id);
		$readLog = new \app\api\model\ZiyuanRead;
		try {
			return $readLog->getCountByZiyuanId($ids, $where);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
	/**
	 * 首页数据统计
	 * @Author   cendxia
	 * @DateTime 2022-08-19T16:34:08+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function index($user_id){
		// 获取昨天日期
		$yday = date("Y-m-d",strtotime("-1 days", time()));
		// 总浏览量
		$read_num = $this->getReadNum($user_id);
		$read_yday_num = $this->ziyuan->where(['user_id'=>$user_id])->whereTime('create_time', '>=', $yday)->whereTime('create_time', '<', date('Y-m-d'))->sum('read_num');
		// 总下载量
		$download_num = $this->getDownloadNum($user_id);
		$download_yday_num = $this->ziyuan->where(['user_id'=>$user_id])->whereTime('create_time', '>=', $yday)->whereTime('create_time', '<', date('Y-m-d'))->sum('download_num');
		// 总粉丝量
		$fans_num = $this->getFansNum($user_id);
		$fans_yday_num = $this->userFollow->where(['follow_user_id'=>$user_id])->whereTime('create_time', '>=', $yday)->whereTime('create_time', '<', date('Y-m-d'))->count();
		// 总收藏量
		$collect_num = $this->getCollectNum($user_id);
		$collect_yday_num = $this->ziyuan->where(['user_id'=>$user_id])->whereTime('create_time', '>=', $yday)->whereTime('create_time', '<', date('Y-m-d'))->sum('collect_num');
		return [
			'read_num' => Str::numberUnit($read_num),
			'read_yday_num' => Str::numberUnit($read_yday_num),
			'download_num' => Str::numberUnit($download_num),
			'download_yday_num' => Str::numberUnit($download_yday_num),
			'fans_num' => Str::numberUnit($fans_num),
			'fans_yday_num' => Str::numberUnit($fans_yday_num),
			'collect_num' => Str::numberUnit($collect_num),
			'collect_yday_num' => Str::numberUnit($collect_yday_num),
		];
	}


	/**
	 * 内容数据
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:19:24+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function contentData($user_id){
		// 概况
		$gaikuang = $this->contentDataGaikuang($user_id);
		$ids = $this->ziyuan->getZiyuanIds($user_id);

		// 免费下载数量
		$orderIds = $this->order->getOrderIdsByZiyuanId($ids, 'price = 0');
		$freeDownloadNum = $this->downloadLog->getCountByOrderId($orderIds);
		// 收费下载数量
		$orderIds = $this->order->getOrderIdsByZiyuanId($ids, 'price > 0');
		$chargeDownloadNum = $this->downloadLog->getCountByOrderId($orderIds);

		// 获取总收益
		$walletModel = new UserWallet();
		$wallet = $walletModel->getByUserId($user_id);

		return [
			'gaikuang' => $gaikuang,
			'downloadTongji' => [
				'downloadNum' => $this->getDownloadNum($user_id),
				'freeDownloadNum' => $freeDownloadNum,
				'chargeDownloadNum' => $chargeDownloadNum,
				'total_profit' => $wallet['total_profit']
			]
		];
	}

	/**
	 * 内容数据 - 概况
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:44:38+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	private function contentDataGaikuang($user_id){
		// 内容总数
		$ziyuan_num = $this->getZiyuanNum($user_id);
		// 购买总数
		$order_num = $this->getOrderNum($user_id);
		// 下载数
		$download_num = $this->getDownloadNum($user_id);
		// 收藏数
		$collect_num = $this->getCollectNum($user_id);
		$dates = $this->getDates();
		$ziyuanArr = array(); // 新增资源
		$downloadLogArr = array(); // 新增下载
		$collectArr = array(); // 新增收藏
		$readArr = array(); // 流量量
		foreach($dates as $key => $val){
			$end_time = $val.' 23:59:59';
			$zy = $this->getZiyuanNum($user_id, "create_time >= '{$val}' and create_time <= '{$end_time}'");
			array_push($ziyuanArr, $zy);
			$xz = $this->getDownloadLogNum($user_id, "create_time >= '{$val}' and create_time <= '{$end_time}'");
			array_push($downloadLogArr, $xz);
			$sc = $this->getCollectLogNum($user_id, "create_time >= '{$val}' and create_time <= '{$end_time}'");
			array_push($collectArr, $sc);
			$read = $this->getReadLogNum($user_id, "create_time >= '{$val}' and create_time <= '{$end_time}'");
			array_push($readArr, $read);
		}

		$data = [
			'title' => '近7天数据',
			'dates' => $dates,
			'field' => ['新增资源数', '新增下载数', '新增收藏数', '浏览量'],
			'data' => [
				[
					'name' => '新增资源数',
				    'type' => 'line',
				    'data' => $ziyuanArr
				],
				[
					'name' => '新增下载数',
				    'type' => 'line',
				    'data' => $downloadLogArr
				],
				[
					'name' => '新增收藏数',
				    'type' => 'line',
				    'data' => $collectArr
				],
				[
					'name' => '浏览量',
				    'type' => 'line',
				    'data' => $readArr
				]
			]
		];

		return [
			'ziyuan_num' => $ziyuan_num,
			'order_num' => $order_num,
			'download_num' => $download_num,
			'collect_num' => $collect_num,
			'data' => $data
		];
	}

	/**
	 * 获取日期范围
	 * @Author   cendxia
	 * @DateTime 2022-08-19T17:40:22+0800
	 * @param    integer                  $days [description]
	 * @return   [type]                         [description]
	 */
	private function getDates($days = 7){
		$start_time = date("Y-m-d", strtotime("-{$days} days", time()));
		return Str::getDateFromRange($start_time, date('Y-m-d'));
	}

	/**
	 * 收益数据
	 * @Author   cendxia
	 * @DateTime 2022-08-20T17:46:44+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getProfit($user_id){
		// 昨天时间
		$ytd = date("Y-m-d", strtotime("-1 days", time()));
		$month = date('Y-m-1');
		$date = date('Y-m-d H:i:s');
		$walletLogModel = new UserWalletLog();
		$walletModel = new UserWallet();
		// 昨日收益
		$ytd_profit = $walletLogModel->getProfit($user_id, "create_time >= '{$ytd}' and create_time <= '{$ytd} 23:59:59'");
		// 本月收益
		$month_profit = $walletLogModel->getProfit($user_id, "create_time >= '{$month}' and create_time <= '{$date}'");
		// 可提现金额
		$wallet = $walletModel->getByUserId($user_id);
		$current_money = $wallet['current_money'];
		// 近30日收益
		$days30 = date("Y-m-d", strtotime("-30 days", time()));
		$dataDays30 = $walletLogModel->getProfit($user_id, "create_time >= '{$days30}' and create_time <= '{$date}'");
		// 累计收益
		$total_profit = $wallet['total_profit'];
		// 近7日数据图
		$dates = $this->getDates();
		$profit_tongji = array();
		foreach($dates as $key => $val){
			$end_time = $val.' 23:59:59';
			$tongji = $walletLogModel->getProfit($user_id, "create_time >= '{$val}' and create_time <= '{$end_time}'");
			array_push($profit_tongji, $tongji);
		}
		$data = [
			'title' => '近7天数据',
			'dates' => $dates,
			'field' => ['当日收益(元)'],
			'data' => [
				[
					'name' => '当日收益(元)',
				    'type' => 'line',
				    'data' => $profit_tongji
				]
			]
		];

		return [
			'ytd_profit' => $ytd_profit,
			'month_profit' => $month_profit,
			'current_money' => $current_money,
			'dataDays30' => $dataDays30,
			'total_profit' => $total_profit,
			'profit_tongji' => $data,
		];
	}


	/**
	 * 收益明细
	 * @Author   cendxia
	 * @DateTime 2022-08-28T11:07:36+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function incomeDetails($user_id, $page, $pageSize){
		$model = new UserWalletLog();
		$result = $model->getList($user_id, $page, $pageSize);
		$count = $model->getCount($user_id);
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	

}