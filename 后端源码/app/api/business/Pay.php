<?php 

namespace app\api\business;

use app\common\lib\Str;
use pay\wxpay\Callback;
use app\api\model\PayCallback;
use app\api\business\Order;
use app\api\business\Kecheng;
use app\api\business\Vip;
use app\api\model\KechengOrder;
use app\api\model\Ziyuan;
use pay\wxpay\NativePay;
use dh2y\qrcode\QRcode;
use think\facade\Db;
use think\facade\Cache;


/**
 * 支付回调
 */
class Pay
{

	/**
	 * 获取订单信息
	 * @Author   cendxia
	 * @DateTime 2022-11-04T22:08:09+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	private function getOrder($user_id, $order_id, $type){
		if(empty($order_id)){
            throw new \think\Exception('无效的订单');
        }
        if($type == 'ziyuan'){
        	$orderModel = new Order();
        	$order = $orderModel->getOrderInfo($order_id, $user_id);
        	if(empty($order)) throw new \think\Exception('该订单不存在');

        	$ziyuanModel = new Ziyuan();

        	// 判断资源是否存在
        	$ziyuan = $ziyuanModel->getInfo($order['ziyuan_id']);
        	if(empty($ziyuan) || $ziyuan['status'] != 1){
        		$orderModel->closeOrder($user_id, $order_id);  // 取消订单
        		throw new \think\Exception('资源已下架');
        	}

        	// 判断是否是卡密订单
        	if($ziyuan['is_kami'] == 1 && empty($ziyuan['kami'])){
        		$orderModel->closeOrder($user_id, $order_id);  // 取消订单
        		$ziyuanModel->editZiayuan($user_id, $ziyuan['ziyuan_id'], ['status'=>4]);  // 下架资源
        		throw new \think\Exception('卡密已售馨');
        	}
	        $order['data_id'] = $order['ziyuan_id'];
	        $order['title'] = $order['ziyuan_title'];
        }else if($type == 'kecheng'){
        	$orderModel = new KechengOrder();
        	$order = $orderModel->getOrderByOrderId($user_id, $order_id);
        	if(empty($order)) throw new \think\Exception('该订单不存在');
	        $order['data_id'] = $order['ke_id'];
	        $order['title'] = $order['ke_title'];
        }        
        
        if($order['status'] != 0){
        	throw new \think\Exception('当前订单状态不能支付');
        }
        return $order;
	}


	/**
	 * 	获取VIP订单
	 * @Author   cendxia
	 * @DateTime 2024-03-22T21:00:31+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	private function getVipOrder($user_id, $order_id){
		$order = (new Vip())->getOrder($user_id, $order_id);
		if(empty($order)) throw new \think\Exception('该订单不存在');
		if($order['pay_status'] != 0) throw new \think\Exception('该订单已支付');
		$order['data_id'] = $order['id'];
		return $order;
	}


	// 微信扫码支付
	public function nativePay($user_id, $order_id, $type){
		if($type == 'ziyuan' || $type == 'kecheng'){
			$order = $this->getOrder($user_id, $order_id, $type);
		}else if($type == 'vip'){
			$order = $this->getVipOrder($user_id, $order_id);
		}else{
			throw new \think\Exception(config('language.param_error'));
		}
		
        $nativePay = new NativePay();
        $price = $order['price'] * 100; // 金额(分)
        // $price = 10; // 金额(分)
        // 获取二维码，扫码支付
        $url = $nativePay->getQRcode($order_id, $price, $order['data_id'], $type);
        $code = new QRcode();
        return $code->png($url, false, 6)->entry();
	}
	
	/**
	 * 微信支付回调
	 * @Author   cendxia
	 * @DateTime 2022-11-03T13:51:15+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function wxPayCallback($data){
		// 记录日志
		$log = [
			'title' => '微信支付回调',
			'data' => $data
		];
		(new PayCallback)->insertLog($log);

		$data = Str::xmlArr($data);
		// 验签
		$result = (new Callback)->checkSignEvent($data);

		if(empty($result)){
			throw new \think\Exception('验签失败');
		}
  		if($data['result_code'] == 'SUCCESS' && $data['return_code'] == 'SUCCESS'){
  			if($data['attach'] == 'ziyuan'){
  				$orderBus = new Order();
  				return $orderBus->payCallback($data, 'wxpay');
  			}else if($data['attach'] == 'kecheng'){
  				$orderBus = new kecheng();
  				return $orderBus->payCallback($data, 'wxpay');
  			}else if($data['attach'] == 'vip'){
  				$orderBus = new Vip();
  				return $orderBus->payCallback($data, 'wxpay');
  			}
  		}
	}



	/////////////支//付//宝////////////////

	// 开发版无支付宝支付

	



	/////////////////虎//皮//椒/////////////////////////

	// 开发版无虎皮椒支付
	

}