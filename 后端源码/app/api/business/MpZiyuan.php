<?php 

namespace app\api\business;

use app\api\business\Ziyuan;
use app\api\model\Ziyuan as ziyuanModel;
use app\api\model\DownloadLog;
use app\api\validate\Ziyuan as ziyuanValidate;
use app\common\lib\Str;


/**
 * 
 */
class MpZiyuan
{
	private $model = null;
	private $category = null;

	public function __construct(){
		$this->model = new ziyuanModel();
	}


	/**
	 * 获取资源列表
	 * @Author   cendxia
	 * @DateTime 2022-10-28T12:54:47+0800
	 * @param    [type]                   $user_id     [description]
	 * @param    [type]                   $page        [description]
	 * @param    [type]                   $pageSize    [description]
	 * @param    [type]                   $category_id [description]
	 * @param    [type]                   $status      [description]
	 * @param    [type]                   $start_time  [description]
	 * @param    [type]                   $end_time    [description]
	 * @param    [type]                   $title       [description]
	 * @return   [type]                                [description]
	 */
	public function getList($user_id, $page, $pageSize, $category_id, $status, $start_time, $end_time, $title){
		$where = array();
		if(!empty($category_id)){
			$where[] = ['category_id', '=', $category_id];
		}
		if($status != null){
			$where[] = ['status', '=', $status];
		}
		if(!empty($start_time)){
			$where[] = ['create_time', '>=', $start_time];
		}
		if(!empty($end_time)){
			$where[] = ['create_time', '<=', $end_time];
		}
		if(!empty($title)){
			$where[] = [['title', 'like', '%' . $title . '%']];
		}
		$where[] = ['user_id', '=', $user_id];
		$where[] = ['status', '<>', -1];

		try {
			$result = $this->model->getList($page, $pageSize, $where);
			$count = $this->model->getCount($where);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			if(!empty($value['cover'])){
				$result[$key]['cover'] = unserialize($value['cover']);
			}
			if($value['is_kami'] == 1){
				$result[$key]['title'] = '[卡密]'.$value['title'];
			}
		}

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	

	/**
	 * 发布资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T15:40:41+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function fabu($user_id, $data){

		// $data['cover'][0] = 'https://lc-data.apiall.cn/data1/2020/05/1588394803-2ff885b1995d270.jpg';


		// dump($data); exit;

		$data['user_id'] = $user_id;
		// 使用验证器
		try {
	    	validate(ziyuanValidate::class)->scene('fabu')->check($data);
	    } catch (ValidateException $e) {
	    	throw new \think\Exception($e->getError());
	    }

	    if(!empty($data['cover'])){
	    	$data['cover'] = serialize($data['cover']);
	    }

	    if($data['is_kami'] != 1 && empty($data['download_url'])){
	    	throw new \think\Exception('下载地址必填');
	    }

	    $content = htmlspecialchars_decode($data['content']);
	    if(empty($data['abstract']) && !empty($content)){
        	$data['abstract'] = Str::htmlStr($content);
        }
        $data['ziyuan_update_time'] = date('Y-m-d H:i:s');

        if($data['is_kami'] == 1){
        	if(empty($data['kami'])){
        		throw new \think\Exception('请输入卡密');
        	}
        	// 判断卡密中是否有中文
        	if(Str::hasChinese($data['kami'])){
        		throw new \think\Exception('卡密中请勿含中文');
        	}
        }

        if($data['vip_dis'] > 1 || $data['vip_dis'] < 0){
        	$data['vip_dis'] = 1;
        }

	    try {
	    	return $this->model->ziyuan($data);
	    } catch (\think\Exception $e) {
	    	throw new \think\Exception(config('language.mysql_error'));
	    }
	}

	/**
	 * 获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-08-20T15:38:48+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getDownloadLog($user_id, $page, $pageSize){
		$downloadModel = new DownloadLog();
		try {
			$result = $downloadModel->getSellList($user_id, $page, $pageSize);
			$count = $downloadModel->where(['sell_user_id'=>$user_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 获取已发布资源类别
	 * @Author   cendxia
	 * @DateTime 2022-09-25T16:56:35+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getUserZiyuanCategory($user_id){
		$result = $this->model->where(['user_id'=>$user_id])->field('category_id')->select()->toArray();
		if(empty($result)) return [];
		$category_ids = array();
		foreach($result as $value){
			array_push($category_ids, $value['category_id']);
		}
		$category_ids = array_unique($category_ids);
		$categoryModel = new \app\api\model\ZiyuanCategory;
		$category = $categoryModel->where('id', 'in', $category_ids)->field('id,title')->order('sort','ASC')->select()->toArray();
		return $category;
	}

	/**
	 * 资源操作，置顶、仅自己可见
	 * @Author   cendxia
	 * @DateTime 2022-09-25T23:46:04+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @param    [type]                   $type      [description]
	 * @return   [type]                              [description]
	 */
	public function operation($user_id, $ziyuan_id, $type){
		$zyOperations = [
			'is_top',
			'self_visible'
		];
		if(empty($type) || array_search($type, $zyOperations) === false){
			throw new \think\Exception(config('language.param_error'));
		}
		$ziyuan = $this->model->find($ziyuan_id);
		if(empty($ziyuan)){
			throw new \think\Exception('资源信息不存在');
		}
		if($type == 'is_top'){
			// 置顶
			$data['is_top'] = $ziyuan['is_top'] == 1 ? 0 :1;
		}else if($type == 'self_visible'){
			// 设置为仅自己可见
			$data['self_visible'] = $ziyuan['self_visible'] == 1 ? 0 :1;
		}
		return $this->model->editZiayuan($user_id, $ziyuan_id, $data);
	}

	/**
	 * 获取资源详情，修改时使用
	 * @Author   cendxia
	 * @DateTime 2022-09-26T16:12:31+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function getZiyuanInfo($user_id, $ziyuan_id){
		try {
			$result = $this->model->where(['user_id'=>$user_id,'id'=>$ziyuan_id])->where('status', '<>', -1)->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		if(empty($result)) return [];
		$result = $result->toArray();
		$result['content'] = htmlspecialchars_decode($result['content']);
		if(!empty($result['cover'])){
			$result['cover'] = unserialize($result['cover']);
		}
		return $result;		
	}

	/**
	 * 删除资源
	 * @Author   cendxia
	 * @DateTime 2022-10-10T11:13:31+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function delZiyuan($user_id, $ziyuan_id){
		return $this->model->delZiyuan($user_id, $ziyuan_id);
	}

}