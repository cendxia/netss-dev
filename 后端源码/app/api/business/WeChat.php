<?php 

namespace app\api\business;

use app\api\model\Users;
use app\api\model\UserWallet;
use think\facade\Cache;
use app\common\lib\WeChat as wxChatLib;
use app\common\lib\Str;

/**
 * 
 */
class WeChat
{
	
	private $config;
	
	public function __construct()
	{
		
		$this->config = config('common.WeChat.web');
	}


	/**
	 * 公众号接入
	 * @Author   cendxia
	 * @DateTime 2022-10-21T19:59:35+0800
	 * @return   [type]                   [description]
	 */
	public function interface(){
		$config = config('common.WeChat.common');
		//微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
		$signature = $_GET['signature'];
		//timestamp	时间戳
		$timestamp = $_GET['timestamp'];
		//nonce	随机数
		$nonce = $_GET['nonce'];
		$dataArray = array($config['token'], $timestamp, $nonce);
		//排序 sort SORT_STRING 快速排序
		sort($dataArray, SORT_STRING);
		//把排序后的数据转为字符器
		$str = implode($dataArray);
		//把字符串加密
		$str = sha1($str);
		//判断是否是第一次验证
		if($str == $signature && isset($_GET['echostr'])){
			// 第一次验证
			echo $_GET['echostr'];
			exit; // 一定要加exit
		}else{
			// 其他事件
			// echo 'success';
            // exit;
            $data = file_get_contents('php://input');
            $this->wxChatEvent($data);
		}
	}

	/**
	 * 处理微信事件
	 * @Author   cendxia
	 * @DateTime 2022-10-22T19:56:31+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	private function wxChatEvent($data){
		// echo 'success';
		$data = Str::xmlArr($data);

		if($data['MsgType'] == 'event'){

			if($data['Event'] == 'subscribe'){
				// 关注

			}else if($data['Event'] == 'unsubscribe'){
				// 取消关注

			}else if($data['Event'] == 'text'){
				// 文本消息
				
			}else if($data['Event'] == 'click'){
				// 点击菜单
				
			}else if($data['Event'] == 'SCAN'){
				// 扫码
				
			}
		}
	}


	/**
	 * 微信登录
	 * @Author   cendxia
	 * @DateTime 2022-10-21T17:39:22+0800
	 * @param    [type]                   $code [description]
	 * @return   [type]                         [description]
	 */
	public function login($code){
		if(empty($code)){
			throw new \think\Exception(config('language.param_error'));
		}
		$config = config('common.WeChat.web');

		$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$config['Appid'].'&secret='.$config['AppSecret'].'&code='.$code.'&grant_type=authorization_code';

		$data = file_get_contents($url);
		$result = json_decode($data, true);

		if(empty($result) || empty($result['unionid'])){
			throw new \think\Exception('登录失败');
		}

		return $this->loginEvent($result);
	}

	/**
	 * 微信扫码关注公众号登录
	 * @Author   cendxia
	 * @DateTime 2023-09-23T11:29:08+0800
	 * @param    [type]                   $openid [description]
	 * @return   [type]                           [description]
	 */
	public function followLogin($openid, $randStr=''){
		if(empty($openid)){
			throw new \think\Exception('登录失败');
		}

		$access_token = wxChatLib::getAccessToken();
        $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';

        $wxUser = httpUrl($url);
        if(!empty($wxUser['errcode'])){
        	throw new \think\Exception($wxUser['errmsg']);
        }

		return $this->loginEvent($wxUser, $randStr);
	}


	/**
	 * 处理微信登录
	 * @Author   cendxia
	 * @DateTime 2023-09-23T16:24:33+0800
	 * @param    [type]                   $data    [description]
	 * @param    string                   $randStr [description]
	 * @return   [type]                            [description]
	 */
	private function loginEvent($data, $randStr=''){
		// 获取用户信息
		$userMode = new Users();
		$user = $userMode->getUserByUnionid($data['unionid']);

		if(empty($user)){
			// 生成随机字符串
			$salt = Str::getRandStr(rand(15,25));
			$login_name = 'gzy'.Str::getRandStr(rand(5,8)).date('dHs');
			$userData = [
	        	'login_name' => $login_name,
	        	'nickname' => '资交网'.rand(10, 9999), // 昵称
	        	'openid' => $result['openid'],
	        	'unionid' => $result['unionid'],
	        	'phone' => '',
	        	'password' => md5($login_name.'_'.md5($salt)),
	        	'avatar' => 'https://zjw.data.zijiao.cn/avatar/avatar_'.rand(1,12).'.png',
	        	'salt' => $salt
	        ];

	        $userMode->startTrans();  // 开启事务
			try {
				$user_id = $userMode->insertGetId($userData);
				(new UserWallet())->addWallet(['user_id'=>$user_id]);
				$userMode->commit(); // 提交事务
			} catch (\think\Exception $e) {
				$userMode->rollback(); // 事务回滚
				throw new \think\Exception('登录失败');
			}

	        // 登录操作
	        // 获取token
	        $token = Str::getLoginToken($login_name);
	        $user = $userMode->getUserByUnionid($result['unionid']);
	        if(empty($user)){
	        	throw new \think\Exception('登录失败');
	        }
		}

		// 获取token
        $token = Str::getLoginToken($user['login_name']);

	    $redisData = [
        	'token' => $token,
            'user_id' => $user['user_id'],
            'nickname' => $user['nickname'],
            'username' => $user['user_name'],
            'avatar' => $user['avatar'],
            'phone' => $user['phone'],
            'type' => $user['type'],
            'token_time' => time() + config('common.user_login_expires_time') // token有效时间
        ];

        // 获取公共缓存前缀
        $prefix = config('common.common_prefix');
        // 判断是否登录过，如已登录，退出之前登录
        $header_token = cache($prefix.'login_lc_'.$user['login_name']);
        // 清除缓存
        if($header_token){
        	// 清除登录token
        	Cache::delete(config('redis.token_pre').$header_token);
        	// 清除单点登录token
        	Cache::delete($prefix.'login_lc_'.$user['login_name']);

        	// // 清除登录token
        	// cache(config('redis.token_pre').$header_token, false);
        	// // 清除单点登录token
        	// cache($prefix.'login_lc_'.$user['login_name'], false);
        }

        $login_log = [
        	'user_id' => $user['user_id'],
        	'ip' => getUserIp(),
        	'mode' => '微信登录',
        	'platform' => 'web'
        ];

        (new \app\api\model\UserLogin())->add($login_log);

        if($randStr){
        	cache($prefix.'login_lc_randstr_'.$randStr, 'lc_app_token_pre'.$token, 300);
        }

        // 单点登录使用
        cache($prefix.'login_lc_'.$user['login_name'], $token, 36000); // 这里需要手动填写参数，不能从配置文件中获取
        // 存储登录token
        $res = cache($prefix.'lc_app_token_pre'.$token, $redisData, 36000); // 这里需要手动填写参数，不能从配置文件中获取
        return $redisData;
	}

	/**
	 * 通过token获取登录信息
	 * @Author   cendxia
	 * @DateTime 2022-10-27T22:19:58+0800
	 * @param    [type]                   $token [description]
	 * @return   [type]                          [description]
	 */
	public function getLoginData($token){
		if(empty($token)){
			throw new \think\Exception('token获取失败');
		}
		$user = cache(config('redis.token_pre').$token);
		if(empty($user)){
			throw new \think\Exception('token错误，请重新登录', config('status.not_login'));
		}
		return $user;
	}


	/**
	 * 获取token
	 * @Author   cendxia
	 * @DateTime 2022-10-21T17:44:02+0800
	 * @param    [type]                   $code [description]
	 * @return   [type]                         [description]
	 */
	private function getAccessToken($code){
		$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->config['Appid'].'&secret='.$this->config['AppSecret'].'&code='.$code.'&grant_type=authorization_code';
		$data = file_get_contents($url);
		return json_decode($data, true);
	}

	/**
	 * 刷新access_token有效期
	 * @Author   cendxia
	 * @DateTime 2022-10-21T17:56:13+0800
	 * @param    [type]                   $refresh_token [description]
	 * @return   [type]                                  [description]
	 */
	private function refreshAccessToken($refresh_token){
		$url = 'https://api.weixin.qq.com/sns/oauth2/refresh_token?appid='.$this->config['Appid'].'&grant_type=refresh_token&refresh_token='.$refresh_token;
		$data = file_get_contents($url);
		return json_decode($data, true);
	}


	/**
	 * 检验授权凭证（access_token）是否有效
	 * @Author   cendxia
	 * @DateTime 2022-10-21T17:56:07+0800
	 * @param    [type]                   $access_token [description]
	 * @param    [type]                   $openid       [description]
	 * @return   [type]                                 [description]
	 */
	private function checkAccessToken($access_token, $openid){
		$url = 'https://api.weixin.qq.com/sns/auth?access_token='.$access_token.'&openid='.$openid;
		$data = file_get_contents($url);
		return json_decode($data, true);
	}

	/**
	 * 获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-21T17:56:19+0800
	 * @param    [type]                   $access_token [description]
	 * @param    [type]                   $openid       [description]
	 * @return   [type]                                 [description]
	 */
	private function getUserInfo($access_token, $openid){
		$url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid;
		$data = file_get_contents($url);
		return json_decode($data, true);
	}

	/**
	 * 获取微信带参数的二维码
	 * @Author   cendxia
	 * @DateTime 2022-10-24T13:16:58+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $type    [description]
	 * @return   [type]                            [description]
	 */
	public function getQrCode($user_id='', $type='', $randStr=''){
		if(empty($type)) {
			throw new \think\Exception(config('language.param_error'));
		}

		if($type == 'binding'){
			// 绑定用户
			$user = (new Users())->getByUserId($user_id);
			if(empty($user)){
				throw new \think\Exception('用户信息不存在');
			}
			if($user['state'] != 0){
				throw new \think\Exception('当前用户已被禁用');
			}
			if(!empty($user['openid']) || !empty($user['unionid'])){
				throw new \think\Exception('当前用户已绑定，请解绑后重试');
			}
			return $this->getQrCodeEvent('bind&'.$user_id);
		}else if($type == 'userLogin'){
			// 用户扫码登录
			return $this->getQrCodeEvent('gzyuserlogin&'.$randStr);
		}
	}


	/**
	 * 获取微信带参数的二维码
	 * @Author   cendxia
	 * @DateTime 2022-10-24T13:06:34+0800
	 * @return   [type]                   [description]
	 */
	private function getQrCodeEvent($scene_str, $time=86400){
		if(empty($scene_str)){
			throw new \think\Exception(config('language.param_error'));
		}
		$acctoken = wxChatLib::getAccessToken();
		$url = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.$acctoken;
		$params = [
			'expire_seconds' => $time,
			'action_name' => 'QR_STR_SCENE',
			'action_info' => [
				'scene' => [
					'scene_str' => $scene_str
				]
			]
		];

		$result = httpUrl($url, json_encode($params));
		if(empty($result) || !isset($result['ticket'])){
			throw new \think\Exception('获取ticket失败');
		}
		// 获取二维码地址
		return 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.$result['ticket'];
	}


	/**
	 * 微信公众号受权登录
	 * @Author   cendxia
	 * @DateTime 2022-10-25T18:13:08+0800
	 * @param    [type]                   $param [description]
	 * @return   [type]                          [description]
	 */
    public function wxlogin(){
    	$config = config('common.WeChat.common');
        // 用户点击受权后跳转的地址
        $urlEncode = urlEncode($config['host'].'/weChat/userInfo');
        // scope=snsapi_userinfo 弹出授权页面 snsapi_base 不会弹出受权页面
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$config['appID'].'&redirect_uri='.$urlEncode.'&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect';
        return rawurldecode($url);
    }


    /**
     * 微信公众号获取用户详情
     * @Author   cendxia
     * @DateTime 2022-10-25T17:47:56+0800
     * @param    [type]                   $code [description]
     * @return   [type]                         [description]
     */
    public function userInfo($code, $param){
        $config = config('common.WeChat.common');
        //通过code换取网页授权access_token
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$config['appID'].'&secret='.$config['appsecret'].'&code='.$code.'&grant_type=authorization_code';
        $result = httpUrl($url);

        // $result 中如果无openid 则未关注公众号

        // 判断用户是否刷新
        if(!empty($result['errcode']) && $result['errcode'] == 40163){
            return ['type' => 'redirect', 'url' => $this->wxlogin()];
        }
    
        // 判断用户是否关注
        $access_token = wxChatLib::getAccessToken();
        $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$access_token.'&openid='.$result['openid'].'&lang=zh_CN';

        $follow = httpUrl($url);
        if(!empty($follow['subscribe']) && $follow['subscribe'] == 1){
            // 已关注
            $data['is_follow'] = 1;
        }else{
            // 未关注
            $data['is_follow'] = 0;
        }

        // 拉取用户信息(需scope为 snsapi_userinfo)
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$result['access_token'].'&openid='.$result['openid'].'&lang=zh_CN';
        $userInfo = httpUrl($url);

        
        if($userInfo && !empty($userInfo['unionid'])){
            // 判断用户是否存在
            $userBus = new Users();
            $users = $userBus->getUserByUnionid($userInfo['unionid']);
            if(empty($users)){
                //保存头像
                $UploadLib = new \app\common\lib\Upload();
                $imgRes = $UploadLib->fetch($userInfo['headimgurl']);
                $data['openid'] = $userInfo['openid'];
                $data['unionid'] = $userInfo['unionid'];
                $data['nickname'] = filterEmoji($userInfo['nickname']);
                $data['avatar'] = $imgRes;
                cache($result['unionid'].'wx_user_info', $data, 1800);
            }else{
                // 判断unionid是否为空
                if($users['unionid'] == '' || $userInfo['unionid'] == NULL){
                    $data['unionid'] = $userInfo['unionid'];
                    $this->updateUser($users, $data);
                }
                // 已存在 更新用户名
                if($users['nickname'] != filterEmoji($userInfo['nickname'])){
                    $data['nickname'] = filterEmoji($userInfo['nickname']);
                    $this->updateUser($users, $data);
                }
            }
            // 跳转到h5页面
            // return $result['unionid'];
        }
        return false;
    }

    /**
     * 更新用户信息
     * @Author   cendxia
     * @DateTime 2022-05-05T21:55:55+0800
     * @param    [type]                   $users  [description]
     * @param    [type]                   $wxUser [description]
     * @return   [type]                           [description]
     */
    private function updateUser($users, $wxUser){
    	$userBus = new User();
    	foreach($users as $Key => $value){
    		$userBus->updateUser($value['user_id'], $wxUser);
    	}
    }


    /**
	 * 微信用户绑定操作
	 * @Author   cendxia
	 * @DateTime 2022-10-25T23:10:18+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function wxBinding($data){
		if(empty($data['login_name']) || empty($data['password']) || empty($data['unionid'])){
			throw new \think\Exception(config('language.param_error'));
		}

		// 判断unionid是否有效
		$result = Cache($data['unionid'].'wx_user_info');
		if(empty($result)){
			throw new \think\Exception('微信未授权，请重试！');
		}

		$userMode = new Users();
		$user = $userMode->getUserByLoginName($data['login_name']);
		// 判断用户名和密码是否正确
		if(empty($user)){
			throw new \think\Exception('用户名或密码错误');
		}
		$utils = new \app\common\lib\Utils;
		$password = $utils->userEncryption(md5($data['password']), $user['salt']);
		if($password != $user['password']){
			throw new \think\Exception('用户名或密码错误');
		}
		// 判断是否已绑定微信
		if(!empty($user['unionid']) || !empty($user['openid'])){
			throw new \think\Exception('您已绑定过微信，请解绑后重试');
		}

		// 判断此微信是否绑定或其他账号
		$userData = $userMode->getUserByUnionid($data['unionid']);
		if(!empty($userData)){
			throw new \think\Exception('此微信已绑定了【'.$userData['nickname'].'】账号，请解绑后重试！');
		}

		// 开始绑定，并做绑定记录
		$log = [
			'user_id' => $user['user_id'],
			'nickname' => filterEmoji($result['nickname']),
			'type' => 1
		];

		$logModel = new \app\api\model\wxBinding;
		$logModel->add($log);

		$upData = [
			'nickname' => filterEmoji($result['nickname']),
			'openid' => $result['openid'],
			'unionid' => $result['unionid']
		];
		return $userMode->edit($user['user_id'], $upData);
	}
}