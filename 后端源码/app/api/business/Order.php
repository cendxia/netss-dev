<?php 

namespace app\api\business;

use app\api\model\Order as orderModel;
use app\api\model\Ziyuan;
use app\api\model\Users;
use app\api\model\UserWallet;
use app\api\model\UserWalletLog;
use app\api\model\DownloadLog;
use app\common\lib\Str;

/**
 * 
 */
class Order
{
	private $model = null;
	private $ziyuan = null;

	public function __construct()
	{
		$this->model = new orderModel();
		$this->ziyuan = new Ziyuan();
	}

	/**
	 * 提交订单
	 * @Author   cendxia
	 * @DateTime 2022-09-15T22:57:21+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function submitOrder($user_id, $ziyuan_id){
		if(empty($ziyuan_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		$ziyuan = $this->ziyuan->getInfo($ziyuan_id);
		if(empty($ziyuan)){
			throw new \think\Exception('当前资源不存在');
		}
		if($ziyuan['status'] != 1){
			throw new \think\Exception('当前资源不存在或未通过审核');
		}

		if($user_id == $ziyuan['user_id']){
			throw new \think\Exception('不能购买自己的资源');
		}

		// 查询是否有未支付订单，禁止重复提交订单
		$order = $this->model->getByZiyuanId($user_id, $ziyuan_id);

		// 有未支付的订单，重新获取支付二维码
		if($order && $order['status'] == 0){
			return $order['order_id'];
		}

		// 判断卡密是否已用完
		if($ziyuan['is_kami'] == 1 && empty($ziyuan['kami'])){
			// 下架资源
			$this->ziyuan->editZiayuan($ziyuan['user_id'], $ziyuan_id, ['status'=>4]);
			throw new \think\Exception('卡密已售馨');
		}

		// 生成订单号
		$order_id = Str::getOrderId();
		$order_id = date('Ymd').$order_id;

		$price = $ziyuan['price'];
		$orderVip = 0;
		if($ziyuan['vip_dis'] < 1 && $ziyuan['price'] > 0){
			$vipBus = new \app\api\business\Vip();
			$vip = $vipBus->getUserVip($user_id, 'ziyuan', $ziyuan['category_id']);
			if($vip){
				// 查询今日VIP订单是数
				$vipOrderNum = $this->model->getVipOrderCount($user_id);
				if($vipOrderNum < config('common.vip.max_order_num')){
					$orderVip = 1;
					$price = $ziyuan['price'] * $ziyuan['vip_dis'];
				}
			}
		}

		$status = $price == 0 ? 1 : 0;

		$download_url = '';
		$orderKami = '';
		$status = 0;
		$pay_time = '';
		if($price == 0){
			$status = 1;
			$pay_time = date('Y-m-d H:i:s');
			$download_url =  $ziyuan['download_url'];
			if($ziyuan['is_kami'] == 1){
				$kami = $this->kamiConsume($ziyuan['kami']);
			}
		}

		$data = [
			'order_id' => $order_id,
			'sell_user_id' => $ziyuan['user_id'],
			'user_id' => $user_id,
			'ziyuan_id' => $ziyuan['id'],
			'ziyuan_title' => $ziyuan['title'],
			'ziyuan_cover' => $ziyuan['cover'],
			'download_url' => $download_url,
			'extract' => $ziyuan['extract'],
			'price' => $price,
			'kami' => $orderKami,
			'status' => $status,
			'vip' => $orderVip,
			'pay_time' => $pay_time
		];

		$this->model->startTrans();  // 开启事务
		try {
			$this->model->add($data);
			if(isset($kami)){	// 卡密消费
				$this->ziyuan->editZiayuan($ziyuan['user_id'], $ziyuan_id, ['kami'=>$kami['kami']]);
			}
			$this->model->commit(); // 提交事务
			return $order_id;
		} catch (\think\Exception $e) {
			$this->model->rollback(); // 事务回滚
			return false;
		}
	}

	/**
	 * 卡密消费
	 * @Author   cendxia
	 * @DateTime 2024-02-26T22:44:56+0800
	 * @param    [type]                   $kami [description]
	 * @return   [type]                         [description]
	 */
	private function kamiConsume($kami){
        $kamiArr = explode("\n", $kami);
        if(empty($kamiArr)){
        	// 下架资源
			$this->ziyuan->editZiayuan($ziyuan['user_id'], $ziyuan_id, ['status'=>4]);
			throw new \think\Exception('卡密已售馨');
        }
        $data['orderKami'] = $kamiArr[0];
        unset($kamiArr[0]);
        $data['kami'] = implode("\n", $kamiArr);
        return $data;
	}

	/**
	 * 获取订单列表
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:03:01+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    string                   $field    [description]
	 * @return   [type]                             [description]
	 */
	public function getList($user_id, $page, $pageSize, $field='user_id'){
		try {
			$result =  $this->model->getList($user_id, $page, $pageSize, $field);
			$count = $this->model->getCount($user_id, $page, $pageSize, $field);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		
		$userModel = new Users();
		foreach($result as $key => $value){
			if ($field == 'sell_user_id') {
				$result[$key]['user_name'] = $userModel->where(['user_id'=>$value['user_id']])->value('user_name');
				unset($result[$key]['download_url']);
			}
			if ($field == 'sell_user_id') {
				$result[$key]['nickname'] = $userModel->where(['user_id'=>$value['user_id']])->value('nickname');
			}
			if($value['status'] == 0){
				$result[$key]['state'] = '未支付';
				unset($result[$key]['download_url']);
			}else if($value['status'] == 1){
				$result[$key]['state'] = '已支付';
			}else if($value['status'] == 2){
				$result[$key]['state'] = '已关闭';
			}else{
				$result[$key]['state'] = '未知';
			}
			if(!empty($value['ziyuan_cover'])){
				$result[$key]['ziyuan_cover'] = unserialize($value['ziyuan_cover']);
			}

			$result[$key]['payType'] = payType($value['pay_type']);

			// 判断下载地址是否为百度网盘
			$result[$key]['paiduPan'] = false;
			if($value['download_url'] && $value['status'] == 1 && strpos($value['download_url'], 'https://pan.baidu.com/') !== false && $value['pay_type'] > date('Y-m-d 00:00:00')){
				$result[$key]['paiduPan'] = true;
			}
		}
		
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 下载记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:01:28+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getDownloadLog($user_id, $page, $pageSize){
		$model = new DownloadLog();
		try {
			$result =  $model->getList($user_id, $page, $pageSize);
			$count = $model->getCount($user_id);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 资源下载
	 * @Author   cendxia
	 * @DateTime 2022-10-20T16:07:22+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $order_id  [description]
	 */
	public function ziyuanDownload($user_id, $order_id){
		if(empty($order_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		$order = $this->getOrderInfo($order_id, $user_id);
		if(empty($order) || $order['status'] != 1){
			return false;
		}

		if(empty($order['download_url'])){
			throw new \think\Exception('无下载地址');
		}

		$log = [
			'order_id' => $order['order_id'],
			'user_id' => $user_id,
			'sell_user_id' => $order['sell_user_id'],
			'ziyuan_id' => $order['ziyuan_id'],
			'ziyuan_title' => $order['ziyuan_title'],
			'ziyuan_cover' => $order['ziyuan_cover'],
			'extract' => $order['extract']
		];

		$downloadModel = new \app\api\model\DownloadLog;
		$this->model->startTrans();  // 开启事务
		try {
			// 自增
			$this->ziyuan->where(['id'=>$order['ziyuan_id'],'user_id'=>$order['sell_user_id']])->inc('download_num')->update();
			$downloadModel->insertLog($log);
			$this->model->commit(); // 提交事务
			return $order;
		} catch (\think\Exception $e) {
			$this->model->rollback(); // 事务回滚
			throw new \think\Exception('未获取到相应数据');
		}
	}


	/**
	 * 处理支付回调订单状态
	 * @Author   cendxia
	 * @DateTime 2022-11-03T16:27:15+0800
	 * @param    [type]                   $data     [description]
	 * @param    string                   $pay_type alipay、wxpay
	 * @return   [type]                             [description]
	 */
	public function payCallback($data, $pay_type=''){
		if(empty($pay_type)){
			throw new \think\Exception('language.param_error');
		}

		if($pay_type == 'hpjwxpay'){
			$order_id = $data['trade_order_id'];
		}else{
			$order_id = $data['out_trade_no'];
		}

		// 查询订单信息
		$order = $this->model->getOrderByOrderId($order_id);
		if(empty($order)){
			throw new \think\Exception('订单不存在');
		}
		// 判断订单金额是否一致，hpjwxpay为虎皮椒微信支付
		if($pay_type == 'wxpay'){
			$total_fee = $data['total_fee'] / 100;
		}else if($pay_type == 'alipay'){
			$total_fee = $data['total_amount'];
		}else if($pay_type == 'hpjwxpay'){
			$total_fee = $data['total_fee'];
		}else{
			throw new \think\Exception('支付类型错误');
		}
		
		if($total_fee != $order['price'] || $order['status'] != 0){
			throw new \think\Exception('订单信息不匹配');
		}

		$ziyuan = $this->ziyuan->getInfo($order['ziyuan_id']);
		if(empty($ziyuan)){
			throw new \think\Exception('资源不存在');
		}

		// 计算用户收入
		$money = round($order['price'] - $order['price'] * config('common.user.rate'), 2);

		$wallet_log = [
			'user_id' => $order['sell_user_id'],
			'project' => 'order',
			'project_id' => $order['order_id'],
			'money' => $money,
			'type' => '+',
			'source' => $order['ziyuan_title'],
		];

		if($ziyuan['is_kami'] == 1){
			$kami = $this->kamiConsume($ziyuan['kami']);
		}

		$walletModel = new UserWallet();
		$logModel = new UserWalletLog();

		$this->model->startTrans();  // 开启事务
		try {
			// 添加用户余额
			$walletModel->increase($order['sell_user_id'], 'current_money', $money);
			// 创建钱包记录
			$logModel->insertLog($wallet_log);
			// 修改订单状态
			$this->model->where(['order_id'=>$order['order_id'],'status'=>0])->save(['status'=>1, 'pay_type'=>$pay_type, 'pay_time'=>date('Y-m-d H:i:s'),'download_url'=>$ziyuan['download_url']]);
			if(isset($kami)){	// 卡密消费
				$this->ziyuan->editZiayuan($ziyuan['user_id'], $order['ziyuan_id'], ['kami'=>$kami['kami']]);
			}
			$this->model->commit(); // 提交事务
			return true;
		} catch (\think\Exception $e) {
			$this->model->rollback(); // 事务回滚
			return false;
		}
	}

	/**
	 * 获取订单详情
	 * @Author   cendxia
	 * @DateTime 2022-11-04T21:52:46+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $user_id  [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderInfo($order_id, $user_id){
		$result = $this->model->getOrderInfo($order_id, $user_id);
		if(empty($result)) return [];
		if($result['status'] != 1){
			unset($result['download_url']);
		}
		return $result;
	}

	/**
	 * 获取资源/课程订单详情，支付宝支付回调时调用
	 * @Author   cendxia
	 * @DateTime 2023-04-20T11:58:47+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $user_id  [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderDetails($order_id, $user_id){
		if(empty($order_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		$result = $this->getOrderInfo($order_id, $user_id);
		if(empty($result)){
			// 课程订单
			$keBus = new \app\api\business\Kecheng;
			$result = $keBus->getOrderInfo($order_id, $user_id);
			if(empty($result)) return [];
			$result['order_type'] = 'kecheng';
		}else{
			$result['order_type'] = 'ziyuan';
			$this->ziyuanDownload($user_id, $order_id);	// 创建下载记录，有逻辑bug
		}
		return $result;
	}


	/**
	 * 查询订单状态
	 * @Author   cendxia
	 * @DateTime 2022-11-30T19:32:21+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $user_id  [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderStatus($order_id, $user_id){
		try {
			return $this->model->where(['order_id'=>$order_id,'user_id'=>$user_id])->value('status');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 关闭订单
	 * @Author   cendxia
	 * @DateTime 2024-02-27T11:47:44+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function closeOrder($user_id, $order_id){
		return $this->model->closeOrder($user_id, $order_id);
	}

}