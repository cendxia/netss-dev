<?php 

namespace app\api\business;
use app\common\lib\Sms;
use app\common\lib\Email;
use app\common\lib\Str;
use app\api\validate\User as userValidate;
use app\api\model\SmsCode as smsLogModel;
use think\facade\Request;

/**
 * 
 */
class Common
{
	
	public function __construct()
	{
		// code...
	}

	/**
	 * 发送验证码
	 * @Author   cendxia
	 * @DateTime 2022-09-30T16:49:41+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public static function smsSend($data){
		// 使用验证器
		try {
            validate(userValidate::class)->scene('sms_send')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }

        if(array_search($data['type'], config('common.sms.type')) === false){
        	throw new \think\Exception(config('language.sms_error'));
        }

        $userModel = new \app\api\model\Users;
        $user = $userModel->getUserByPhone($data['phone']);

        // 判断手机号是否存在
        if($data['type'] == 'login' || $data['type'] == 'findPwd'){
        	if(empty($user) || $user['state'] == -1){
        		throw new \think\Exception('手机号不存在');
        	}
        }

        // 注册判断
        if($data['type'] == 'register'){
            if($user && $user['state'] != -1){
                throw new \think\Exception('手机号已存在');
            }
        }

        // 修改手机号/修改密码
        if($data['type'] == 'editPhone' || $data['type'] == 'editPwd'){
            // 获取当前登录用户信息
            $token = Request::header('token');
            $user = cache(config('redis.token_pre').$token);
            if(empty($user)){
                throw new \think\Exception(config('language.sms_error'));
            }
            $userInfo = $userModel->getByUserId($user['user_id']);
            // 判断手机号是否一致
            if($data['phone'] != $userInfo['phone']){
                throw new \think\Exception('手机号错误');
            }
        }

        // 前缀
        $prefix = smsPrefix($data['type']);

        // 判断验证码是否存在
        $smsCode = cache($prefix.$data['phone'].$data['type']);
        if($smsCode){
            // throw new \think\Exception('验证码发送过于频繁');
        }

        $config = config('common.sms');
        $lib = new Sms($config[$config['provider']]);
        if($config['provider'] == 'wangyi'){ // 网易
            $result = $lib->wangyi($data['type'], $data['phone']);
        }else{ // 阿里
            $result = $lib->aliSms($data['type'], $data['phone']);
        }
        
        $logModel = new smsLogModel();

        if(empty($result)){
            // 写入日志
            $log = [
                'phone' => $data['phone'],
                'code' => '',
                'type' => $data['type'],
                'status' => 0
            ];
            $logModel->add($log);
        	throw new \think\Exception(config('language.sms_error'));
        }


        // 写入日志
        $log = [
            'phone' => $data['phone'],
            'code' => $result,
            'type' => $data['type'],
            'status' => 1
        ];
        $logModel->add($log);

        // 存储短信验证码
        $res = cache($prefix.$data['phone'].$data['type'], $result, config('common.sms.sms_code_time'));
        return $res ? true : false;
	}

    /**
     * 发送邮件
     * @Author   cendxia
     * @DateTime 2022-10-14T19:30:39+0800
     * @param    [type]                   $data [description]
     * @return   [type]                         [description]
     */
    public static function emailSend($data){
        // 使用验证器
        try {
            validate(userValidate::class)->scene('email_send')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }
        $title = isset($data['title']) ? $data['title'] : '资交网';
        $content = isset($data['content']) ? $data['content'] : '这里是内容';
        $user_name = isset($data['user_name']) ? $data['user_name'] : '资交网用户';
        $code = ''; // 验证码

        if($data['email_type'] == 'code'){
            // 获取当前登录用户信息
            $token = Request::header('token');
            $user = cache(config('redis.token_pre').$token);
            if(empty($user)){
                throw new \think\Exception(config('language.email_error'));
            }
            $userModel = new \app\api\model\Users;
            $userInfo = $userModel->getByUserId($user['user_id']);
            $user_name = $userInfo['nickname'];
            $code = Str::getRandStr(rand(4, 6));
            $content = "尊敬的".'"'.$user_name.'"用户'."，验证码为：{$code}";
        }

        // 修改邮箱号
        if($data['email_type'] == 'editEmail'){
            // 获取当前登录用户信息
            $token = Request::header('token');
            $user = cache(config('redis.token_pre').$token);
            if(empty($user)){
                throw new \think\Exception(config('language.email_error'));
            }
            $userModel = new \app\api\model\Users;
            $userInfo = $userModel->getByUserId($user['user_id']);
            // 判断手机号是否一致
            if($data['email'] != $userInfo['email']){
                throw new \think\Exception('邮箱号错误');
            }
            $title = '【资交网】修改邮箱通知';
            $user_name = $userInfo['nickname'];
            $code = Str::getRandStr(rand(4, 6));
            $content = "尊敬的".'"'.$user_name.'"用户'."，您正在修改邮箱号。验证码为：{$code}";
        }

        $res = (new Email)->sendCodeToEmail($data['email'], $title, $content, $user_name);
        if(empty($res)) return false;
        if(empty($code)) return true;

        // 存储验证码
        $res = cache($data['email'].$data['email_type'], $code, config('common.email.email_time'));
        return $res ? true : false;
    }


    /**
     * 获取系统配置信息
     * @Author   cendxia
     * @DateTime 2022-10-26T21:24:02+0800
     * @return   [type]                   [description]
     */
    public function getSystemConfig(){

        // throw new \think\Exception('错误');
        

        $model = new \app\api\model\SystemConfig;
        $result = $model->getConfig();
        $data = array();
        foreach($result as $key => $value){
            $data[$value['field']] = $value;
        }

        if(!empty($_SERVER['HTTP_X_HOST'])){
            $xhost = explode(':', $_SERVER['HTTP_X_HOST']);
            if(!empty($xhost[0]) && $xhost[0] == 'www.zijiao.cn'){
                $data['icp']['value'] = '黔ICP备15010373号-6';
            }
        }

        return $data;
    }

    /**
     * 平台数据统计，平台流量、资源总数、下载次数
     * @Author   cendxia
     * @DateTime 2023-11-28T14:21:48+0800
     * @return   [type]                   [description]
     */
    public function getIndexTongji(){        
        $ziyuanModel = new \app\api\model\Ziyuan();
        $directoryModel = new \app\api\model\KechengDirectory();

        // 获取平台流量（资源总浏览量）
        $read_num = $ziyuanModel->sum('read_num');
        // 下载数
        $download_num = $ziyuanModel->sum('download_num');

        // 资源总数
        $ziyuan_count = $ziyuanModel->getCount(['status'=>1]);
        // 课程总数
        $kecheng_count = $directoryModel->count();

        $count = $ziyuan_count + $kecheng_count;

        return [
            'read_num' => Str::numberUnit($read_num),
            'count' => Str::numberUnit($count),
            'download_num' => Str::numberUnit($download_num),
        ];
    }


    

}