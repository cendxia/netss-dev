<?php 

namespace app\api\business;

use app\api\model\Article as articleModel;
use app\api\model\ArticleCategory;

/**
 * 
 */
class Article
{

	private $model;
	private $category;


	public function __construct(){
		$this->model = new articleModel();
		$this->category = new ArticleCategory();
	}


	/**
	 * 获取文章栏目
	 * @Author   cendxia
	 * @DateTime 2023-11-29T13:17:49+0800
	 * @return   [type]                   [description]
	 */
	public function getArticleColumn(){
		return $this->category->getList();
	}


	/**
	 * 搜索文章
	 * @Author   cendxia
	 * @DateTime 2023-11-29T13:46:45+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    [type]                   $keyWord  [description]
	 * @return   [type]                             [description]
	 */
	public function getArticleSearch($page, $pageSize, $keyWord){
		if(empty($keyWord)){
			throw new \think\Exception('请输入关键词');
		}

		try {
			$result = $this->model->where(['show'=>1])->where('title|keyword|describe', 'like', "%{$keyWord}%")->order('update_time','DESC')->page($page, $pageSize)->field('id,category_id,title,describe,read_num,read_num,update_time')->select()->toArray();
			$count = $this->model->where(['show'=>1])->where('title|keyword|describe', 'like', "%{$keyWord}%")->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 获取文章详情
	 * @Author   cendxia
	 * @DateTime 2022-10-20T21:59:46+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getArticleInfo($id){
		if(empty($id)){
			throw new \think\Exception(config('language.param_error'));
		}
		$result = $this->model->getById($id);
		if(empty($result) || $result['show'] != 1){
			throw new \think\Exception('内容不存在或审核未通过');
		}
		$result['content'] = htmlspecialchars_decode($result['content']);
		$category = $this->category->getById($result['category_id']);
		if(!empty($category)){
			$result['category_name'] = $category['name'];
		}
		return $result;
	}

	/**
	 * 获取栏目文章
	 * @Author   cendxia
	 * @DateTime 2022-10-20T22:30:11+0800
	 * @param    [type]                   $category_id [description]
	 * @param    [type]                   $page        [description]
	 * @param    [type]                   $pageSize    [description]
	 * @return   [type]                                [description]
	 */
	public function getColumnArticleList($category_id, $page, $pageSize){
		if(empty($category_id)){
			throw new \think\Exception(config('language.param_error'));
		}

		$where = ['category_id' => $category_id];
		$result = $this->model->getList($page, $pageSize, $where);
		$count = $this->model->getCount($where);

		foreach($result as $key => $val){
			$year = date('Y', strtotime($val['create_time']));
			if($year == date('Y')){
				$result[$key]['date'] = date('m-d', strtotime($val['create_time']));
			}else{
				$result[$key]['date'] = date('Y-m', strtotime($val['create_time']));
			}
		}

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

}