<?php 

namespace app\api\business;


use app\api\business\Users;
use app\api\model\Vip as vipModel;
use app\api\model\UserVip;
use app\common\lib\Str;

/**
 * 
 */
class Vip
{

	private $model = null;

	public function __construct(){
		$this->model = new vipModel;
	}


	/**
	 * 获取VIP列表
	 * @Author   cendxia
	 * @DateTime 2024-03-22T18:36:47+0800
	 * @return   [type]                   [description]
	 */
	public function getVipList(){
		return $this->model->getList();
	}


	
	/**
	 * 提交VIP订单
	 * @Author   cendxia
	 * @DateTime 2024-03-22T12:02:30+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function submitVipOrder($id, $user_id){
		$result = $this->model->getVipById($id);
		if(empty($result) || $result['status'] != 1){
			throw new \think\Exception(config('language.param_error'));
		}
		$userVipModel = new UserVip();
		$userVip = $userVipModel->getUnpaidOrder($id, $user_id);
		if(isset($userVip)) return $userVip['order_id'];

		// 生成订单号
		$order_id = Str::getOrderId();
		$order_id = date('Ymd').$order_id;
		$data = [
			'vip_id' => $id,
			'order_id' => $order_id,
			'user_id' => $user_id,
			'title' => $result['title'],
			'category_id' => $result['category_id'],
			'type' => $result['type'],
			'price' => $result['price'],
			'pay_status' => 0,
			'status' => 0
		];

		if($userVipModel->add($data)){
			return $order_id;
		}
	}


	/**
	 * 获取订单信息
	 * @Author   cendxia
	 * @DateTime 2024-03-22T20:55:04+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $order_id [description]
	 * @return   [type]                             [description]
	 */
	public function getOrder($user_id, $order_id){
		$model = new UserVip();
		return $model->getOrderByUserOrderId($user_id, $order_id);
	}



	/**
	 * 处理支付回调订单状态
	 * @Author   cendxia
	 * @DateTime 2024-03-22T21:19:56+0800
	 * @param    [type]                   $data     [description]
	 * @param    string                   $pay_type [description]
	 * @return   [type]                             [description]
	 */
	public function payCallback($data, $pay_type=''){
		if(empty($pay_type)){
			throw new \think\Exception('language.param_error');
		}

		if($pay_type == 'hpjwxpay'){
			$order_id = $data['trade_order_id'];
		}else{
			$order_id = $data['out_trade_no'];
		}

		// 查询订单信息
		$model = new UserVip();
		$order = $model->getOrderInfo($order_id);
		if(empty($order)){
			throw new \think\Exception('订单不存在');
		}

		// 判断订单金额是否一致，hpjwxpay为虎皮椒微信支付
		if($pay_type == 'wxpay'){
			$total_fee = $data['total_fee'] / 100;
		}else if($pay_type == 'alipay'){
			$total_fee = $data['total_amount'];
		}else if($pay_type == 'hpjwxpay'){
			$total_fee = $data['total_fee'];
		}else{
			throw new \think\Exception('支付类型错误');
		}
		
		if($total_fee != $order['price'] || $order['pay_status'] != 0){
			throw new \think\Exception('订单信息不匹配');
		}

		// 获取1年后的日期
		$end_time = date('Y', time()) + 1 . '-' . date('m-d 23:59:59');
		$upDate = [
			'pay_status' => 1,
			'pay_type' => $pay_type,
			'pay_time' => date('Y-m-d H:i:s'),
			'end_time' => $end_time
		];

		// 修改订单状态
		return $model->where(['order_id'=>$order['order_id'],'pay_status'=>0])->save($upDate);	
	}


	/**
	 * 查询订单状态
	 * @Author   cendxia
	 * @DateTime 2024-03-23T17:06:04+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $user_id  [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderStatus($order_id, $user_id){
		$model = new UserVip();
		$result = $model->getOrderStatus($order_id, $user_id);
		if(!isset($result)) throw new \think\Exception('订单不存在');
		return $result;
	}


	/**
	 * 查询我的VIP记录
	 * @Author   cendxia
	 * @DateTime 2024-03-23T18:22:15+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    [type]                   $user_id  [description]
	 * @return   [type]                             [description]
	 */
	public function getUserVipList($page, $pageSize, $user_id){
		$model = new UserVip();
		$result = $model->getUserVipList($page, $pageSize, $user_id, ['pay_status'=>1]);
		$count = $model->getUserVipCount($user_id, ['pay_status'=>1]);
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 获取我的VIP信息
	 * @Author   cendxia
	 * @DateTime 2024-03-24T10:08:30+0800
	 * @param    [type]                   $user_id     [description]
	 * @param    string                   $type        [description]
	 * @param    string                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getUserVip($user_id, $type = 'ziyuan', $category_id=''){
		if($type == 'ziyuan' && empty($category_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		$where = array();
		$where[] = [
			['type', '=', $type]
		];
		$model = new UserVip();
		if($type == 'ziyuan'){
			// 判断是否有全站会员
			$where[] = [
				['category_id', '=', 'all']
			];
			$result = $model->getUserVip($user_id, $where);
			// 判断是否有其他类别VIP
			if(empty($result)){
				$ziyuanBus = new \app\api\business\Ziyuan();
				$category = $ziyuanBus->getParentCategoryId($category_id);
				unset($where[1]);
				$where[] = [
					['category_id', '=', $category['id']]
				];
				$result = $model->getUserVip($user_id, $where);
				if (empty($result)) return false;
				return true;
			}
			return true;
		}
		// 课程VIP
		$result = $model->getUserVip($user_id, $where);
		if (empty($result)) return false;
		return true;
	}
}