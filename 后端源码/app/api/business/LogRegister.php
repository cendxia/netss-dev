<?php 

namespace app\api\business;

use app\api\model\Users;
use app\api\model\UserWallet;
use app\api\validate\User as userValidate;
use app\common\lib\Utils;
use app\common\lib\Str;
use think\facade\Cache;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * 用户登录与注册
 */
class LogRegister
{
	
	private $model = null;

	public function __construct(){
		$this->model = new Users;
	}


	/**
	 * 用户注册
	 * @Author   cendxia
	 * @DateTime 2022-10-11T13:46:49+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function register($data){
		// 使用验证器
		try {
            validate(userValidate::class)->scene('register')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }

        // 判断验证码是否正确
        // 前缀
        $prefix = smsPrefix('register');

        // 判断验证码是否存在
        $smsCode = cache($prefix.$data['phone'].'register');
        if(empty($smsCode) || $smsCode != $data['sms_code']){
            throw new \think\Exception('验证码错误');
        }

        // 判断用户名是否存在
        $user = $this->model->getUserByLoginName($data['login_name']);
        if($user){
        	throw new \think\Exception('用户名已存在');
        }

        // 判断手机号是否存在
        $user = $this->model->getUserByPhone($data['phone']);
        if($user){
        	throw new \think\Exception('手机号已存在');
        }

        // 生成随机字符串
		$salt = Str::getRandStr(rand(15,25));
		// 密码
		$password = (new utils())->userEncryption(md5($data['password']), $salt);

        $userData = [
        	'login_name' => $data['login_name'],
        	'nickname' => '资交网'.substr($data['phone'], 7, 11), // 昵称
        	'phone' => $data['phone'],
        	'password' => $password,
        	'avatar' => 'https://zjw.data.zijiao.cn/avatar/avatar_'.rand(1,12).'.png',
        	'salt' => $salt
        ];

        $this->model->startTrans();  // 开启事务
		try {
			$user_id = $this->model->insertGetId($userData);
			(new UserWallet())->addWallet(['user_id'=>$user_id]);
			$this->model->commit(); // 提交事务
			return true;
		} catch (\think\Exception $e) {
			$this->model->rollback(); // 事务回滚
			return false;
		}
	}


	/**
	 * 登录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T12:44:37+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function login($data){
		// $data['password'] = md5($data['password']); // 临时

		// 使用验证器
		try {
	    	validate(userValidate::class)->scene('login')->check($data);
	    } catch (ValidateException $e) {
	    	throw new \think\Exception($e->getError());
	    }

	    $user = $this->model->getUserByLoginnamePhone($data['login_name']);
	    if(empty($user)){
	    	throw new \think\Exception('用户名或密码错误');
	    }

	    $password = (new utils())->userEncryption($data['password'], $user['salt']);
	    if($password != $user['password']){
	    	throw new \think\Exception('用户名或密码错误');
	    }


	    // 获取公共缓存前缀
        $prefix = config('common.common_prefix');


	    $redisData = [
            'user_id' => $user['user_id'],
            'nickname' => $user['nickname'],
            'username' => $user['user_name'],
            'avatar' => $user['avatar'],
            'phone' => $user['phone'],
            'type' => $user['type'],
            'token_time' => time() + config('common.user_login_expires_time') // token有效时间
        ];

	    // 获取token
        $token = JwT::encode($redisData, $prefix.'login_lc_', 'HS256'); //encode加码

        $decoded = JwT::decode($token, new Key($prefix.'login_lc_', 'HS256')); //decode解码

        $redisData['token'] = md5($token);

        // 判断是否登录过，如已登录，退出之前登录
        $header_token = cache($prefix.'login_lc_'.$user['user_id']);
        // 清除缓存
        if($header_token){
        	// 清除登录token
        	Cache::delete(config('redis.token_pre').$header_token);
        	// 清除单点登录token
        	Cache::delete($prefix.'login_lc_'.$user['user_id']);

        	// // 清除登录token
        	// cache(config('redis.token_pre')..$header_token, false);
        	// // 清除单点登录token
        	// cache($prefix.'login_lc_'.$user['user_id'], false);
        }

        $login_log = [
        	'user_id' => $user['user_id'],
        	'ip' => getUserIp(),
        	'mode' => '密码登录',
        	'platform' => 'web'
        ];

        (new \app\api\model\UserLogin())->add($login_log);


        // 单点登录使用
        cache($prefix.'login_lc_'.$user['user_id'], $redisData['token'], config('common.user_login_expires_time'));
        // 存储登录token
        $res = cache(config('redis.token_pre').$redisData['token'], $token, config('common.user_login_expires_time'));
        return $res?$redisData:false;
	}

	/**
	 * 退出登录
	 * @Author   cendxia
	 * @DateTime 2022-10-10T21:15:18+0800
	 * @param    [type]                   $token [description]
	 * @return   [type]                          [description]
	 */
	public function loginOut($token){
		$jwtToken = cache(config('redis.token_pre').$token);
		if(empty($jwtToken)) return false;
		// 获取公共缓存前缀
        $prefix = config('common.common_prefix');
		$user = JwT::decode($jwtToken, new Key($prefix.'login_lc_', 'HS256'));
		if(empty($user)) return false;
		// 清除登录token
    	Cache::delete(config('redis.token_pre').$token);
    	// 清除单点登录token
    	Cache::delete($prefix.'login_lc_'.$user->user_id);
		return true;
	}
}