<?php 

namespace app\api\business;

use app\api\model\Users;
use app\api\model\UserTixian;
use app\api\model\UserAccount;
use app\api\model\UserWallet;
use app\api\model\UserWalletLog;
use app\api\validate\User as userValidate;

/**
 * 
 */
class MpUser
{

	private $tixian;
	private $userModel;
	
	public function __construct()
	{
		$this->tixian = new UserTixian();
		$this->userModel = new Users();
	}


	/**
	 * 用户提现
	 * @Author   cendxia
	 * @DateTime 2024-01-17T12:36:44+0800
	 * @param    [type]                   $user_id     [description]
	 * @param    [type]                   $money       [description]
	 * @param    [type]                   $accountType [description]
	 * @return   [type]                                [description]
	 */
	public function tixian($user_id, $money, $accountType){
		if(empty($accountType) || !in_array($accountType, config('common.account_types'))){
			throw new \think\Exception('请选择收款账号');
		}

		$min_tixian = config('common.wallet.min_tixian');
		if(empty($money) || $money < $min_tixian){
			throw new \think\Exception('提现金额必须大于等于'.$min_tixian.'元的整数');
		}

		// 获取收款账号
		$accountModel = new UserAccount();
		$account = $accountModel->getByAccountType($user_id, $accountType);

		$walletModel = new UserWallet();
		$wallet = $walletModel->getByUserId($user_id);

		// 判断金额是否正确
		if($money > $wallet['current_money']){
			throw new \think\Exception('余额不足');
		}

		// 提现记录
		$logData = [
			'user_id' => $user_id,
			'money' => $money,
			'account_name' => $account['account_name'], // 账号
			'account_holder' => $account['account_holder'], // 姓名
			'opening_bank' => $account['opening_bank'], // 开户行
			'fee' => 0, // 手续费
			'type' => $accountType // alipay/wxpay/bank
		];

		$walletLog = [
			'user_id' => $user_id,
			'project' => 'user_tixian',
			'money' => $money,
			'after' => $wallet['current_money'] - $money, // 更新后余额
			'type' => '-',
			'source' => '提现',
		];
		$walletLogModel = new UserWalletLog();
		$this->tixian->startTrans(); // 开启事务
		try {
			// 添加记录
			$id = $this->tixian->add($logData);
			// 扣费
			$walletModel->reduce($user_id, 'current_money', $money);
			// 创建记录
			$walletLog['project_id'] = $id;
			$walletLogModel->insertLog($walletLog);
			$this->tixian->commit(); // 提交事务
			return true;
		} catch (\think\Exception $e) {
			$this->tixian->rollback(); // 事务回滚
			return false;
		}
	}

	/**
	 * 提现记录
	 * @Author   cendxia
	 * @DateTime 2022-08-29T23:19:56+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getTixianLog($user_id, $page, $pageSize){
		$result = $this->tixian->getList($user_id, $page, $pageSize);
		$count = $this->tixian->getCount($user_id);

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 申请创作者
	 * @Author   cendxia
	 * @DateTime 2022-10-10T15:58:06+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function registerMp($user_id, $data){
		// 使用验证器
		try {
	    	validate(userValidate::class)->scene('registerMp')->check($data);
	    } catch (ValidateException $e) {
	    	throw new \think\Exception($e->getError());
	    }

		$user = $this->userModel->getByUserId($user_id);
		if($user['state'] == 1){
			throw new \think\Exception('当前用户已被禁用');
		}

		if($user['type'] != 1){
			throw new \think\Exception('当前不可申请创作者');
		}

		// 判断身份证号是否存在
		$res = $this->userModel->getByIdCard($data['id_card']);
		if($res){
			throw new \think\Exception('身份证号已被使用');
		}

		$logData = [
			'user_id' => $user_id,
			'id_card' => $data['id_card'],
			'email' => $data['email'],
			'qq' => $data['qq']
		];

		// 判断手机号是否一致
		if(!empty($user['phone']) && $data['phone'] != $user['phone']){
			throw new \think\Exception('手机号与注册时的手机号不一致');
		}

		// 判断验证码是否正确
		// 前缀
        $prefix = smsPrefix('common');

        // 判断验证码是否存在
        $smsCode = cache($prefix.$data['phone'].'common');
        if(empty($smsCode) || $smsCode != $data['sms_code']){
            throw new \think\Exception('验证码错误');
        }

        $upData = [
        	'type' => 3,
        	'user_name' => $data['user_name'],
        	'id_card' => $data['id_card'],
        	'email' => $data['email']
        ];

		$logModel = new \app\api\model\UserRegisterMp;
		$this->userModel->startTrans(); // 开启事务
		try {
			// 添加记录
			$logModel->add($logData);
			// 修改用户状态
			$this->userModel->where(['user_id'=>$user_id,'type'=>1])->save($upData);
			$this->userModel->commit(); // 提交事务
			cache($prefix.$data['phone'].'common', false);
			return true;
		} catch (\think\Exception $e) {
			$this->userModel->rollback(); // 事务回滚
			return false;
		}
	}


	/**
	 * 获取申请创作者状态
	 * @Author   cendxia
	 * @DateTime 2022-10-12T13:29:37+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getMpStatus($user_id){
		
        $userModel = new \app\api\model\Users;
        $user = $userModel->getByUserId($user_id);

        if($user['state'] == 1){
        	throw new \think\Exception('该账号已被禁用');
        }
		if($user['type'] == 3){
			return true;
		}else if($user['type'] == 2){
			throw new \think\Exception('已是创作者', 5);
		}else if($user['type'] == 1){
			return false;
		}else{
			throw new \think\Exception('无权限访问', 3);
		}
	}

	/**
	 * 获取收款账户列表信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T14:15:26+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getUserAccount($user_id){
		$model = new \app\api\model\UserAccount();
		return $model->getList($user_id);
	}

	/**
	 * 添加收款信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T14:45:25+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 */
	public function setUserAccount($user_id, $data){
		if(empty($data['account_type']) || array_search($data['account_type'], config('common.account_types')) === false){
			throw new \think\Exception(config('language.param_error'));
		}

		if($data['account_type'] == 'bank'){
			// 使用验证器
			try {
		    	validate(userValidate::class)->scene('userAccount')->check($data);
		    } catch (ValidateException $e) {
		    	throw new \think\Exception($e->getError());
		    }
		}

		$model = new \app\api\model\UserAccount();
		// 判断收款信息是否存在
		$res = $model->getByAccountType($user_id, $data['account_type']);
		if(empty($res)){
			// 添加
			$data['user_id'] = $user_id;
			return $model->add($data);
		}
		// 修改
		return $model->edit($user_id, $res['id'], $data);
	}


	/**
	 * 删除收款信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T17:39:33+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $id      [description]
	 * @return   [type]                            [description]
	 */
	public function delUserAccount($user_id, $id){
		return true;
		$model = new \app\api\model\UserAccount();
		return $model->del($user_id, $id);
	}


	/**
	 * 获取用户最新状态
	 * @Author   cendxia
	 * @DateTime 2022-11-12T00:41:22+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $token   [description]
	 * @return   [type]                            [description]
	 */
	public function upUserStatus($user_id, $token){
		$userModel = new \app\api\model\Users;
        $user = $userModel->getByUserId($user_id);
        $cache = cache(config('redis.token_pre').$token);
        $redisData = [
        	'token' => $token,
            'user_id' => $user['user_id'],
            'nickname' => $user['nickname'],
            'username' => $user['user_name'],
            'avatar' => $user['avatar'],
            'phone' => $user['phone'],
            'type' => $user['type'],
            'token_time' => $cache['token_time']
        ];
        $res = cache(config('redis.token_pre').$token, $redisData, config('common.user_login_expires_time'));

        return $res?$redisData:false;
	}
}