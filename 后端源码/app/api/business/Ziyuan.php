<?php 

namespace app\api\business;

use app\api\model\ZiyuanCategory;
use app\api\model\Ziyuan as ziyuanModel;
use app\api\model\ZiyuanCollect;
use app\api\model\ZiyuanRead;
use app\api\model\Users;
use think\facade\Cache;
use think\facade\Db;

/**
 * 
 */
class Ziyuan
{

	private $model = null;
	private $category = null;
	private $read = null;
	
	public function __construct()
	{
		$this->category = new ZiyuanCategory();
		$this->model = new ziyuanModel();
		$this->read = new ZiyuanRead();
	}

	/**
	 * 随机获取资源列表，在首页使用
	 * @Author   cendxia
	 * @DateTime 2023-11-28T13:27:46+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getRandomList($page, $pageSize){
		$where['status'] = 1;
		$field = 'id,title,cover,price,vip_dis,user_id';
		try {
			$result = $this->model->getList($page, $pageSize, $where, true, $field);
			$count = $this->model->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		$userModel = new Users();
		foreach($result as $key => $value){
			$user = $userModel->getSellUserBasics($value['user_id']);
			$result[$key]['nickname'] = $user['nickname'];
			$result[$key]['cover'] = unserialize($value['cover']);
		}

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 获取浏览量最高的资源列表
	 * @Author   cendxia
	 * @DateTime 2023-11-28T14:04:03+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getBrowseList($page, $pageSize){
		$field = 'id,title,cover,price,vip_dis,user_id,read_num';
		$key = 'Adobe';
		try {
			$result = $this->model->where(['status'=>1])->where('title|label', 'like', "%{$key}%")->orderRaw('rand()')->page($page, $pageSize)->field($field)->select()->toArray();
			$count = $this->model->where(['status'=>1])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		$userModel = new Users();
		foreach($result as $key => $value){
			$user = $userModel->getSellUserBasics($value['user_id']);
			$result[$key]['nickname'] = $user['nickname'];
			$result[$key]['cover'] = unserialize($value['cover']);
		}
		
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 获取资源列表
	 * @Author   cendxia
	 * @DateTime 2024-02-18T13:08:34+0800
	 * @param    [type]                   $page        [description]
	 * @param    [type]                   $pageSize    [description]
	 * @param    string                   $user_id     [description]
	 * @param    string                   $category_id [description]
	 * @param    string                   $all         [description]
	 * @param    boolean                  $paging      [description]
	 * @param    boolean                  $rand        [description]
	 * @return   [type]                                [description]
	 */
	public function getList($page, $pageSize, $user_id='', $category_id='', $all='', $paging=true, $rand=false){
		$where = array();
		if(!empty($user_id)){
			$where['user_id'] = $user_id;
		}else{
			$where['self_visible'] = 0;
		}

		if(!empty($all) && !empty($category_id)){
			// 是否查询所有子分类数据
			$categorys = $this->category->getByIdAll($category_id);
			$ids = [$category_id];
			foreach($categorys as $key => $value){
				array_push($ids, $value['id']);
			}
			$where['category_id'] = $ids;
		}else if(!empty($category_id)){
			$where['category_id'] = $category_id;
		}

		$where['status'] = 1;
		try {
			$result = $this->model->getList($page, $pageSize, $where, $rand);
			$count = $this->model->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			$result[$key]['cover'] = unserialize($value['cover']);
			$category = $this->category->getById($value['category_id']);
			if(empty($category)){
				$result[$key]['category'] = '';
			}else{
				$result[$key]['category'] = $category['title'];
			}
			if($value['is_kami'] == 1){
				$result[$key]['title'] = '[卡密]'.$value['title'];
			}
		}
		if($paging){
			// 计算总页数
			$pageSum = intval(ceil($count / $pageSize));
			return dataPage($page, $pageSize, $pageSum, $count, $result);
		}else{
			return $result;
		}
		
	}

	/**
	 * 资源详情
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:08:02+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getInfo($user_id, $id, $info=false){
		$result = $this->ziyuanInfo($id);
		if(empty($result)){
			throw new \think\Exception('当前资源不存在');
		}
		if($result['status'] != 1){
			throw new \think\Exception('当前资源不存在或未通过审核');
		}

		// 判断是否是仅自己可见
		if($result['self_visible'] == 1 && $result['user_id'] != $user_id){
			throw new \think\Exception('当前资源不存在或未通过审核');
		}

		if($info){
			// 获取当前所属分类
			$categoryData = $this->category->field('id,pid,title')->find($result['category_id']);
			if(!empty($categoryData) && $categoryData['pid'] > 0){
				$categoryData1 = $this->category->field('id,pid,title')->find($categoryData['pid']);
				$result['category'][] = $categoryData1;
			}
			if(!empty($categoryData)){
				$result['category'][] = $categoryData;
			}

			$seoCategoryTitle = '';
			foreach($result['category'] as $key => $value){
				if(empty($seoCategoryTitle)){
					$seoCategoryTitle .= $value['title'];
				}else{
					$seoCategoryTitle = $seoCategoryTitle.'_'.$value['title'];
				}
			}
			$result['seoCategoryTitle'] = $seoCategoryTitle;

			// 判断是否已购买该资源
			if(empty($user_id)){
				$result['is_buy'] = false;
			}else{
				$orderModel = new \app\api\model\Order;
				$order = $orderModel->getByZiyuanId($user_id, $id, ['status'=>1]);

				if(empty($order) || $order['status'] != 1){
					$result['is_buy'] = false;
				}else{
					$result['is_buy'] = true;
					$result['order_id'] = $order['order_id'];
				}
			}

			// 判断是否已收藏
			if(empty($user_id)){
				$result['is_collect'] = false;
			}else{
				$collectModel = new ZiyuanCollect();
				$collect = $collectModel->getCollectFind($user_id, $id);
				$result['is_collect'] = empty($collect) ? false : true;
			}

			$user = (new Users())->getSellUserBasics($result['user_id']);
			if(empty($user)){
				throw new \think\Exception('当前资源不存在或未通过审核');
			}

			$result['user_name'] = $user['nickname'];
			$result['qq'] = $user['qq'];
		}

		// 判断下载地址是否为百度网盘
		$result['paiduPan'] = false;
		if($result['download_url'] && strpos($result['download_url'], 'https://pan.baidu.com/') !== false){
			$result['paiduPan'] = true;
		}

		unset($result['download_url']);	
		$result['content'] = htmlspecialchars_decode($result['content']);

		$result['create_time'] = date('Y-m-d', strtotime($result['create_time']));
		if(!empty($result['ziyuan_update_time'])){
			$result['ziyuan_update_time'] = date('Y-m-d', strtotime($result['ziyuan_update_time']));
		}

		$vipBus = new \app\api\business\Vip();
		$vip = $vipBus->getUserVip($user_id, 'ziyuan', $result['category_id']);
		if($vip){
			$result['price'] = $result['price'] * $result['vip_dis'];
		}


		$result['labels'] = array();
		if(!empty($result['label'])){
			$labels = array();
			$res = stripos($result['label'], ',');
			if($res !== false){
				$labels = explode(',', $result['label']);
			}else if(stripos($result['label'], '，') !== false){
				$labels = explode('，', $result['label']);
			}
			$result['labels'] = $labels;
			unset($result['label']);
		}

		

		return $result;
	}

	/**
	 * 资源详情
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:12:06+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	private function ziyuanInfo($id){
		try {
			$result = $this->model->find($id);
			$this->model->where(['id'=>$id])->inc('read_num')->update(); // 更新阅读量
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		$result = $result->toArray();
		$result['cover'] = unserialize($result['cover']);
		$result['content'] = htmlspecialchars_decode($result['content']);
		return $result;
	}

	/**
	 * 获取分类
	 * @Author   cendxia
	 * @DateTime 2022-07-27T15:53:13+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys($subclass = false, $auto = false){
		$prefix = config('common.common_prefix');
		if($subclass){
			if($auto){
				$data = false;
			}else{
				$data = Cache($prefix.'categorySubclass');
			}
			if(empty($data)){
				$result = $this->category->getList(['pid'=>0]);
				foreach($result as $key => $value){
					$res = $this->category->getList(['pid'=>$value['id']]);
					$result[$key]['subclass'] = $res;
				}
				Cache($prefix.'categorySubclass', $result, 32400);
				return $result;
			}
			return $data;
		}else{
			if($auto){
				$res = false;
			}else{
				$res = Cache($prefix.'categorys');
			}
			if(empty($res)){
				$result = $this->category->getList(['pid'=>0]);
				foreach($result as $key => $value){
					$result[$key]['subclass'] = $this->category->getList(['pid'=>$value['id']]);
				}
				$data = array();
				foreach($result as $key => $value){
					$res = [
						'id' => $value['id'],
						'pid' => $value['pid'],
						'title' => $value['title'],
						'icon' => $value['icon'],
						'subclass' => false
					];
					array_push($data, $res);
					foreach($value['subclass'] as $k => $val){
						$val['subclass'] = true;
						array_push($data, $val);
					}
				}
				Cache($prefix.'categorys', $data, 32400);
				return $data;
			}
			return $res;
		}
	}


	/**
	 * 获取分类详情
	 * @Author   cendxia
	 * @DateTime 2024-04-23T23:16:26+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getCategoryInfo($category_id){
		if(empty($category_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		$result = $this->category->getInfo($category_id);
		if($result){
			unset($result['create_time']);
			unset($result['update_time']);
		}
		return $result;
	}

	/**
	 * 获取推荐资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:29:46+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getTuijian($category_id){
		try {
			$result = $this->model
				->where(['category_id'=>$category_id,'status'=>1])
				->orderRaw('rand()')
				->limit(6)
				->field('id,title,cover')
				->select()
				->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			if(!empty($value['cover'])){
				$result[$key]['cover'] = unserialize($value['cover']);
			}
		}
		return $result;
	}

	/**
	 * 获取收藏记录
	 * @Author   cendxia
	 * @DateTime 2022-07-27T17:37:22+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getCollect($user_id, $page, $pageSize){
		$model = new ZiyuanCollect();
		try {
			$result = $model->getList($user_id, $page, $pageSize);
			$count = $model->getCount($user_id);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		$userModel = new Users();
		foreach($result as $key => $value){
			$result[$key]['user_name'] = $userModel->where(['user_id'=>$value['user_id']])->value('user_name');
			$ziyuan = $this->model->getInfo($value['ziyuan_id']);
			if(empty($ziyuan)){
				$result[$key]['status'] = false;
			}else{
				$result[$key]['ziyuan_price'] = $ziyuan['price'];
				$result[$key]['status'] = $ziyuan['status'] == 1 ? true : false;
			}
		}

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 收藏/取消收藏资源
	 * @Author   cendxia
	 * @DateTime 2022-07-27T21:05:22+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 */
	public function setCollect($user_id, $ziyuan_id){
		$ziyuanCollect = new ZiyuanCollect();
		$data = $ziyuanCollect->getCollectFind($user_id, $ziyuan_id);
		if(empty($data)){
			// 收藏
			$ziyuan = $this->ziyuanInfo($ziyuan_id);
			if(empty($ziyuan)){
				throw new \think\Exception('当前资源不存在');
			}
			$collectData = [
				'user_id' => $user_id,
				'ziyuan_id' => $ziyuan_id,
				'ziyuan_title' => $ziyuan['title'],
				'ziyuan_cover' => $ziyuan['cover'][0],
				'price' => $ziyuan['price']
			];
			return $ziyuanCollect->addCollect($collectData);
		}
		// 取消收藏
		return $ziyuanCollect->delCollect($user_id, $ziyuan_id);
	}


	/**
	 * 专辑列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T09:52:46+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function zhuanji($this_user_id, $user_id){
		$model = new ziyuanModel();
		$category = new ZiyuanCategory();
		$result = $model->getCategoryGroup($user_id);
		foreach($result as $key => $val){
			$categoryData = $category->getById($val['category_id']);
			$result[$key]['category_name'] = $categoryData['title'];
			$result[$key]['num'] = $model->getCategoryCount($user_id, $val['category_id']);
			$result[$key]['ziyuan'] = $this->getList(1, 4, $user_id, $val['category_id']);
		}
		return $result;
	}

	/**
	 * 添加浏览记录
	 * @Author   cendxia
	 * @DateTime 2022-08-19T23:34:47+0800
	 * @param    [type]                   $user_id    [description]
	 * @param    [type]                   $ziyuan_id  [description]
	 * @return   [type]                               [description]
	 */
	public function read($user_id, $ziyuan_id){
		$this->model->startTrans();  // 开启事务
		try {
			$this->model->inc('read_num');
			$this->read->add(['user_id'=>$user_id,'ziyuan_id'=>$ziyuan_id]);
			$this->model->commit(); // 提交事务
			return true;
		} catch (\think\Exception $e) {
			$this->model->rollback(); // 事务回滚
			return false;
		}
	}


	/**
	 * 获取当前账号和资源信息，如：是否收藏、购买
	 * @Author   cendxia
	 * @DateTime 2022-08-21T21:23:50+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function getUserInfo($user_id, $ziyuan_id){
		// 是否收藏、购买
		if(empty($user_id)){
			// 未登录
			return [
				'is_buy' => false,
				'is_collect' => false
			];
		}
		// 判断是否购买
		$orderModel = new \app\api\model\Order;
		$order = $orderModel->getByZiyuanId($user_id, $ziyuan_id);
		if(!empty($order) && $order['status'] == 1){
			$is_buy = true;
		}else{
			$is_buy = false;
		}

		// 判断是否收藏
		$collectModel = new ZiyuanCollect();
		$collect = $collectModel->getByZiyuanId($user_id, $ziyuan_id);
		$is_collect = !empty($collect) ? true : false;

		return [
			'is_buy' => $is_buy,
			'is_collect' => $is_collect
		];
	}

	/**
	 * 根据分类id获取兄弟分类信息
	 * @Author   cendxia
	 * @DateTime 2022-09-01T12:37:09+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getBrotherCategorys($category_id){
		$category = $this->category->getById($category_id);
		if(empty($category) || $category['pid'] == 0){
			// 获取一级分类
			$categorys = $this->category->where(['pid'=>0,'show'=>1])->order('sort','ASC')->field('id,title,keywords,description')->select()->toArray();

			foreach($categorys as $key => $value){
				$categorys[$key]['subclass'] = $this->category->where(['pid'=>$value['id'],'show'=>1])->order('sort','ASC')->field('id,title,keywords,description')->select()->toArray();
			}
		}else{
			$categorys = $this->category->where(['pid'=>$category['pid'],'show'=>1])->order('sort','ASC')->field('id,title,keywords,description')->select()->toArray();
			foreach($categorys as $key => $value){
				$categorys[$key]['subclass'] = [];
			}
		}
		return $categorys;
	}

	/**
	 * 搜索
	 * @Author   cendxia
	 * @DateTime 2022-10-08T22:18:26+0800
	 * @param    [type]                   $keyWord     [description]
	 * @param    [type]                   $category_id [description]
	 * @param    [type]                   $page        [description]
	 * @param    [type]                   $pageSize    [description]
	 * @return   [type]                                [description]
	 */
	public function search($keyWord, $category_id, $page, $pageSize){
		if(empty($keyWord)){
			throw new \think\Exception('请输入关键词');
		}

		$where = array();

		if(!empty($category_id)){
			// 是否查询所有子分类数据
			$categorys = $this->category->getByIdAll($category_id);
			$ids = [$category_id];
			foreach($categorys as $key => $value){
				array_push($ids, $value['id']);
			}
			$where[] = [
				['category_id', 'in', $ids]
			];
		}
		$where[] = [
			['status', '=', 1]
		];

		try {
			$result = $this->model->search($page, $pageSize, $keyWord, $where);
			$count = $this->model->searchCount($keyWord, $where);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			$result[$key]['cover'] = unserialize($value['cover']);
			$category = $this->category->getById($value['category_id']);
			if(empty($category)){
				$result[$key]['category'] = '';
			}else{
				$result[$key]['category'] = $category['title'];
			}
		}
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 获取分类所有一级分类下的资源
	 * @Author   cendxia
	 * @DateTime 2022-11-07T23:08:53+0800
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getCategoryZiyuan($pageSize, $auto=false){
		$prefix = config('common.common_prefix');
		if($auto){
			// 自动任务
			$data = false;
		}else{
			$data = Cache($prefix.'categoryZiyuan');
			$e = empty($data) ? 0 : 1;
		}
		if(empty($data)){
			$categorys = $this->category->getList(['pid'=>0]);
			$result = array();
			foreach($categorys as $key => $value){
				if($key < 5){
			        $result[$value['id']] = $this->getList(1, $pageSize, '', $value['id'], true, false, true);
			    }
			}
			Cache($prefix.'categoryZiyuan', $result, 1800);
			return $result;
		}
		return $data;
	}

	/**
	 * 获取Ta的资源
	 * @Author   cendxia
	 * @DateTime 2022-12-26T17:46:50+0800
	 * @param    [type]                   $user_id   [description]
	 * @param    [type]                   $ziyuan_id [description]
	 * @return   [type]                              [description]
	 */
	public function getUserZiyuan($user_id, $ziyuan_id){
		if(empty($user_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		try {
			$result = $this->model
				->where(['user_id'=>$user_id,'status'=>1])
				->where('id','<>',$ziyuan_id)
				->orderRaw('rand()')
				->limit(6)
				->field('id,title,cover')
				->select()
				->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			if(!empty($value['cover'])){
				$result[$key]['cover'] = unserialize($value['cover']);
			}
		}
		return $result;
	}


	/**
	 * 获取专辑信息
	 * @Author   cendxia
	 * @DateTime 2023-10-19T20:10:25+0800
	 * @param    [type]                   $zhuanji [description]
	 * @return   [type]                            [description]
	 */
	public function getZhuanjiInfo($zhuanji){
		if(empty($zhuanji)){
			throw new \think\Exception(config('language.param_error'));
		}
		// 转换为小写
		$zhuanji = strtolower($zhuanji);
		$zhuanjiArr = array('adobe', 'mac');
		if(!in_array($zhuanji, $zhuanjiArr)){
			throw new \think\Exception(config('language.param_error'));
		}

		return $this->zhuanjiData($zhuanji);
	}


	/**
	 * 封装专辑数据
	 * @Author   cendxia
	 * @DateTime 2023-10-19T20:19:22+0800
	 * @param    [type]                   $zhuanji [description]
	 * @return   [type]                            [description]
	 */
	private function zhuanjiData($zhuanji){
		switch ($zhuanji) {
			case 'adobe':
				return [
					'title' => 'Adobe全家桶',
					'subtitle' => '让想像力自由驰骋',
					'image' => 'https://www.zijiao.cn/images/zhuanji/Adobe.png',
					'keywords' => 'Adobe全家桶,Adobe全套软件下载,Adobe软件下载,ps软件下载,pr软件下载,lr软件下载,Ae软件下载,AI软件下载', // 关键词
					'description' => 'Adobe软件的主要功能包括图像编辑、照片修复、视频制作等。在图像编辑方面，它提供了各种工具，如剪切、旋转、调整大小、调整颜色等，可以帮助用户实现图像的个性化编辑', // 描述
					'describe' => '<p>Adobe软件是一款专业的图像处理软件，拥有广泛的应用范围，包括平面设计、广告制作、影视制作、品牌宣传等。它以图像处理为核心，通过使用各种工具和功能，帮助用户实现自己的创意。</p>
                        <p>Adobe软件的主要功能包括图像编辑、照片修复、视频制作等。在图像编辑方面，它提供了各种工具，如剪切、旋转、调整大小、调整颜色等，可以帮助用户实现图像的个性化编辑。在照片修复方面，它提供了各种工具，如消除红眼、磨皮、去斑等，可以帮助用户轻松修复照片。在视频制作方面，它提供了各种工具，如视频剪辑、视频特效、音频添加等，可以帮助用户制作出精美的视频作品。</p>
                        <p>Adobe软件的独特之处在于它的开放性。它支持多种平台，如Windows、macOS等，并且可以与其他软件进行无缝集成。同时，它还提供了大量的在线帮助和文档，方便用户进行学习和使用。</p>' // 专辑描述
				];
				break;
			case 'mac':
				return [
					'title' => 'Mac软件下载专区',
					'subtitle' => 'mac精品软件',
					'image' => 'https://www.zijiao.cn/images/zhuanji/mac.jpg',
					'keywords' => 'Mac版软件下载,苹果软件下载,mac软件,mac精品软件', // 关键词
					'description' => 'Mac苹果电脑软件下载大全,提供专业的Mac软件下载、精品软件插件以及各种图片素材，图标素材,Mac使用教程,mac软件使用解答等等综合性资源下载网站', // 描述
					'describe' => '<p>Mac软件下载排行榜为您展示受欢迎的Mac软件下载排行，好用的Mac软件可以这里下载一下。</p>
                        <p>为您提供最新的Mac应用软件下载，应用软件 for mac下载，并提供相关软件的中文汉化破解教程，更多苹果电脑软件、Mac软件、苹果笔记本软件下载尽在这里。</p>
                        <p>本站提供专业的Mac苹果电脑软件、Mac游戏、精品插件以及各类海量素材下载，mac下载网站有Mac平台上常用好用的软件，还有各类PS、AE插件等，致力于打造从软件到服务都是一流的Mac下载网站。</p>' // 专辑描述
				];
				break;
			default:
				throw new \think\Exception(config('language.param_error'));
				break;
		}
	}


	/**
	 * 根据category_id获取顶级类别ID
	 * @Author   cendxia
	 * @DateTime 2024-03-24T10:18:27+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getParentCategoryId($category_id){
		$category = $this->category->getById($category_id);
		if(isset($category) && $category['pid'] == 0){
			return $category;
		}
		return self::getParentCategoryId($category['pid']);
	}


	/**
	 * 获取标签列表
	 * @Author   cendxia
	 * @DateTime 2024-04-09T21:22:30+0800
	 * @param    [type]                   $num [description]
	 * @return   [type]                        [description]
	 */
	public function getLabelList($num){
		// 缓存时间
		$cacheTime = empty($num) ? 120000 : 1800;
		$cache = Cache('labels'.$num);
		if(empty($cache)){
			$result = $this->model->getLabelList($num);

			$labelArr = array();
			foreach($result as $key => $val){
				$res = stripos($val['label'], ',');
				if($res !== false){
					$labels = explode(',', $val['label']);
				}else if(stripos($val['label'], '，') !== false){
					$labels = explode('，', $val['label']);
				}

				if(isset($labels)){
					foreach($labels as $k => $v){
						array_push($labelArr, $v);
					}
				}
			}

			// 数组去重
			$labelArr = array_unique($labelArr);
			shuffle($labelArr); // 打乱数组
			if($num > 0){
				$labelArr = array_slice($labelArr, 0, $num);
			}
			$colors = ['pink', 'red', 'orange', 'green', 'cyan', 'blue', 'purple'];
			$data = array();
			foreach($labelArr as $key => $val){
				$rand = mt_rand(0, count($colors) - 1);
				array_push($data, ['color'=>$colors[$rand], 'name'=>$val]);
			}
			Cache('labels'.$num, $data, $cacheTime);
			return $data;
		}
		return $cache;
	}
	
}