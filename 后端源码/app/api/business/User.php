<?php 

namespace app\api\business;

use app\api\model\Users;
use app\api\model\Ziyuan;
use app\api\model\Order;
use app\common\lib\Str;
use app\common\lib\Log;
use think\facade\Cache;

/**
 * 
 */
class User
{

	private $model = null;

	public function __construct(){
		$this->model = new Users;
	}
	
	/**
	 * 获取卖家信息
	 * @Author   cendxia
	 * @DateTime 2022-07-27T18:22:14+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function sellUser($user_id, $sell_user_id, $info=false){
		$ziyuanModel = new Ziyuan();
		$followModel = new \app\api\model\UserFollow;
		$result = $this->model->getSellUserBasics($sell_user_id);
		if(empty($result)){
			throw new \think\Exception('用户信息不存在');
		}
		try {
			if($info){
				// 获取资源数量
				$result['ziyuan_num'] = $ziyuanModel->where(['user_id'=>$sell_user_id,'status'=>1])->count();
				// 获取专辑数量
				$zhuanji = $ziyuanModel->getCategoryGroup($sell_user_id);
				$result['zhuanji_num'] = count($zhuanji);
				// 获取粉丝数量
				$result['fans_num'] = $followModel->where(['follow_user_id'=>$sell_user_id])->count();
				// 获取关注数量
				$result['follow_num'] = $followModel->where(['user_id'=>$sell_user_id])->count();
			}
			// 获取下载量(人气)
			$result['popularity'] = $ziyuanModel->where(['user_id'=>$sell_user_id])->sum('download_num');
			$result['read_num'] = $ziyuanModel->where(['user_id'=>$sell_user_id])->sum('read_num');
			// 判断自己是否关注该用户
			if($user_id == $sell_user_id){
				// 判断是否是自己
				$result['is_follow'] = true;
				$result['is_own'] = true;
			}else{
				$follow = $followModel->where(['user_id'=>$user_id,'follow_user_id'=>$sell_user_id])->find();
				$result['is_follow'] = empty($follow) ? false : true;
				$result['is_own'] = false;
			}
			
			$result['bgUrl'] = ''; // 背景
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		return $result;
	}
	

	/**
	 * 个人中心，获取粉丝列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T14:43:26+0800
	 * @param    [type]                   $user_id      [description]
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $page         [description]
	 * @param    [type]                   $pageSize     [description]
	 * @return   [type]                                 [description]
	 */
	public function getFansList($user_id, $this_user_id, $page, $pageSize){
		$ziyuanModel = new \app\api\model\Ziyuan;
		$model = new \app\api\model\UserFollow;
		try {
			$result = $model->with('user')
				->where(['follow_user_id'=>$user_id])
				->order('create_time', 'DESC')
				->page($page, $pageSize)
				->field('user_id')
				->select()
				->toArray();
			foreach($result as $key => $val){
				// 获取粉丝数量
				$result[$key]['fensi_num'] = $model->where(['follow_user_id'=>$val['user_id']])->count();
				// 获取作品数量
				$result[$key]['ziyuan_num'] = $ziyuanModel->where(['user_id'=>$val['user_id']])->count();
				// 判断自己是否关注该用户
				$follow = $model->where(['user_id'=>$this_user_id,'follow_user_id'=>$val['user_id']])->find();
				$result[$key]['is_follow'] = empty($follow) ? false : true;
			}
			$count = $model->with('user')->where(['follow_user_id'=>$user_id])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}		

		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	
	/**
	 * 个人中心，获取粉丝列表
	 * @Author   cendxia
	 * @DateTime 2022-08-19T11:57:23+0800
	 * @param    [type]                   $user_id      [description]
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $page         [description]
	 * @param    [type]                   $pageSize     [description]
	 * @return   [type]                                 [description]
	 */
	public function getFollowList($user_id, $this_user_id, $page, $pageSize){
		$ziyuanModel = new \app\api\model\Ziyuan;
		$model = new \app\api\model\UserFollow;
		try {
			$result = $model->with('followUser')
				->where(['user_id'=>$user_id])
				->order('create_time', 'DESC')
				->page($page, $pageSize)
				->field('follow_user_id')
				->select()
				->toArray();
			foreach($result as $key => $val){
				// 获取粉丝数量
				$result[$key]['fensi_num'] = $model->where(['user_id'=>$val['follow_user_id']])->count();
				// 获取作品数量
				$result[$key]['ziyuan_num'] = $ziyuanModel->where(['user_id'=>$val['follow_user_id']])->count();
				// 判断自己是否关注该用户
				$follow = $model->where(['user_id'=>$this_user_id,'follow_user_id'=>$val['follow_user_id']])->find();
				$result[$key]['is_follow'] = empty($follow) ? false : true;
			}
			$count = $model->with('user')->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		
		// 计算总页数
		$pageSum = intval(ceil($count / $pageSize));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 关注/取消关注
	 * @Author   cendxia
	 * @DateTime 2022-08-19T15:02:38+0800
	 * @param    [type]                   $user_id      [description]
	 * @param    [type]                   $this_user_id [description]
	 * @return   [type]                                 [description]
	 */
	public function follow($user_id, $this_user_id){
		if($user_id == $this_user_id){
			throw new \think\Exception('不能关注自己');
		}

		// 判断该用户是否存在
		$user = $this->model->getByUserId($user_id);
		if(empty($user)){
			throw new \think\Exception('该用户不存在');
		}

		$model = new \app\api\model\UserFollow;
		// 判断是否关注
		$res = $model->getByUserId($user_id, $this_user_id);
		if(!empty($res)){
			// 取消关注
			return $model->delData($user_id, $this_user_id);
		}		
		// 关注
		$data = [
			'follow_user_id' => $user_id,
			'user_id' => $this_user_id
		];
		return $model->add($data);
	}


	/**
	 * 获取用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T18:49:43+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getUserInfo($user_id){
		$user = $this->model->getByUserId($user_id);
		if(empty($user)){
			throw new \think\Exception('该用户不存在');
		}
		$result = [
			'user_id' => $user['user_id'],
			'unionid' => $user['unionid'],
			'nickname' => $user['nickname'],
			'avatar' => $user['avatar'],
			'user_name' => $user['user_name'],
			'id_card' => !empty($user['id_card']) ? substr_replace($user['id_card'],str_repeat("*",6), 3, 11) : '',
			'phone' => !empty($user['phone']) ? substr_replace($user['phone'],'****',3,4) : '',
			'qq' => $user['qq'],
			'email' => $user['email'],
			'address' => $user['address'],
			'last_password_time' => $user['last_password_time'],
			'describe' => $user['describe'],
			'qrCode' => 'https://zjw.data.zijiao.cn/avatar/avatar_2.png'
		];
		return $result;
	}


	/**
	 * 	修改用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-13T21:53:54+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function editUser($user_id, $data){
		$actions = ['email','phone','address','unionid','avatar','describe','password','qq','nickname'];

		if(empty($data['action']) || array_search($data['action'], $actions) === false){
			throw new \think\Exception(config('language.param_error'));
		}

		switch ($data['action']) {
			case 'address':
				// 修改地址
				return $this->model->edit($user_id, ['address'=>$data['address']]);
				break;
			case 'nickname':
				// 修改昵称
				return $this->model->edit($user_id, ['nickname'=>$data['nickname']]);
				break;
			case 'describe':
				// 修改简介
				return $this->model->edit($user_id, ['describe'=>$data['describe']]);
				break;
			case 'qq':
				// 修改QQ
				return $this->model->edit($user_id, ['qq'=>$data['qq']]);
				break;
			case 'avatar':
				// 修改头像
				return $this->model->edit($user_id, ['avatar'=>$data['avatar']]);
				break;
			case 'phone':
				// 修改手机号
				return self::editPhone($user_id, $data);
				break;
			case 'password':
				// 修改密码
				return self::editPwd($user_id, $data);
				break;
			case 'email':
				// 修改邮箱号
				return self::editEmail($user_id, $data);
				break;
			default:
				return false;
				break;
		}
	}


	/**
	 * 修改邮箱号
	 * @Author   cendxia
	 * @DateTime 2022-10-14T20:16:08+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	private function editEmail($user_id, $data){
		if(empty($data['email_code'])){
			throw new \think\Exception(config('language.param_error'));
		}
		$userInfo = $this->model->getByUserId($user_id);
		// 判断是第一次验证还是第二次验证
		if(empty($data['email'])){
			// 第一次验证
			$email = $userInfo['email'];
			$code_type = 'editEmail';
		}else{
			// 第二次验证
			$email = $data['email'];
			$code_type = 'code';
			if($data['email'] == $userInfo['email']){
				throw new \think\Exception('新邮箱号不能和原邮箱号一致');
			}
			// 判断邮箱号是否存在
			$res = $this->model->where(['email'=>$data['email']])->find();
			if($res){
				throw new \think\Exception('邮箱号已存在');
			}
		}
		// 判断验证码是否正确
        $emailCode = cache($email.$code_type);

        if(empty($emailCode) || $emailCode != $data['email_code']){
            throw new \think\Exception('验证码错误');
        }
        if(!empty($data['email'])){
        	// 执行修改操作
        	$this->model->startTrans();  // 开启事务
		    try {
		    	$this->model->edit($user_id, ['email'=>$data['email']]);
		    	// 生成日志
	    		$log = logData($user_id, '修改邮箱号', ['email'=>$data['email']]);
	    		Log::insertUserLog($log);
		    	// 清除验证码
		    	cache($email.$code_type, false);
		    	$this->model->commit(); // 提交事务
				return true;
		    } catch (\think\Exception $e) {
		    	$this->model->rollback(); // 事务回滚
				return false;
		    }
        }
        return true;
	}

	/**
	 * 修改密码
	 * @Author   cendxia
	 * @DateTime 2022-10-14T15:03:42+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	private function editPwd($user_id, $data){
		$params = ['password','phone','sms_code','newPassword','rpassword'];

		if(empty($data['password']) || empty($data['phone']) || empty($data['sms_code']) || empty($data['newPassword']) || empty($data['rpassword'])){
			throw new \think\Exception(config('language.param_error'));
		}

		// 判断新密码和确认密码是否一致
		if($data['newPassword'] != $data['rpassword']){
			throw new \think\Exception('两次密码不一致');
		}

		// 判断新密码和原密码是否一致
		if($data['newPassword'] == $data['password']){
			throw new \think\Exception('新密码不能和原密码一致');
		}

		$userInfo = $this->model->getByUserId($user_id);

		// 判断手机号是否一致
		if($data['phone'] != $userInfo['phone']){
			throw new \think\Exception('手机号错误');
		}

		// 判断验证码错误
        $prefix = smsPrefix('editPwd');
        // 判断验证码是否存在
        $smsCode = cache($prefix.$data['phone'].'editPwd');
        if(empty($smsCode) || $smsCode != $data['sms_code']){
            throw new \think\Exception('验证码错误');
        }

        $utils = new \app\common\lib\Utils;

        // 判断原密码是否一致
        $password = $utils->userEncryption(md5($data['password']), $userInfo['salt']);
	    if($password != $userInfo['password']){
	    	throw new \think\Exception('用户名或密码错误');
	    }

	    // 生成新密码和salt
	    $salt = Str::getRandStr(rand(15,25));
	    $newPassword = $utils->userEncryption(md5($data['newPassword']), $salt);

	    $upData = [
	    	'password' => $newPassword,
	    	'salt' => $salt,
	    	'last_password_time' => date('Y-m-d H:i:s')
	    ];

	    $this->model->startTrans();  // 开启事务
	    try {
	    	$this->model->edit($user_id, $upData);
	    	// 生成日志
	    	$log = logData($user_id, '修改密码');
	    	Log::insertUserLog($log);
	    	// 清除登录信息
	    	$token = request()->header('token');
	    	(new \app\api\business\LogRegister)->loginOut($token);
	    	// 清除短信
	    	cache($prefix.$data['phone'].'editPwd', false);
	    	$this->model->commit(); // 提交事务
			return true;
	    } catch (\think\Exception $e) {
	    	$this->model->rollback(); // 事务回滚
			return false;
	    }
	}


	/**
	 * 修改手机号
	 * @Author   cendxia
	 * @DateTime 2022-10-14T11:53:49+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	private function editPhone($user_id, $data){
		if(empty($data['sms_code'])){
			throw new \think\Exception(config('language.param_error'));
		}
		$userInfo = $this->model->getByUserId($user_id);
		// 判断是第一次验证还是第二次验证
		if(empty($data['phone'])){
			// 第一次验证
			$phone = $userInfo['phone'];
			$code_type = 'editPhone';
		}else{
			// 第二次验证
			$phone = $data['phone'];
			$code_type = 'common';
			if($data['phone'] == $userInfo['phone']){
				throw new \think\Exception('新手机号不能和原手机号一致');
			}
			// 判断手机号是否存在
			$res = $this->model->getUserByPhone($data['phone']);
			if($res){
				throw new \think\Exception('手机号已存在');
			}
		}
		// 判断验证码是否正确
        // 前缀
        $prefix = smsPrefix($code_type);
        // 判断验证码是否存在
        $smsCode = cache($prefix.$phone.$code_type);
        if(empty($smsCode) || $smsCode != $data['sms_code']){
            throw new \think\Exception('验证码错误');
        }
        if(!empty($data['phone'])){
        	// 执行修改操作
        	$this->model->startTrans();  // 开启事务
		    try {
		    	$this->model->edit($user_id, ['phone'=>$data['phone']]);
		    	// 生成日志
	    		$log = logData($user_id, '修改手机号', ['phone'=>$data['phone']]);
	    		Log::insertUserLog($log);
		    	// 清除短信
		    	cache($prefix.$phone.$code_type, false);
		    	$this->model->commit(); // 提交事务
				return true;
		    } catch (\think\Exception $e) {
		    	$this->model->rollback(); // 事务回滚
				return false;
		    }
        }
        return true;
	}

	/**
	 * 微信解绑
	 * @Author   cendxia
	 * @DateTime 2022-10-26T11:31:59+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function unBinding($user_id){
		$user = $this->model->getByUserId($user_id);
		if(empty($user)){
			throw new \think\Exception('解绑失败');
		}
		if(empty($user['unionid'])){
			throw new \think\Exception('解绑失败，当前账号未绑定微信！');
		}
		$log = [
			'user_id' => $user_id,
			'nickname' => $user['nickname'],
			'type' => 0
		];
		$logModel = new \app\api\model\wxBinding;
		$logModel->add($log);
		$upData = [
			'openid' => '',
			'unionid' => ''
		];
		return $this->model->edit($user['user_id'], $upData);
	}

	/**
	 * 找回密码
	 * @Author   cendxia
	 * @DateTime 2024-03-27T10:21:15+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function findPwd($data){
		if(empty($data['phone']) || empty($data['sms_code']) || empty($data['password']) || empty($data['rpassword'])){
			throw new \think\Exception(config('language.param_error'));
		}
		if($data['password'] != $data['rpassword']){
			throw new \think\Exception('两次密码不一致');
		}

		// 判断验证码是否正确
		// 前缀
		$code_type = 'findPwd';
        $prefix = smsPrefix($code_type);
        // 判断验证码是否存在
        $smsCode = cache($prefix.$data['phone'].$code_type);
        if(empty($smsCode) || $smsCode != $data['sms_code']){
            throw new \think\Exception('验证码错误');
        }

        // 判断用户是否存在
        $user = $this->model->getUserByPhone($data['phone']);
        if(empty($user)){
        	throw new \think\Exception('当前手机号未注册');
        }

        // 生成随机字符串
		$salt = Str::getRandStr(rand(15,25));
		// 密码
		$utils = new \app\common\lib\Utils;
		$password = $utils->userEncryption(md5($data['password']), $salt);

        $upData = [
        	'user_id' => $user['user_id'],
        	'password' => $password,
        	'salt' => $salt,
        	'last_password_time' => date('Y-m-d H:i:s')
        ];

        $this->model->startTrans();  // 开启事务
	    try {
	    	$this->model->edit($user['user_id'], $upData);
	    	// 生成日志
    		$log = logData($user['user_id'], '重置密码');
    		Log::insertUserLog($log);
	    	// 清除短信
	    	cache($prefix.$data['phone'].$code_type, false);
	    	$this->model->commit(); // 提交事务
			return true;
	    } catch (\think\Exception $e) {
	    	$this->model->rollback(); // 事务回滚
			return false;
	    }
	}


	/**
	 * 用户反馈
	 * @Author   cendxia
	 * @DateTime 2022-12-03T12:50:59+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function feedback($user_id, $data){
		if(empty($data['title']) || empty($data['content'])){
			throw new \think\Exception(config('language.param_error'));
		}
		$data['user_id'] = $user_id;
		$model = new \app\api\model\UserFeedback;
		return $model->add($data);
	}

	/**
	 * 获取绑定微信需要的参数
	 * @Author   cendxia
	 * @DateTime 2022-12-03T22:27:16+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getWeChatBindingParams($user_id){
		$user = $this->getUserInfo($user_id);
		if(!empty($user['unionid'])){
			throw new \think\Exception('当前账号已绑定微信');
		}
		$str = Str::getRandStr(rand(5,15));
		$randStr = md5($str.'_'.$user_id);
		Cache($randStr, $user_id, 650);
		return 'https://wx.zijiao.cn/WeChat/wxlogin?apiToken='.$randStr;
	}

	/**
	 * 获取带参数的二维码进行登录
	 * @Author   cendxia
	 * @DateTime 2023-09-22T16:41:02+0800
	 * @return   [type]                   [description]
	 */
	public function getQrCodeLogin(){
		$str = Str::getRandStr(rand(10,25));  // 生成一个随机数
		$str = strtolower($str); // 转小写
		$service = new \app\api\business\WeChat();
		$data = $service->getQrCode('', 'userLogin', $str);
		$result = [
			'code' => $str, 
			'qrCode' => $data
		];
		return $result;
	}


	/**
	 * 前端通过api查询微信扫码关注登录信息
	 * @Author   cendxia
	 * @DateTime 2023-09-23T16:43:37+0800
	 * @param    [type]                   $code [description]
	 * @return   [type]                         [description]
	 */
	public function getWxLoginData($code){
		if(empty($code)){
			throw new \think\Exception(config('language.login_error'));
		}
		// 获取公共缓存前缀
        $prefix = config('common.common_prefix');
		$token = cache($prefix.'login_lc_randstr_'.$code);
		// 不能判断token和result是否为空
		if(empty($token)) return false;
		$result = cache($token);
		if(empty($result)) return false;
		cache($prefix.'login_lc_randstr_'.$code, false); // 清除缓存
		return $result;
	}


	/**
     * 获取百度网盘下载工具
     * @Author   cendxia
     * @DateTime 2024-04-26T09:04:19+0800
     * @param    [type]                   $user_id [description]
     * @return   [type]                            [description]
     */
    public function getPandownload($user_id){
        $model = new Order();
        $result = $model->gerTodayOrderBaiduPan($user_id);
        if(empty($result)){
        	$url = '';
        }else{
        	$url = 'https://www.zijiao.cn';
        }
        return [
            'url' => $url
        ];
    }

}