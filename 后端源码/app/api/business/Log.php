<?php 

namespace app\api\business;

use app\api\model\UserLogin;
use app\api\model\UserActionLog;

/**
 * 
 */
class Log
{

	private $loginLog;
	private $actionLog;
	
	public function __construct()
	{
		$this->loginLog = new UserLogin();
		$this->actionLog = new UserActionLog();
	}

	/**
	 * 用户登录日志
	 * @Author   cendxia
	 * @DateTime 2022-10-15T11:43:04+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function loginLog($user_id, $page, $pageSize){
		return $this->loginLog->getList($user_id, $page, $pageSize);
	}

	/**
	 * 获取用户操作日志
	 * @Author   cendxia
	 * @DateTime 2022-10-15T11:54:02+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getActionLog($user_id, $page, $pageSize){
		$result = $this->actionLog->getList($user_id, $page, $pageSize);
		foreach($result as $key => $value){
			if(!empty($value['value'])){
				$result[$key]['value'] = unserialize($value['value']);
			}
		}
		return $result;
	}
}