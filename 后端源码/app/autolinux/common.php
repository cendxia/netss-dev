<?php 

// 封装操作日志
function logData($region_id, $title, $value=[], $type='select'){
    $result = [
        'region_id' => $region_id,
        'title' => $title,
        'ip' => getUserIp(),
        'type' => $type,
        // 存在安全隐患
        // 'controller' => request()->controller(),
        // 'action' => request()->action(),
        'controller' => '',
        'action' => '',
        'value' => serialize($value)
    ];
    return $result;
}
