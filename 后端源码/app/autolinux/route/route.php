<?php 

use think\facade\Route;


// 更新首页资源列表
Route::get('updateIndexZiyuan', 'autolinux/Common/updateIndexZiyuan');
// 更新首页资源分类数据
Route::get('updateIndexCategory', 'autolinux/Common/updateIndexCategory');
// 从缓存中获取文章信息，并根据关键词获取百度图片封装后写入数据库
Route::get('articleEvent', 'autolinux/ArticleNet/articleEvent');
// 超时取消资源订单
Route::get('cancelZiyuanOrder', 'autolinux/Order/cancelZiyuanOrder');


// 封装360搜索引擎提交的XML格式文件
Route::get('sitemap360', 'autolinux/Common/sitemap360');
// 生成url到text文件中
Route::get('siteText', 'autolinux/Common/siteText');
// 百度API提交sitemap
Route::get('baiduApiSitemap', 'autolinux/Common/baiduApiSitemap');