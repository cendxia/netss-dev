<?php 

namespace app\autolinux\controller;

use app\BaseController;

use think\facade\Db;

/**
 * 
 */
class Order extends BaseController
{
	/**
	 * 超时取消资源订单
	 * @Author   cendxia
	 * @DateTime 2024-02-17T17:12:47+0800
	 * @return   [type]                   [description]
	 */
	public function cancelZiyuanOrder(){
		$time = time() - 1800;	// 半小时后自动取消
		$date = date('Y-m-d H:i:s', $time);
		Db::name('order')->where(['status'=>0])->where('create_time', '<', $date)->update(['status'=>2]);
	}
}