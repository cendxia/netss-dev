<?php

namespace app\autolinux\controller;

use app\BaseController;
use app\api\business\Ziyuan;
use app\autolinux\business\Sitemap;


/**
 * 
 */
class Common extends BaseController
{
	
	/**
	 * 更新首页资源列表
	 * @Author   cendxia
	 * @DateTime 2022-12-12T13:14:50+0800
	 * @return   [type]                   [description]
	 */
	public function updateIndexZiyuan(){
		$service = new Ziyuan();
		$service->getCategoryZiyuan(15, true);
	}


	/**
	 * 更新首页资源分类数据
	 * @Author   cendxia
	 * @DateTime 2022-12-12T13:15:15+0800
	 * @return   [type]                   [description]
	 */
	public function updateIndexCategory(){
		$service = new Ziyuan();
		$service->getCategorys(false, true);
		$service->getCategorys(true, true);
	}


	/**
	 * 封装360搜索引擎提交的XML格式文件
	 * @Author   cendxia
	 * @DateTime 2023-10-11T12:27:21+0800
	 * @return   [type]                   [description]
	 */
	public function sitemap360(){
		$service = new Sitemap();
		$result = $service->sitemap360();
	}

	/**
	 * 生成url到text文件中
	 * @Author   cendxia
	 * @DateTime 2023-10-18T17:32:40+0800
	 * @return   [type]                   [description]
	 */
	public function siteText(){
		$service = new Sitemap();
		$result = $service->siteText();
	}


	/**
	 * 百度API提交sitemap
	 * @Author   cendxia
	 * @DateTime 2024-03-31T14:49:34+0800
	 * @return   [type]                   [description]
	 */
	public function baiduApiSitemap(){
		$service = new Sitemap();
		$result = $service->baiduApiSitemap();
	}
}