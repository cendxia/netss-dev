<?php 

namespace app\autolinux\controller;

use app\BaseController;
use app\autolinux\model\Article;
use app\common\lib\GetBaiduImage;
use app\common\lib\SubmitSeo;
use think\facade\Cache;

/**
 * 自动文章采集写入数据库
 */
class ArticleNet extends BaseController
{
	
	/**
	 * 从缓存中获取文章信息，并根据关键词获取百度图片封装后写入数据库
	 * @Author   cendxia
	 * @DateTime 2023-05-24T00:34:11+0800
	 * @return   [type]                   [description]
	 */
	public function articleEvent(){
		// 出队列
		$aiArticles = Cache::rpop('lcnet_gzy_ai_caiji_articles');
		if(!empty($aiArticles)){
			$value = unserialize($aiArticles);
			if($value['content_src'] == '此文章处于编辑状态'){
				return false;
			}
			$data = [
				'category_id' => 5,
				'title' => $value['title'],
				'keyword' => $value['keywords'],
				'describe' => $value['description'],
				'content' => $value['content_src'],
				'content_html' => $value['ai_article_html']
			];

			// 查询文章是否存在
			$articleModel = new Article();
			$res = $articleModel->getArticleByTitle($data['title']);
			if(!empty($res)) return false;
			$imgs = GetBaiduImage::getImgs($data['keyword']); // 获取网络图片
			$imgArr = GetBaiduImage::imageProcessing($imgs, true); // 上传图片到服务器
			if(empty($imgArr)) return false;
			$data['cover'] = $imgArr[0];			
			$result = $articleModel->add($data);
			if(empty($result)){
				return resError('操作失败');
			}
			SubmitSeo::baidu('article', ['article_id'=>$result]);
			return R('操作成功');
		}

	}
}