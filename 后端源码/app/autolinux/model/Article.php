<?php 

namespace app\autolinux\model;

use think\Model;

/**
 * 
 */
class Article extends Model
{
	
	/**
	 * 添加文章
	 * @Author   cendxia
	 * @DateTime 2023-05-24T23:03:15+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 根据title查询文章
	 * @Author   cendxia
	 * @DateTime 2023-05-24T23:04:59+0800
	 * @param    [type]                   $title [description]
	 * @return   [type]                          [description]
	 */
	public function getArticleByTitle($title){
		try {
			$result = $this->where(['title'=>$title])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}
}