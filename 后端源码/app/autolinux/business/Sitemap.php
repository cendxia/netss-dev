<?php 

namespace app\autolinux\business;

use think\facade\Db;

/**
 * 
 */
class Sitemap
{
	
	public function __construct()
	{
		// code...
	}

	/**
	 * 封装360搜索引擎提交的XML格式文件
	 * @Author   cendxia
	 * @DateTime 2023-10-11T12:32:50+0800
	 * @return   [type]                   [description]
	 */
	public function sitemap360(){
		$xmlHtml = '';

	    $ziyuan = $this->getZiyuan();
	    foreach($ziyuan as $key => $value){
	    	$url = 'https://www.zijiao.cn/ziyuan/'.$value['id'];
	    	$date = date('Y-m-d', strtotime($value['update_time']));
	    	$data = '<url>
				<loc>'.$url.'</loc>
				<lastmod>'.$date.'</lastmod>
				<changefreq>daily</changefreq>
				<priority>0.8</priority>
	        </url>';
	        $xmlHtml .= $data;
	    }

	    $category = $this->getUserZiyuanCategory();
	    foreach($category as $key => $value){
	    	$url = 'https://www.zijiao.cn/ziyuan/category/'.$value['id'];
	    	$date = date('Y-m-d', strtotime($value['update_time']));
	    	$data = '<url>
				<loc>'.$url.'</loc>
				<lastmod>'.$date.'</lastmod>
				<changefreq>daily</changefreq>
				<priority>0.8</priority>
	        </url>';
	        $xmlHtml .= $data;
	    }

	    $kecheng = $this->getTrylistKe();
	    foreach($kecheng as $key => $value){
	    	// https://www.gouziyuan.cn/kecheng/course/23590_49_457
	    	$url = 'https://www.zijiao.cn/kecheng/course/'.$value['chapters']['ke_id'].'_'.$value['ke_chapters_id'].'_'.$value['id'];
	    	$date = date('Y-m-d', strtotime($value['update_time']));
	    	$data = '<url>
				<loc>'.$url.'</loc>
				<lastmod>'.$date.'</lastmod>
				<changefreq>daily</changefreq>
				<priority>0.8</priority>
	        </url>';
	        $xmlHtml .= $data;
	    }

	    $XML = '<?xml version="1.0" encoding="utf-8"?>
	    <urlset>
	    	'.$xmlHtml.'
	    </urlset>';

		$fp = fopen("/www/wwwroot/apizijiao/public/sitemap360.xml", 'w');
		fwrite($fp, $XML);
		fclose($fp);
	}

	/**
	 * 生成url到text文件中
	 * @Author   cendxia
	 * @DateTime 2023-10-18T17:33:11+0800
	 * @return   [type]                   [description]
	 */
	public function siteText(){
		$urls = '';
		$ziyuan = $this->getZiyuan();
	    foreach($ziyuan as $key => $value){
	    	$urls .= 'https://www.zijiao.cn/ziyuan/'.$value['id']."\n";
	    }

	    $category = $this->getUserZiyuanCategory();
	    foreach($category as $key => $value){
	    	$urls .= 'https://www.zijiao.cn/ziyuan/category/'.$value['id']."\n";
	    }

	    $kecheng = $this->getTrylistKe();
	    foreach($kecheng as $key => $value){
	    	$urls .= 'https://www.zijiao.cn/kecheng/course/'.$value['chapters']['ke_id'].'_'.$value['ke_chapters_id'].'_'.$value['id']."\n";
	    }

	    $fp = fopen("/www/wwwroot/apizijiao/public/sitemap.txt", 'w');
		fwrite($fp, $urls);
		fclose($fp);
	}


	/**
	 * 百度API提交sitemap
	 * @Author   cendxia
	 * @DateTime 2024-03-31T14:45:52+0800
	 * @return   [type]                   [description]
	 */
	public function baiduApiSitemap(){
		$urls = array();
		$ziyuan = $this->getZiyuan();
	    foreach($ziyuan as $key => $value){
	    	$url = 'https://www.zijiao.cn/ziyuan/'.$value['id'];
	    	array_push($urls, $url);
	    }

	    $category = $this->getUserZiyuanCategory();
	    foreach($category as $key => $value){
	    	$url = 'https://www.zijiao.cn/ziyuan/category/'.$value['id'];
	    	array_push($urls, $url);
	    }

	    $kecheng = $this->getTrylistKe();
	    foreach($kecheng as $key => $value){
	    	$url = 'https://www.zijiao.cn/kecheng/course/'.$value['chapters']['ke_id'].'_'.$value['ke_chapters_id'].'_'.$value['id'];
	    	array_push($urls, $url);
	    }

	    self::apiSitemapData($urls);

	}


	/**
	 * 封装百度API提交sitemap数据
	 * @Author   cendxia
	 * @DateTime 2024-03-31T15:07:15+0800
	 * @param    [type]                   $urls [description]
	 * @return   [type]                         [description]
	 */
	private function apiSitemapData($urls){

		if(count($urls) == 0){
			dump('已完成');
			exit;
		}

		if(count($urls) < 100){
			self::apiSitemap($urls);
		}else{
			// 从数组中取出一段子数组
        	$res = array_slice($urls, 0, 100);
        
        	self::apiSitemap($res);
        
        	// 从数组中删除一段数组
        	array_splice($urls, 0, 100);

        	self::apiSitemapData($urls);
		}
	}


	/**
	 * 百度API提交sitemap
	 * @Author   cendxia
	 * @DateTime 2024-03-31T15:36:57+0800
	 * @param    [type]                   $urls [description]
	 * @return   [type]                         [description]
	 */
	private function apiSitemap($urls){
		$api = 'http://data.zz.baidu.com/urls?site=https://www.zijiao.cn&token=lt134mqdENABKx6q';
		$ch = curl_init();
		$options =  array(
		    CURLOPT_URL => $api,
		    CURLOPT_POST => true,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POSTFIELDS => implode("\n", $urls),
		    CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
		);
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		// echo $result;

		dump($result);

		sleep(3); // 休眠3秒后执行
	}


	/**
	 * 获取资源url
	 * @Author   cendxia
	 * @DateTime 2023-10-11T12:33:24+0800
	 * @return   [type]                   [description]
	 */
	private function getZiyuan(){
		try {
			return Db::name('ziyuan')->where(['status'=>1])->order('id', 'DESC')->field('id,title,update_time')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取分类信息
	 * @Author   cendxia
	 * @DateTime 2023-10-11T12:53:11+0800
	 * @return   [type]                   [description]
	 */
	private function getUserZiyuanCategory(){
		try {
			return Db::name('ziyuan_category')->where(['show'=>1])->order('sort', 'ASC')->field('id,title,update_time')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取试看课程
	 * @Author   cendxia
	 * @DateTime 2023-10-11T12:57:55+0800
	 * @return   [type]                   [description]
	 */
	public function getTrylistKe(){
		try {
			$result = Db::name('kecheng_directory')->where(['is_free'=>1])->order('id', 'DESC')->field('id,title,ke_chapters_id,update_time')->select()->toArray();

			foreach($result as $key => $value){
				$result[$key]['chapters'] = Db::name('kecheng_chapters')->where(['id'=>$value['ke_chapters_id']])->field('id,ke_id')->find();
			}
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		return $result;
	}
}