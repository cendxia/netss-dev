<?php 

namespace app\platform\validate;

use think\Validate;

/**
 * 
 */
class Ziyuan extends Validate
{
	
	protected $rule = [
		'title' => 'require|min:3',
		'pid' => 'require|number',
		'sort' => 'require|number',
		'show' => 'require|number',
		'home_show' => 'require|number',
		'icon' => 'require',
		'url' => 'require',
		'keywords' => 'require',
		'description' => 'require'
	];


	protected $message = [
		'title.require' => '名称必填',
		'title.min' => '名称长度不足3个字符',
		'pid.require' => 'pid必填',
		'pid.number' => 'pid参数需为整数',
		'sort.require' => '排序必填',
		'sort.number' => '排序参数需为整数',
		'show.require' => '显示参数必填',
		'show.number' => '显示参数需为整数',
		'home_show.require' => '是否在首页显示参数必填',
		'home_show.number' => '是否在首页显示参数需为整数',
		'icon.require' => '请上传图标',
		'url.require' => '请填写url参数',
		'keywords.require' => '请填写关键词',
		'description.require' => '请填写描述',
	];


	// 验证场景
	protected $scene = [
		'category' => ['title','sort','show','home_show']
	];
}