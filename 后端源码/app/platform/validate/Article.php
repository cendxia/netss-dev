<?php 

namespace app\platform\validate;

use think\Validate;

/**
 * 
 */
class Article extends Validate
{
	
	// 验证规则
	protected $rule = [
		'user_id'=>'require|number|>=:1',
		'category_id'=>'require|number',
		'name'=>'require|min:3',
		'sort' => 'require|number',
		'type' => 'require',
		'title'=>'require|min:3',
		'content'=>'require',
		'cover' => 'require',
		'show' => 'require|number'
	];

	// 提示
	protected $message = [
		'user_id.require'=>'用户id错误',
		'user_id.number'=>'用户id错误',
		'user_id.egt'=>'用户id错误',
		'category_id.require'=>'请选择类别',
		'category_id.number'=>'类别id必须为整形',
		'name.require'=>'分类名称必填',
		'name.min'=>'分类名称不能小于3个字符',
		'title.require'=>'请填写文章标题',
		'title.min'=>'标题名称必须大于3个字符',
		'content.require'=>'文章详情必填',
		'cover.require' => '请上传封面',
		'show.require' => '参数错误'
	];


	// 验证场景
	protected $scene = [
		'category' => ['name','sort','type'],
		'article' => ['title','category_id','content','cover','show'],
	];
}