<?php

namespace app\platform\validate;

use think\Validate;

/**
 * 
 */
class AdminUser extends Validate{
	// 验证规则
	protected $rule = [
		'login_name'=>'require',
		'password'=>'require',
		'newPassword' => 'require|different:password',
		'rPassword' => 'require|confirm:newPassword',
		'phone' => 'require|mobile',
		'sms_code'=>'require',
	];

	// 提示
	protected $message = [
		'login_name.require'=>'请输入用户名',
		'password.require'=>'请输入密码',
		'newPassword.require'=>'请输入新密码',
		'newPassword.different'=>'新密码不能和原密码一致',
		'rPassword.require'=>'请输入确认密码',
		'rPassword.confirm'=>'确认密码不正确',
		'phone.require' => '请输入手机号',
		'phone.mobile' => '手机号格式不正确',
		'sms_code.require'=>'请输入验证码',
	];

	// 验证场景
	protected $scene = [
		'login' => ['login_name','password','phone','sms_code'],
		'editPassword' => ['password','newPassword','rPassword']
	];


}