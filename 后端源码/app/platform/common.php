<?php 

// 验证码前缀
function smsPrefix($type){
	$config = config('common.sms.platform_login_pre');
	$prefix = '';
    if($type == 'formlc_login'){
        $prefix = $config[0];
    }else if($type == 'formlc_action'){
        $prefix = $config[1];
    }
    return $prefix;
}


// 封装用户操作日志
function logData($user_id, $title, $value=[]){
    $result = [
        'user_id' => $user_id,
        'title' => $title,
        'ip' => getUserIp(),
        'value' => !empty($value) ? serialize($value) : ''
    ];
    return $result;
}
