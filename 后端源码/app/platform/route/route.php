<?php 

use think\facade\Route;


// 登录
Route::post('login', 'platform/Login/loginEvent');

// 发送短信
Route::post('smsSend', 'platform/Common/smsSend');

// Route::get('test', 'platform/Test/index');

// 需要token
Route::group('/', function(){

	// Route::get('test', 'platform/Test/index');

	// 退出登录
	Route::post('loginOut', 'platform/AdminUser/loginOut');

	// 修改密码
	Route::post('editPassword', 'platform/AdminUser/editPassword');

	// 首页数据
	Route::get('indexData', 'platform/Tongji/indexData');

	// 获取用户列表
	Route::get('getUserList', 'platform/User/getUserList');
	// 获取申请创作者记录
	Route::get('getMpUser', 'platform/User/getMpUser');
	// 创作者审核
	Route::post('mpAudit', 'platform/User/mpAudit');
	// 用户登录记录
	Route::get('getUserLoginLog', 'platform/Tongji/getUserLoginLog');
	// 获取短信记录
	Route::get('smsLog', 'platform/Tongji/smsLog');
	// 获取错误日志
	Route::get('getErrorLog', 'platform/Tongji/getErrorLog');

	// 获取资源分类
	Route::get('getZiyuanCategorys', 'platform/Ziyuan/getCategorys');
	// 添加/修改资源分类
	Route::post('ziyuanCategory', 'platform/Ziyuan/ziyuanCategory');
	// 删除资源分类
	Route::post('delCategory', 'platform/Ziyuan/delCategory');
	// 获取资源列表
	Route::get('getZiyuanList', 'platform/Ziyuan/getZiyuanList');
	// 获取订单列表
	Route::get('getOrderList', 'platform/Order/getOrderList');
	// 获取资源详情
	Route::get('getZiyuanInfo', 'platform/Ziyuan/getZiyuanInfo');
	// 下载记录
	Route::get('getDownloadLog', 'platform/Order/getDownloadLog');
	// 提现记录
	Route::get('getUserTixian', 'platform/User/getUserTixian');
	// 提现审核
	Route::post('tixianAudit', 'platform/User/tixianAudit');
	// 上传图片
	Route::post('imgUploadQiniu', 'platform/Upload/imgUploadQiniu');
	// 资源审核
	Route::post('ziyuanAudit', 'platform/Ziyuan/ziyuanAudit');

	// 系统设置 - 基础配置
	Route::post('systemConfig', 'platform/System/systemConfig');

	// 添加/修改文章分类
	Route::post('article/category', 'platform/Article/category');
	// 获取文章分类列表
	Route::get('article/getCategorys', 'platform/Article/getCategorys');
	// 删除文章分类
	Route::post('article/delCategory', 'platform/Article/delCategory');

	// 获取文章列表
	Route::get('article/getList', 'platform/Article/getList');
	// 添加/修改文章
	Route::post('article/article', 'platform/Article/article');
	// 获取文章详情
	Route::get('article/getInfo', 'platform/Article/getArticlesInfo');
	// 删除文章
	Route::post('article/delArticle', 'platform/Article/delArticle');

	// 获取课程列表
	Route::get('kecheng/list', 'platform/kecheng/getKechengList');
	// 获取课程订单列表
	Route::get('kecheng/orderList', 'platform/kecheng/gerOrderList');


	// 获取广告位列表
	Route::get('getAdvertisementSpaceList', 'platform/Ad/getAdvertisementSpaceList');
	// 添加广告位
	Route::post('advertisementSpace', 'platform/Ad/advertisementSpace');
	// 删除广告位
	Route::post('delAdvertisementSpace', 'platform/Ad/delAdvertisementSpace');
	// 获取广告列表
	Route::get('getAdList', 'platform/Ad/getAdList');
	// 获取广告详情
	Route::get('getAdInfo', 'platform/Ad/getAdInfo');
	// 添加/修改广告
	Route::post('adOperation', 'platform/Ad/adOperation');
	// 删除广告
	Route::post('delAd', 'platform/Ad/delAd');

	// 获取用户反馈信息
	Route::get('getUserFeedback', 'platform/User/getUserFeedback');

	// 获取VIP订单列表
	Route::get('getVipOrderList', 'platform/User/getVipOrderList');

})->middleware(\app\platform\middleware\AuthTokenMiddleware::class);