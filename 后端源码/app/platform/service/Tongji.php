<?php 

namespace app\platform\service;

use app\platform\model\SmsCode;
use app\platform\model\ErrorLog;
use app\platform\model\Users;
use app\platform\model\UserTixian;
use app\platform\model\Ziyuan;
use app\platform\model\Order;
use app\platform\model\UserOnline;
use app\platform\model\UserLogin;
use app\platform\model\DownloadLog;
use app\common\lib\Str;
use think\facade\Cache;


/**
 * 
 */
class Tongji
{

	private $smsCode;

	public function __construct(){
		$this->smsCode = new SmsCode();
	}


	private function getDates(){
		$month = strtotime('-1 month +1 day');
		return Str::getDateFromRange(date('Y-m-d', $month), date('Y-m-d', time()+86400));
	}


	/**
	 * 首页数据
	 * @Author   cendxia
	 * @DateTime 2022-11-09T20:23:06+0800
	 * @return   [type]                   [description]
	 */
	public function indexData(){
		$day = date('Y-m-d 00:00:00');

		// 用户总数
		$userModel = new Users;
		$userNum = $userModel->getUserCount();

		// 0待审核 1审核通过 2审核未通过 3草稿箱
		$ziyuanModel = new Ziyuan();
		$ziyuanNum = $ziyuanModel->getCount(['status'=>1]); // 销售资源数
		$ziyuaNoAuditNum = $ziyuanModel->getCount(['status'=>0]); // 待审核资源数

		$orderModel = new Order();
		$userTrading = $orderModel->getByUserCount(['status'=>1]); // 成交用户数

		$totalPrice = $orderModel->getByTotalPrice(['status'=>1]); // 成交总金额

		$orderNumWhere = "status = 1 and create_time >= '{$day}'";
		$todayOrderNum = $orderModel->getCount($orderNumWhere); // 今日订单数

		$todayOrderPrice = $orderModel->getByTotalPrice($orderNumWhere); // 今日订单(元)

		$tixianModel = new UserTixian();
		$tixianPrice = $tixianModel->getByTotalPrice(['status'=>0]); // 提现待审核(元)


		$prefix = config('common.common_prefix');
		// 查询待创作者数量
		$mpNotAuditUserNum = Cache($prefix.'mpNotAuditUserNum');
		if(!isset($mpNotAuditUserNum)){
			$mpNotAuditUserNum = $userModel->getMpNotAuditUserNum();
			Cache($prefix.'mpNotAuditUserNum', $mpNotAuditUserNum, 300);
		}

		$res = [
			[
				'name' => '用户总数',
				'value' => $userNum
			],
			[
				'name' => '销售资源数',
				'value' => $ziyuanNum
			],
			[
				'name' => '待审核资源数',
				'value' => $ziyuaNoAuditNum
			],
			[
				'name' => '成交用户数',
				'value' => $userTrading
			],
			[
				'name' => '成交总金额',
				'value' => $totalPrice
			],

			[
				'name' => '今日订单数',
				'value' => $todayOrderNum
			],
			[
				'name' => '今日订单(元)',
				'value' => $todayOrderPrice
			],
			[
				'name' => '提现待审核(元)',
				'value' => $tixianPrice
			]
		];

		$result = [
			'module' => $res,
			'tongji' => $this->getTongjiData(),
			'mpNotAuditUserNum' => $mpNotAuditUserNum
		];
		return $result;
	}


	// 首页近30数据
	private function getTongjiData(){
		$dates = $this->getDates();
		$ziyuanCountDate = $this->getZiyuanCountDate($dates);
		$orderCountDate = $this->getOrderCountDate($dates);
		$downloadCountDate = $this->getDownloadCountDate($dates);
		$dailyOrderPrice = $this->getDailyOrderPrice($dates);

		$result = [
			'title' => '近1个月数据',
			'dates' => $dates,
			'field' => ['新增资源', '订单数', '订单金额','下载数'],
			'data' => [
				[
					'name' => '新增资源',
				    'type' => 'line',
				    'data' => $ziyuanCountDate
				],
				[
					'name' => '订单数',
				    'type' => 'line',
				    'data' => $orderCountDate
				],
				[
					'name' => '订单金额',
				    'type' => 'line',
				    'data' => $dailyOrderPrice
				],
				[
					'name' => '下载数',
				    'type' => 'line',
				    'data' => $downloadCountDate
				]
			]
		];
		return $result;
	}

	// 用户在线数据
	private function getUserOnlineData($dates){
		$model = new UserOnline();
		$result = array();
		foreach($dates as $key => $value){
			$data = $model->getDayLogCount($value);
			array_push($result, $data);
		}
		return $result;
	}

	// 获取当日数据
	private function getZiyuanCountDate($dates){
		$model = new Ziyuan();
		$result = array();
		foreach($dates as $key => $value){
			if($value == '2024-04-07'){ // 临时
				$data = 19;
			}else{
				$data = $model->getCountDate($value);
			}
			
			array_push($result, $data);
		}
		return $result;
	}

	// 获取每日订单数
	private function getOrderCountDate($dates){
		$model = new Order();
		$result = array();
		foreach($dates as $key => $value){
			$data = $model->getCountDate($value);
			array_push($result, $data);
		}
		return $result;
	}

	// 获取每日下载数
	private function getDownloadCountDate($dates){
		$model = new DownloadLog();
		$result = array();
		foreach($dates as $key => $value){
			$data = $model->getCountDate($value);
			array_push($result, $data);
		}
		return $result;
	}


	/**
	 * 获取每日资源订单金额
	 * @Author   cendxia
	 * @DateTime 2024-03-27T14:05:23+0800
	 * @param    [type]                   $dates [description]
	 * @return   [type]                          [description]
	 */
	private function getDailyOrderPrice($dates){
		$model = new Order();
		$result = array();
		foreach($dates as $key => $value){
			$data = $model->getDailyOrderPrice($value);
			array_push($result, $data);
		}
		return $result;
	}



	///////////////////////////////////////////////////////


	/**
	 * 获取短信记录
	 * @Author   cendxia
	 * @DateTime 2022-10-16T20:05:35+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function smsLog($phone, $page, $pageSize){
		$where = !empty($phone) ? "phone = '{$phone}'" : [];
		$result = $this->smsCode->getList($page, $pageSize, $where);
		$count = $this->smsCode->getCount($where);
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 错误日志
	 * @Author   cendxia
	 * @DateTime 2022-10-18T10:28:12+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getErrorLog($page, $pageSize){
		$model = new ErrorLog();
		$result = $model->getList($page, $pageSize);
		$count = $model->getCount();
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 用户登录日志
	 * @Author   cendxia
	 * @DateTime 2023-06-30T22:14:08+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getUserLoginLog($page, $pageSize){		
		$model = new UserLogin();
		$result = $model->getList($page, $pageSize);
		$count = $model->getCount();
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}
	
}