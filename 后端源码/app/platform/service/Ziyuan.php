<?php 

namespace app\platform\service;

use app\platform\model\Ziyuan as ziyuanModel;
use app\platform\model\ZiyuanCategory;
use app\platform\model\Users;
use app\platform\validate\Ziyuan as ziyuanValidate;
use app\common\lib\SubmitSeo;
use app\common\lib\Log;

/**
 * 
 */
class Ziyuan
{
	
	private $model;
	private $category;

	public function __construct()
	{
		$this->model = new ziyuanModel();
		$this->category = new ZiyuanCategory();
	}

	/**
	 * 获取资源类型
	 * @Author   cendxia
	 * @DateTime 2022-10-16T20:23:42+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys(){
		$result = $this->category->getList(['pid'=>0]);
		foreach($result as $key => $value){
			$result[$key]['subclass'] = $this->category->getList(['pid'=>$value['id']]);
		}
		$data = array();
		foreach($result as $key => $value){
			$res = [
				'id' => $value['id'],
				'pid' => $value['pid'],
				'title' => $value['title'],
				'sort' => $value['sort'],
				'show' => $value['show'],
				'home_show' => $value['home_show'],
				'icon' => $value['icon'],
				'url' => $value['url'],
				'keywords' => $value['keywords'],
				'description' => $value['description'],
				'subclass' => false
			];
			array_push($data, $res);
			foreach($value['subclass'] as $k => $val){
				$val['subclass'] = true;
				array_push($data, $val);
			}
		}
		return $data;
	}

	/**
	 * 添加/修改资源分类
	 * @Author   cendxia
	 * @DateTime 2022-10-17T17:41:51+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function ziyuanCategory($data){
		try {
            validate(ziyuanValidate::class)->scene('category')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }

        $categoryData = [
        	'pid' => $data['pid'],
        	'title' => $data['title'],
        	'icon' => $data['icon'],
        	'keywords' => $data['keywords'],
        	'description' => $data['description'],
        	'sort' => $data['sort'],
        	'show' => $data['show'],
        	'home_show' => $data['home_show']
        ];
        if(!empty($data['id'])){
        	$categoryData['id'] = $data['id'];
        }
        return $this->category->category($categoryData);
	}

	/**
	 * 删除资源分类
	 * @Author   cendxia
	 * @DateTime 2022-10-25T21:07:02+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function delCategory($category_id){
		if(empty($category_id)){
			throw new \think\Exception(config('language.param_error'));
		}
		try {
			return $this->category->where(['id'=>$category_id])->where('id','>',27)->delete();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取资源列表
	 * @Author   cendxia
	 * @DateTime 2022-10-18T12:16:50+0800
	 * @param    [type]                   $page         [description]
	 * @param    [type]                   $pageSize     [description]
	 * @param    [type]                   $category_id  [description]
	 * @param    [type]                   $ziyuan_title [description]
	 * @param    [type]                   $user_name    [description]
	 * @return   [type]                                 [description]
	 */
	public function getZiyuanList($page, $pageSize, $category_id, $ziyuan_title, $user_name, $status){

		if(!empty($status) && $status != 0 && $status != 1 && $status != 2){
			throw new \think\Exception(config('language.param_error'));
		}

		$where = array();
		if (!empty($category_id)) {
            $where[] = [
                ['z.category_id', '=', $category_id]
            ];
        }
		if (!empty($data['user_name'])) {
            $where[] = [
                ['u.user_name', 'like', '%' . $user_name . '%']
            ];
        }
        if (!empty($ziyuan_title)) {
            $where[] = [
                ['z.title', 'like', '%' . $ziyuan_title . '%']
            ];
        }

        if($status != null){
        	$where[] = [
                ['z.status', '=', $status]
            ];
        }

        try {
        	$result = $this->model
				->alias('z')
				->join('lc_users u', 'u.user_id=z.user_id')
				->join('lc_ziyuan_category c', 'c.id=z.category_id')
				->where('z.status != -1 and z.status != 3')
				->where($where)
				->order('z.create_time','DESC')
				->page($page, $pageSize)
				->field('z.id,z.user_id,u.user_name,u.nickname user_nickname,c.title category_title,z.title,z.cover,price,vip_dis,z.status,collect_num,download_num,report_num,read_num,praise_num,z.create_time,ziyuan_update_time,fail_reason,audit_time')
				->select()
				->toArray();

			$count = $this->model
				->alias('z')
				->join('lc_users u', 'u.user_id=z.user_id')
				->join('lc_ziyuan_category c', 'c.id=z.category_id')
				->where('z.status != -1 and z.status != 3')
				->where($where)
				->count();
        } catch (\think\Exception $e) {
        	throw new \think\Exception(config('language.mysql_error'));
        }
        
		foreach($result as $key => $value){
			$result[$key]['cover'] = unserialize($value['cover']);
		}
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 资源审核
	 * @Author   cendxia
	 * @DateTime 2022-10-18T22:08:11+0800
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $id           [description]
	 * @param    [type]                   $user_id      [description]
	 * @param    [type]                   $type         [description]
	 * @param    [type]                   $fail         [description]
	 * @return   [type]                                 [description]
	 */
	public function ziyuanAudit($this_user_id, $id, $user_id, $status, $fail){
		if(empty($status) || ($status != 1 && $status != 2)){
			throw new \think\Exception(config('language.param_error'));
		}
		if($status == 2 && empty($fail)){
			throw new \think\Exception('请填写拒接原因');
		}
		$result = $this->model->getById($id, $user_id);
		if(empty($result) || ($result['status'] != 0 && $result['status'] != 1 && $result['status'] != 2)){
			throw new \think\Exception('当前状态不可操作');
		}
		if($status == 1){
			// 提交地址给百度收录api
			SubmitSeo::baidu('ziyuan', ['ziyuan_id'=>$id]);
		}
		$upData = [
			'status' => $status,
			'fail_reason' => $status==2 ? $fail : '',
			'audit_time' => date('Y-m-d H:i:s')
		];
		// 日志
		$logData = logData($this_user_id, '资源审核', $upData);
		$this->model->startTrans();  // 开启事务
		try {
			$this->model->edit($id, $user_id, $upData);
			// 日志
			Log::insertAdminLog($logData);
			$this->model->commit(); // 提交事务
			return $id;			
		} catch (\think\Exception $e) {
			$this->model->rollback(); // 事务回滚
			return false;
		}
	}

	/**
	 * 获取资源详情
	 * @Author   cendxia
	 * @DateTime 2024-03-21T12:41:13+0800
	 * @param    [type]                   $id   [description]
	 * @param    [type]                   $info [description]
	 * @return   [type]                         [description]
	 */
	public function getZiyuanInfo($id, $info){
		if(empty($id)){
			throw new \think\Exception(config('language.param_error'));
		}
		try {
			$result = $this->model->where(['id'=>$id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		$result = $result->toArray();

		if($info){
			$userModel = new Users();
			$user = $userModel->getUserInfo($result['user_id']);
			$result['user_name'] = $user['user_name'];
			$category = $this->category->getInfo($result['category_id']);
			if($category['pid'] != 0){
				$subclass = $this->category->getInfo($category['pid']);
				$result['category_name'] = $subclass['title'].'/'.$category['title'];
			}else{
				$result['category_name'] = $category['title'];
			}
		}else{
			unset($result['download_url']);
		}
		
		$result['cover'] = unserialize($result['cover']);
		$result['content'] = htmlspecialchars_decode($result['content']);
		return $result;
	}
}