<?php 

/**
 * 
 */

namespace app\platform\service;
use app\common\lib\Sms;
use app\api\validate\User as userValidate;
use app\api\model\SmsCode as smsLogModel;

use app\common\lib\Utils;


class Common
{
	
	/**
	 * 发送验证码
	 * @Author   cendxia
	 * @DateTime 2022-09-30T16:49:41+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public static function smsSend($data){
		// 使用验证器
		try {
            validate(userValidate::class)->scene('sms_send')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }

        if(array_search($data['type'], config('common.sms.platform_type')) === false){
        	throw new \think\Exception(config('language.sms_error'));
        }

        $userModel = new \app\platform\model\AdminUser;
        $user = $userModel->getByPhone($data['phone']);

        // 判断手机号是否存在
        if($data['type'] == 'login' || $data['type'] == 'findPwd'){
        	if(empty($user) || $user['status'] == -1){
        		throw new \think\Exception('手机号不存在');
        	}else if($user['status'] != 0){
        		throw new \think\Exception('账号异常');
        	}
        }

        // 前缀
        $prefix = smsPrefix($data['type']);

        // 判断验证码是否存在
        $smsCode = cache($prefix.$data['phone'].$data['type']);
        if($smsCode){
            // throw new \think\Exception('验证码发送过于频繁');
        }

        $lib = new Sms(config('common.sms'));
        // $result = $lib->wangyi($data['type'], $data['phone']);

        $result = '2131';

        $logModel = new smsLogModel();
        if(empty($result)){
            // 写入日志
            $log = [
                'phone' => $data['phone'],
                'code' => '',
                'type' => $data['type'],
                'status' => 0
            ];
            $logModel->add($log);
        	throw new \think\Exception(config('language.sms_error'));
        }


        // 写入日志
        $log = [
            'phone' => $data['phone'],
            'code' => $result,
            'type' => $data['type'],
            'status' => 1
        ];

        $logModel->add($log);

        // 存储短信验证码
        $res = cache($prefix.$data['phone'].$data['type'], $result, config('common.sms.sms_code_time'));
        return $res ? true : false;
	}
}