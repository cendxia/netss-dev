<?php 

namespace app\platform\service;

use app\platform\model\Article as articleModel;
use app\platform\model\ArticleCategory;
use app\platform\validate\Article as articleValidate;
use app\common\lib\Str;

/**
 * 
 */
class Article
{

	private $model;
	private $category;

	public function __construct(){
		$this->model = new articleModel();
		$this->category = new ArticleCategory();
	}

	/**
	 * 获取文章分类列表
	 * @Author   cendxia
	 * @DateTime 2022-10-25T12:51:17+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys(){
		try {
			return $this->category->getCategorys();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
	/**
	 * 添加/修改分类
	 * @Author   cendxia
	 * @DateTime 2022-10-25T12:18:22+0800
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $data         [description]
	 * @return   [type]                                 [description]
	 */
	public function category($this_user_id, $data){
		// 使用验证器
		try {
            validate(articleValidate::class)->scene('category')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }
        $types = ['notice','protocol','common'];
        if(array_search($data['type'], $types) === false){
        	throw new \think\Exception(config('language.param_error'));
        }

        try {
			return $this->category->category($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 删除文章分类
	 * @Author   cendxia
	 * @DateTime 2022-10-25T12:55:01+0800
	 * @param    [type]                   $id           [description]
	 * @param    [type]                   $this_user_id [description]
	 * @return   [type]                                 [description]
	 */
	public function delCategory($id, $this_user_id){
		if(empty($id)){
			throw new \think\Exception(config('language.param_error'));
		}
		try {
			return $this->category->delCategory($id);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取文章列表
	 * @Author   cendxia
	 * @DateTime 2022-10-25T14:25:34+0800
	 * @param    [type]                   $page        [description]
	 * @param    [type]                   $pageSize    [description]
	 * @param    [type]                   $title       [description]
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getList($page, $pageSize, $title, $category_id){
		$where = [];
		if (!empty($title)) {
            $where[] = [
                ['title', 'like', '%' . $title . '%']
            ];
        }
        if (!empty($category_id)) {
            $where[] = [
                ['category_id', '=', $category_id]
            ];
        }		
		try {
			$result = $this->model->with(['ArticleCategory'=>function($query){
				$query->field('id,name');
			}])->where($where)->order('update_time', 'DESC')->page($page, $pageSize)->select()->toArray();
			$count = $this->model->articleCount($where);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		// 计算总页数
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 获取文章详情
	 * @Author   cendxia
	 * @DateTime 2022-10-25T13:19:28+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getArticlesInfo($id){
		if(empty($id)){
			throw new \think\Exception(config('language.param_error'));
		}
		$result = $this->model->getArticlesInfo($id);
		if(!empty($result)){
			$result['content'] = htmlspecialchars_decode($result['content']);
		}
		return $result;
	}

	/**
	 * 添加/修改文章
	 * @Author   cendxia
	 * @DateTime 2022-10-25T13:06:36+0800
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $data         [description]
	 * @return   [type]                                 [description]
	 */
	public function article($this_user_id, $data){
		// 使用验证器
		try {
            validate(articleValidate::class)->scene('article')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }
        if(empty($data['describe'])){
        	$data['describe'] = Str::htmlStr($data['content']);
        }
        try {
        	return $this->model->article($data);
        } catch (\think\Exception $e) {
        	throw new \think\Exception(config('language.mysql_error'));
        }
	}

	/**
	 * 删除文章
	 * @Author   cendxia
	 * @DateTime 2022-10-25T13:51:03+0800
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $id           [description]
	 * @return   [type]                                 [description]
	 */
	public function delArticle($this_user_id, $id){
		if(empty($id)){
			throw new \think\Exception(config('language.param_error'));
		}
		try {
			return $this->model->delArticle($id);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}