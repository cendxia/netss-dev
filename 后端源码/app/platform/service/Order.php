<?php 

namespace app\platform\service;

use app\platform\model\Order as service;

use app\platform\model\Users;
use app\platform\model\DownloadLog;

/**
 * 
 */
class Order
{
	
	private $service;
	private $userModel;

	public function __construct()
	{
		$this->service = new service();
		$this->userModel = new Users();
	}


	/**
	 * 订单列表
	 * @Author   cendxia
	 * @DateTime 2022-10-17T18:56:48+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    [type]                   $keyword  [description]
	 * @return   [type]                             [description]
	 */
	public function getOrderList($page, $pageSize, $keyword){
		$where = [];
		if(!empty($keyword)){
			$where = "login_name = '{$keyword}' or user_name = '{$keyword}' or phone = '{$keyword}'";
		}

		try {
			$result = $this->service
			->alias('o')
			->join('lc_users u','u.user_id=o.user_id')
			->where($where)
			->order('o.create_time','DESC')
			->page($page, $pageSize)
			->field('o.order_id,o.sell_user_id,o.user_id,user_name,nickname,o.ziyuan_title,o.ziyuan_cover,o.download_num,o.price,o.pay_type,o.status,o.create_time')
			->select()
			->toArray();

			$count = $this->service
			->alias('o')
			->join('lc_users u','u.user_id=o.user_id')
			->where($where)
			->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			$sell_user = $this->userModel->getUserInfo($value['sell_user_id']);
			$result[$key]['sell_user_nickname'] = $sell_user['nickname'];
			$result[$key]['sell_user_name'] = $sell_user['user_name'];
		}

		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-10-17T21:35:19+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getDownloadLog($page, $pageSize){
		$model = new DownloadLog();
		try {
			$result = $model->with(['user' => function($query){
				$query->field('user_id,nickname');
			}])->order('id','DESC')->page($page, $pageSize)->select()->toArray();

			$count = $model->getCount();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}
}