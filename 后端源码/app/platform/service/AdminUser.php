<?php 

namespace app\platform\service;

use app\platform\model\AdminUser as userModel;
use app\platform\validate\AdminUser as adminUserValidate;
use app\common\lib\Str;
use app\common\lib\Utils;
use app\common\lib\Log;
use think\facade\Cache;

/**
 * 
 */
class AdminUser
{

	private $model;
	
	public function __construct()
	{
		$this->model = new userModel();
	}

	/**
	 * 登录
	 * @Author   cendxia
	 * @DateTime 2022-10-15T18:22:24+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function loginEvent($data){
		// 使用验证器
		try {
            validate(adminUserValidate::class)->scene('login')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }


        // 前缀
        $prefix = smsPrefix('formlc_login');
        // 判断验证码是否正确
        $smsCode = cache($prefix.$data['phone'].'formlc_login');
        if(empty($smsCode) || $smsCode != $data['sms_code']){
            // throw new \think\Exception('验证码不正确');
        }

        // $data['password'] = md5($data['password']); // 临时

		$user = $this->model->getUserByLoginName($data['login_name']);
		if(empty($user)){
			throw new \think\Exception('用户名或密码错误');
		}

		if($data['phone'] != $user['phone']){
			throw new \think\Exception('手机号错误');
		}

		$password = Utils::adminUserEncryption($user['phone'], $data['password'], $user['salt']);

		if($password != $user['password']){
			throw new \think\Exception('用户名或密码错误！');
		}

		// 获取token
        $token = Str::getLoginToken($user['login_name']);

	    $redisData = [
        	'token' => $token,
        	'user_id' => $user['user_id'],
            'username' => $user['user_name'],
            'phone' => $user['phone'],
            'token_time' => time() + config('common.user_login_expires_time') // token有效时间
        ];

        // 清除登录信息
        self::logOutEvent($user['login_name']);

        $login_log = [
        	'user_id' => $user['user_id'],
        	'ip' => getUserIp()
        ];

        (new \app\platform\model\AdminUserLogin())->add($login_log);

        // 获取公共缓存前缀
        $prefix = config('common.common_prefix');

        // 单点登录使用
        cache($prefix.'login_admin_lc_'.$user['login_name'], $token, config('common.user_login_expires_time'));
        // 存储登录token
        $res = cache(config('redis.token_pre').$token, $redisData, config('common.user_login_expires_time'));
        return $res?$redisData:false;
	}


	/**
	 * 退出登录
	 * @Author   cendxia
	 * @DateTime 2024-02-03T16:08:35+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function loginOut($user_id){
		$user = $this->model->getUserByUid($user_id);
        if(empty($user)){
			throw new \think\Exception(config('language.textError'));
		}
		self::logOutEvent($user['login_name']);
		return true;
	}


	/**
	 * 退出登录事件
	 * @Author   cendxia
	 * @DateTime 2024-02-03T15:59:12+0800
	 * @param    [type]                   $login_name [description]
	 * @return   [type]                               [description]
	 */
	private static function logOutEvent($login_name){
		// 获取公共缓存前缀
        $prefix = config('common.common_prefix');
		// 判断是否登录过，如已登录，退出之前登录
        $header_token = cache($prefix.'login_admin_lc_'.$login_name);
        // 清除缓存
        if($header_token){
        	// 清除登录token，windows
        	Cache::delete(config('redis.token_pre').$header_token);
        	// 清除单点登录token
        	Cache::delete($prefix.'login_admin_lc_'.$login_name);

        	// // 清除登录token, linux
        	// cache(config('redis.token_pre').$header_token, false);
        	// // 清除单点登录token
        	// cache($prefix.'login_admin_lc_'.$login_name, false);
        }
	}


	/**
	 * 修改密码
	 * @Author   cendxia
	 * @DateTime 2024-02-03T13:45:26+0800
	 * @param    [type]                   $data    [description]
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function editPassword($data, $user_id){
		// 使用验证器
		try {
            validate(adminUserValidate::class)->scene('editPassword')->check($data);
        } catch (ValidateException $e) {
            throw new \think\Exception($e->getError());
        }

        // 临时
        // $data['password'] = md5($data['password']);
        // $data['newPassword'] = md5($data['newPassword']);
        // $data['rPassword'] = md5($data['rPassword']);

        $user = $this->model->getUserByUid($user_id);
        if(empty($user)){
			throw new \think\Exception(config('language.param_error'));
		}

		// 原密码
        $password = Utils::adminUserEncryption($user['phone'], $data['password'], $user['salt']);
        // 判断原密码是否一致
        if($password != $user['password']){
        	throw new \think\Exception('原密码不正确');
        }

        // 生成salt
        $salt = Str::getRandStr();
        // 新密码
        $newPassword = Utils::adminUserEncryption($user['phone'], $data['newPassword'], $salt);

        // 日志
		$logData = logData($user_id, '修改密码');
		Log::insertAdminLog($logData);
        return $this->model->editPassword($user_id, $newPassword, $salt);
	}
}