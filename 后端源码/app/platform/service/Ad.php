<?php 

	namespace app\platform\service;

	use app\platform\model\Ad as adModel;
	use app\platform\model\Advertisement;
	use app\common\lib\Log;

	/**
	 * 
	 */
	class Ad
	{
		
		private $obj = null;
		public $Advertisement = null;

		public function __construct(){
	        $this->obj = new adModel();
	        $this->Advertisement = new Advertisement();
	    }


	    /**
	     * 获取广告位列表
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:40:10+0800
	     * @return   [type]                   [description]
	     */
	    public function getAdvertisementSpaceList(){
	    	try {
	    		$result = $this->Advertisement->getAdvertisements();
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    	return $result->toArray();
	    }


	    /**
	     * 添加/修改广告位
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:40:05+0800
	     * @param    [type]                   $data    [description]
	     * @param    [type]                   $user_id [description]
	     * @return   [type]                            [description]
	     */
	    public function advertisementSpace($data, $user_id){
	    	if(empty($data['id'])){
	    		$title = '添加广告位';
	    		$type = 'inter';
	    	}else{
	    		$title = '修改广告位';
	    		$type = 'edit';
	    	}
	    	// Log::insertLog(logData($region_id, $user_id, $title, $data, $type));
	    	$data['region_id'] = $region_id;
	    	try {
	    		return $this->Advertisement->advertisementSpace($data);
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    }


	    /**
	     * 删除广告位
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:40:52+0800
	     * @param    [type]                   $id      [description]
	     * @param    [type]                   $user_id [description]
	     * @return   [type]                            [description]
	     */
	    public function delAdvertisementSpace($id, $user_id){
	    	Log::insertLog(logData($region_id, $user_id, '删除广告位', ['id'=>$id], 'delete'));
	    	try {
	    		return $this->Advertisement->delAdvertisementSpace($id);
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    }

	    /**
	     * 根据广告位id获取广告列表
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:41:20+0800
	     * @param    [type]                   $id [description]
	     * @return   [type]                       [description]
	     */
	    public function getAdList($id){
	    	$res = $this->getAdvertisementSpace($id, $user_id);
	    	Log::insertLog(logData($region_id, $user_id, '获取广告列表', ['ad_id'=>$id]));
	    	if(empty($res)) return [];
	    	try {
	    		return $this->obj->getAds($id);
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    }


	    /**
	     * 根据id获取广告位信息
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:41:39+0800
	     * @param    [type]                   $id      [description]
	     * @param    [type]                   $user_id [description]
	     * @return   [type]                            [description]
	     */
	    private function getAdvertisementSpace($id, $user_id){
	    	try {
	    		$result = $this->Advertisement->getAdvertisementSpace($id);
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    	Log::insertLog(logData($region_id, $user_id, '获取广告详情', ['id'=>$id]));
	    	if($result){
	    		return $result->toArray();
	    	}
	    	return $result;
	    }

	    /**
	     * 添加/修改广告
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:42:10+0800
	     * @param    [type]                   $data    [description]
	     * @param    [type]                   $user_id [description]
	     * @return   [type]                            [description]
	     */
	    public function adOperation($data, $user_id){
	    	if(empty($data['ad_id'])) throw new \think\Exception(config('language.param_error'));
	    	$res = $this->getAdvertisementSpace($data['ad_id'], $user_id);
	    	if(empty($res)) throw new \think\Exception(config('language.param_error'));
	    	if(empty($data['id'])){
	    		$title = '添加广告';
	    		$type = 'inter';
	    	}else{
	    		$title = '修改广告';
	    		$type = 'edit';
	    	}
	    	Log::insertLog(logData($region_id, $user_id, $title, $res, $type));
	    	try {
	    		return $this->obj->ad($data);
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    }

	    /**
	     * 删除广告
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:42:23+0800
	     * @param    [type]                   $ad_id   [description]
	     * @param    [type]                   $id      [description]
	     * @param    [type]                   $user_id [description]
	     * @return   [type]                            [description]
	     */
	    public function delAd($ad_id, $id, $user_id){
	    	$res = $this->getAdvertisementSpace($ad_id, $user_id);
	    	Log::insertLog(logData($region_id, $user_id, '删除广告', ['id'=>$id], 'delete'));
	    	if(empty($res)) return false;
	    	try {
	    		return $this->obj->delAd($id);
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    }


	    /**
	     * 广告详情
	     * @Author   cendxia
	     * @DateTime 2024-02-03T18:42:37+0800
	     * @param    [type]                   $id      [description]
	     * @param    [type]                   $user_id [description]
	     * @return   [type]                            [description]
	     */
	    public function getAdInfo($id, $user_id){
	    	try {
	    		$result = $this->obj->getAdInfo($id);
	    	} catch (\think\Exception $e) {
	    		throw new \think\Exception(config('language.mysql_error'));
	    	}
	    	Log::insertLog(logData($region_id, $user_id, '获取广告详情', ['id'=>$id]));
	    	if(empty($result)) return [];
	    	return $result->toArray();
	    }
	}

