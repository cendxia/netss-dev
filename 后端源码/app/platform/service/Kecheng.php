<?php

namespace app\platform\service;

use app\platform\model\Kecheng as kechengModel;
use app\platform\model\KechengChapters;
use app\platform\model\KechengDirectory;
use app\platform\model\KechengOrder;
use app\platform\model\Users;

/**
 * 
 */
class Kecheng 
{

	private $model;
	private $chapters;
	private $directory;
	private $order;
	
	public function __construct()
	{
		$this->model = new kechengModel();
		$this->chapters = new KechengChapters();
		$this->directory = new KechengDirectory();
		$this->order = new KechengOrder();
	}

	/**
	 * 格式化课程状态
	 * @Author   cendxia
	 * @DateTime 2023-07-03T13:56:31+0800
	 * @param    [type]                   $status [description]
	 * @return   [type]                           [description]
	 */
	private function keState($status){
		switch ($status) { // 0待审核 1审核通过 2审核未通过
			case -1:
				return '回收站';
				break;
			case 0:
				return '待审核';
				break;
			case 1:
				return '审核通过';
				break;
			case 2:
				return '审核未通过';
				break;			
			default:
				return '未知';
				break;
		}
	}

	/**
	 * 获取课程列表
	 * @Author   cendxia
	 * @DateTime 2023-07-03T13:16:34+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    [type]                   $keyword  [description]
	 * @return   [type]                             [description]
	 */
	public function getKechengList($page, $pageSize, $keyword){
		$result = $this->model->getList($page, $pageSize, $keyword);
		$count = $this->model->getCount(); // 待优化查询条件
		foreach($result as $key => $value){
			$result[$key]['state'] = self::keState($value['status']);
			$result[$key]['directoryNum'] = 0;
			$chapters = $this->chapters->getChapters(['ke_id'=>$value['id']]);
			$result[$key]['chaptersNum'] = count($chapters);
			if(count($chapters) > 0){
				$ids = array();
				foreach($chapters as $k => $v){
					array_push($ids, $v['id']);
				}
				$directory = $this->directory->getChapters([['ke_chapters_id','in',$ids]]);
				$result[$key]['directoryNum'] = count($directory);
			}
		}
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}


	/**
	 * 获取订单列表
	 * @Author   cendxia
	 * @DateTime 2023-07-03T16:15:34+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function gerOrderList($page, $pageSize){
		$result = $this->order->getList($page, $pageSize);
		$count = $this->order->getCount();
		$userModel = new Users();
		foreach($result as $key => $value){
			$result[$key]['ke_cover'] = unserialize($value['ke_cover']);
			$user = $userModel->getUserInfo($value['user_id']);
			$result[$key]['user_nickname'] = $user['nickname'];
			$sellUser = $userModel->getUserInfo($value['sell_user_id']);
			$result[$key]['sell_user_nickname'] = $sellUser['nickname'];
			$result[$key]['sell_user_name'] = $sellUser['user_name'];
			$result[$key]['pay_status'] = $value['status'] == 1 ? '已支付' : '未支付';
			unset($result[$key]['status']);
		}
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}
}