<?php 

namespace app\platform\service;

use app\platform\model\Users as userModel;
use app\platform\model\UserWallet;
use app\platform\model\Ziyuan;
use app\platform\model\Order;
use app\platform\model\UserTixian;
use app\platform\model\UserVip;
use app\common\lib\Log;
use app\common\lib\Str;

/**
 * 
 */
class User
{

	private $model;
	private $wallet;
	private $ziyuan;
	
	public function __construct()
	{
		$this->model = new userModel();
		$this->wallet = new UserWallet();
		$this->ziyuan = new Ziyuan();
	}

	/**
	 * 用户列表
	 * @Author   cendxia
	 * @DateTime 2022-10-16T18:43:17+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getUserList($page, $pageSize, $user_name, $type, $phone){
		if(!empty($type) && $type != 1 && $type != 2 && $type != 3){
			throw new \think\Exception(config('language.param_error'));
		}

		$where = [];
		if (!empty($user_name)) {
            $where[] = [
                ['user_name', 'like', '%' . $user_name . '%']
            ];
        }
        if (!empty($type)) {
            $where[] = [
                ['type', '=', $type]
            ];
        }
        if (!empty($phone)) {
            $where[] = [
                ['phone', '=', $phone]
            ];
        }

		$result = $this->model->getList($page, $pageSize, $where);
		$count = $this->model->getUserCount($where);
		$pageSum = intval(ceil($count / config('common.page_size')));

		$orderModel = new Order();

		foreach($result as $key => $value){
			// 获取余额
			$userWallet = $this->wallet->getByUserId($value['user_id']);
			$result[$key]['money'] = $userWallet['money'];

			// 获取资源数
			$result[$key]['ziyuan_num'] = $this->ziyuan->getUserZiyuanCount($value['user_id']);

			$result[$key]['state'] = self::userState($value['state']);
			$result[$key]['type'] = self::userType($value['type']);
			$result[$key]['order_num'] = $orderModel->where(['user_id'=>$value['user_id']])->count();
		}

		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 用户状态
	 */
	private function userState($state){
		if($state == 0){
			return '正常';
		}else if($state == 1){
			return '已被禁用';
		}else{
			return '未知状态';
		}
	}

	/**
	 * 用户类型
	 * @Author   cendxia
	 * @DateTime 2022-10-16T19:27:52+0800
	 * @param    [type]                   $type [description]
	 * @return   [type]                         [description]
	 */
	private function userType($type){
		switch ($type) {
			case 1:
				return '普通用户';
				break;
			case 2:
				return '创作者';
				break;
			case 3:
				return '创作者申请中';
				break;
			default:
				return '未知类型';
				break;
		}
	}

	/**
	 * 获取申请创作者记录
	 * @Author   cendxia
	 * @DateTime 2022-10-16T19:38:01+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getMpUser($page, $pageSize){
		$model = new \app\platform\model\UserRegisterMp();
		try {
			$result = $model
				->alias('m')
				->join('lc_users u','u.user_id = m.user_id')
				->order('m.status','ASC')
				->order('m.create_time','DESC')
				->field('m.id,m.user_id,u.avatar,u.nickname,u.user_name,u.phone,m.id_card,m.email,m.qq,m.fail_reason,m.audit_time,m.status,m.create_time register_time,u.create_time')
				->select()
				->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			$result[$key]['state'] = self::registerMpState($value['status']);
		}

		$count = $model->getCount();
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 申请创作者状态
	 * @Author   cendxia
	 * @DateTime 2022-10-16T19:55:46+0800
	 * @param    [type]                   $state [description]
	 * @return   [type]                          [description]
	 */
	private function registerMpState($state){
		switch ($state) {
			case 0:
				return '待审核';
				break;
			case 1:
				return '已通过';
				break;
			case 2:
				return '未通过';
				break;
			default:
				return '未知状态';
				break;
		}
	}

	/**
	 * 用户提现
	 * @Author   cendxia
	 * @DateTime 2022-10-17T21:51:51+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getUserTixian($page, $pageSize){
		$model = new UserTixian();
		try {
			$result = $model->with(['user' => function($query){
				$query->field('user_id,nickname');
			}])->order('create_time','DESC')->page($page, $pageSize)->select()->toArray();
			$count = $model->getCount();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		foreach($result as $key => $value){
			$result[$key]['pay_type'] = Str::payType($value['type']);
		}
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}

	/**
	 * 提现审核
	 * @Author   cendxia
	 * @DateTime 2022-10-18T13:36:11+0800
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $id           [description]
	 * @param    [type]                   $user_id      [description]
	 * @param    [type]                   $status       [description]
	 * @param    [type]                   $fail         [description]
	 * @return   [type]                                 [description]
	 */
	public function tixianAudit($this_user_id, $id, $user_id, $status, $fail){
		if(empty($status) || ($status != 1 && $status != 2)){
			throw new \think\Exception(config('language.param_error'));
		}
		if($status == 2 && empty($fail)){
			throw new \think\Exception('请填写拒接原因');
		}

		$model = new UserTixian();
		$result = $model->getInfo($id, $user_id);
		if(empty($result) || $result['status'] != 0){
			throw new \think\Exception('当前状态不可提现');
		}
		$upData = [
			'status' => $status,
			'fail' => $status == 2 ? $fail : '',
			'audit_time' => date('Y-m-d H:i:s')
		];
		// 日志
		$logData = logData($this_user_id, '审核用户提现', $upData);
		$model->startTrans();  // 开启事务
		try {
			if($status == 2){
				// 恢复余额
				$this->wallet->increase($user_id, 'current_money', $result['money']);
				// 审核不通过，返还用户余额
				$userWallet = $this->wallet->getByUserId($user_id);
				// 余额使用记录
				$walletLog = [
					'user_id' => $user_id,
					'project' => 'user_tixian',
					'project_id' => $id,
					'money' => $result['money'],
					'after' => $userWallet['money'], // 更新后余额
					'type' => '+',
					'source' => '提现被驳回'
				];
				$walletLogModel = new \app\platform\model\UserWalletLog;
				$walletLogModel->add($walletLog);
			}
			// 日志
			Log::insertAdminLog($logData);
			$model->edit($id, $user_id, $upData);
			$model->commit(); // 提交事务
			return true;
		} catch (\think\Exception $e) {
			$model->rollback(); // 事务回滚
			return false;
		}
	}

	/**
	 * 创作者审核
	 * @Author   cendxia
	 * @DateTime 2022-10-18T21:20:12+0800
	 * @param    [type]                   $this_user_id [description]
	 * @param    [type]                   $id           [description]
	 * @param    [type]                   $user_id      [description]
	 * @param    [type]                   $type         [description]
	 * @param    [type]                   $fail         [description]
	 * @return   [type]                                 [description]
	 */
	public function mpAudit($this_user_id, $id, $user_id, $type, $fail){
		if(empty($type) || ($type != 1 && $type != 2)){
			throw new \think\Exception(config('language.param_error'));
		}
		if($type == 2 && empty($fail)){
			throw new \think\Exception('请填写拒接原因');
		}
		$user = $this->model->getUserInfo($user_id);
		if(empty($user) || $user['type'] != 3 || $user['state'] != 0){
			throw new \think\Exception('当前状态不可操作');
		}

		$model = new \app\platform\model\UserRegisterMp;
		$info = $model->getInfo($id, $user_id);
		if(empty($info) || $info['status'] != 0){
			throw new \think\Exception('当前状态不可操作');
		}

		$upData = [
			'status' => $type == 1 ? 1 : 2,
			'fail_reason' => $fail,
			'audit_time' => date('Y-m-d H:i:s')
		];

		$userType = $type == 1 ? 2 : 1;

		// 日志
		$logData = logData($this_user_id, '创作者申请审核', $upData);

		$model->startTrans();  // 开启事务
		try {
			$this->model->edit($user_id, ['type'=>$userType]);
			// 日志
			Log::insertAdminLog($logData);
			$model->edit($id, $user_id, $upData);
			$model->commit(); // 提交事务
			return true;			
		} catch (\think\Exception $e) {
			$model->rollback(); // 事务回滚
			return false;
		}
	}


	/**
	 * 获取用户反馈信息
	 * @Author   cendxia
	 * @DateTime 2024-03-26T10:28:03+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getUserFeedback($page, $pageSize){
		$model = new \app\platform\model\UserFeedback();
		$result = $model->getList($page, $pageSize);
		$count = $model->getCount();
		foreach($result as $key => $value){
			$user = $this->model->getUserInfo($value['user_id']);
			$result[$key]['nickname'] = $user['nickname'];
		}
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);

	}


	/**
	 * 获取VIP订单列表
	 * @Author   cendxia
	 * @DateTime 2024-03-26T09:22:31+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getVipOrderList($page, $pageSize){
		$model = new UserVip();
		$result = $model->getList($page, $pageSize);
		$count = $model->getCount();
		foreach($result as $key => $value){
			$user = $this->model->getUserInfo($value['user_id']);
			$result[$key]['nickname'] = $user['nickname'];
			if($value['end_time'] >= date('Y-m-d H:i:s')){
				$result[$key]['status'] = 1;
			}else{
				$result[$key]['status'] = 0;
			}
		}
		$pageSum = intval(ceil($count / config('common.page_size')));
		return dataPage($page, $pageSize, $pageSum, $count, $result);
	}
}