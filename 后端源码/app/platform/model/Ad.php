<?php 

namespace app\platform\model;

use think\Model;

/**
 * 广告
 */
class Ad extends Model
{
	
	/**
	 * 根据广告位id获取广告信息
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:44:50+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getAds($id){
		return $this->where(['ad_id'=>$id])->select()->toArray();
	}

	/**
	 * 添加/编辑广告
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:44:55+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function ad($data){
		// 判断是否有id
		if (isset($data['id'])) {
			$id = $data['id'];
			unset($data['id']);
			return $this->where(['id'=>$id])->save($data);
		}
		return $this->insertGetId($data);
	}


	/**
	 * 删除广告
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:45:00+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function delAd($id){
		return $this->where(['id'=>$id])->delete();
	}

	/**
	 * 详情
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:45:08+0800
	 * @param    [type]                   $region_id [description]
	 * @param    [type]                   $id        [description]
	 * @return   [type]                              [description]
	 */
	public function getAdInfo($region_id, $id){
		return $this->where(['id'=>$id,'region_id'=>$region_id])->find();
	}
}