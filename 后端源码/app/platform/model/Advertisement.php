<?php 

namespace app\platform\model;

use think\Model;

/**
 *  广告位
 */
class Advertisement extends Model
{
	
	/**
	 * 获取广告位列表
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:45:39+0800
	 * @param    [type]                   $region_id [description]
	 * @return   [type]                              [description]
	 */
	public function getAdvertisements($region_id){
		return $this->where(['region_id'=>$region_id])->select();
	}


	/**
	 * 根据id获取广告位信息
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:45:47+0800
	 * @param    [type]                   $region_id [description]
	 * @param    [type]                   $id        [description]
	 * @return   [type]                              [description]
	 */
	public function getAdvertisementSpace($region_id, $id){
		return $this->where(['region_id'=>$region_id,'id'=>$id])->find();
	}

	/**
	 * 添加/修改广告位
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:46:00+0800
	 * @param    [type]                   $region_id [description]
	 * @param    [type]                   $data      [description]
	 * @return   [type]                              [description]
	 */
	public function advertisementSpace($region_id, $data){
		// 判断是否有id
		if (isset($data['id'])) {
			$id = $data['id'];
			unset($data['id']);
			return $this->where(['region_id'=>$region_id, 'id'=>$id])->save($data);
		}
		return $this->insertGetId($data);
	}

	/**
	 * 删除广告位
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:46:07+0800
	 * @param    [type]                   $region_id [description]
	 * @param    [type]                   $id        [description]
	 * @return   [type]                              [description]
	 */
	public function delAdvertisementSpace($region_id, $id){
		return $this->where(['region_id'=>$region_id,'id'=>$id])->delete();
	}
}