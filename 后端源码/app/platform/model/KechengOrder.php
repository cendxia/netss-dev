<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class KechengOrder extends Model
{
	
	/**
	 * 获取订单列表
	 * @Author   cendxia
	 * @DateTime 2023-07-03T16:39:58+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize, $where=[]){
		try {
			return $this->where($where)->order('create_time', 'DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2023-07-03T16:40:58+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCount($where=[]){
		try {
			return $this->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}