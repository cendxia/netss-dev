<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class Order extends Model
{
	
	/**
	 * 列表
	 * @Author   cendxia
	 * @DateTime 2022-10-17T18:35:03+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize, $where=[]){
		try {
			return $this->where($where)->order('create_time','DESC')->page($page, $pageSize)->field('order_id,sell_user_id,user_id,ziyuan_title,ziyuan_cover,download_num,price,pay_type,status,create_time')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取下单用户数
	 * @Author   cendxia
	 * @DateTime 2022-11-09T20:37:33+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getByUserCount($where=[]){
		try {
			return $this->where($where)->group('user_id')->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 订单总金额
	 * @Author   cendxia
	 * @DateTime 2022-11-09T20:40:53+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getByTotalPrice($where=[]){
		try {
			return $this->where($where)->sum('price');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 查询条数
	 * @Author   cendxia
	 * @DateTime 2022-11-09T20:44:36+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCount($where=[]){
		try {
			return $this->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取当日数据
	 * @Author   cendxia
	 * @DateTime 2022-11-27T23:22:32+0800
	 * @param    [type]                   $date [description]
	 * @return   [type]                         [description]
	 */
	public function getCountDate($date){
		try {
			return $this->where('create_time', '>=', $date.' 00:00:00')->where('create_time', '<', $date.' 23:59:59')->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取每日资源订单金额
	 * @Author   cendxia
	 * @DateTime 2024-03-27T14:14:07+0800
	 * @param    [type]                   $date [description]
	 * @return   [type]                         [description]
	 */
	public function getDailyOrderPrice($date){
		try {
			return $this->where('pay_time', '>=', $date.' 00:00:00')->where('pay_time', '<', $date.' 23:59:59')->sum('price');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}