<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class ZiyuanCategory extends Model
{
	
	/**
	 * 获取列表
	 * @Author   cendxia
	 * @DateTime 2022-10-16T20:25:30+0800
	 * @return   [type]                   [description]
	 */
	public function getList($where=[]){
		try {
			return $this->where($where)->order('sort', 'ASC')->field('id,pid,title,sort,show,home_show,icon,url,keywords,description')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取详情
	 * @Author   cendxia
	 * @DateTime 2022-10-17T17:33:24+0800
	 * @param    [type]                   $category_id [description]
	 * @return   [type]                                [description]
	 */
	public function getInfo($category_id){
		try {
			$result = $this->where(['id'=>$category_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 添加/修改类别
	 * @Author   cendxia
	 * @DateTime 2022-10-17T18:08:20+0800
	 * @param    [type]                   $data [description]
	 */
	public function category($data){
		try {
			if(empty($data['id'])){
				// 添加
				return $this->insertGetId($data);
			}else{
				// 修改
				$id = $data['id'];
				unset($data['id']);
				return $this->where(['id'=>$id])->save($data);
			}
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}