<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class Kecheng extends Model
{
	
	/**
	 * 获取课程订单
	 * @Author   cendxia
	 * @DateTime 2023-07-03T13:22:16+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    string                   $keyword  [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize, $keyword=''){
		try {
			return $this->alias('k')
				->join('users u', 'k.user_id=u.user_id')
				->where('title|label', 'like', "%{$keyword}%")
				->where('k.status', '<>', -1)
				->order('k.create_time', 'DESC')
				->field('k.id,k.title,k.study_num,k.status,k.create_time,k.update_time,u.nickname,u.user_name')
				->page($page, $pageSize)
				->select()
				->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2023-07-03T13:33:06+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCount($where=[]){
		try {
			return $this->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
}