<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class AdminUserLogin extends Model
{
	/**
	 * 添加日志
	 * @Author   cendxia
	 * @DateTime 2022-10-15T19:12:05+0800
	 * @param    [type]                   $data [description]
	 */
	public function add($data){
		try {
			return $this->insertGetId($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}