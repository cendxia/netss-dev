<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class Users extends Model
{
	
	/**
	 * 用户列表
	 * @Author   cendxia
	 * @DateTime 2022-10-16T18:43:45+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize, $where=[]){
		try {
			return $this->where($where)->order('create_time', 'DESC')->page($page, $pageSize)->field('user_id,login_name,nickname,avatar,user_name,phone,state,type,create_time')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 查询用户条数
	 * @Author   cendxia
	 * @DateTime 2022-10-16T18:47:07+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getUserCount($where=[]){
		try {
			return $this->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取用户详情
	 * @Author   cendxia
	 * @DateTime 2022-10-17T17:32:04+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getUserInfo($user_id){
		try {
			$result = $this->where(['user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 修改
	 * @Author   cendxia
	 * @DateTime 2022-10-18T21:42:34+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @param    array                    $where   [description]
	 * @return   [type]                            [description]
	 */
	public function edit($user_id, $data, $where=[]){
		if(empty($data)) return false;
		try {
			return $this->where(['user_id'=>$user_id])->where($where)->save($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 查询待创作者数量
	 * @Author   cendxia
	 * @DateTime 2024-02-27T18:56:00+0800
	 * @return   [type]                   [description]
	 */
	public function getMpNotAuditUserNum(){
		try {
			return $this->where(['type'=>3])->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}