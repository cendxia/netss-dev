<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class UserLogin extends Model
{
	
	/**
	 * 获取列表
	 * @Author   cendxia
	 * @DateTime 2023-06-30T22:15:55+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize){
		try {
			return $this
				->alias('l')
				->join('users u','l.user_id=u.user_id')
				->order('l.create_time', 'DESC')
				->page($page, $pageSize)
				->field('l.id,l.user_id,u.nickname,l.mode,l.platform,l.ip,l.create_time')
				->select()
				->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2023-06-30T22:17:49+0800
	 * @return   [type]                   [description]
	 */
	public function getCount(){
		try {
			return $this->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}