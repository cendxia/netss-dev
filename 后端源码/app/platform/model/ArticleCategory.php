<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class ArticleCategory extends Model
{
	
	/**
	 * 获取类别列表
	 * @Author   cendxia
	 * @DateTime 2022-10-25T12:51:52+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys(){
		return $this->order('sort','DESC')->field('id,name,sort,type,show')->select()->toArray();
	}

	/**
	 * 添加/修改操作
	 * @Author   cendxia
	 * @DateTime 2022-02-26T12:54:01+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function category($data){
		if(isset($data['id'])){
			// 修改操作
			$id = $data['id'];
			unset($data['id']);
			return $this->where(['id'=>$id])->save($data);
		}
		return $this->insertGetId($data);
	}

	/**
	 * 删除分类
	 * @Author   cendxia
	 * @DateTime 2022-10-25T12:44:19+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function delCategory($id){
		return $this->where(['id'=>$id])->where('id', '>', 3)->delete();
	}
}