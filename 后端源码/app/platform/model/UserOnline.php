<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class UserOnline extends Model
{
	
	/**
	 * 获取每天在线数量
	 * distinct 为去重
	 * @Author   cendxia
	 * @DateTime 2022-04-13T10:54:50+0800
	 * @param    [type]                   $region_id [description]
	 * @param    [type]                   $date      [description]
	 * @return   [type]                              [description]
	 */
	public function getDayLogCount($date){
		try {
			return $this->where('create_time', '>=', $date.' 00:00:00')->where('create_time', '<', $date.' 23:59:59')->count('distinct user_id');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('common.mysql_error'));
		}
	}
}