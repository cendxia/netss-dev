<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class KechengChapters extends Model
{
	
	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2023-07-03T14:08:12+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getCount($where=[]){
		try {
			return $this->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取数据
	 * @Author   cendxia
	 * @DateTime 2023-07-03T15:56:39+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getChapters($where=[]){
		try {
			return $this->where($where)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
}