<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class AdminUser extends Model
{
	
	/**
	 * 根据登录名查询用户信息
	 * @Author   cendxia
	 * @DateTime 2022-10-15T18:37:15+0800
	 * @param    [type]                   $login_name [description]
	 * @return   [type]                               [description]
	 */
	public function getUserByLoginName($login_name){
		try {
			$result = $this->where(['login_name'=>$login_name])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 根据手机号查询用户信息
	 * @Author   cendxia
	 * @DateTime 2024-01-19T21:04:27+0800
	 * @param    [type]                   $phone [description]
	 * @return   [type]                          [description]
	 */
	public function getByPhone($phone){
		if(empty($phone)) throw new \think\Exception(config('language.param_error'));		
		try {
			$result = $this->where(['phone'=>$phone])->field('user_id,login_name,status')->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 根据用户id查询用户信息
	 * @Author   cendxia
	 * @DateTime 2024-02-03T14:14:00+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getUserByUid($user_id){
		if(empty($user_id)) throw new \think\Exception(config('language.param_error'));		
		try {
			$result = $this->where(['user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 修改密码
	 * @Author   cendxia
	 * @DateTime 2024-02-03T14:28:08+0800
	 * @param    [type]                   $user_id  [description]
	 * @param    [type]                   $password [description]
	 * @param    [type]                   $salt     [description]
	 * @return   [type]                             [description]
	 */
	public function editPassword($user_id, $password, $salt){
		try {
			return $this->where(['user_id'=>$user_id])->save(['password'=>$password,'salt'=>$salt]);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}