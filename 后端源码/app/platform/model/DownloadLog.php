<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class DownloadLog extends Model
{
	
	/**
	 * 列表
	 * @Author   cendxia
	 * @DateTime 2022-10-17T21:37:24+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize){
		try {
			return $this->order('create_time', 'DESC')->page($page, $pageSize)->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	// 关联
	public function user(){
		return $this->hasOne(users::class, 'user_id', 'user_id');
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-10-29T16:25:39+0800
	 * @return   [type]                   [description]
	 */
	public function getCount(){
		try {
			return $this->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取每日下载数
	 * @Author   cendxia
	 * @DateTime 2022-11-27T23:24:41+0800
	 * @param    [type]                   $date [description]
	 * @return   [type]                         [description]
	 */
	public function getCountDate($date){
		try {
			return $this->where('create_time', '>=', $date.' 00:00:00')->where('create_time', '<', $date.' 23:59:59')->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('common.mysql_error'));
		}
	}
}