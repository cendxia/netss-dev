<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class UserTixian extends Model
{
	
	// 关联
	public function user(){
		return $this->hasOne(users::class, 'user_id', 'user_id');
	}

	/**
	 * 获取详情
	 * @Author   cendxia
	 * @DateTime 2022-10-18T17:52:48+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getInfo($id, $user_id){
		try {
			$result = $this->where(['id'=>$id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 修改操作
	 * @Author   cendxia
	 * @DateTime 2022-10-18T18:53:44+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function edit($id, $user_id, $data){
		try {
			return $this->where(['id'=>$id,'user_id'=>$user_id])->save($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-11-04T20:47:22+0800
	 * @return   [type]                   [description]
	 */
	public function getCount(){
		try {
			return $this->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取提现金额
	 * @Author   cendxia
	 * @DateTime 2022-11-09T20:50:14+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function getByTotalPrice($where=[]){
		try {
			return $this->where($where)->sum('money');
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}