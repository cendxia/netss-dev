<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class Article extends Model
{
	
	/**
	 * 关联分类
	 * @Author   cendxia
	 * @DateTime 2022-02-26T13:21:45+0800
	 */
	public function ArticleCategory(){
		return $this->hasOne(ArticleCategory::class, 'id', 'category_id');
	}

	/**
	 * 获取文章总条数
	 * @Author   cendxia
	 * @DateTime 2022-10-25T13:16:03+0800
	 * @param    array                    $where [description]
	 * @return   [type]                          [description]
	 */
	public function articleCount($where=[]){
		return $this->where($where)->count();
	}

	/**
	 * 获取文章详情
	 * @Author   cendxia
	 * @DateTime 2022-10-25T13:16:15+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function getArticlesInfo($id){
		try {
			$this->where(['id'=>$id])->inc('read_num')->update(); // 自增阅读数
			$result = $this->where(['id'=>$id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 添加/修改文章
	 * @Author   cendxia
	 * @DateTime 2022-02-26T14:40:28+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function article($data){
		if(isset($data['id'])){
			$id = $data['id'];
			unset($data['id']);
			return $this->where(['id'=>$id])->save($data);
		}
		return $this->insertGetId($data);
	}

	/**
	 * 删除文章
	 * @Author   cendxia
	 * @DateTime 2022-10-25T13:16:41+0800
	 * @param    [type]                   $id [description]
	 * @return   [type]                       [description]
	 */
	public function delArticle($id){
		return $this->where(['id'=>$id])->delete();
	}
	
}