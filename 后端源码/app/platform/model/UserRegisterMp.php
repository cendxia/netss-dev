<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class UserRegisterMp extends Model
{
	

	/**
	 * 获取总条数
	 * @Author   cendxia
	 * @DateTime 2022-10-16T19:44:02+0800
	 * @return   [type]                   [description]
	 */
	public function getCount(){
		try {
			return $this->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取详情
	 * @Author   cendxia
	 * @DateTime 2022-10-18T21:21:18+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getInfo($id, $user_id){
		try {
			$result = $this->where(['id'=>$id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}

		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 修改
	 * @Author   cendxia
	 * @DateTime 2022-10-18T21:40:31+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function edit($id, $user_id, $data){
		try {
			return $this->where(['id'=>$id,'user_id'=>$user_id])->save($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}