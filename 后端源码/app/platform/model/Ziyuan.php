<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class Ziyuan extends Model
{
	/**
	 * 获取资源列表, 废弃
	 * @Author   cendxia
	 * @DateTime 2022-10-17T17:20:37+0800
	 * @param    [type]                   $page     [description]
	 * @param    [type]                   $pageSize [description]
	 * @param    array                    $where    [description]
	 * @return   [type]                             [description]
	 */
	public function getList($page, $pageSize, $where=[]){
		try {
			return $this->where('status', '<>', -1)->where($where)->order('create_time','DESC')->page($page, $pageSize)->field('id,user_id,category_id,title,cover,price,status,collect_num,download_num,report_num,read_num,praise_num,create_time,ziyuan_update_time,fail_reason,audit_time')->select()->toArray();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取资源总数
	 * @Author   cendxia
	 * @DateTime 2022-10-17T17:28:52+0800
	 * @param    array                    $wher [description]
	 * @return   [type]                         [description]
	 */
	public function getCount($where=[]){
		try {
			return $this->where($where)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
	
	/**
	 * 获取用户资源数
	 * @Author   cendxia
	 * @DateTime 2022-10-16T19:17:06+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getUserZiyuanCount($user_id){
		try {
			return $this->where(['user_id'=>$user_id])->where('status', '<>', -1)->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 获取资源详情
	 * @Author   cendxia
	 * @DateTime 2022-10-18T22:10:35+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getById($id, $user_id){
		try {
			$result = $this->where(['id'=>$id,'user_id'=>$user_id])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}

	/**
	 * 修改
	 * @Author   cendxia
	 * @DateTime 2022-10-18T22:22:31+0800
	 * @param    [type]                   $id      [description]
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function edit($id, $user_id, $data){
		try {
			return $this->where(['id'=>$id,'user_id'=>$user_id])->save($data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 获取当日数据
	 * @Author   cendxia
	 * @DateTime 2022-11-27T23:17:12+0800
	 * @param    [type]                   $date [description]
	 * @return   [type]                         [description]
	 */
	public function getCountDate($date){
		try {
			return $this->where('create_time', '>=', $date.' 00:00:00')->where('create_time', '<', $date.' 23:59:59')->count();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}
}