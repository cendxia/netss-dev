<?php 

namespace app\platform\model;

use think\Model;

/**
 * 
 */
class UserWallet extends Model
{
	/**
	 * 获取用户钱包
	 * @Author   cendxia
	 * @DateTime 2022-10-16T18:57:32+0800
	 * @param    [type]                   $user_id [description]
	 * @return   [type]                            [description]
	 */
	public function getByUserId($user_id){
		try {
			$result = $this->where(['user_id'=>$user_id])->field('money,current_money,retain_money,total_profit')->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
		if(empty($result)) return [];
		return $result->toArray();
	}


	/**
	 * 扣费
	 * @Author   cendxia
	 * @DateTime 2022-08-20T17:02:16+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $field   [description]
	 * @param    [type]                   $price   [description]
	 * @return   [type]                            [description]
	 */
	public function reduce($user_id, $field, $price){
		if($field != 'current_money' && $field != 'retain_money'){
			throw new \think\Exception(config('language.param_error'));
		}
		return $this->where(['user_id'=>$user_id])->dec($field, $price)->dec('money', $price)->update();
	}


	/**
	 * 增费
	 * @Author   cendxia
	 * @DateTime 2022-08-20T17:02:08+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $field   [description]
	 * @param    [type]                   $price   [description]
	 * @return   [type]                            [description]
	 */
	public function increase($user_id, $field, $price){
		if($field != 'current_money' && $field != 'retain_money'){
			throw new \think\Exception(config('language.param_error'));
		}
		return $this->where(['user_id'=>$user_id])->inc($field, $price)->inc('money', $price)->update();
	}
}