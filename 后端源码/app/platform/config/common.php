<?php 

return [
	'common_prefix' => 'lcsadmin_',	// 公共缓存前缀
	'password_admin_prefix' => 'lcs_',  // 后管密码前缀, 谨慎修改
	'user_login_expires_time' => 360000,  // token有效期，单位为秒
	'login_ip' => [522301], // 登录ip范围限制
];