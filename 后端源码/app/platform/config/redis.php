<?php 

return [
	'code_pre' => 'lckj_code_pre_',  // 验证码前缀
	'login_ip' => 'lckj_admin_login_ip_pre_', // ip前缀
	'code_expire' => 60,  // 验证码有限时间
	'token_pre' => 'lckj_token_pre_'
];