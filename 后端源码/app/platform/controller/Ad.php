<?php 

namespace app\platform\controller;

use app\platform\service\Ad as adBus;


/**
 * 
 */
class Ad extends AuthBase
{
	
	/**
	 * 获取广告位列表
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:50:51+0800
	 * @return   [type]                   [description]
	 */
	public function getAdvertisementSpaceList(){
		$adBus = new adBus();
		$result = $adBus->getAdvertisementSpaceList();
		return show($result);
	}

	/**
	 * 添加/修改广告位
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:50:55+0800
	 * @return   [type]                   [description]
	 */
	public function advertisementSpace(){
		$adBus = new adBus();
		$result = $adBus->advertisementSpace(input('post.'), $this->user_id);
		if($result){
			return R(config('language.textSuccess'));
		}
		return resError(config('language.textError'));
	}

	/**
	 * 删除广告位
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:51:00+0800
	 * @return   [type]                   [description]
	 */
	public function delAdvertisementSpace(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		if(empty($id)){
			return resError(config('language.param_error'));
		}
		$adBus = new adBus();
		$result = $adBus->delAdvertisementSpace($id, $this->user_id);
		if($result){
			return R(config('language.textSuccess'));
		}
		return resError(config('language.textError'));
	}

	/**
	 * 根据广告位id获取广告信息
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:51:05+0800
	 * @return   [type]                   [description]
	 */
	public function getAdList(){
		$id = $this->request->param('id', null, 'intval');
		if(empty($id)){
			return resError(config('language.param_error'));
		}
		$adBus = new adBus();
		$result = $adBus->getAdList($id, $this->user_id);
		return show($result);
	}

	/**
	 * 添加/修改广告
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:51:10+0800
	 * @return   [type]                   [description]
	 */
	public function adOperation(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$data = input('post.');
		$adBus = new adBus();
		$result = $adBus->adOperation($data, $this->user_id);
		if($result){
			return R(config('language.textSuccess'));
		}
		return resError(config('language.textError'));
	}

	/**
	 * 删除广告
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:51:15+0800
	 * @return   [type]                   [description]
	 */
	public function delAd(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$ad_id = $this->request->param('ad_id', null, 'intval');
		if(empty($id) || empty($ad_id)){
			return resError(config('language.param_error'));
		}
		$adBus = new adBus();
		$result = $adBus->delAd($ad_id, $id, $this->user_id);
		if($result){
			return R(config('language.textSuccess'));
		}
		return resError(config('language.textError'));
	}

	/**
	 * 广告详情
	 * @Author   cendxia
	 * @DateTime 2024-02-03T17:51:21+0800
	 * @return   [type]                   [description]
	 */
	public function getAdInfo(){
		$id = $this->request->param('id', null, 'intval');
		$adBus = new adBus();
		$result = $adBus->getAdInfo($id, $this->user_id);
		return show($result);
	}

}