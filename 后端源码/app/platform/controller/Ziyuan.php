<?php 

namespace app\platform\controller;

use app\platform\service\Ziyuan as service;

/**
 * 
 */
class Ziyuan extends Auth
{
	/**
	 * 获取资源分类
	 * @Author   cendxia
	 * @DateTime 2022-10-16T20:20:59+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys(){
		$service = new service();
		$result = $service->getCategorys();
		return show($result);
	}

	/**
	 * 添加/修改资源分类
	 * @Author   cendxia
	 * @DateTime 2022-10-17T17:40:44+0800
	 * @return   [type]                   [description]
	 */
	public function ziyuanCategory(){
		if(!request()->isPost()){
			return requestError();
		}
		$service = new service();
		$result = $service->ziyuanCategory(input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 删除资源分类
	 * @Author   cendxia
	 * @DateTime 2022-10-25T21:05:51+0800
	 * @return   [type]                   [description]
	 */
	public function delCategory(){
		if(!request()->isPost()){
			return requestError();
		}
		$category_id = $this->request->param('category_id', null, 'intval');
		$service = new service();
		$result = $service->delCategory($category_id);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 获取资源列表
	 * @Author   cendxia
	 * @DateTime 2022-10-17T17:16:01+0800
	 * @return   [type]                   [description]
	 */
	public function getZiyuanList(){
		$category_id = $this->request->param('category_id', null, 'intval');
		$ziyuan_title = $this->request->param('ziyuan_title', null, 'trim');
		$user_name = $this->request->param('user_name', null, 'trim');
		$status = $this->request->param('status', null, 'trim');

		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getZiyuanList($page, $pageSize, $category_id, $ziyuan_title, $user_name, $status);
		return show($result);
	}

	/**
	 * 资源审核
	 * @Author   cendxia
	 * @DateTime 2022-10-18T22:06:37+0800
	 * @return   [type]                   [description]
	 */
	public function ziyuanAudit(){
		if(!request()->isPost()){
			return requestError();
		}

		$id = $this->request->param('id', null, 'intval');
		$user_id = $this->request->param('user_id', null, 'intval');		
		$status = $this->request->param('status', null, 'intval');
		$fail = $this->request->param('fail_reason', null, 'trim');
		$service = new service();
		$result = $service->ziyuanAudit($this->user_id, $id, $user_id, $status, $fail);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 获取资源详情
	 * @Author   cendxia
	 * @DateTime 2022-12-13T12:19:16+0800
	 * @return   [type]                   [description]
	 */
	public function getZiyuanInfo(){
		$id = $this->request->param('id', null, 'intval');
		$info = $this->request->param('info', false, 'boolean');
		$service = new service();
		$result = $service->getZiyuanInfo($id, $info);
		return show($result);
	}
}