<?php 

namespace app\platform\controller;
use app\platform\service\Article as service;

/**
 * 
 */
class Article extends Auth
{
	/**
	 * 获取类别列表
	 * @Author   cendxia
	 * @DateTime 2022-02-26T12:11:54+0800
	 * @return   [type]                   [description]
	 */
	public function getCategorys(){
		$service = new service();
		$result = $service->getCategorys();
		return show($result);
	}


	/**
	 * 添加/修改分类
	 * @Author   cendxia
	 * @DateTime 2022-02-26T12:22:18+0800
	 * @return   [type]                   [description]
	 */
	public function category(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$service = new service();
		$result = $service->category($this->user_id, input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 删除分类
	 * @Author   cendxia
	 * @DateTime 2022-02-26T13:49:11+0800
	 * @return   [type]                   [description]
	 */
	public function delCategory(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$service = new service();
		$result = $service->delCategory($id, $this->user_id);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}


	/**
	 * 获取文章列表
	 * @Author   cendxia
	 * @DateTime 2022-02-26T12:57:12+0800
	 * @return   [type]                   [description]
	 */
	public function getList(){
		$title = $this->request->param('title', null, 'trim');
		$category_id = $this->request->param('category_id', null, 'intval');
		$page = $this->request->param('page', 1, 'intval');
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getList($page, $pageSize, $title, $category_id);
		return show($result);
	}

	/**
	 * 获取文章详情
	 * @Author   cendxia
	 * @DateTime 2022-02-26T14:23:57+0800
	 * @return   [type]                   [description]
	 */
	public function getArticlesInfo(){
		$id = $this->request->param('id', null, 'intval');
		$service = new service();
		$result = $service->getArticlesInfo($id);
		return show($result);
	}


	/**
	 * 添加/修改文章
	 * @Author   cendxia
	 * @DateTime 2022-02-26T14:29:31+0800
	 * @return   [type]                   [description]
	 */
	public function article(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$service = new service();
		$result = $service->article($this->user_id, input('post.'));
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 删除文章
	 * @Author   cendxia
	 * @DateTime 2022-02-26T14:45:27+0800
	 * @return   [type]                   [description]
	 */
	public function delArticle(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$service = new service();
		$result = $service->delArticle($this->user_id, $id);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}
}