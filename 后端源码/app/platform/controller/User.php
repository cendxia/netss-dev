<?php 

namespace app\platform\controller;

use app\platform\service\User as service;

/**
 * 
 */
class User extends Auth
{
	/**
	 * 获取用户列表
	 * @Author   cendxia
	 * @DateTime 2022-10-16T19:36:31+0800
	 * @return   [type]                   [description]
	 */
	public function getUserList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$user_name = $this->request->param('user_name', null, 'trim');
		$type = $this->request->param('type', null, 'intval');
		$phone = $this->request->param('phone', null, 'intval');
		$service = new service();
		$result = $service->getUserList($page, $pageSize, $user_name, $type, $phone);
		return show($result);
	}

	/**
	 * 获取申请创作者记录
	 * @Author   cendxia
	 * @DateTime 2022-10-16T19:36:45+0800
	 * @return   [type]                   [description]
	 */
	public function getMpUser(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getMpUser($page, $pageSize);
		return show($result);
	}


	/**
	 * 提现记录
	 * @Author   cendxia
	 * @DateTime 2022-10-17T21:52:38+0800
	 * @return   [type]                   [description]
	 */
	public function getUserTixian(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getUserTixian($page, $pageSize);
		return show($result);
	}

	/**
	 * 提现审核
	 * @Author   cendxia
	 * @DateTime 2022-10-18T13:30:06+0800
	 * @return   [type]                   [description]
	 */
	public function tixianAudit(){
		if(!request()->isPost()){
			return requestError();
		}
		$id = $this->request->param('id', null, 'intval');
		$user_id = $this->request->param('user_id', null, 'intval');
		$status = $this->request->param('status', null, 'intval');
		$fail = $this->request->param('fail', null, 'trim');
		$service = new service();
		$result = $service->tixianAudit($this->user_id, $id, $user_id, $status, $fail);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}

	/**
	 * 创作者审核
	 * @Author   cendxia
	 * @DateTime 2022-10-18T21:03:52+0800
	 * @return   [type]                   [description]
	 */
	public function mpAudit(){
		if(!request()->isPost()){
			return requestError();
		}
		$user_id = $this->request->param('user_id', null, 'intval');
		$id = $this->request->param('id', null, 'intval');
		$type = $this->request->param('type', null, 'intval');
		$fail = $this->request->param('fail_reason', null, 'trim');
		$service = new service();
		$result = $service->mpAudit($this->user_id, $id, $user_id, $type, $fail);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R(config('language.textSuccess'));
	}


	/**
	 * 获取用户反馈信息
	 * @Author   cendxia
	 * @DateTime 2024-03-26T10:27:33+0800
	 * @return   [type]                   [description]
	 */
	public function getUserFeedback(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getUserFeedback($page, $pageSize);
		return show($result);
	}


	/**
	 * 获取VIP订单列表
	 * @Author   cendxia
	 * @DateTime 2024-03-26T09:21:36+0800
	 * @return   [type]                   [description]
	 */
	public function getVipOrderList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getVipOrderList($page, $pageSize);
		return show($result);
	}

	
}