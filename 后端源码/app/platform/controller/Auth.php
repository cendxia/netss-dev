<?php 

namespace app\platform\controller;

use app\BaseController;
use think\facade\Request;

/**
 * 
 */
class Auth extends BaseController
{
	
	protected $user_id;
	protected $user;

	public function initialize(){
		parent::initialize();
		$this->getUser();
	}

	private function getUser(){
		$token = Request::header('token');
		// $token = 'fe1b82b4c4261b1e1b87a563535977b6d4761351';

		$this->user = cache(config('redis.token_pre').$token);
		if(isset($this->user['user_id'])){
			$this->user_id = $this->user['user_id'];
		}
	}
}