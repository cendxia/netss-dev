<?php 

namespace app\platform\controller;

use app\platform\service\AdminUser as service;

/**
 * 管理员用户
 */
class AdminUser extends Auth
{


	/**
	 * 退出登录
	 * @Author   cendxia
	 * @DateTime 2024-02-03T16:09:06+0800
	 * @return   [type]                   [description]
	 */
	public function loginOut(){
		$service = new service();
		$result = $service->loginOut($this->user_id);
		if(empty($result)){
			return resError(config('language.textError'));
		}
		return R('已注销');
	}
	

	/**
	 * 修改密码
	 * @Author   cendxia
	 * @DateTime 2024-02-03T13:42:31+0800
	 * @return   [type]                   [description]
	 */
	public function editPassword(){
		$service = new service();
		$result = $service->editPassword(input('post.'), $this->user_id);
		if(empty($result)){
			return resError('修改失败');
		}
		return R('修改成功');
	}
}