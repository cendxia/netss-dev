<?php 

namespace app\platform\controller;

use app\BaseController;

use app\platform\service\AdminUser;

use think\facade\Request;
use think\facade\Cache;

/**
 * 
 */
class Login extends BaseController{


	/**
	 * 登录
	 * @Author   cendxia
	 * @DateTime 2022-10-15T18:12:49+0800
	 * @return   [type]                   [description]
	 */
	public function loginEvent(){
		if(!request()->isPost()){
			return requestError();
		}

		// 参数校验
		$loginname = $this->request->param('login_name', '', 'trim');
		$password = $this->request->param('password', '', 'trim');
		$phone = $this->request->param('phone', null, 'intval');
		$sms_code = $this->request->param('sms_code', '', 'trim');

		$data = [
			'login_name'=>$loginname,
			'password'=>$password,
			'phone' => $phone,
			'sms_code'=>$sms_code
		];

		$result = (new AdminUser())->loginEvent($data);

		if($result){
			return show($result, config('language.login_success'));
		}
		return show([], config('language.login_success'), config('status.error'));
	}


	public function test(){
		dump('test');
	}

	

	/**
	 * 退出登录
	 * @Author   cendxia
	 * @DateTime 2022-02-08T21:16:40+0800
	 * @return   [type]                   [description]
	 */
	public function loginOut(){
		if(!$this->request->isPost()){
			return requestError();
		}
		$token = Request::header('token');
		if(empty($token)){
			return resError(config('language.token_error'));
		}
		// 清除缓存
		// $result = Cache::delete(config('redis.token_pre').$token);
		$result = cache(config('redis.token_pre').$token, false);
		if($result){
			return R(config('language.textSuccess'));
		}
		return resError(config('language.textError'));
	}


}