<?php 

namespace app\platform\controller;

use app\common\lib\Qiniu;
use app\common\lib\Upload as uploadLib;

/**
 *  上传
 */
class Upload extends Auth
{

	private $config;
	
	public function __construct()
	{
		$this->config = new Qiniu();
	}


	/**
	 * 单文件上传到七牛云
	 * @Author   cendxia
	 * @DateTime 2022-02-13T14:15:59+0800
	 * @return   [type]                   [description]
	 */
	public function imgUploadQiniu(){
		if(!request()->isPost()){
			return requestError();
		}
		$type = '';
		if(!empty(input('post.type'))){
			$type = input('post.type');
		}

		if(empty($_FILES['file'])){
			return resError(config('language.param_error'));
		}

		// return show(['url'=>'https://zjw.data.zijiao.cn/gouziyuan/pic/202210/9k73L8SRBZ5WpeNd6McihObzTgCIPJUKy_www_zijiao_cn_183223.png']);


		$result = (new uploadLib())->fileUploadQiniu($_FILES['file']);

		if($type == 'wangeditor'){
			$res = [
				'errno' => 0,
				'data' => [
					'url' => $result['url'],
					'alt' => '上传成功',
					'href' => ''
				]
			];
			return json($res);
		}
		return show($result);
	}

	

	/**
	 * 上传文件到七牛云
	 * @Author   cendxia
	 * @DateTime 2022-09-28T17:02:24+0800
	 * @return   [type]                   [description]
	 */
	public function fileUploadQiniu(){
		if(!request()->isPost()){
			return requestError();
		}
		if(empty($_FILES['file'])){
			return resError(config('language.param_error'));
		}
		$result = (new uploadLib())->fileUploadQiniu($_FILES['file'], 'file');
		return show($result);
	}
}