<?php 

namespace app\platform\controller;

use app\platform\service\Kecheng as service;

/**
 * 
 */
class Kecheng extends Auth
{
	
	/**
	 * 获取课程列表
	 * @Author   cendxia
	 * @DateTime 2023-07-03T13:14:38+0800
	 * @return   [type]                   [description]
	 */
	public function getKechengList(){
		$keyword = $this->request->param('keyword', null, 'trim');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getKechengList($page, $pageSize, $keyword);
		return show($result);
	}


	/**
	 * 获取订单列表
	 * @Author   cendxia
	 * @DateTime 2023-07-03T16:14:47+0800
	 * @return   [type]                   [description]
	 */
	public function gerOrderList(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->gerOrderList($page, $pageSize);
		return show($result);
	}
}