<?php 

namespace app\platform\controller;

use app\platform\service\Tongji as service;

/**
 * 
 */
class Tongji extends Auth
{

	/**
	 * 首页数据
	 * @Author   cendxia
	 * @DateTime 2022-11-09T20:22:33+0800
	 * @return   [type]                   [description]
	 */
	public function indexData(){
		$service = new service();
		$result = $service->indexData();
		return show($result);
	}
	
	/**
	 * 获取短信记录
	 * @Author   cendxia
	 * @DateTime 2022-10-16T20:03:20+0800
	 * @return   [type]                   [description]
	 */
	public function smsLog(){
		$phone = $this->request->param('phone', null, 'intval');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->smsLog($phone, $page, $pageSize);
		return show($result);
	}

	/**
	 * 错误日志
	 * @Author   cendxia
	 * @DateTime 2022-10-18T10:27:41+0800
	 * @return   [type]                   [description]
	 */
	public function getErrorLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getErrorLog($page, $pageSize);
		return show($result);
	}


	/**
	 * 用户登录日志
	 * @Author   cendxia
	 * @DateTime 2023-06-30T22:12:58+0800
	 * @return   [type]                   [description]
	 */
	public function getUserLoginLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getUserLoginLog($page, $pageSize);
		return show($result);
	}
}