<?php 

namespace app\platform\controller;

use app\platform\service\Order as service;

/**
 * 
 */
class Order extends Auth
{
	/**
	 * 订单列表
	 * @Author   cendxia
	 * @DateTime 2022-10-17T18:57:35+0800
	 * @return   [type]                   [description]
	 */
	public function getOrderList(){
		$keyword = $this->request->param('keyword', null, 'trim');
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getOrderList($page, $pageSize, $keyword);
		return show($result);
	}


	/**
	 * 获取下载记录
	 * @Author   cendxia
	 * @DateTime 2022-10-17T21:34:41+0800
	 * @return   [type]                   [description]
	 */
	public function getDownloadLog(){
		$page = $this->request->param('page', 1, 'intval');  // 当前页数
		$pageSize = $this->request->param('pageSize', config('common.page_size'), 'intval');
		$service = new service();
		$result = $service->getDownloadLog($page, $pageSize);
		return show($result);
	}
}