<?php 

namespace app\platform\controller;

use app\platform\service\System as service;

/**
 * 系统配置
 */
class System extends Auth
{
	/**
	 * 基础配置
	 * @Author   cendxia
	 * @DateTime 2022-10-18T11:03:53+0800
	 * @return   [type]                   [description]
	 */
	public function systemConfig(){
		if(!$this->request->isPost()){
			return requestError();
		}

		$service = new service();

		$result = $service->systemConfig(input('post.'));
	}
}