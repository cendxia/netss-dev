<?php 

namespace app\platform\controller;

use app\BaseController;
use app\platform\service\Common as commonBus;



/**
 * 公共类
 */
class Common extends BaseController
{
	

	/**
	 * 发送短信验证码
	 * @Author   cendxia
	 * @DateTime 2024-01-19T20:58:55+0800
	 * @return   [type]                   [description]
	 */
	public function smsSend(){
		if(!request()->isPost()){
			return requestError();
		}

		$service = new commonBus();

		$result = $service->smsSend(input('post.'));
		if($result){
			return R(config('language.sms_success'));
		}
		return resError(config('language.sms_error'));
	}
}

