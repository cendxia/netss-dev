<?php
namespace app;

// 应用请求对象类
class Request extends \think\Request
{
	// 定义全局过滤信息 防XSS攻击
    protected $filter = 'htmlspecialchars';
}
