<?php 

namespace app\common\lib;

/**
 *  Seo提交
 */
class SubmitSeo
{
	
	/**
	 * 百度seo提交
	 * @Author   cendxia
	 * @DateTime 2023-05-03T21:57:02+0800
	 * @param    string                   $type [description]
	 * @param    array                    $data [description]
	 * @return   [type]                         [description]
	 */
	public static function baidu($type='ziyuan', $data=[]){
		$urls = self::getParams($type, $data);
		if(!empty($urls)){
			$api = 'http://data.zz.baidu.com/urls?site=https://www.gouziyuan.cn&token=To6xRs0o6YIHv75g';
			// $api = 'http://data.zz.baidu.com/urls?site=https://www.zijiao.cn&token=lt134mqdENABKx6q';
			$ch = curl_init();
			$options =  array(
			    CURLOPT_URL => $api,
			    CURLOPT_POST => true,
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_POSTFIELDS => implode("\n", $urls),
			    CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
			);
			curl_setopt_array($ch, $options);
			$result = curl_exec($ch);
			// echo $result;
		}
	}

	/**
	 * 封装参数
	 * @Author   cendxia
	 * @DateTime 2023-05-03T21:57:13+0800
	 * @param    [type]                   $type [description]
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	private static function getParams($type, $data){
		if($type == 'ziyuan'){ // 资源
			if(empty($data['ziyuan_id']) || $data['ziyuan_id'] <= 0){
				return false;
			}
			return array(
			    'https://www.gouziyuan.cn/ziyuan/'.$data['ziyuan_id']
			);
		}else if($type == 'kecheng'){ // 课程
			if(empty($data['ke_id']) || $data['ke_id'] <= 0){
				return false;
			}
			return array(
			    'https://www.gouziyuan.cn/kecheng/'.$data['ke_id']
			);
		}else if($type == 'keTry'){	// 课程试看
			if(empty($data['ke_id']) || empty($data['chapters_id']) || empty($data['directory_id'])){
				return false;
			}
			return array(
			    'https://www.gouziyuan.cn/kecheng/course/'.$data['ke_id'].'_'.$data['chapters_id'].'_'.$data['directory_id']
			);
		}else if($type == 'article'){ // 文章
			if(empty($data['article_id']) || $data['article_id'] <= 0){
				return false;
			}
			return array(
			    'https://www.gouziyuan.cn/article/'.$data['article_id']
			);
		}else{
			return false;
		}
	}
}