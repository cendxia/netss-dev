<?php 

namespace app\common\lib;

use think\captcha\facade\Captcha as thinkCaptcha;

class Captcha{

	/**
	 * 生成验证码
	 * @Author   cendxia
	 * @DateTime 2022-02-08T14:15:37+0800
	 * @return   [type]                   [description]
	 */
	public static function getCaptcha(){

		return thinkCaptcha::create();
	}
}