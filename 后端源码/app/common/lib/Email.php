<?php 

namespace app\common\lib;

use PHPMailer\PHPMailer\PHPMailer;//引用发送邮件类
use PHPMailer\PHPMailer\SMTP;//引用smtp授权码类
use PHPMailer\PHPMailer\Exception;//引用发送邮件异常类

/**
 * 
 */
class Email
{


	private $host;
	private $name;
	private $username;
	private $password;

	public function __construct(){
		$config = config('common.email');
		$this->host = $config['host'];
		$this->name = $config['name'];
		$this->username = $config['username'];
		$this->password = $config['password'];
	}
	
	/**
	 * 发送邮件
	 * @Author   cendxia
	 * @DateTime 2022-10-14T19:17:43+0800
	 * @param    [type]                   $toemail   对方邮箱号
	 * @param    [type]                   $title     邮件标题
	 * @param    [type]                   $content   邮件内容
	 * @param    string                   $user_name 对方用户名
	 * @return   [type]                              [description]
	 */
	public function sendCodeToEmail($toemail, $title, $content, $user_name=''){
		$user_name = empty($user_name) ? $toemail : $user_name;
		$mail = new PHPMailer();
		$mail->isSMTP();  //使用smtp鉴权方式发送邮件
		$mail->CharSet = 'utf8';   //设置编码
		$mail->Host = $this->host;  //邮箱smtp邮箱
		$mail->SMTPAuth = true;    //是否需要认证身份
		$mail->Username = $this->username;  //发送方邮箱
		$mail->Password = $this->password;    //发送方smtp密码
		$mail->SMTPSecure = 'ssl';    //使用的协议
		$mail->Port = 465;   //邮箱接收的端口号
		$mail->setFrom($this->username, $this->name);  //定义邮件及昵称
		$mail->addAddress($toemail, $user_name);  //要发送的地址和设置地址的昵称
		$mail->Subject = $title;  //添加该邮件的主题
		$mail->Body = $content; //该邮件内容
		return empty($mail->send()) ? false : true;
	}
}