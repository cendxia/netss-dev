<?php 

namespace app\common\lib;

/**
 * 
 */
class Str {
	
	/**
	 * 生成随机字符串
	 * @Author   cendxia
	 * @DateTime 2022-02-07T12:51:08+0800
	 * @param    integer                  $length [description]
	 * @return   [type]                           [description]
	 */
	public static function getRandStr($length = 'auto'){
		$str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
		$randStr = str_shuffle($str);//打乱字符串
		if($length == 'auto'){
			$length = mt_rand(10, 25);
		}
		$rands= substr($randStr, 0, $length);//substr(string,start,length);返回字符串的一部分
		return $rands;
	}

	/**
	 * 生成短信验证码
	 * @Author   cendxia
	 * @DateTime 2022-02-21T16:06:18+0800
	 * @return   [type]                   [description]
	 */
	public static function smsCode(){
		return mt_rand(1000, 99999);
	}

	/**
	 * 生成登录token
	 * @Author   cendxia
	 * @DateTime 2022-02-07T16:53:12+0800
	 * @param    [type]                   $string [description]
	 * @return   [type]                           [description]
	 */
	public static function getLoginToken($string){
		$str = md5(uniqid(md5(microtime(true)), true)); // 生成一个不会重复的随机字符串
		return sha1($str.$string); // 加密
	}

	/**
	 * 图片转base64
	 * @Author   cendxia
	 * @DateTime 2022-02-20T18:35:53+0800
	 * @param    [type]                   $file [description]
	 * @return   [type]                         [description]
	 */
	public static function base64($file){
		$base64 = '';
		if($fp = fopen($file,"rb", 0)){
		    $gambar = fread($fp,filesize($file));
		    fclose($fp);
		    $base64 = chunk_split(base64_encode($gambar));
		}
		return $base64;
	}

	/**
	 * 生成订单号
	 * @Author   cendxia
	 * @DateTime 2022-03-01T19:21:39+0800
	 * @return   [type]                   [description]
	 */
	public static function getOrderId(){
		$snowflake = new \Godruoyi\Snowflake\Snowflake;
		return $snowflake->id();
	}


	/**
	 * 获取指定日期段内的每一天日期
	 * @Author   cendxia
	 * @DateTime 2022-04-12T15:58:16+0800
	 * @param    [type]                   $start_date [description]
	 * @param    [type]                   $end_date   [description]
	 * @return   [type]                               [description]
	 */
	public static function getDateFromRange($start_date, $end_date){
        $date = [];
        $stimestamp = is_numeric($start_date)?$start_date:strtotime($start_date);
        $etimestamp = is_numeric($end_date)?$end_date:strtotime($end_date);
 
        // 计算日期段内有多少天
        $days = ( $etimestamp - $stimestamp ) / 86400;
        for( $i=0; $i<$days; $i++ ){
            $date[] = date('Y-m-d', $stimestamp + ( 86400 * $i ) );
        }
        return $date;
    }


    // 数组转换为XML
	public static function ArrToXml($arr){
		if(!is_array($arr) || count($arr) == 0) return '';
		$xml = "<xml>";
		foreach ($arr as $key=>$val)
		{
			if (is_numeric($val)){
				$xml.="<".$key.">".$val."</".$key.">";
			}else{
				$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
			}
		}
		$xml.="</xml>";
		return $xml; 
	}

	// xml转数组
	public static function xmlArr($xml){
		if($xml == '') return '';
		libxml_disable_entity_loader(true);
		return  json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
	}

	/**
	 * 提取html标签中的文字
	 * @Author   cendxia
	 * @DateTime 2022-10-31T20:56:23+0800
	 * @param    [type]                   $data [description]
	 * @param    integer                  $len  [description]
	 * @return   [type]                         [description]
	 */
	public static function htmlStr($data, $len=200){
		$result = strip_tags($data);
		$result= str_replace(array("\r", "\n", "\s", "\t"), '', $result);
		$result = mb_substr($result, 0, $len, 'utf-8');
		return $result;
	}

	/**
	 * 判断字符串中是否有中文
	 * @Author   cendxia
	 * @DateTime 2024-02-26T21:09:15+0800
	 * @param    [type]                   $str [description]
	 * @return   boolean                       [description]
	 */
	public static function hasChinese($str) {
	    $pattern = '/[\x{4e00}-\x{9fa5}]/u'; // Unicode编码范围内的汉字
	    return preg_match($pattern, $str);
	}

	/**
	 * 数字转换为万为单位
	 * @Author   cendxia
	 * @DateTime 2023-11-28T15:05:43+0800
	 * @param    [type]                   $num [description]
	 * @return   [type]                        [description]
	 */
	public static function numberUnit($num){
		if(!is_numeric($num)) return 0;
		$res = 0;
        if($num < 10000) return rtrim($num);
        return rtrim(round($num/10000, 1)).'万';
	}


	/**
	 * 支付方式转换
	 * @Author   cendxia
	 * @DateTime 2024-01-18T10:45:35+0800
	 * @param    [type]                   $type [description]
	 * @return   [type]                         [description]
	 */
	public static function payType($type){
		switch ($type) {
			case 'alipay':
				return '支付宝';
				break;
			case 'wxpay':
				return '微信';
				break;
			case 'bank':
				return '银行卡';
				break;	
			default:
				return '未知';
				break;
		}
	}


	/**
	 * URL转base64编码
	 * @Author   cendxia
	 * @DateTime 2024-04-04T22:41:19+0800
	 * @param    [type]                   $url [description]
	 * @return   [type]                        [description]
	 */
	public function urlsafe_b64encode($url='') {
		$url = empty($url) ? 'https://www.zijiao.cn/images/logo.png' : $url;
		$data = base64_encode($url);
		return str_replace(array('+', '/', '='), array('-', '_', ''), $data);
	}
	
}