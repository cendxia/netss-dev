<?php 

namespace app\common\lib;

use Qiniu\Storage\UploadManager;
use Qiniu\Storage\BucketManager;
use app\common\lib\Qiniu;
use Qiniu\Auth;
use app\common\lib\Str;

/**
 *  上传类
 */
class Upload 
{
	
	/**
	 * 单文件上传到七牛云
	 * @Author   cendxia
	 * @DateTime 2022-02-13T14:16:33+0800
	 * @param    [type]                   $file [description]
	 * @return   [type]                         [description]
	 */
	public function fileUploadQiniu($file, $type="img", $bucket=''){

		$bucketStr = empty($bucket) ? config('common.qiniu.bucket') : $bucket;

		$qiniu = new Qiniu();
		$token = $qiniu->getToken($bucketStr);

		if(!isset($file['error']) || $file['error'] != 0){
			throw new \think\Exception(config('language.upload_error'));
		}

		if($type == 'img'){
			if($file['type'] != 'image/jpeg' && $file['type'] != 'image/png' && $file['type'] != 'image/gif'){
				throw new \think\Exception('文件类型错误，只允许上传图片！');
			}
			$upPath = 'gouziyuan/pic/'.date('Ym').'/';
			// $upPath = 'testPic/pic/'.date('Ym').'/';
		}else{
			$suffixs = explode('.', $file['name']);
			$suffix = end($suffixs); // 获取文件后缀名

			$fileSuffix = ['exe','rar','zip','iso','doc','docx','ppt','xls','wps','txt','lrc','rm','rmvb','mp3','mp4','wma','wav','jpg','jpeg','png','JPG','JPEG','PNG','gif'];

			if(array_search($suffix, $fileSuffix) == false){
				throw new \think\Exception('不支持该类型文件，请重新上传');
			}
			$upPath = 'gouziyuan/ziyuan/'.date('Ym').'/';
		}


		// 获取文件扩展名
		$suffixs = explode('.', $file['name']);
		$suffix = '.'.end($suffixs);
		$strLib = new Str();
		$fileName = $upPath.$strLib->getRandStr(rand(10, 40)).'_www_zijiao_cn_'.date('His').$suffix;

		$uploadMgr = new UploadManager();
		$result = $uploadMgr->putFile($token, $fileName, $file['tmp_name']);
		if(is_array($result[0]) && isset($result[0]['key'])){
			$host = '';
			if(empty($bucket)){
				$host = config('common.qiniu.host');
			}else if($bucket = 'lc-data'){
				$host = 'https://lc-data.zijiao.cn';
			}else{
				$host = config('common.qiniu.host');
			}
			return array('url' => $host.'/'.$result[0]['key']);
		}
		throw new \think\Exception(config('language.upload_error'));
	}

	/**
	 * 网络图片保存到七牛云
	 * @Author   cendxia
	 * @DateTime 2022-05-05T22:39:03+0800
	 * @param    [type]                   $url [description]
	 * @return   [type]                        [description]
	 */
	public function fetch($url, $path=''){
		// 把"\"替换为"/"
		$path = str_replace('\\', '/', $path);

		$path_str = '';
		if(!empty($path)){
			if(substr($path, -1) == '/'){
				// 删除最后一个字符"/"
				$path = substr($path, 0, -1);
				$path_str = $path;
				if(substr($path, 0, 1) == '/'){
					// 删除第一个字符
					$path_str = substr($path, 1);
				}else{
					$path_str = $path;
				}
			}else if(substr($path, 0, 1) == '/'){
				// 删除第一个字符
				$path_str = substr($path, 1);
			}else{
				$path_str = $path;
			}
		}
		
		$path_str = $path_str == '' ? $path_str : $path_str.'/';
		$key = Str::getRandStr();

		$auth = new Auth(config('common.qiniu.accessKey'), config('common.qiniu.secretKey'));
		$BucketManager = new BucketManager($auth);
		$result = $BucketManager->fetch($url, config('common.qiniu.bucket'), $path_str.$key);

		if(is_array($result[0]) && isset($result[0]['key'])){
			return config('common.qiniu.host').'/'.$result[0]['key'];
		}
		return '';
	}
}