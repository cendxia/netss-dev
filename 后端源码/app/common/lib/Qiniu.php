<?php 

namespace app\common\lib;

use Qiniu\Auth;

/**
 * 
 */
class Qiniu
{
	
	/**
	 * 获取七牛云token
	 * @Author   cendxia
	 * @DateTime 2022-02-12T14:45:59+0800
	 * @return   [type]                   [description]
	 */
	public function getToken($bucket=''){
		$bucket = !empty($bucket) ? $bucket : config('common.qiniu.bucket');
		$token = cache('qiniuToken_'.$bucket);
		if(empty($token)){
			$accessKey = config('common.qiniu.accessKey');
			$secretKey = config('common.qiniu.secretKey');
		    $auth = new Auth($accessKey, $secretKey);
		    $token = $auth->uploadToken($bucket);
		    // 存储token
		    cache('qiniuToken_'.$bucket, $token, 3600);
		    return $token;
		}
		return $token;
	}
}