<?php 

namespace app\common\lib;

use common\Sms as smsServer;
use common\AliSms;

/**
 * 
 */
class Sms
{

    private $config;
    
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 网易发送验证码
     * @Author   cendxia
     * @DateTime 2022-09-18T01:01:41+0800
     * @param    [type]                   $templateid [description]
     * @param    [type]                   $phone      [description]
     * @return   [type]                               [description]
     */
    public function wangyi($templateid, $phone){
        $AppKey = $this->config['AppKey'];
        $AppSecret = $this->config['AppSecret'];
        $templateid = $this->config['templateids'][$templateid];
        $smsServer = new smsServer($AppKey, $AppSecret);
        // 发送短信验证码，随机
        $result = $smsServer->sendSmsCode($templateid, $phone);

        // 发送模板短信，未使用
        // $result = $smsServer->sendSMSTemplate($templateid, [$phone], ['cendxia','netgzs']);

        if(!empty($result) && $result['code'] == 200){
            // $result['obj']
            // 验证码发送成功
            return $result['obj'];
        }
        return false;
    }


    /**
     * 阿里云发送短信
     * @Author   cendxia
     * @DateTime 2024-02-16T10:21:39+0800
     * @param    [type]                   $templateid [description]
     * @param    [type]                   $phone      [description]
     * @return   [type]                               [description]
     */
    public function aliSms($templateid, $phone){
        // 生成随机数
        $code = mt_rand(1000, 9999);
        $templateCode = $this->config['templateids'][$templateid];
        $result = AliSms::smsCode($this->config, $templateCode, $phone, $code);
        if($result == false) return false;
        return $code;
    }
}