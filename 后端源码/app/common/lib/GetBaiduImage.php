<?php

/**
 *  AI图片搜索 
 */
namespace app\common\lib;

class GetBaiduImage
{ 

	/**
	 * 根基关键词获取图片
	 * @Author   cendxia
	 * @DateTime 2023-05-22T14:37:04+0800
	 * @param    [type]                   $keyword [description]
	 * @return   [type]                            [description]
	 */
	public static function getImgs($keyword){
		$url = 'https://image.baidu.com/search/acjson?tn=resultjson_com&ipn=rj&word='.$keyword.'&pn=&rn=10';
    	$ifpost = 0;
	    $datafields = '';
	    $cookiefile = '';
	    $v = false;
	    //构造随机ip
	    $ip_long = array(
	        array('607649792', '608174079'), //36.56.0.0-36.63.255.255
	        array('1038614528', '1039007743'), //61.232.0.0-61.237.255.255
	        array('1783627776', '1784676351'), //106.80.0.0-106.95.255.255
	        array('2035023872', '2035154943'), //121.76.0.0-121.77.255.255
	        array('2078801920', '2079064063'), //123.232.0.0-123.235.255.255
	        array('-1950089216', '-1948778497'), //139.196.0.0-139.215.255.255
	        array('-1425539072', '-1425014785'), //171.8.0.0-171.15.255.255
	        array('-1236271104', '-1235419137'), //182.80.0.0-182.92.255.255
	        array('-770113536', '-768606209'), //210.25.0.0-210.47.255.255
	        array('-569376768', '-564133889'), //222.16.0.0-222.95.255.255
	    );
	    $rand_key = mt_rand(0, 9);
	    $ip= long2ip(mt_rand($ip_long[$rand_key][0], $ip_long[$rand_key][1]));
		//模拟http请求header头
	    $header = array("Connection: Keep-Alive","Accept: text/html, application/xhtml+xml, */*", "Pragma: no-cache", "Accept-Language: zh-Hans-CN,zh-Hans;q=0.8,en-US;q=0.5,en;q=0.3","User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)",'CLIENT-IP:'.$ip,'X-FORWARDED-FOR:'.$ip,'Content-Type:application/x-www-form-urlencoded');
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, $v);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    $ifpost && curl_setopt($ch, CURLOPT_POST, $ifpost);
	    $ifpost && curl_setopt($ch, CURLOPT_POSTFIELDS, $datafields);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	    $cookiefile && curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiefile);
	    $cookiefile && curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiefile);
	    curl_setopt($ch,CURLOPT_TIMEOUT,60); //允许执行的最长秒数
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    $result = curl_exec($ch);
	    curl_close($ch);
	    return $result;
	}


	/**
	 * 百度图片处理
	 * @Author   cendxia
	 * @DateTime 2023-05-23T00:33:17+0800
	 * @param    [type]                   $imagesData [description]
	 * @param    boolean                  $storage    [description]
	 * @return   [type]                               [description]
	 */
	public static function imageProcessing($imagesData, $storage=false){
		if(empty($imagesData)){
			// throw new \think\Exception(config('language.param_error'));
			return false;
		}
		$data = json_decode($imagesData, true);
		if(empty($data)){
			// throw new \think\Exception(config('language.param_error'));
			return false;
		}

		if(empty($data['data'])){
			// throw new \think\Exception(config('language.param_error'));
			return false;
		}

		// return [
		// 	'https://gzy.data.gouziyuan.cn/articles_net/IUaklXJbhq',
		// 	'https://gzy.data.gouziyuan.cn/articles_net/3Ii9XdDv0tu',
		// 	'https://gzy.data.gouziyuan.cn/articles_net/5c3XdKFR6mpNtVwqUsM'
		// ];

		$result = array();
		foreach($data['data'] as $key => $value){
			if(count($value) > 0 && count($result) < 1){
				$wInt = $value['width'] / $value['height'];
				$hInt = $value['height'] / $value['width'];
				if($wInt < 1.5 || $hInt < 1.5){ // 获取横向图片
					array_push($result, $value['thumbURL']);
				}
			}			
		}

		if($storage === false){
			return $result;
		}

		$uploadLib = new \app\common\lib\Upload();
		$imageArr = array();
		foreach($result as $key => $value){
			$res = $uploadLib->fetch($value, 'articles_net');
			array_push($imageArr, $res);
		}
		return $imageArr;
	}
}