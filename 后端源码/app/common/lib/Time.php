<?php 

namespace app\common\lib;


class Time{
	
	/**
	 * 用户登录保存时间
	 * @Author   cendxia
	 * @DateTime 2022-02-07T17:22:21+0800
	 * @param    integer                  $type [description]
	 * @return   [type]                         [description]
	 */
	public static function userLoginExpiresTime($type = 2){
		$type = !in_array($type, [1, 2])?2:$type;
		if($type == 1){
			$day = $type * 7;
		}else if($type == 2){
			$day = $type * 30;
		}
		return $type * 86400;
	}
}

