<?php 

namespace app\common\lib;
use app\api\model\UserActionLog as userLog;
use app\platform\model\AdminActionLog;

/**
 * 日志
 */
class Log
{
	

	/**
	 * 添加用户操作日志
	 * @Author   cendxia
	 * @DateTime 2022-04-07T16:26:55+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public static function insertUserLog($data){
		return (new userLog())->insertLog($data);
	}

	/**
	 * 添加后台操作日志
	 * @Author   cendxia
	 * @DateTime 2022-10-18T18:16:38+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public static function insertAdminLog($data){
		return (new AdminActionLog())->insertLog($data);
	}
}