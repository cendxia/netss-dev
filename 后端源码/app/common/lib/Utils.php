<?php 

namespace app\common\lib;

/**
 * 
 */
class Utils{
	
	/**
	 * 判断是否是一维数组
	 * @Author   cendxia
	 * @DateTime 2022-02-10T13:08:35+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	function getOneArr($data){
		if (count($data) == count($data, 1)) {
		    // echo '是一维数组';
		    return true;
		}
		return false;
	}

	/**
	 * 用户密码加密方式
	 * @Author   cendxia
	 * @DateTime 2022-10-14T15:26:04+0800
	 * @param    [type]                   $password [description]
	 * @param    [type]                   $salt     [description]
	 * @return   [type]                             [description]
	 */
    public static function userEncryption($password, $salt){
		return md5(md5(config('common.password_prefix').$password).$salt);
	}

	/**
	 * 后台用户加密方式
	 * @Author   cendxia
	 * @DateTime 2022-10-15T19:02:10+0800
	 * @param    [type]                   $phone    [description]
	 * @param    [type]                   $password [description]
	 * @param    [type]                   $salt     [description]
	 * @return   [type]                             [description]
	 */
	public static function adminUserEncryption($phone, $password, $salt){
		return md5(md5(config('common.password_admin_prefix').$phone.$password).$salt);
	}
}