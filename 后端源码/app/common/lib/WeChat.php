<?php

namespace app\common\lib;

use think\facade\Cache;

/**
 * 
 */
class WeChat
{
	/**
	 * 获取公众号access_token
	 * @Author   cendxia
	 * @DateTime 2022-10-22T13:10:06+0800
	 * @return   [type]                   [description]
	 */
	public static function getAccessToken(){
		$prefix = config('common.common_prefix');
		$access_oken = Cache($prefix.'wx_access_oken');
		if(empty($access_oken)){
			$config = config('common.WeChat.common');
			$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$config['appID'].'&secret='.$config['appsecret'];
		    $data = file_get_contents($url);
			$result = json_decode($data, true);
			if(!empty($result) && isset($result['access_token'])){
				Cache($prefix.'wx_access_oken', $result['access_token'], 7000);
				return $result['access_token'];
			}
			throw new \think\Exception('获取access_token失败');
		}
		return $access_oken;		
	}
}