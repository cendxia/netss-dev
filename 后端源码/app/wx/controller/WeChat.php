<?php
namespace app\wx\controller;

use app\BaseController;

use app\wx\business\WeChat as wxBus;

class WeChat extends BaseController{

	/**
	 * 公众号接入入口
	 * @Author   cendxia
	 * @DateTime 2022-05-05T11:30:22+0800
	 * @return   [type]                   [description]
	 */
	public function interface(){
		echo (new wxBus)->interface();
		exit;
	}
	
	/**
	 * 微信登录
	 * @Author   cendxia
	 * @DateTime 2022-05-05T11:48:20+0800
	 * @return   [type]                   [description]
	 */
	public function wxlogin(){
		$apiToken = request()->param('apiToken', null, 'trim');
		// 类型，bing:绑定，login:登录
		$type = request()->param('type', 'bind', 'trim');
		// 业务处理完成后需要返回的url对应的key值，其他平台用户登录时使用。如duanju
		$return_key = request()->param('return_key', null, 'trim');
		$url = (new wxBus)->wxlogin($apiToken, $type, $return_key);
		return redirect($url);
	}

	/**
	 * 获取用户详情
	 * @Author   cendxia
	 * @DateTime 2022-05-05T11:48:32+0800
	 * @return   [type]                   [description]
	 */
	public function userInfo(){
		$result = (new wxBus)->userInfo();
		if($result){
			return redirect($result);
		}
	}

	/**
	 * 设置微信自定义菜单
	 * @Author   cendxia
	 * @DateTime 2023-08-05T00:18:28+0800
	 */
	public function setMenu(){
		$result = (new wxBus)->setMenu();
	}

	/**
	 * 更新用户微信信息、未使用
	 * @Author   cendxia
	 * @DateTime 2022-05-06T11:48:03+0800
	 * @return   [type]                   [description]
	 */
	public function updateUser(){

	}

}