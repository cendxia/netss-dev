<?php 

namespace app\wx\model;

use think\Model;

/**
 * 
 */
class Users extends Model
{
	
	/**
	 * 根据unionid查询用户信息
	 * @Author   cendxia
	 * @DateTime 2022-12-05T20:59:05+0800
	 * @param    [type]                   $unionid [description]
	 * @return   [type]                            [description]
	 */
	public function getUserByUnionid($unionid){
		try {
			return $this->where(['unionid'=>$unionid])->find();
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}

	/**
	 * 更新用户
	 * @Author   cendxia
	 * @DateTime 2022-12-05T21:30:35+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function updateUser($user_id, $data){
		return $this->where(['user_id'=>$user_id])->save($data);
	}
}