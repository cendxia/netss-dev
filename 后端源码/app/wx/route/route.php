<?php 

use think\facade\Route;


// test
Route::post('index', 'wx/Index/index');


// 微信接入入口
Route::rule('weChat/interface', 'wx/WeChat/interface');
// 微信登录
Route::get('weChat/wxlogin', 'wx/WeChat/wxlogin');
// 自定义微信菜单
Route::get('weChat/setMenu', 'wx/WeChat/setMenu');

