<?php
/**
 * 中间键
 */
namespace app\wx\middleware;

class Auth {

    public function handle($request, \Closure $next) {

        if(empty(session(config('admin.session_admin'))) && !preg_match('/login/', $request->pathinfo())){
            // return redirect(url('login/login'));
        }


        // 前置中间键
        $response = $next($request);


        // 使用后置中间键有buy,代码会执行所有流程
        // if(empty(session(config('admin.session_admin'))) && $request->controller() != 'Login'){
        //     return redirect(url('login/login'));
        // }

        // 后置中间键
        // dump($request);


        return $response;
        
    }

    /**
     * 中间件结束调度
     * @param \think\Response $response
     */
    public function end(\think\Response $response) {

    }
}