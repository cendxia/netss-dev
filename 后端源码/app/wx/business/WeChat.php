<?php 

namespace app\wx\business;
use app\wx\business\User;
use app\wx\business\MessageContent;
use app\common\lib\WeChat as wxChatLib;
// use app\common\lib\Upload; // 未使用
use think\facade\Request;
use think\facade\Cache;
use think\facade\Db;

/**
 * 
 */
class WeChat
{

	protected $appID;
	protected $appsecret;
	protected $Token;
	protected $url;
	
	public function __construct()
	{
		$config = config('common.WeChat.common');
		$this->appID = $config['appID'];
		$this->appsecret = $config['appsecret'];
		$this->Token = $config['token'];
		$this->url = $config['host'];
	}

	// 公众号接入入口
	public function interface(){
		//微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
		$data = serialize($_GET);

		Db::name('test')->insert(['title'=>'微信接入', 'data' => $data]);


		$signature = $_GET['signature'];
		//timestamp	时间戳
		$timestamp = $_GET['timestamp'];
		//nonce	随机数
		$nonce = $_GET['nonce'];
		$dataArray = array($this->Token,$timestamp,$nonce);
		//排序 sort SORT_STRING 快速排序
		sort($dataArray,SORT_STRING);
		//把排序后的数据转为字符器
		$str = implode($dataArray);
		//把字符串加密
		$str = sha1($str);
		//判断是否是第一次验证
		if($str==$signature && isset($_GET['echostr'])){
			// 第一次验证
			return $_GET['echostr'];
		}else{
			// 接收微信推送消息
			$this->reponseMwg();
		}
	}

	// 接收事件推送并回复
    private function reponseMwg(){
		//1.获取到微信推送过来post数据（xml格式）
		$postArr = file_get_contents('php://input');

		Db::name('test')->insert(['title'=>'微信接入input', 'data' => $postArr]);


		// '<xml><ToUserName><![CDATA[gh_0405d9a2b3d1]]></ToUserName>
		// <FromUserName><![CDATA[o4q-25rWKgHVi8yAmDFNTYKX9hQ4]]></FromUserName>
		// <CreateTime>1693063374</CreateTime>
		// <MsgType><![CDATA[event]]></MsgType>
		// <Event><![CDATA[SCAN]]></Event>
		// <EventKey><![CDATA[21311]]></EventKey>
		// <Ticket><![CDATA[gQHs8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyWUJyZElJeUxlU0MxUVJHSE5BMWoAAgS1GOpkAwSAUQEA]]></Ticket>
		// </xml>'


		//2.处理消息类型并设置回复类型和内容
		/*
		 <xml>
		    <ToUserName><![CDATA[toUser]]></ToUserName>
		    <FromUserName><![CDATA[FromUser]]></FromUserName>
		    <CreateTime>123456789</CreateTime>
		    <MsgType><![CDATA[event]]></MsgType>
		    <Event><![CDATA[subscribe]]></Event>
		</xml>
		* */
		$postObj = simplexml_load_string($postArr);
		$MessageContent = new MessageContent; // 获取自定义消息内容


		// 公共变量
		$toUser = $postObj->FromUserName;
		$fromUser = $postObj->ToUserName;
		$time = time();


		// db('test')->insert(['data'=>$postArr,'time'=>time()]);

		//判断该数据包是否是订阅的事件推送///
		if(strtolower($postObj->MsgType) == 'event'){
			if(strtolower($postObj->Event) == 'subscribe'){
				// 关注事件
				$open_id = strtolower($postObj->FromUserName);
				// db('user')->where(['open_id'=>$open_id])->update(['is_follow'=>1]);
			    //回复用户消息
			    $msgType = 'text';
			    $content = $MessageContent->follow();
			    //回复消息模版
			    $template = "
		            <xml>
			            <ToUserName><![CDATA[%s]]></ToUserName>
			            <FromUserName><![CDATA[%s]]></FromUserName>
			            <CreateTime>%s</CreateTime>
			            <MsgType><![CDATA[%s]]></MsgType>
			            <Content><![CDATA[%s]]></Content>
		            </xml>";
			    $info=sprintf($template,$toUser,$fromUser,$time,$msgType,$content);
				echo $info;


				$eventKeys = explode('&', strtolower($postObj->EventKey));

				// 判断是否是扫码登录
				if($eventKeys[0] == 'qrscene_gzyuserlogin'){
					$service = new \app\api\business\WeChat();
					$result = $service->followLogin($_GET['openid'], $eventKeys[1]);
					if($result){
						$info=sprintf($template,$toUser,$fromUser,$time,$msgType,'登录成功');
						Db::name('test')->insert(['title'=>'登录成功11','data'=>serialize($result)]);
					}else{
						$info=sprintf($template,$toUser,$fromUser,$time,$msgType,'登录失败');
						Db::name('test')->insert(['title'=>'登录失败22','data'=>'']);
					}
					echo $info;
				}

		    }else if(strtolower($postObj->Event) == 'unsubscribe'){
				// 取消关注事件
				$open_id = strtolower($postObj->FromUserName);
				// db('user')->where(['open_id'=>$open_id])->update(['is_follow'=>0]);
			}else if(strtolower($postObj->Event) == 'click'){
		    	// 菜单点击事件
		    	$content = $MessageContent->menuReply(strtolower($postObj->EventKey));
		    	$msgType = 'text';
		    	//回复消息模版
			   	$template = "
		            <xml>
			            <ToUserName><![CDATA[%s]]></ToUserName>
			            <FromUserName><![CDATA[%s]]></FromUserName>
			            <CreateTime>%s</CreateTime>
			            <MsgType><![CDATA[%s]]></MsgType>
			            <Content><![CDATA[%s]]></Content>
		            </xml>";
			    $info = sprintf($template,$toUser,$fromUser,$time,$msgType,$content);
				
				// 如果点击了商务合作，发送图片给用户
				if(strtolower($postObj->EventKey) == 'swhz_v1'){
					$msgType = 'image';
					$template = "
		            <xml>
						<ToUserName><![CDATA[%s]]></ToUserName>
						<FromUserName><![CDATA[%s]]></FromUserName>
						<CreateTime>%s</CreateTime>
						<MsgType><![CDATA[%s]]></MsgType>
						<Image>
							<MediaId><![CDATA[%s]]></MediaId>
						</Image>
					</xml>";
					$media_id = 'Ad4MZIhalxRMGLi0t3PmulEvXDbUvCQI5JlSXcqVsYQ';
				    $info = sprintf($template,$toUser,$fromUser,$time,$msgType,$media_id);
					echo $info;
				}else{
					echo $info;
				}

		    }else if(strtolower($postObj->Event) == 'LOCATION'){
		    	// 获取用户在理位置信息

		  //   	<xml>
		  //   	<ToUserName><![CDATA[gh_c496c6a2f949]]></ToUserName>
				// <FromUserName><![CDATA[oxcsawV5PojLYb3eCCtIf9s4FGZU]]></FromUserName>
				// <CreateTime>1604487119</CreateTime>
				// <MsgType><![CDATA[event]]></MsgType>
				// <Event><![CDATA[LOCATION]]></Event>
				// <Latitude>25.109827</Latitude>
				// <Longitude>104.913803</Longitude>
				// <Precision>225.000000</Precision>
				// </xml>

				// 25.109827,104.913803   104.913803,25.109827

				// db('test')->insert(['data'=>$postArr,'time'=>time()]);

				echo 'success';
		    }else if(strtolower($postObj->Event) == 'scan'){
				// 扫码事件

				// db('test')->insert(['data'=>$postArr,'time'=>time()]);

				$template = "
		            <xml>
			            <ToUserName><![CDATA[%s]]></ToUserName>
			            <FromUserName><![CDATA[%s]]></FromUserName>
			            <CreateTime>%s</CreateTime>
			            <MsgType><![CDATA[%s]]></MsgType>
			            <Content><![CDATA[%s]]></Content>
		            </xml>";

				if(strtolower($postObj->EventKey) != ''){
					// 微信扫带参数的二维码绑定用户
					$scene_str_arr = explode('&', strtolower($postObj->EventKey));

					if(count($scene_str_arr) == 2 && $scene_str_arr[0] == 'bind' && !empty($scene_str_arr[1]) && !empty($_GET['openid'])){
						Db::name('test')->insert(['title'=>'绑定用户','data'=>strtolower($postObj->EventKey)]);

						$userBus = new User();
						$userBus->wxScanBindUser($_GET['openid'], $scene_str_arr);
					}else if(isset($scene_str_arr[0]) && $scene_str_arr[0] == 'gzyUserLogin'){
						// 购资源用户扫码登录

						$service = new \app\api\business\WeChat();
						$result = $service->followLogin($_GET['openid'], $scene_str_arr[1]);
						if($result){
							$info=sprintf($template,$toUser,$fromUser,$time,'text','登录成功');
						}else{
							$info=sprintf($template,$toUser,$fromUser,$time,'text','登录失败');
						}
						echo $info;
					}

					// db('test')->insert(['data'=>$scene_str[0].'||'.$scene_str[1],'time'=>time()]);
				}
				// return redirect(url('index/index'));
			}
		}else{
			// db('test')->insert(['data'=>'有新客服消息','time'=>time()]);
			// 用户主动发来文字消息 微信客服 接入一次失效
			// 转发消息给客服
			$msgType = 'text';
			$content = $postObj->Content;

			  // <xml>
			    //     <ToUserName><![CDATA[%s]]></ToUserName>
			    //     <FromUserName><![CDATA[%s]]></FromUserName>
			    //     <CreateTime>%s</CreateTime>
			    //     <MsgType><![CDATA[%s]]></MsgType>
			    //     <Content><![CDATA[%s]]></Content>
			    // </xml>";

			//回复消息模版
			$template = "			  
			    <xml> 
					<ToUserName><![CDATA[%s]]></ToUserName>  
					<FromUserName><![CDATA[%s]]></FromUserName>  
					<CreateTime>%s</CreateTime>  
					<MsgType><![CDATA[transfer_customer_service]]></MsgType>  
				</xml>";

			$KfAccount = 'CENi921';

			$info=sprintf($template,$toUser,$fromUser,$time);
			echo $info;
		}
    }


    /**
     * 微信受权登录并绑定账号
     * @Author   cendxia
     * @DateTime 2022-12-05T19:43:30+0800
     * @param    string                   $apiToken [description]
     * @return   [type]                             [description]
     */
    public function wxlogin($apiToken, $type='otherLogin', $return_key='duanju'){
    	$str = $apiToken.'_'.$type.'_'.$return_key;
        // 用户点击受权后跳转的地址
        $urlEncode = urlEncode($this->url.'/WeChat/userInfo?apiToken='.$str);

        // scope=snsapi_userinfo 弹出授权页面 snsapi_base 不会弹出受权页面
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$this->appID.'&redirect_uri='.$urlEncode.'&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect';
        return rawurldecode($url);
    }

    public function userInfo(){
        if(empty($_GET['code']) || empty($_GET['apiToken'])){
        	throw new \think\Exception(config('language.param_error'));
        }

        $params = explode('_', $_GET['apiToken']);

        // $user_id = Cache($_GET['apiToken']);

        //通过code换取网页授权access_token
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->appID.'&secret='.$this->appsecret.'&code='.$_GET['code'].'&grant_type=authorization_code';
        $result = httpUrl($url);
   
        // $result 中如果无openid 则未关注公众号

        // 判断用户是否刷新
        if(!empty($result['errcode']) && $result['errcode'] == 40163){
            return $this->url.'/WeChat/wxlogin'; // 待优化，因加上参数
        }

        // 拉取用户信息(需scope为 snsapi_userinfo)
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$result['access_token'].'&openid='.$result['openid'].'&lang=zh_CN';
        $userInfo = httpUrl($url);


        // 其他平台登录
        if($params[1] == 'otherLogin'){
        	$config = config('wx_config.return_url');
        	// 存入缓存，前端通过openid请求api拉取缓存信息信息
        	cache($params[2].'_'.md5($userInfo['openid']), $userInfo, 60);
        	$config = config('common.wx_config.return_url');
        	return $config[$params[2]].'?token='.md5($userInfo['openid']);
        }else{
        	return $this->bind($params[0], $userInfo, $result);
        }
        
    }

    /**
     * 绑定微信
     * @Author   cendxia
     * @DateTime 2023-08-28T01:44:49+0800
     * @return   [type]                   [description]
     */
    private function bind($user_id, $userInfo, $result){
    	// 判断用户是否关注
        $access_token = wxChatLib::getAccessToken();
        $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$access_token.'&openid='.$result['openid'].'&lang=zh_CN';

        $follow = httpUrl($url);
        if(!empty($follow['subscribe']) && $follow['subscribe'] == 1){
            // 已关注
            $data['is_follow'] = 1;
        }else{
            // 未关注
            $data['is_follow'] = 0;
        }
        
        if($userInfo && !empty($userInfo['unionid'])){
            // 判断用户是否存在
            $userBus = new User();
            $users = $userBus->getUserByUnionid($userInfo['unionid']);
            
            if(empty($users)){
                //保存头像, 未使用
                // $UploadLib = new Upload();
                // $imgRes = $UploadLib->fetch($userInfo['headimgurl']);

                $data = [
                	'openid' => $userInfo['openid'],
                	'unionid' => $userInfo['unionid']
                ];
                cache($result['openid'].'user_info', $data, 600);
                $result = $this->updateUser($user_id, $data);
                if(empty($result)){
                	throw new \think\Exception('绑定失败');
                }
                return 'https://www.zijiao.cn/user/wx-binding';
            }else{
            	// 已绑定
                return 'https://www.zijiao.cn/login';
            }
            return 'https://www.zijiao.cn/user/wx-binding';
        }
    }

    /**
     * 更新用户信息
     * @Author   cendxia
     * @DateTime 2022-12-05T20:51:50+0800
     * @param    [type]                   $user_id [description]
     * @param    [type]                   $wxUser  [description]
     * @return   [type]                            [description]
     */
    private function updateUser($user_id, $wxUser){
    	$userBus = new User();
    	return $userBus->updateUser($user_id, $wxUser);
    }


    /**
     * 自定义微信菜单
     * @Author   cendxia
     * @DateTime 2023-08-05T00:08:18+0800
     */
    public function setMenu(){
		$access_token = wxChatLib::getAccessToken();

		//调用接口
		$cn1=curl_init();
		//请求接口地址
		$url1="https://api.weixin.qq.com/cgi-bin/menu/create?access_token={$access_token}";
		//附加参数
		$post='{
			"button":[
			{  
				"type":"view",
				"name":"莱创课堂",
				"url":"http://ke.netss.cn/app/index.php?i=4953&c=entry&do=index&m=fy_lessonv2"
			},
			{  
				"type":"view",
				"name":"全部课程",
				"url":"http://ke.netss.cn/app/index.php?i=4953&c=entry&do=search&m=fy_lessonv2"
			},

		    {
		    	"type":"miniprogram",
				"name":"去水印",
				"url":"http://ke.netss.cn/app/index.php?i=4953&c=entry&do=index&m=fy_lessonv2",
				"appid":"wx7330ff073ca654aa",
				"pagepath":"pages/index/index"
		    }]
		}';

		curl_setopt($cn1,CURLOPT_URL,$url1);
		// CURLOPT_POST 模拟post
		curl_setopt($cn1,CURLOPT_POST,1);
		// CURLOPT_POSTFIELDS 传递参数
		curl_setopt($cn1,CURLOPT_POSTFIELDS,$post);
		$r=curl_exec($cn1);
		curl_close($cn1);

		json_encode($r);

		dump($r);
	}


}