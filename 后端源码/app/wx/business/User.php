<?php 

namespace app\wx\business;

use app\wx\model\Users;
use app\common\lib\WeChat as wxChatLib;

/**
 * 
 */
class User
{
	private $userModel;
	public function __construct()
	{
		$this->userModel = new Users();
	}

	/**
	 * 根据unionid查询用户信息
	 * @Author   cendxia
	 * @DateTime 2022-12-05T20:58:23+0800
	 * @param    [type]                   $unionid [description]
	 * @return   [type]                            [description]
	 */
	public function getUserByUnionid($unionid){
		try {
			return $this->userModel->getUserByUnionid($unionid);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 更新用户信息
	 * @Author   cendxia
	 * @DateTime 2022-12-05T19:16:46+0800
	 * @param    [type]                   $user_id [description]
	 * @param    [type]                   $data    [description]
	 * @return   [type]                            [description]
	 */
	public function updateUser($user_id, $data){
		try {
			return $this->userModel->updateUser($user_id, $data);
		} catch (\think\Exception $e) {
			throw new \think\Exception(config('language.mysql_error'));
		}
	}


	/**
	 * 微信扫带参数的二维码绑定用户
	 * @Author   cendxia
	 * @DateTime 2023-08-27T16:26:51+0800
	 * @param    [type]                   $openid [description]
	 * @param    [type]                   $data   [description]
	 * @return   [type]                           [description]
	 */
	public function wxScanBindUser($openid, $data){
		if(count($data) != 2 || $data[0] != 'bind' || empty($data[1]) || empty($openid)){
			return false;
		}

		$acctoken = wxChatLib::getAccessToken();
		$url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$acctoken.'&openid='.$openid.'&lang=zh_CN';

		$res = file_get_contents($url);
		$result = json_decode($res, true);

		if(!empty($result['errcode']) || empty($result['openid']) || empty($result['unionid'])){
			return false;
		}

		// 判断是否绑定了其他账号
		$user = $this->userModel->getUserByUnionid($result['unionid']);
		if($user) return false;

		$upData = [
			'openid' => $result['openid'],
			'unionid' => $result['unionid']
		];

		return $this->userModel->updateUser($data[1], $upData);
	}
}