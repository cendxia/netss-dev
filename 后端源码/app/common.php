<?php
// 应用公共文件


// 封闭json返回数据
function show($data = [], $message = 'SUCCESS', $status = 1, $httpStatus = 200){

	$result = [
		'status' => $status,
		'message' => $message,
		'result' => $data
	];

	return json($result, $httpStatus);
}

// 正确返回，默认不带数据
function R($msg){
	return show([], $msg);
}


// 类型错误
function requestError(){
	return show([], '请求类型错误', 'error');
}


// 错误结果
function resError($msg, $status = 0){
	return show([], $msg, $status);
}



// 模拟请求
function httpUrl($url,$data=NULL){
    // dump($data); exit;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if (!empty($data)){
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $output = curl_exec($curl);
    curl_close($curl);
    return json_decode($output,true);
}


function GetHttp($url){
    // 关闭句柄
    $curl = curl_init(); // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
    $tmpInfo = curl_exec($curl); //返回api的json对象
    if(curl_exec($curl) === false){
        return 'Curl error: ' . curl_error($ch);
    }
    //关闭URL请求
    curl_close($curl);
    return $tmpInfo; //返回json对象
}


// 转换查询条件
function whereStr($where){
    $where_str = '';
    foreach($where as $key => $value){
        if($key > 0){
            $where_str = $where_str.' and '.$value;
        }else{
            $where_str .= $value;
        }
    }
    return $where_str;
}


// 获取ip
function getUserIp(){
    return isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : request()->ip();
}


// 数据分页封装
function dataPage($page, $pageSize, $pageSum, $count, $data){
    return [
        'page' => $page,
        'pageSize' => $pageSize,
        'pageSum' => $pageSum,
        'count' => $count,
        'data' => $data
    ];
}

// 支付方式
function payType($type){
    switch ($type) {
        case 'alipay':
            return '支付宝';
            break;
        case 'wxpay':
            return '微信';
            break;
        default:
            return '未知';
            break;
    }
}