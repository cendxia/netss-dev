<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2021003130640472",

		//商户私钥
		'merchant_private_key' => "MIIEogIBAAKCAQEAh8d20DC/ZHiPf3Tl09kGa99bYaJEtcFdYbvE44EenQjj0Y95cC+r6otEqs1+0pu1lEBxuLqFY3xvlCbZCRdfIR1E2suzt+Fd+YPAk/IV4mpe6QCpQwObIfqmbAHfxLzG5CuT/JQs92DBIeXv9ZvpM3RKT0Vr0g8hqiUl6AcDfZeqXpU2rMJiCeaEfAzu4ldV0enyeWvk7xpv97QldwEVMuomSS0e0oIGPfq/Xhc84iPDWQioa95fPJxF0hVDs/PegNZqLlHRjEZ7Vpo0SFmfHPdrpBjlCym+oyrbrkn3sFpp2AKYTAD4LEZrKbh6D+3KChNzPV5F+P8nnniJ6A/aKwIDAQABAoIBACu04WISzwkgZdC5aGHRMmf29gtvmH6JR+3162LwZVJXUj5a2NRsBALz4Z65l2FkDgcy12iHYfisTZ57AtISW+Tk7w0fFTLTsNRovMzi2HiRlQub0uBngMBnVrrdv5Is0jkQbgqYCAF5e7HVunBtR5vwixsJVbPAey2x14ng+BatC3r6kb1h0ABsN3DpYPe7XksBVVoqpU28T9X3FEGEsfhKN3Ds7BOZTtBvCi8mDKM6/g5pxr5L/IOPjog5zS73XiSRlT9COuMsyRX5zHzxI/qgIE9xBbCTk5bQgqprjZTXIs4bcB0B1oM4feh53fswkqB7teDQE1vJ3a5TA9FawQECgYEAuqt2OxWO+/tz2I4MZGxVrxkvSruldS76DrCL9t6DdDLOmEHtgBgBAln9TJbnEJ/6aEb6+jVEXwqZw51iB+bK2aO1MJEzRUno4nhg1c4fBfxgkoVbctReXGpGat0RfUQPafV3C+PBiYihqo/Hm6dc8pADz9ZDavBaH+Twb/XBVpECgYEAujVUOPIfJAPQpY6o2tRppd4cBawbSneWjkuixGIEbkOf4MyoBHZJ2C72O4/ArdhAcfbfgUOsmyP7GupVMcr4IDFNLiXpUm5YydwmgXEI+qWi2bUyqBL04iUJzGzcix2yr3BmfElaR9fJoQngeyGeKwzE6BScmdifrEXmoZyBWvsCgYBjFkj4LAVuNJfVx/FEDU2eWJwF6310qq8JxSVGse7Na3rGz5gmepdujB4s3Z1h8cmWOLdYwZW9bX8Zmx1CXHwo8wFm+mxgx07biFK+q19EaidJH4jfd2NvYupmJ1r1R393G1lJ8mf1fZVzWEb4lOp9x3x+gpne1h7A92G5zJYIQQKBgCQpLRTt+BwvXXp5LNNqQqYCbyCu3pHkjWlOyOooOsILAeZO+CX9rrN70zeX4AwinclD7v2UXdW29BGzBh5oUy01Rc67cAchBL8WsKecgujh97EdnhvDqqisFq4LSfz+JHArfMuQFycygaZzW5Ot4KBtl2OWdR7vUXrMDFDAKuK3AoGAKlwSk7n9OiDRguuS21qMYwZE3oeWISRDL6yEjaENzGeydUlFioWF1IbbnvGmwIMKCjIW1hmKEHJi5wsRwJ7FG2/v7GGRvVxfj5KdG6aWCSixUm8dHtE85OaRjc57rSU6louKOF8k5ECoC4xUOWjx0ADwRsMQNlFFCEBge/aqS+E=",
		
		//异步通知地址
		'notify_url' => "https://www.gouziyuan.cn/alipay_log.php",
		
		//同步跳转
		'return_url' => "https://www.gouziyuan.cn/alipay_log.php",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh8d20DC/ZHiPf3Tl09kGa99bYaJEtcFdYbvE44EenQjj0Y95cC+r6otEqs1+0pu1lEBxuLqFY3xvlCbZCRdfIR1E2suzt+Fd+YPAk/IV4mpe6QCpQwObIfqmbAHfxLzG5CuT/JQs92DBIeXv9ZvpM3RKT0Vr0g8hqiUl6AcDfZeqXpU2rMJiCeaEfAzu4ldV0enyeWvk7xpv97QldwEVMuomSS0e0oIGPfq/Xhc84iPDWQioa95fPJxF0hVDs/PegNZqLlHRjEZ7Vpo0SFmfHPdrpBjlCym+oyrbrkn3sFpp2AKYTAD4LEZrKbh6D+3KChNzPV5F+P8nnniJ6A/aKwIDAQAB",

        //日志路径
        'log_path' => "",
);