<?php 

namespace pay\wxpay;

use app\common\lib\Str;


/**
 * 微信扫码支付
 */
class NativePay extends Base
{

	private $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
	private $notify_url = 'https://api.xxxxx.com/v1/weChat/payCallback'; // 回调地址
	private $appid = '';
	private $mch_id = '';

	
	
	public function __construct()
	{

		// dump($result);
	}

	/**
	 * 获取二维码
	 * @Author   cendxia
	 * @DateTime 2023-04-12T20:58:53+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $price    [description]
	 * @param    [type]                   $goods_id [description]
	 * @param    string                   $attach   [description]
	 * @return   [type]                             [description]
	 */
	public function getQRcode($order_id, $price, $goods_id, $attach='ziyuan'){
		$params = self::params($order_id, $price, $goods_id, $attach);
		$params['sign'] = self::getSign($params);
		// 验证签名
		$checkSign = self::checkSign($params);

		$xmlData = Str::ArrToXml($params);
		// 发送请求
		$result = self::htmlUrl($this->url, $xmlData);
		// xml转数组
		$result = Str::xmlArr($result);
		if(!empty($result) && $result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS'){
			return $result['code_url']; // 二维码
		}else if(!empty($result) && isset($result['err_code_des'])){
			// 写入日志
			throw new \think\Exception($result['err_code_des']);
		}else{
			// 写入日志
			throw new \think\Exception($result['return_msg']);
		}
	}

	/**
	 * 封装需要的参数
	 * @Author   cendxia
	 * @DateTime 2023-04-12T20:59:32+0800
	 * @param    [type]                   $order_id [description]
	 * @param    [type]                   $price    [description]
	 * @param    [type]                   $goods_id [description]
	 * @param    [type]                   $attach   [description]
	 * @param    string                   $body     [description]
	 * @return   [type]                             [description]
	 */
	private function params($order_id, $price, $goods_id, $attach, $body='扫码支付'){
		return [
			'appid' => $this->appid,
			'mch_id' => $this->mch_id,
			'nonce_str' => md5(time()), // 随机字符串
			'body' => $body, // 商品描述
			'out_trade_no' => $order_id, // 商户订单号
			'total_fee' => $price, // 标价金额(分)
			'spbill_create_ip' => request()->ip(), // IP
			'notify_url' => $this->notify_url,
			'trade_type' => 'NATIVE', // 交易类型
			'product_id' => $goods_id, // trade_type=NATIVE时，此参数必传
			'attach' => $attach
		];
	}

}