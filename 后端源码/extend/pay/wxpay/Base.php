<?php 

namespace pay\wxpay;

/**
 *  微信支付签名
 */
class Base
{

	private $key = 'xxxxxxxxxxxxxxxxxxxxxx';
	
	public function __construct()
	{
		// code...
	}


	// 签名
	protected function getSign($data){
		// 去除数组的空值
		array_filter($data);
		if(isset($data['sign'])){
			unset($data['sign']);
		}
		// 排序
		ksort($data);
		// 组装字符，urldecode为转义字符
		$str = $this->arrToUrl($data) . '&key=' . $this->key;
		// 使用md5加密,并转换成大写
		return strtoupper(md5($str));
	}

	// 验证签名
	protected function checkSign($data){
		$sign = $this->getSign($data);
		if($sign != $data['sign']){
			throw new \think\Exception('签名错误');
		}
		return true;
	}

	// 数组转url
	protected function arrToUrl($data){
		return urldecode(http_build_query($data));
	}

	
	protected function htmlUrl($url, $data){
		$ch = curl_init();
		$params[CURLOPT_URL] = $url;    //请求url地址
		$params[CURLOPT_HEADER] = false; //是否返回响应头信息
		$params[CURLOPT_RETURNTRANSFER] = true; //是否将结果返回
		$params[CURLOPT_FOLLOWLOCATION] = true; //是否重定向

		$params[CURLOPT_POST] = true;
		$params[CURLOPT_POSTFIELDS] = $data;

		// 解决方案一，生产环境不使用
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  //不验证 SSL 证书
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);//不验证 SSL 证书域名

		//解决方案二 设置证书
		// curl_setopt($ch,CURLOPT_CAINFO,root_path().'/public/cacert.pem'); //证书路径需要正确

		curl_setopt_array($ch, $params); //传入curl参数
		$content = curl_exec($ch); //执行
		curl_close($ch); //关闭连接

		return $content;
	}
}