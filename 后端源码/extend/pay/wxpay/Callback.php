<?php 

namespace pay\wxpay;

/**
 * 微信支付回调
 */
class Callback extends Base
{
	/**
	 * 验签
	 * @Author   cendxia
	 * @DateTime 2022-11-03T15:42:26+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function checkSignEvent($data){
		return $this->checkSign($data);
	}
}