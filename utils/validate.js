import config from "~/utils/config.js"
import language from "~/utils/language.js"

export default{
    // 手机号验证
    phone(value){
        let reg = config.phoneReg
        if(reg.test(value)){
            return true
        }
        return false
    },

    // 姓名验证
    userName(value){
        let reg = config.userNameReg
        if(reg.test(value)){
            return true
        }
        return false
    },

    // 身份证号验证
    idCardReg(value){
        let reg = config.idCardReg
        if(reg.test(value)){
            return true
        }
        return false
    },


    // 密码验证规则
    password(value){
        let reg = config.passwordReg
        if(value.length < config.passwordMin || value.length > config.passwordMax){
            return {
                status:false,
                msg:language.passwordRange
            }
        }
        if(!reg.test(value)){
            return {
                status:false,
                msg:language.passwordForm
            }
        }
        return {
            status:true,
            msg:'SUCCESS'
        }
    },

    // 邮箱验证
    email(value){
        var reg = config.emailReg
        if(reg.test(value)){
            return true
        }
        return false
    },
    
    // QQ号验证
    qq(value){
        var reg = config.qqReg
        if(reg.test(value)){
            return true
        }
        return false
    }

}
