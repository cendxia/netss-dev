module.exports = {
    "codeCountDown": 60,  // 获取验证码倒计时时间，单位为秒
    "phoneReg": /^1[3456789]\d{9}$/,
    "userNameReg": /^[\u4e00-\u9fa5]{2,15}$/, // 中文姓名规则
    "idCardReg": /^[1-9]\d{5}[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}(\d|x|X)$/
, // 身份证号验证规则
    "emailReg": /^\w+@[a-zA-Z0-9]{2,10}(?:\.[a-z]{2,4}){1,3}$/,
    "qqReg": /^[1-9][0-9]{4,12}$/,
    "passwordReg": /^[a-zA-Z0-9_]{1,}$/,
    "passwordMin":6, // 密码最小长度
    "passwordMax":25, // 密码最大长度

    "txMoneyMin":10, // 最小提现金额

    "downloadTipsCountdown":15, // 下载提示框自动关闭秒数


    "bankArr":['建设银行','工商银行','招商银行','农商银行','农业银行','邮政储蓄银行'],

    "mobileUrl":"https://www.dtku.cn/"   // 移动端地址


}
