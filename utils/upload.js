import axios from 'axios'

const imgUrl = 'https://xxx.com/v1/imgUpload'   // 你的文件上传地址

export default {

    // 上传文件
    uploadFile(file, token='', bucket='', type='img', editor=''){
        if(!token){
            alert('参数错误')
            return false
        }

        // let url = type == 'img' ? imgUrl : fileUrl

        let url = imgUrl

        let params = new FormData();
        // let timeer = Date.now();
        params.append("file", file);
        params.append("bucket", bucket);
        axios.defaults.headers.common['token'] = token
        axios.defaults.timeout = 800000

        if(editor){ // 判断是否是wangeditor编辑器
            params.append("type", editor);
        }else{
            params.append("type", type);
        }
        return axios.post(url, params)
    },

    // 验证图片类型
    fileTypeCheck(file, type="img"){
        if (type == 'img') {
            if(file.type !== 'image/jpeg' && file.type !== 'image/png' && file.type !== 'image/gif'){
                return false
            }
            return true
        }
    }

}
