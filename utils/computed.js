export default{
    // 计算总页数
    page(total, per_page){  // total 总条数 per_page 每页显示条数
        if (total == null || total == "") {
            return 0;
        } else {
            if (per_page != 0 && total % per_page == 0) {
                return parseInt(total / per_page)
            }
            if (per_page != 0 && total % per_page != 0) {
                return parseInt(total / per_page) + 1;
            }
        }
    },

    // 根据身份证号计算年龄
    getAge(identityCard) {
        var len = (identityCard + "").length;
        if (len == 0) {
            return 0;
        } else {
            if ((len != 15) && (len != 18))//身份证号码只能为15位或18位其它不合法
            {
                return 0;
            }
        }
        var strBirthday = "";
        if (len == 18)//处理18位的身份证号码从号码中得到生日和性别代码
        {
            strBirthday = identityCard.substr(6, 4) + "/" + identityCard.substr(10, 2) + "/" + identityCard.substr(12, 2);
        }
        if (len == 15) {
            strBirthday = "19" + identityCard.substr(6, 2) + "/" + identityCard.substr(8, 2) + "/" + identityCard.substr(10, 2);
        }
        //时间字符串里，必须是“/”
        var birthDate = new Date(strBirthday);
        var nowDateTime = new Date();
        var age = nowDateTime.getFullYear() - birthDate.getFullYear();
        //再考虑月、天的因素;.getMonth()获取的是从0开始的，这里进行比较，不需要加1
        if (nowDateTime.getMonth() < birthDate.getMonth() || (nowDateTime.getMonth() == birthDate.getMonth() && nowDateTime.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    },

	// 根据身份证号获取生日
	getBirthday(idCard) {
		var birthday = "";
		if(idCard != null && idCard != ""){
			if(idCard.length == 15){
				birthday = "19"+idCard.substr(6,6);
			} else if(idCard.length == 18){
				birthday = idCard.substr(6,8);
			}
			birthday = birthday.replace(/(.{4})(.{2})/,"$1-$2-");
		}
		return birthday;
	},

	getDate(date, _d=''){ // 根据 new Date() 转换为 日期格式
		// Ymd
		let y = date.getFullYear()
		let m = date.getMonth()
		let d = date.getDate()

		//获取当前年的月份 月份要 + 1 （0代表1月）date.getMonth() + 1
		m = parseInt(m)+1
		let result = y+'-'+m
		if(_d != '') result += '-'+d
		return result
	},

	// 计算时间是否在指定时间范围内
	isDuringDate(beginDateStr, endDateStr) {
		var curDate = new Date(),
		beginDate = new Date(beginDateStr),
		endDate = new Date(endDateStr);
		if (curDate >= beginDate && curDate <= endDate) {
			return true;
		}
		return false;
	},

	getDateCompare(date1,date2){
		let oDate1 = new Date(date1);
		let oDate2 = new Date(date2);

		if(oDate1.getTime() > oDate2.getTime()){
			// console.log('第一个大');
			return true
		}
		// console.log('第二个大');
		return false
	},

    // 获取获取前7天日期
    getAWeekAgoDate(){
        let arr = []
        for(let i = -7;i <= -1;i++){
            arr.push(this.fun_date(i))
        }
        return arr
    },

    fun_date(num){
        let date1 = new Date(),
        time1=date1.getFullYear()+"-"+(date1.getMonth()+1)+"-"+date1.getDate();//time1表示当前时间
        let date2 = new Date(date1);
        date2.setDate(date1.getDate()+num);
        let time2 = date2.getFullYear()+"-"+(date2.getMonth()+1)+"-"+date2.getDate();
        return time2
    },

}
