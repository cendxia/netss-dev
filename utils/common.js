export default{
    isMobile() {
        const userAgent = navigator.userAgent || navigator.vendor || window.opera;
        return /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent);
        // let res = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent);

        // console.log(res, 'rrrrrrrrrrrrr')
        // if(res === true){
        //     // 移动端
        // }else{
        //     // PC端
        // }
    },


    // 获取js参数
    getUrlParams(url){
        // 通过 ? 分割获取后面的参数字符串
        let urlStr = url.split('?')[1];
        if(!url || !urlStr){
            return '';
        }
        // 创建空对象存储参数
        let obj = {};
        // 再通过 & 将每一个参数单独分割出来
        let paramsArr = urlStr.split('&');
        for(let i = 0,len = paramsArr.length;i < len;i++){
            // 再通过 = 将每一个参数分割为 key:value 的形式
            let arr = paramsArr[i].split('=');
            obj[arr[0]] = arr[1];
        }
        return obj;
    },


    /**
     * 随机生成字符串
     */
    randomString() {
        // 先生成随机长度
        let length = Math.round(Math.random()*18+15);
        var str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var result = '';
        for (var i = length; i > 0; --i)
        result += str[Math.floor(Math.random() * str.length)];
        return result;
    },

    // 音频格式
    audioForm(){
        return ['MP3','WAV','WMA','MP2','Flac','MIDI','RA','APE','AAC','CDA','MOV'];
    },

    // 视频格式
    videoForm(){
        return ['MP4','FLV','M3U8'];
    },

    // 复制内容
    copyText(text){
        // 创建一个 input 元素并设置其值为需要复制的文本
        const copyText = document.createElement('input');
        copyText.value = text;
        document.body.appendChild(copyText);

        // 选中输入框中的文本
        copyText.select();

        try {
          // 将选中的文本复制到剪贴板
          const successful = document.execCommand('copy');
          if (successful) {
            console.log('成功复制内容！');
          } else {
            console.error('无法复制内容！');
          }
        } catch (err) {
          console.error('无法复制内容！', err);
        }

        // 移除添加的 input 元素
        document.body.removeChild(copyText);
    }


}
