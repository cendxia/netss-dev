import area from 'china-area-data'

export default{
    areaData(){
        let result = []

        let areaData = area[86]
        for(let key  in areaData){
            let arr = {
                value: areaData[key],
                label: areaData[key],
                children: []
            }
            for(let children in area[key]){
                let childrenVar = {
                    value: area[key][children],
                    label: area[key][children],
                    children:[]
                }
                for(let k in area[children]){
                    let val = {
                        value: area[children][k],
                        label: area[children][k]
                    }
                    childrenVar.children.push(val)
                }
                arr.children.push(childrenVar)
            }
            result.push(arr)
        }

        return result
    }
}
