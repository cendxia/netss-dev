module.exports = {
    "emptyFont": "暂无数据",
    "imgUploadError": "图片上传失败",
    "imgUploadSuccess": "图片上传成功",
    "imgTypeError": "图片类型错误",
    "fileUploadError": "文件上传失败",

    "noAccount": "未添加收款账户",

    "phoneError": "手机号错误",
    "phoneEditSuccess": "手机号修改成功",

    "emailError": "邮箱号错误",
    "emailSetSuccess": "邮箱设置成功",


    "codeError": "验证码错误",
    "getCode": "获取验证码",
    "rGetCode": "重新获取验证码",


    "passwordError": "密码错误",
    "passwordRange": "密码长度范围在6-25个字符之间！",
    "passwordForm": "密码格式不正确，只允许字母、数字和下划线！",
    "rpasswordError": "两次密码不一致",
    "passwordEditSuccess": "密码修改成功",


    "copySuccess": "复制成功",

    "tixianError": "提现失败",
    "tixianSuccess": "提现成功",
    "tixianMinLimit": "提现金额小于",
    "tixianMaxLimit": "提现金额大于可用余额",
    
}
