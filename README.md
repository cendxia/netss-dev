

# NETSS资源网+云课堂系统

NETSS资源网+云课堂系统，框架采用Vue+ThinkPHP6+MySQL+Ant Design，系统可商用；采用前后端分离开发，前后端源码100%开源，可二开。支持微信+支付宝+虎皮椒微信支付、创作者中心，支持用户入驻平台、微信扫码登录、七牛云存储、阿里云短信接口+网易云短信接口


#
创作者中心：用户申请为创作者后，可以在上面发布资源和课程   
平台支付方式：微信、支付宝和虎皮椒微信支付   
登录方式：账户密码登录、微信扫码登录、强制关注公众号后扫码登录   
资源存储：七牛云存储

#

#### 盈利方式：
1、自己发布资源或课程销售  
2、用户在平台上发布资源或课程，产生交易后平台可获得相应手续费

#

#### 正式版购买：
https://www.netss.cn


#### 演示地址：
https://www.gouziyuan.cn 或 https://www.zijiao.cn


#### 测试接口地址：
http://demozjwapi.apiall.cn



## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
