/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : gouziyuan

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2024-05-05 14:11:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lc_ad
-- ----------------------------
DROP TABLE IF EXISTS `lc_ad`;
CREATE TABLE `lc_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ad_id` int(10) unsigned NOT NULL COMMENT '广告位id',
  `region_id` int(10) unsigned NOT NULL COMMENT '区域id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '跳转url',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示  0不显示 1显示',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_ad
-- ----------------------------
INSERT INTO `lc_ad` VALUES ('2', '2', '3', '广告1', 'https://scpic.chinaz.net/files/pic/pic9/201507/apic13094.jpg', 'demo.com', '1', '2022-02-28 11:48:40', '2022-03-09 20:28:44');

-- ----------------------------
-- Table structure for lc_admin_action_log
-- ----------------------------
DROP TABLE IF EXISTS `lc_admin_action_log`;
CREATE TABLE `lc_admin_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '操作名称',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '操作值',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=492 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_admin_action_log
-- ----------------------------

-- ----------------------------
-- Table structure for lc_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `lc_admin_user`;
CREATE TABLE `lc_admin_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_name` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '登录名',
  `user_name` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '姓名',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `salt` varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '随机字符串',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '手机号',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1正常 2禁用 -1删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  KEY `login_name` (`login_name`,`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_admin_user
-- ----------------------------
INSERT INTO `lc_admin_user` VALUES ('1', 'netss', 'NETSS系统', '410080fbea312b10a3febb14917124ca', '9P7bsqUw5v1sMKY', '18888888888', '1', '2024-05-05 18:09:55', '2024-05-05 13:49:43');

-- ----------------------------
-- Table structure for lc_admin_user_login
-- ----------------------------
DROP TABLE IF EXISTS `lc_admin_user_login`;
CREATE TABLE `lc_admin_user_login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_admin_user_login
-- ----------------------------
INSERT INTO `lc_admin_user_login` VALUES ('131', '1', '127.0.0.1', '2024-05-05 13:50:09', '2024-05-05 13:50:09');

-- ----------------------------
-- Table structure for lc_advertisement
-- ----------------------------
DROP TABLE IF EXISTS `lc_advertisement`;
CREATE TABLE `lc_advertisement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) unsigned NOT NULL COMMENT '区域id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '描述',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 0正常 1禁用',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_advertisement
-- ----------------------------
INSERT INTO `lc_advertisement` VALUES ('2', '3', '广告位名称', '描述', '1', '2022-02-12 12:19:32', '2022-02-12 12:23:35');
INSERT INTO `lc_advertisement` VALUES ('3', '3', '广告位名称', '描述', '1', '2022-02-21 16:31:17', '2022-02-21 16:31:17');

-- ----------------------------
-- Table structure for lc_article
-- ----------------------------
DROP TABLE IF EXISTS `lc_article`;
CREATE TABLE `lc_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL COMMENT '类别id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '封面',
  `keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '关键词',
  `describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '摘要',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '详情',
  `content_html` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '文章格式化后的数据，自动采集时使用',
  `read_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读次数',
  `show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示；0不显示，1显示',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_article
-- ----------------------------

-- ----------------------------
-- Table structure for lc_article_category
-- ----------------------------
DROP TABLE IF EXISTS `lc_article_category`;
CREATE TABLE `lc_article_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '类别名称',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `type` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '类型：notice、protocol、common',
  `show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示，0不显示，1显示',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_article_category
-- ----------------------------
INSERT INTO `lc_article_category` VALUES ('1', '用户协议', '0', 'protocol', '1', '2022-10-20 21:40:16', '2022-11-28 20:03:45');
INSERT INTO `lc_article_category` VALUES ('2', '平台公告', '0', 'protocol', '1', '2022-10-20 21:41:41', '2022-11-28 20:03:37');
INSERT INTO `lc_article_category` VALUES ('3', '平台文章', '0', 'common', '1', '2022-10-20 21:42:11', '2022-11-28 20:03:56');
INSERT INTO `lc_article_category` VALUES ('4', '公司介绍', '0', 'common', '1', '2023-01-07 12:08:03', '2023-01-07 12:08:03');
INSERT INTO `lc_article_category` VALUES ('5', '资源社区', '0', 'common', '1', '2023-05-24 23:35:21', '2023-05-24 23:36:32');

-- ----------------------------
-- Table structure for lc_download_log
-- ----------------------------
DROP TABLE IF EXISTS `lc_download_log`;
CREATE TABLE `lc_download_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '订单号',
  `user_id` int(10) unsigned NOT NULL,
  `sell_user_id` int(10) unsigned NOT NULL COMMENT '卖家id',
  `ziyuan_id` int(10) unsigned NOT NULL,
  `ziyuan_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '资源名称',
  `ziyuan_cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '封面',
  `extract` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '提取码/解压密码',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `sell_user_id` (`sell_user_id`),
  KEY `ziyuan_id` (`ziyuan_id`),
  KEY `ziyuan_title` (`ziyuan_title`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_download_log
-- ----------------------------

-- ----------------------------
-- Table structure for lc_error_log
-- ----------------------------
DROP TABLE IF EXISTS `lc_error_log`;
CREATE TABLE `lc_error_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mode` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '模块名称',
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '文件',
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '描述',
  `code` int(11) NOT NULL COMMENT '状态码',
  `line` int(10) unsigned NOT NULL COMMENT '行号',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'url',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'IP地址',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `mode` (`mode`)
) ENGINE=InnoDB AUTO_INCREMENT=56062 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_error_log
-- ----------------------------
INSERT INTO `lc_error_log` VALUES ('53122', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/think-orm/src/db/Builder.php', 'fields not exists:[ke_id]', '10500', '164', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 12:07:13', '2023-05-04 12:07:13');
INSERT INTO `lc_error_log` VALUES ('53123', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/think-orm/src/db/Builder.php', 'fields not exists:[ke_id]', '10500', '164', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 12:07:33', '2023-05-04 12:07:33');
INSERT INTO `lc_error_log` VALUES ('53124', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/think-orm/src/db/Builder.php', 'fields not exists:[ke_id]', '10500', '164', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 12:08:39', '2023-05-04 12:08:39');
INSERT INTO `lc_error_log` VALUES ('53125', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/think-orm/src/db/Builder.php', 'fields not exists:[ke_id]', '10500', '164', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 12:10:51', '2023-05-04 12:10:51');
INSERT INTO `lc_error_log` VALUES ('53126', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/think-orm/src/db/Builder.php', 'fields not exists:[ke_id]', '10500', '164', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 12:11:39', '2023-05-04 12:11:39');
INSERT INTO `lc_error_log` VALUES ('53127', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/think-orm/src/db/Builder.php', 'fields not exists:[ke_id]', '10500', '164', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 12:12:20', '2023-05-04 12:12:20');
INSERT INTO `lc_error_log` VALUES ('53128', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.18', '2023-05-04 13:07:56', '2023-05-04 13:07:56');
INSERT INTO `lc_error_log` VALUES ('53129', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.134', '2023-05-04 13:08:10', '2023-05-04 13:08:10');
INSERT INTO `lc_error_log` VALUES ('53130', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '185.142.236.40', '2023-05-04 13:14:16', '2023-05-04 13:14:16');
INSERT INTO `lc_error_log` VALUES ('53131', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-04 13:31:48', '2023-05-04 13:31:48');
INSERT INTO `lc_error_log` VALUES ('53132', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/mp/delKechengDirectory', '117.189.227.214', '2023-05-04 13:53:56', '2023-05-04 13:53:56');
INSERT INTO `lc_error_log` VALUES ('53133', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/mp/delKechengDirectory', '117.189.227.214', '2023-05-04 13:54:17', '2023-05-04 13:54:17');
INSERT INTO `lc_error_log` VALUES ('53134', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-05-04 13:57:35', '2023-05-04 13:57:35');
INSERT INTO `lc_error_log` VALUES ('53135', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-05-04 14:02:30', '2023-05-04 14:02:30');
INSERT INTO `lc_error_log` VALUES ('53136', 'api', '/www/wwwroot/gouziyuan/app/api/business/MpKecheng.php', '数据操作失败', '0', '198', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 14:09:35', '2023-05-04 14:09:35');
INSERT INTO `lc_error_log` VALUES ('53137', 'api', '/www/wwwroot/gouziyuan/app/api/model/KechengDirectory.php', 'Undefined index: id', '0', '32', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-04 14:10:32', '2023-05-04 14:10:32');
INSERT INTO `lc_error_log` VALUES ('53138', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-05-04 14:21:09', '2023-05-04 14:21:09');
INSERT INTO `lc_error_log` VALUES ('53139', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-05-04 14:21:38', '2023-05-04 14:21:38');
INSERT INTO `lc_error_log` VALUES ('53140', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-05-04 14:21:53', '2023-05-04 14:21:53');
INSERT INTO `lc_error_log` VALUES ('53141', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-05-04 14:32:07', '2023-05-04 14:32:07');
INSERT INTO `lc_error_log` VALUES ('53142', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '147.182.130.98', '2023-05-04 14:33:10', '2023-05-04 14:33:10');
INSERT INTO `lc_error_log` VALUES ('53143', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '147.182.130.98', '2023-05-04 14:33:17', '2023-05-04 14:33:17');
INSERT INTO `lc_error_log` VALUES ('53144', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?rest_route=/wp/v2/users/', '147.182.130.98', '2023-05-04 14:33:17', '2023-05-04 14:33:17');
INSERT INTO `lc_error_log` VALUES ('53145', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/api/search?folderIds=0', '147.182.130.98', '2023-05-04 14:33:18', '2023-05-04 14:33:18');
INSERT INTO `lc_error_log` VALUES ('53146', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/getOrderList?page=1&pageSize=20', '66.249.72.130', '2023-05-04 15:14:24', '2023-05-04 15:14:24');
INSERT INTO `lc_error_log` VALUES ('53147', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '65.49.20.69', '2023-05-04 16:52:59', '2023-05-04 16:52:59');
INSERT INTO `lc_error_log` VALUES ('53148', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-05-04 16:59:23', '2023-05-04 16:59:23');
INSERT INTO `lc_error_log` VALUES ('53149', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.142.52', '2023-05-04 17:02:26', '2023-05-04 17:02:26');
INSERT INTO `lc_error_log` VALUES ('53150', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '65.49.20.69', '2023-05-04 17:03:57', '2023-05-04 17:03:57');
INSERT INTO `lc_error_log` VALUES ('53151', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-05-04 17:35:43', '2023-05-04 17:35:43');
INSERT INTO `lc_error_log` VALUES ('53152', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-05-04 18:32:44', '2023-05-04 18:32:44');
INSERT INTO `lc_error_log` VALUES ('53153', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-05-04 18:32:46', '2023-05-04 18:32:46');
INSERT INTO `lc_error_log` VALUES ('53154', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?XDEBUG_SESSION_START=phpstorm', '152.89.196.144', '2023-05-04 20:21:16', '2023-05-04 20:21:16');
INSERT INTO `lc_error_log` VALUES ('53155', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.147.108', '2023-05-04 20:24:30', '2023-05-04 20:24:30');
INSERT INTO `lc_error_log` VALUES ('53156', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.162.134', '2023-05-04 20:28:58', '2023-05-04 20:28:58');
INSERT INTO `lc_error_log` VALUES ('53157', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.176.225', '2023-05-04 20:35:09', '2023-05-04 20:35:09');
INSERT INTO `lc_error_log` VALUES ('53158', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.157.131', '2023-05-04 21:05:32', '2023-05-04 21:05:32');
INSERT INTO `lc_error_log` VALUES ('53159', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.165.117', '2023-05-04 21:05:32', '2023-05-04 21:05:32');
INSERT INTO `lc_error_log` VALUES ('53160', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.176.55', '2023-05-04 21:25:31', '2023-05-04 21:25:31');
INSERT INTO `lc_error_log` VALUES ('53161', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.164.14', '2023-05-04 21:29:58', '2023-05-04 21:29:58');
INSERT INTO `lc_error_log` VALUES ('53162', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.153.26', '2023-05-04 21:35:39', '2023-05-04 21:35:39');
INSERT INTO `lc_error_log` VALUES ('53163', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.162.134', '2023-05-04 21:44:37', '2023-05-04 21:44:37');
INSERT INTO `lc_error_log` VALUES ('53164', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '193.118.53.194', '2023-05-04 23:43:28', '2023-05-04 23:43:28');
INSERT INTO `lc_error_log` VALUES ('53165', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/imgUpload', '117.189.227.214', '2023-05-05 00:10:23', '2023-05-05 00:10:23');
INSERT INTO `lc_error_log` VALUES ('53166', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/getOrderList?page=1&pageSize=20', '36.99.136.129', '2023-05-05 00:35:36', '2023-05-05 00:35:36');
INSERT INTO `lc_error_log` VALUES ('53167', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '167.248.133.36', '2023-05-05 00:42:29', '2023-05-05 00:42:29');
INSERT INTO `lc_error_log` VALUES ('53168', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '34.76.96.55', '2023-05-05 02:19:35', '2023-05-05 02:19:35');
INSERT INTO `lc_error_log` VALUES ('53169', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '134.209.21.151', '2023-05-05 02:29:20', '2023-05-05 02:29:20');
INSERT INTO `lc_error_log` VALUES ('53170', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-05 02:42:33', '2023-05-05 02:42:33');
INSERT INTO `lc_error_log` VALUES ('53171', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-05 02:42:33', '2023-05-05 02:42:33');
INSERT INTO `lc_error_log` VALUES ('53172', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.17', '2023-05-05 03:05:33', '2023-05-05 03:05:33');
INSERT INTO `lc_error_log` VALUES ('53173', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-05-05 03:09:43', '2023-05-05 03:09:43');
INSERT INTO `lc_error_log` VALUES ('53174', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-05-05 03:09:44', '2023-05-05 03:09:44');
INSERT INTO `lc_error_log` VALUES ('53175', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '23.251.102.74', '2023-05-05 04:03:59', '2023-05-05 04:03:59');
INSERT INTO `lc_error_log` VALUES ('53176', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.21', '2023-05-05 05:05:20', '2023-05-05 05:05:20');
INSERT INTO `lc_error_log` VALUES ('53177', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '198.235.24.78', '2023-05-05 05:19:15', '2023-05-05 05:19:15');
INSERT INTO `lc_error_log` VALUES ('53178', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-05-05 07:17:26', '2023-05-05 07:17:26');
INSERT INTO `lc_error_log` VALUES ('53179', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '40.68.5.205', '2023-05-05 07:20:40', '2023-05-05 07:20:40');
INSERT INTO `lc_error_log` VALUES ('53180', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.25', '2023-05-05 08:34:23', '2023-05-05 08:34:23');
INSERT INTO `lc_error_log` VALUES ('53181', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.27', '2023-05-05 08:34:26', '2023-05-05 08:34:26');
INSERT INTO `lc_error_log` VALUES ('53182', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.27', '2023-05-05 08:34:31', '2023-05-05 08:34:31');
INSERT INTO `lc_error_log` VALUES ('53183', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '205.210.31.22', '2023-05-05 08:50:40', '2023-05-05 08:50:40');
INSERT INTO `lc_error_log` VALUES ('53184', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.134', '2023-05-05 09:42:50', '2023-05-05 09:42:50');
INSERT INTO `lc_error_log` VALUES ('53185', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '162.221.192.26', '2023-05-05 11:07:46', '2023-05-05 11:07:46');
INSERT INTO `lc_error_log` VALUES ('53186', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '192.241.210.43', '2023-05-05 11:38:05', '2023-05-05 11:38:05');
INSERT INTO `lc_error_log` VALUES ('53187', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '64.62.197.142', '2023-05-05 11:59:10', '2023-05-05 11:59:10');
INSERT INTO `lc_error_log` VALUES ('53188', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '64.62.197.142', '2023-05-05 12:06:15', '2023-05-05 12:06:15');
INSERT INTO `lc_error_log` VALUES ('53189', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.135', '2023-05-05 12:16:24', '2023-05-05 12:16:24');
INSERT INTO `lc_error_log` VALUES ('53190', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '185.100.87.136', '2023-05-05 13:15:02', '2023-05-05 13:15:02');
INSERT INTO `lc_error_log` VALUES ('53191', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-05 14:23:54', '2023-05-05 14:23:54');
INSERT INTO `lc_error_log` VALUES ('53192', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-05 14:23:55', '2023-05-05 14:23:55');
INSERT INTO `lc_error_log` VALUES ('53193', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.138', '2023-05-05 14:43:12', '2023-05-05 14:43:12');
INSERT INTO `lc_error_log` VALUES ('53194', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.141', '2023-05-05 14:44:15', '2023-05-05 14:44:15');
INSERT INTO `lc_error_log` VALUES ('53195', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.141', '2023-05-05 14:44:18', '2023-05-05 14:44:18');
INSERT INTO `lc_error_log` VALUES ('53196', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-05 15:05:29', '2023-05-05 15:05:29');
INSERT INTO `lc_error_log` VALUES ('53197', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-05-05 15:13:54', '2023-05-05 15:13:54');
INSERT INTO `lc_error_log` VALUES ('53198', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-05-05 15:13:54', '2023-05-05 15:13:54');
INSERT INTO `lc_error_log` VALUES ('53199', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-05 15:18:41', '2023-05-05 15:18:41');
INSERT INTO `lc_error_log` VALUES ('53200', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-05-05 15:24:42', '2023-05-05 15:24:42');
INSERT INTO `lc_error_log` VALUES ('53201', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-05-05 15:24:42', '2023-05-05 15:24:42');
INSERT INTO `lc_error_log` VALUES ('53202', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.227.214', '2023-05-05 15:32:36', '2023-05-05 15:32:36');
INSERT INTO `lc_error_log` VALUES ('53203', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-05-05 17:25:47', '2023-05-05 17:25:47');
INSERT INTO `lc_error_log` VALUES ('53204', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.176', '2023-05-05 17:25:49', '2023-05-05 17:25:49');
INSERT INTO `lc_error_log` VALUES ('53205', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-05-05 17:33:23', '2023-05-05 17:33:23');
INSERT INTO `lc_error_log` VALUES ('53206', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-05 17:41:03', '2023-05-05 17:41:03');
INSERT INTO `lc_error_log` VALUES ('53207', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-05-05 17:45:38', '2023-05-05 17:45:38');
INSERT INTO `lc_error_log` VALUES ('53208', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-05 17:47:43', '2023-05-05 17:47:43');
INSERT INTO `lc_error_log` VALUES ('53209', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-05-05 17:50:37', '2023-05-05 17:50:37');
INSERT INTO `lc_error_log` VALUES ('53210', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-05 17:51:09', '2023-05-05 17:51:09');
INSERT INTO `lc_error_log` VALUES ('53211', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-05-05 17:51:13', '2023-05-05 17:51:13');
INSERT INTO `lc_error_log` VALUES ('53212', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-05-05 17:51:27', '2023-05-05 17:51:27');
INSERT INTO `lc_error_log` VALUES ('53213', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '176.58.119.216', '2023-05-05 17:51:37', '2023-05-05 17:51:37');
INSERT INTO `lc_error_log` VALUES ('53214', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.106', '2023-05-05 18:14:14', '2023-05-05 18:14:14');
INSERT INTO `lc_error_log` VALUES ('53215', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.38', '2023-05-05 18:14:19', '2023-05-05 18:14:19');
INSERT INTO `lc_error_log` VALUES ('53216', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-05-05 18:26:54', '2023-05-05 18:26:54');
INSERT INTO `lc_error_log` VALUES ('53217', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '36.99.136.129', '2023-05-05 19:38:35', '2023-05-05 19:38:35');
INSERT INTO `lc_error_log` VALUES ('53218', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '119.29.80.239', '2023-05-06 03:07:47', '2023-05-06 03:07:47');
INSERT INTO `lc_error_log` VALUES ('53219', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.163', '2023-05-06 03:08:26', '2023-05-06 03:08:26');
INSERT INTO `lc_error_log` VALUES ('53220', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-05-06 03:08:36', '2023-05-06 03:08:36');
INSERT INTO `lc_error_log` VALUES ('53221', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-05-06 03:08:41', '2023-05-06 03:08:41');
INSERT INTO `lc_error_log` VALUES ('53222', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-05-06 03:08:41', '2023-05-06 03:08:41');
INSERT INTO `lc_error_log` VALUES ('53223', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '36.99.136.138', '2023-05-06 03:20:06', '2023-05-06 03:20:06');
INSERT INTO `lc_error_log` VALUES ('53224', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '36.99.136.138', '2023-05-06 03:20:07', '2023-05-06 03:20:07');
INSERT INTO `lc_error_log` VALUES ('53225', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.152.28.95', '2023-05-06 03:54:54', '2023-05-06 03:54:54');
INSERT INTO `lc_error_log` VALUES ('53226', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.152.33.49', '2023-05-06 03:55:46', '2023-05-06 03:55:46');
INSERT INTO `lc_error_log` VALUES ('53227', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.4', '2023-05-06 11:21:40', '2023-05-06 11:21:40');
INSERT INTO `lc_error_log` VALUES ('53228', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-06 12:04:00', '2023-05-06 12:04:00');
INSERT INTO `lc_error_log` VALUES ('53229', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '112.13.112.68', '2023-05-06 13:23:20', '2023-05-06 13:23:20');
INSERT INTO `lc_error_log` VALUES ('53230', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.26', '2023-05-06 14:08:11', '2023-05-06 14:08:11');
INSERT INTO `lc_error_log` VALUES ('53231', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.27', '2023-05-06 14:09:06', '2023-05-06 14:09:06');
INSERT INTO `lc_error_log` VALUES ('53232', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.27', '2023-05-06 14:09:09', '2023-05-06 14:09:09');
INSERT INTO `lc_error_log` VALUES ('53233', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.156.254.81', '2023-05-06 14:28:16', '2023-05-06 14:28:16');
INSERT INTO `lc_error_log` VALUES ('53234', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-05-06 16:04:22', '2023-05-06 16:04:22');
INSERT INTO `lc_error_log` VALUES ('53235', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-05-06 19:50:39', '2023-05-06 19:50:39');
INSERT INTO `lc_error_log` VALUES ('53236', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-06 21:50:41', '2023-05-06 21:50:41');
INSERT INTO `lc_error_log` VALUES ('53237', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-05-06 22:20:58', '2023-05-06 22:20:58');
INSERT INTO `lc_error_log` VALUES ('53238', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-05-06 23:31:06', '2023-05-06 23:31:06');
INSERT INTO `lc_error_log` VALUES ('53239', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-06 23:37:35', '2023-05-06 23:37:35');
INSERT INTO `lc_error_log` VALUES ('53240', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-05-07 00:00:06', '2023-05-07 00:00:06');
INSERT INTO `lc_error_log` VALUES ('53241', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-07 00:11:46', '2023-05-07 00:11:46');
INSERT INTO `lc_error_log` VALUES ('53242', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请填写描述', '0', '524', '/v1/mp/kecheng', '117.189.225.211', '2023-05-07 00:11:48', '2023-05-07 00:11:48');
INSERT INTO `lc_error_log` VALUES ('53243', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23582&order_id=&chapters_id=19&directory_id=240', '116.179.37.13', '2023-05-07 00:14:32', '2023-05-07 00:14:32');
INSERT INTO `lc_error_log` VALUES ('53244', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23582&order_id=&chapters_id=19&directory_id=240', '116.179.37.76', '2023-05-07 00:14:34', '2023-05-07 00:14:34');
INSERT INTO `lc_error_log` VALUES ('53245', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.199', '2023-05-07 12:15:06', '2023-05-07 12:15:06');
INSERT INTO `lc_error_log` VALUES ('53246', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.198', '2023-05-07 15:52:24', '2023-05-07 15:52:24');
INSERT INTO `lc_error_log` VALUES ('53247', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-05-07 15:56:03', '2023-05-07 15:56:03');
INSERT INTO `lc_error_log` VALUES ('53248', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-05-07 15:56:05', '2023-05-07 15:56:05');
INSERT INTO `lc_error_log` VALUES ('53249', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '108', '/v1/login', '117.189.225.211', '2023-05-07 16:08:35', '2023-05-07 16:08:35');
INSERT INTO `lc_error_log` VALUES ('53250', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/mp/delKechengDirectory', '117.189.225.211', '2023-05-07 16:12:06', '2023-05-07 16:12:06');
INSERT INTO `lc_error_log` VALUES ('53251', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-07 23:27:39', '2023-05-07 23:27:39');
INSERT INTO `lc_error_log` VALUES ('53252', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-07 23:27:39', '2023-05-07 23:27:39');
INSERT INTO `lc_error_log` VALUES ('53253', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-08 00:42:17', '2023-05-08 00:42:17');
INSERT INTO `lc_error_log` VALUES ('53254', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '111.7.100.27', '2023-05-08 01:49:03', '2023-05-08 01:49:03');
INSERT INTO `lc_error_log` VALUES ('53255', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.142', '2023-05-08 01:56:30', '2023-05-08 01:56:30');
INSERT INTO `lc_error_log` VALUES ('53256', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.140', '2023-05-08 01:57:24', '2023-05-08 01:57:24');
INSERT INTO `lc_error_log` VALUES ('53257', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.140', '2023-05-08 01:57:34', '2023-05-08 01:57:34');
INSERT INTO `lc_error_log` VALUES ('53258', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.140', '2023-05-08 01:57:34', '2023-05-08 01:57:34');
INSERT INTO `lc_error_log` VALUES ('53259', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.140', '2023-05-08 01:57:47', '2023-05-08 01:57:47');
INSERT INTO `lc_error_log` VALUES ('53260', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.149', '2023-05-08 02:33:42', '2023-05-08 02:33:42');
INSERT INTO `lc_error_log` VALUES ('53261', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.159', '2023-05-08 02:34:24', '2023-05-08 02:34:24');
INSERT INTO `lc_error_log` VALUES ('53262', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.159', '2023-05-08 02:34:25', '2023-05-08 02:34:25');
INSERT INTO `lc_error_log` VALUES ('53263', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.213.126', '2023-05-08 02:37:44', '2023-05-08 02:37:44');
INSERT INTO `lc_error_log` VALUES ('53264', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.41.161.186', '2023-05-08 02:38:25', '2023-05-08 02:38:25');
INSERT INTO `lc_error_log` VALUES ('53265', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '36.99.136.131', '2023-05-08 03:50:17', '2023-05-08 03:50:17');
INSERT INTO `lc_error_log` VALUES ('53266', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.134', '2023-05-08 04:00:00', '2023-05-08 04:00:00');
INSERT INTO `lc_error_log` VALUES ('53267', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.131', '2023-05-08 04:01:18', '2023-05-08 04:01:18');
INSERT INTO `lc_error_log` VALUES ('53268', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.131', '2023-05-08 04:01:24', '2023-05-08 04:01:24');
INSERT INTO `lc_error_log` VALUES ('53269', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '36.99.136.130', '2023-05-08 08:21:23', '2023-05-08 08:21:23');
INSERT INTO `lc_error_log` VALUES ('53270', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.139', '2023-05-08 08:23:59', '2023-05-08 08:23:59');
INSERT INTO `lc_error_log` VALUES ('53271', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '36.99.136.143', '2023-05-08 08:25:13', '2023-05-08 08:25:13');
INSERT INTO `lc_error_log` VALUES ('53272', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-08 09:52:55', '2023-05-08 09:52:55');
INSERT INTO `lc_error_log` VALUES ('53273', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getQiniuToekn?bucket=', '117.189.225.211', '2023-05-08 10:51:00', '2023-05-08 10:51:00');
INSERT INTO `lc_error_log` VALUES ('53274', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.70', '2023-05-08 10:51:28', '2023-05-08 10:51:28');
INSERT INTO `lc_error_log` VALUES ('53275', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.70', '2023-05-08 10:51:29', '2023-05-08 10:51:29');
INSERT INTO `lc_error_log` VALUES ('53276', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-08 10:51:36', '2023-05-08 10:51:36');
INSERT INTO `lc_error_log` VALUES ('53277', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.84', '2023-05-08 10:51:42', '2023-05-08 10:51:42');
INSERT INTO `lc_error_log` VALUES ('53278', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getQiniuToekn', '27.115.124.38', '2023-05-08 10:52:28', '2023-05-08 10:52:28');
INSERT INTO `lc_error_log` VALUES ('53279', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.118', '2023-05-08 10:53:03', '2023-05-08 10:53:03');
INSERT INTO `lc_error_log` VALUES ('53280', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.114', '2023-05-08 10:53:12', '2023-05-08 10:53:12');
INSERT INTO `lc_error_log` VALUES ('53281', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-05-08 11:11:32', '2023-05-08 11:11:32');
INSERT INTO `lc_error_log` VALUES ('53282', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '117.189.225.211', '2023-05-08 11:53:15', '2023-05-08 11:53:15');
INSERT INTO `lc_error_log` VALUES ('53283', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.225.211', '2023-05-08 11:53:15', '2023-05-08 11:53:15');
INSERT INTO `lc_error_log` VALUES ('53284', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-08 11:54:13', '2023-05-08 11:54:13');
INSERT INTO `lc_error_log` VALUES ('53285', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-08 11:58:10', '2023-05-08 11:58:10');
INSERT INTO `lc_error_log` VALUES ('53286', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '111.7.100.22', '2023-05-08 12:22:40', '2023-05-08 12:22:40');
INSERT INTO `lc_error_log` VALUES ('53287', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.75', '2023-05-08 13:31:03', '2023-05-08 13:31:03');
INSERT INTO `lc_error_log` VALUES ('53288', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.6', '2023-05-08 13:31:14', '2023-05-08 13:31:14');
INSERT INTO `lc_error_log` VALUES ('53289', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-05-08 14:27:33', '2023-05-08 14:27:33');
INSERT INTO `lc_error_log` VALUES ('53290', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-05-08 14:27:34', '2023-05-08 14:27:34');
INSERT INTO `lc_error_log` VALUES ('53291', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.198', '2023-05-08 15:11:51', '2023-05-08 15:11:51');
INSERT INTO `lc_error_log` VALUES ('53292', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.22', '2023-05-08 16:20:54', '2023-05-08 16:20:54');
INSERT INTO `lc_error_log` VALUES ('53293', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-05-08 16:32:00', '2023-05-08 16:32:00');
INSERT INTO `lc_error_log` VALUES ('53294', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-05-08 17:03:28', '2023-05-08 17:03:28');
INSERT INTO `lc_error_log` VALUES ('53295', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.106.101', '2023-05-08 18:39:16', '2023-05-08 18:39:16');
INSERT INTO `lc_error_log` VALUES ('53296', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.106.101', '2023-05-08 18:41:20', '2023-05-08 18:41:20');
INSERT INTO `lc_error_log` VALUES ('53297', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '103.203.59.1', '2023-05-08 22:41:13', '2023-05-08 22:41:13');
INSERT INTO `lc_error_log` VALUES ('53298', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.160.208.141', '2023-05-09 00:50:08', '2023-05-09 00:50:08');
INSERT INTO `lc_error_log` VALUES ('53299', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.26', '2023-05-09 00:54:52', '2023-05-09 00:54:52');
INSERT INTO `lc_error_log` VALUES ('53300', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '111.7.100.24', '2023-05-09 00:56:13', '2023-05-09 00:56:13');
INSERT INTO `lc_error_log` VALUES ('53301', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-05-09 11:21:06', '2023-05-09 11:21:06');
INSERT INTO `lc_error_log` VALUES ('53302', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-09 12:02:10', '2023-05-09 12:02:10');
INSERT INTO `lc_error_log` VALUES ('53303', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-09 12:19:28', '2023-05-09 12:19:28');
INSERT INTO `lc_error_log` VALUES ('53304', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-09 12:19:28', '2023-05-09 12:19:28');
INSERT INTO `lc_error_log` VALUES ('53305', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-05-09 12:50:32', '2023-05-09 12:50:32');
INSERT INTO `lc_error_log` VALUES ('53306', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-05-09 12:51:38', '2023-05-09 12:51:38');
INSERT INTO `lc_error_log` VALUES ('53307', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.15', '2023-05-09 13:31:06', '2023-05-09 13:31:06');
INSERT INTO `lc_error_log` VALUES ('53308', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.171', '2023-05-09 13:31:44', '2023-05-09 13:31:44');
INSERT INTO `lc_error_log` VALUES ('53309', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.171', '2023-05-09 13:31:44', '2023-05-09 13:31:44');
INSERT INTO `lc_error_log` VALUES ('53310', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-05-09 13:31:48', '2023-05-09 13:31:48');
INSERT INTO `lc_error_log` VALUES ('53311', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-05-09 13:31:48', '2023-05-09 13:31:48');
INSERT INTO `lc_error_log` VALUES ('53312', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-09 17:51:19', '2023-05-09 17:51:19');
INSERT INTO `lc_error_log` VALUES ('53313', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-09 17:51:19', '2023-05-09 17:51:19');
INSERT INTO `lc_error_log` VALUES ('53314', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-09 17:51:30', '2023-05-09 17:51:30');
INSERT INTO `lc_error_log` VALUES ('53315', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-09 17:51:30', '2023-05-09 17:51:30');
INSERT INTO `lc_error_log` VALUES ('53316', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/data?after=-120&chart=system.cpu&dimensions=iowait&format=json&group=average&gtime=0&options=ms%7Cflip%7Cjsonwrap%7Cnonzero&points=125', '124.221.253.185', '2023-05-09 17:52:33', '2023-05-09 17:52:33');
INSERT INTO `lc_error_log` VALUES ('53317', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/graphql', '124.221.253.185', '2023-05-09 18:01:28', '2023-05-09 18:01:28');
INSERT INTO `lc_error_log` VALUES ('53318', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/snapshots/:key', '124.221.253.185', '2023-05-09 18:48:24', '2023-05-09 18:48:24');
INSERT INTO `lc_error_log` VALUES ('53319', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/whoami', '124.221.253.185', '2023-05-09 19:16:51', '2023-05-09 19:16:51');
INSERT INTO `lc_error_log` VALUES ('53320', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/snapshots', '124.221.253.185', '2023-05-09 20:59:31', '2023-05-09 20:59:31');
INSERT INTO `lc_error_log` VALUES ('53321', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v3/users', '124.221.253.185', '2023-05-09 21:15:20', '2023-05-09 21:15:20');
INSERT INTO `lc_error_log` VALUES ('53322', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-09 21:24:03', '2023-05-09 21:24:03');
INSERT INTO `lc_error_log` VALUES ('53323', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-09 21:32:17', '2023-05-09 21:32:17');
INSERT INTO `lc_error_log` VALUES ('53324', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/users/admin?fields=*,privileges/PrivilegeInfo/cluster_name,privileges/PrivilegeInfo/permission_name', '124.221.253.185', '2023-05-09 22:09:43', '2023-05-09 22:09:43');
INSERT INTO `lc_error_log` VALUES ('53325', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/add-article-by-text', '124.221.253.185', '2023-05-09 22:11:21', '2023-05-09 22:11:21');
INSERT INTO `lc_error_log` VALUES ('53326', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/security/ticket', '124.221.253.185', '2023-05-09 23:07:21', '2023-05-09 23:07:21');
INSERT INTO `lc_error_log` VALUES ('53327', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '221.218.79.244', '2023-05-09 23:33:41', '2023-05-09 23:33:41');
INSERT INTO `lc_error_log` VALUES ('53328', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/whoami', '124.221.253.185', '2023-05-10 01:40:28', '2023-05-10 01:40:28');
INSERT INTO `lc_error_log` VALUES ('53329', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '163.125.202.151', '2023-05-10 02:10:28', '2023-05-10 02:10:28');
INSERT INTO `lc_error_log` VALUES ('53330', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/user/login', '124.221.253.185', '2023-05-10 02:11:56', '2023-05-10 02:11:56');
INSERT INTO `lc_error_log` VALUES ('53331', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v4/auth', '124.221.253.185', '2023-05-10 03:34:43', '2023-05-10 03:34:43');
INSERT INTO `lc_error_log` VALUES ('53332', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/users/admin?fields=*,privileges/PrivilegeInfo/cluster_name,privileges/PrivilegeInfo/permission_name', '124.221.253.185', '2023-05-10 03:47:30', '2023-05-10 03:47:30');
INSERT INTO `lc_error_log` VALUES ('53333', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/system', '124.221.253.185', '2023-05-10 05:46:45', '2023-05-10 05:46:45');
INSERT INTO `lc_error_log` VALUES ('53334', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/config/application?public=true', '124.221.253.185', '2023-05-10 06:10:49', '2023-05-10 06:10:49');
INSERT INTO `lc_error_log` VALUES ('53335', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/six/batch-requests', '124.221.253.185', '2023-05-10 08:22:37', '2023-05-10 08:22:37');
INSERT INTO `lc_error_log` VALUES ('53336', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/2PZaZ29lcS1JuPaoAVu6b0PQIhj', '124.221.253.185', '2023-05-10 08:22:52', '2023-05-10 08:22:52');
INSERT INTO `lc_error_log` VALUES ('53337', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.25', '2023-05-10 09:10:51', '2023-05-10 09:10:51');
INSERT INTO `lc_error_log` VALUES ('53338', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.187.173.69', '2023-05-10 10:09:26', '2023-05-10 10:09:26');
INSERT INTO `lc_error_log` VALUES ('53339', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.30.226', '2023-05-10 10:18:25', '2023-05-10 10:18:25');
INSERT INTO `lc_error_log` VALUES ('53340', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/users/admin?fields=*,privileges/PrivilegeInfo/cluster_name,privileges/PrivilegeInfo/permission_name', '124.221.253.185', '2023-05-10 12:45:10', '2023-05-10 12:45:10');
INSERT INTO `lc_error_log` VALUES ('53341', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-10 13:26:53', '2023-05-10 13:26:53');
INSERT INTO `lc_error_log` VALUES ('53342', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/six/admin/migrate/export', '124.221.253.185', '2023-05-10 15:13:59', '2023-05-10 15:13:59');
INSERT INTO `lc_error_log` VALUES ('53343', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '1.13.181.62', '2023-05-10 16:42:53', '2023-05-10 16:42:53');
INSERT INTO `lc_error_log` VALUES ('53344', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/snapshots/:key', '124.221.253.185', '2023-05-10 16:49:14', '2023-05-10 16:49:14');
INSERT INTO `lc_error_log` VALUES ('53345', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.72.65', '2023-05-10 16:53:07', '2023-05-10 16:53:07');
INSERT INTO `lc_error_log` VALUES ('53346', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.72.93', '2023-05-10 16:53:34', '2023-05-10 16:53:34');
INSERT INTO `lc_error_log` VALUES ('53347', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/snapshots', '124.221.253.185', '2023-05-10 17:02:45', '2023-05-10 17:02:45');
INSERT INTO `lc_error_log` VALUES ('53348', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/image/cover-upload?filename=../appsettings.json', '124.221.253.185', '2023-05-10 17:17:04', '2023-05-10 17:17:04');
INSERT INTO `lc_error_log` VALUES ('53349', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.109', '2023-05-10 17:22:29', '2023-05-10 17:22:29');
INSERT INTO `lc_error_log` VALUES ('53350', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.109', '2023-05-10 17:22:29', '2023-05-10 17:22:29');
INSERT INTO `lc_error_log` VALUES ('53351', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.118', '2023-05-10 17:22:41', '2023-05-10 17:22:41');
INSERT INTO `lc_error_log` VALUES ('53352', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.118', '2023-05-10 17:22:41', '2023-05-10 17:22:41');
INSERT INTO `lc_error_log` VALUES ('53353', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-05-10 20:30:10', '2023-05-10 20:30:10');
INSERT INTO `lc_error_log` VALUES ('53354', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-05-10 20:30:10', '2023-05-10 20:30:10');
INSERT INTO `lc_error_log` VALUES ('53355', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-10 21:18:44', '2023-05-10 21:18:44');
INSERT INTO `lc_error_log` VALUES ('53356', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-10 21:20:49', '2023-05-10 21:20:49');
INSERT INTO `lc_error_log` VALUES ('53357', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 21:30:18', '2023-05-10 21:30:18');
INSERT INTO `lc_error_log` VALUES ('53358', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 21:48:50', '2023-05-10 21:48:50');
INSERT INTO `lc_error_log` VALUES ('53359', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '参数错误', '0', '101', '/v1/getKeInfo?ke_id=23584&order_id=&chapters_id=&directory_id=', '117.189.225.211', '2023-05-10 21:51:19', '2023-05-10 21:51:19');
INSERT INTO `lc_error_log` VALUES ('53360', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '参数错误', '0', '101', '/v1/getKeInfo?ke_id=23584&order_id=&chapters_id=27&directory_id=300', '117.189.225.211', '2023-05-10 21:51:58', '2023-05-10 21:51:58');
INSERT INTO `lc_error_log` VALUES ('53361', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '参数错误', '0', '105', '/v1/getKeInfo?ke_id=23584&order_id=&chapters_id=27&directory_id=300', '117.189.225.211', '2023-05-10 21:53:45', '2023-05-10 21:53:45');
INSERT INTO `lc_error_log` VALUES ('53362', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-10 21:54:33', '2023-05-10 21:54:33');
INSERT INTO `lc_error_log` VALUES ('53363', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/mp/delKechengDirectory', '117.189.225.211', '2023-05-10 22:01:13', '2023-05-10 22:01:13');
INSERT INTO `lc_error_log` VALUES ('53364', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/mp/delKechengDirectory', '117.189.225.211', '2023-05-10 22:01:15', '2023-05-10 22:01:15');
INSERT INTO `lc_error_log` VALUES ('53365', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 22:14:12', '2023-05-10 22:14:12');
INSERT INTO `lc_error_log` VALUES ('53366', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-10 22:18:40', '2023-05-10 22:18:40');
INSERT INTO `lc_error_log` VALUES ('53367', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/mp/delKechengDirectory', '117.189.225.211', '2023-05-10 22:19:22', '2023-05-10 22:19:22');
INSERT INTO `lc_error_log` VALUES ('53368', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/mp/delKechengDirectory', '117.189.225.211', '2023-05-10 22:19:50', '2023-05-10 22:19:50');
INSERT INTO `lc_error_log` VALUES ('53369', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'method not exists:app\\api\\controller\\v1\\MpKecheng->delDirectory()', '0', '107', '/v1/mp/delKechengDirectory', '117.189.225.211', '2023-05-10 22:21:05', '2023-05-10 22:21:05');
INSERT INTO `lc_error_log` VALUES ('53370', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 22:23:49', '2023-05-10 22:23:49');
INSERT INTO `lc_error_log` VALUES ('53371', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 22:31:31', '2023-05-10 22:31:31');
INSERT INTO `lc_error_log` VALUES ('53372', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 22:33:52', '2023-05-10 22:33:52');
INSERT INTO `lc_error_log` VALUES ('53373', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 22:36:58', '2023-05-10 22:36:58');
INSERT INTO `lc_error_log` VALUES ('53374', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 22:40:14', '2023-05-10 22:40:14');
INSERT INTO `lc_error_log` VALUES ('53375', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-10 22:41:51', '2023-05-10 22:41:51');
INSERT INTO `lc_error_log` VALUES ('53376', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23585&order_id=&chapters_id=29&directory_id=302', '116.179.37.249', '2023-05-11 00:23:00', '2023-05-11 00:23:00');
INSERT INTO `lc_error_log` VALUES ('53377', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23585&order_id=&chapters_id=29&directory_id=302', '116.179.37.232', '2023-05-11 00:23:02', '2023-05-11 00:23:02');
INSERT INTO `lc_error_log` VALUES ('53378', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.153.215', '2023-05-11 02:27:20', '2023-05-11 02:27:20');
INSERT INTO `lc_error_log` VALUES ('53379', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.125.13', '2023-05-11 02:30:10', '2023-05-11 02:30:10');
INSERT INTO `lc_error_log` VALUES ('53380', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '110.42.200.114', '2023-05-11 03:29:35', '2023-05-11 03:29:35');
INSERT INTO `lc_error_log` VALUES ('53381', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.73.218', '2023-05-11 03:33:07', '2023-05-11 03:33:07');
INSERT INTO `lc_error_log` VALUES ('53382', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.132.207', '2023-05-11 03:33:40', '2023-05-11 03:33:40');
INSERT INTO `lc_error_log` VALUES ('53383', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.123.125', '2023-05-11 03:33:56', '2023-05-11 03:33:56');
INSERT INTO `lc_error_log` VALUES ('53384', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.123.125', '2023-05-11 03:35:25', '2023-05-11 03:35:25');
INSERT INTO `lc_error_log` VALUES ('53385', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.147.108', '2023-05-11 04:25:36', '2023-05-11 04:25:36');
INSERT INTO `lc_error_log` VALUES ('53386', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.26.81.8', '2023-05-11 05:18:16', '2023-05-11 05:18:16');
INSERT INTO `lc_error_log` VALUES ('53387', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.107.87', '2023-05-11 05:19:47', '2023-05-11 05:19:47');
INSERT INTO `lc_error_log` VALUES ('53388', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23585&order_id=&chapters_id=29&directory_id=316', '116.179.37.92', '2023-05-11 05:47:27', '2023-05-11 05:47:27');
INSERT INTO `lc_error_log` VALUES ('53389', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23585&order_id=&chapters_id=29&directory_id=316', '116.179.37.250', '2023-05-11 05:47:30', '2023-05-11 05:47:30');
INSERT INTO `lc_error_log` VALUES ('53390', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-11 08:00:08', '2023-05-11 08:00:08');
INSERT INTO `lc_error_log` VALUES ('53391', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-11 08:00:08', '2023-05-11 08:00:08');
INSERT INTO `lc_error_log` VALUES ('53392', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:12', '2023-05-11 08:07:12');
INSERT INTO `lc_error_log` VALUES ('53393', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:12', '2023-05-11 08:07:12');
INSERT INTO `lc_error_log` VALUES ('53394', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:12', '2023-05-11 08:07:12');
INSERT INTO `lc_error_log` VALUES ('53395', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:12', '2023-05-11 08:07:12');
INSERT INTO `lc_error_log` VALUES ('53396', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:12', '2023-05-11 08:07:12');
INSERT INTO `lc_error_log` VALUES ('53397', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '123.57.235.51', '2023-05-11 08:07:12', '2023-05-11 08:07:12');
INSERT INTO `lc_error_log` VALUES ('53398', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:13', '2023-05-11 08:07:13');
INSERT INTO `lc_error_log` VALUES ('53399', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '123.57.235.51', '2023-05-11 08:07:13', '2023-05-11 08:07:13');
INSERT INTO `lc_error_log` VALUES ('53400', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:13', '2023-05-11 08:07:13');
INSERT INTO `lc_error_log` VALUES ('53401', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/index.php', '123.57.235.51', '2023-05-11 08:07:21', '2023-05-11 08:07:21');
INSERT INTO `lc_error_log` VALUES ('53402', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:25', '2023-05-11 08:07:25');
INSERT INTO `lc_error_log` VALUES ('53403', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-11 08:07:25', '2023-05-11 08:07:25');
INSERT INTO `lc_error_log` VALUES ('53404', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '58.211.23.183', '2023-05-11 09:50:44', '2023-05-11 09:50:44');
INSERT INTO `lc_error_log` VALUES ('53405', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '58.211.23.183', '2023-05-11 10:30:09', '2023-05-11 10:30:09');
INSERT INTO `lc_error_log` VALUES ('53406', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-05-11 12:14:09', '2023-05-11 12:14:09');
INSERT INTO `lc_error_log` VALUES ('53407', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-11 12:23:24', '2023-05-11 12:23:24');
INSERT INTO `lc_error_log` VALUES ('53408', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-05-11 12:26:24', '2023-05-11 12:26:24');
INSERT INTO `lc_error_log` VALUES ('53409', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.25', '2023-05-11 14:14:12', '2023-05-11 14:14:12');
INSERT INTO `lc_error_log` VALUES ('53410', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '66.249.79.15', '2023-05-11 15:11:02', '2023-05-11 15:11:02');
INSERT INTO `lc_error_log` VALUES ('53411', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-11 19:21:51', '2023-05-11 19:21:51');
INSERT INTO `lc_error_log` VALUES ('53412', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-11 19:54:12', '2023-05-11 19:54:12');
INSERT INTO `lc_error_log` VALUES ('53413', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-11 19:54:18', '2023-05-11 19:54:18');
INSERT INTO `lc_error_log` VALUES ('53414', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请填写描述', '0', '524', '/v1/mp/kecheng', '117.189.225.211', '2023-05-11 19:54:24', '2023-05-11 19:54:24');
INSERT INTO `lc_error_log` VALUES ('53415', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-11 20:08:57', '2023-05-11 20:08:57');
INSERT INTO `lc_error_log` VALUES ('53416', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-11 20:12:11', '2023-05-11 20:12:11');
INSERT INTO `lc_error_log` VALUES ('53417', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-11 20:16:58', '2023-05-11 20:16:58');
INSERT INTO `lc_error_log` VALUES ('53418', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-11 20:28:55', '2023-05-11 20:28:55');
INSERT INTO `lc_error_log` VALUES ('53419', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-11 20:33:35', '2023-05-11 20:33:35');
INSERT INTO `lc_error_log` VALUES ('53420', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-11 20:40:14', '2023-05-11 20:40:14');
INSERT INTO `lc_error_log` VALUES ('53421', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-11 20:52:17', '2023-05-11 20:52:17');
INSERT INTO `lc_error_log` VALUES ('53422', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.211', '2023-05-11 21:54:33', '2023-05-11 21:54:33');
INSERT INTO `lc_error_log` VALUES ('53423', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请填写描述', '0', '524', '/v1/mp/kecheng', '117.189.225.211', '2023-05-11 21:54:36', '2023-05-11 21:54:36');
INSERT INTO `lc_error_log` VALUES ('53424', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '203.57.233.197', '2023-05-11 22:43:50', '2023-05-11 22:43:50');
INSERT INTO `lc_error_log` VALUES ('53425', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getKeList?page=1&pageSize=20', '117.189.225.211', '2023-05-11 22:57:48', '2023-05-11 22:57:48');
INSERT INTO `lc_error_log` VALUES ('53426', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.17', '2023-05-12 06:48:57', '2023-05-12 06:48:57');
INSERT INTO `lc_error_log` VALUES ('53427', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称长度需大于等于3个字符', '0', '524', '/v1/mp/keDirectory', '117.189.225.211', '2023-05-12 13:49:48', '2023-05-12 13:49:48');
INSERT INTO `lc_error_log` VALUES ('53428', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//ziyuanAudit', '101.43.98.47', '2023-05-12 14:13:58', '2023-05-12 14:13:58');
INSERT INTO `lc_error_log` VALUES ('53429', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.205.171.132', '2023-05-12 15:35:16', '2023-05-12 15:35:16');
INSERT INTO `lc_error_log` VALUES ('53430', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '115.205.171.132', '2023-05-12 15:36:37', '2023-05-12 15:36:37');
INSERT INTO `lc_error_log` VALUES ('53431', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '115.205.171.132', '2023-05-12 15:36:41', '2023-05-12 15:36:41');
INSERT INTO `lc_error_log` VALUES ('53432', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '115.205.171.132', '2023-05-12 15:36:41', '2023-05-12 15:36:41');
INSERT INTO `lc_error_log` VALUES ('53433', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-05-13 08:34:41', '2023-05-13 08:34:41');
INSERT INTO `lc_error_log` VALUES ('53434', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-05-13 08:36:25', '2023-05-13 08:36:25');
INSERT INTO `lc_error_log` VALUES ('53435', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-05-13 08:36:30', '2023-05-13 08:36:30');
INSERT INTO `lc_error_log` VALUES ('53436', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '103.213.96.234', '2023-05-13 08:57:40', '2023-05-13 08:57:40');
INSERT INTO `lc_error_log` VALUES ('53437', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.21', '2023-05-13 15:03:30', '2023-05-13 15:03:30');
INSERT INTO `lc_error_log` VALUES ('53438', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.12.87.244', '2023-05-13 15:53:33', '2023-05-13 15:53:33');
INSERT INTO `lc_error_log` VALUES ('53439', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-13 17:37:56', '2023-05-13 17:37:56');
INSERT INTO `lc_error_log` VALUES ('53440', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-13 17:37:56', '2023-05-13 17:37:56');
INSERT INTO `lc_error_log` VALUES ('53441', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '下载地址必填', '0', '524', '/v1/mp/fabu', '117.189.225.211', '2023-05-13 21:24:14', '2023-05-13 21:24:14');
INSERT INTO `lc_error_log` VALUES ('53442', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.32.36', '2023-05-14 06:08:32', '2023-05-14 06:08:32');
INSERT INTO `lc_error_log` VALUES ('53443', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.196', '2023-05-14 06:08:35', '2023-05-14 06:08:35');
INSERT INTO `lc_error_log` VALUES ('53444', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.32.239', '2023-05-14 06:21:41', '2023-05-14 06:21:41');
INSERT INTO `lc_error_log` VALUES ('53445', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.208', '2023-05-14 06:21:43', '2023-05-14 06:21:43');
INSERT INTO `lc_error_log` VALUES ('53446', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-05-14 11:11:21', '2023-05-14 11:11:21');
INSERT INTO `lc_error_log` VALUES ('53447', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.24', '2023-05-14 15:10:30', '2023-05-14 15:10:30');
INSERT INTO `lc_error_log` VALUES ('53448', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '52.167.144.31', '2023-05-14 15:47:19', '2023-05-14 15:47:19');
INSERT INTO `lc_error_log` VALUES ('53449', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '40.77.188.180', '2023-05-14 16:03:34', '2023-05-14 16:03:34');
INSERT INTO `lc_error_log` VALUES ('53450', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-15 01:31:12', '2023-05-15 01:31:12');
INSERT INTO `lc_error_log` VALUES ('53451', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-15 01:31:12', '2023-05-15 01:31:12');
INSERT INTO `lc_error_log` VALUES ('53452', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '39.106.11.231', '2023-05-15 06:21:57', '2023-05-15 06:21:57');
INSERT INTO `lc_error_log` VALUES ('53453', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '39.106.11.231', '2023-05-15 06:23:05', '2023-05-15 06:23:05');
INSERT INTO `lc_error_log` VALUES ('53454', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-05-15 11:07:17', '2023-05-15 11:07:17');
INSERT INTO `lc_error_log` VALUES ('53455', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '124.223.94.42', '2023-05-15 12:30:02', '2023-05-15 12:30:02');
INSERT INTO `lc_error_log` VALUES ('53456', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.150', '2023-05-15 15:03:19', '2023-05-15 15:03:19');
INSERT INTO `lc_error_log` VALUES ('53457', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.101', '2023-05-15 17:46:26', '2023-05-15 17:46:26');
INSERT INTO `lc_error_log` VALUES ('53458', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.93', '2023-05-15 17:46:36', '2023-05-15 17:46:36');
INSERT INTO `lc_error_log` VALUES ('53459', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.93', '2023-05-15 17:46:39', '2023-05-15 17:46:39');
INSERT INTO `lc_error_log` VALUES ('53460', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.26', '2023-05-15 19:31:50', '2023-05-15 19:31:50');
INSERT INTO `lc_error_log` VALUES ('53461', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-15 21:20:04', '2023-05-15 21:20:04');
INSERT INTO `lc_error_log` VALUES ('53462', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-05-15 21:32:33', '2023-05-15 21:32:33');
INSERT INTO `lc_error_log` VALUES ('53463', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-15 21:49:42', '2023-05-15 21:49:42');
INSERT INTO `lc_error_log` VALUES ('53464', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-15 22:29:24', '2023-05-15 22:29:24');
INSERT INTO `lc_error_log` VALUES ('53465', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.66.125', '2023-05-15 22:46:12', '2023-05-15 22:46:12');
INSERT INTO `lc_error_log` VALUES ('53466', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/chapters', '117.189.224.211', '2023-05-15 22:49:17', '2023-05-15 22:49:17');
INSERT INTO `lc_error_log` VALUES ('53467', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.225.39', '2023-05-15 22:50:57', '2023-05-15 22:50:57');
INSERT INTO `lc_error_log` VALUES ('53468', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-15 22:52:02', '2023-05-15 22:52:02');
INSERT INTO `lc_error_log` VALUES ('53469', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.75.211', '2023-05-16 01:47:58', '2023-05-16 01:47:58');
INSERT INTO `lc_error_log` VALUES ('53470', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.75.211', '2023-05-16 01:50:04', '2023-05-16 01:50:04');
INSERT INTO `lc_error_log` VALUES ('53471', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.72.65', '2023-05-16 09:48:00', '2023-05-16 09:48:00');
INSERT INTO `lc_error_log` VALUES ('53472', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '150.129.136.36', '2023-05-16 11:31:29', '2023-05-16 11:31:29');
INSERT INTO `lc_error_log` VALUES ('53473', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=80&info=true', '116.179.32.107', '2023-05-16 12:59:30', '2023-05-16 12:59:30');
INSERT INTO `lc_error_log` VALUES ('53474', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=80&info=true', '116.179.37.180', '2023-05-16 12:59:31', '2023-05-16 12:59:31');
INSERT INTO `lc_error_log` VALUES ('53475', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=80&info=true', '116.179.37.214', '2023-05-16 12:59:33', '2023-05-16 12:59:33');
INSERT INTO `lc_error_log` VALUES ('53476', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-16 19:43:20', '2023-05-16 19:43:20');
INSERT INTO `lc_error_log` VALUES ('53477', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-16 19:53:09', '2023-05-16 19:53:09');
INSERT INTO `lc_error_log` VALUES ('53478', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.187.173.112', '2023-05-16 21:16:03', '2023-05-16 21:16:03');
INSERT INTO `lc_error_log` VALUES ('53479', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.63.211', '2023-05-16 21:25:04', '2023-05-16 21:25:04');
INSERT INTO `lc_error_log` VALUES ('53480', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 21:35:13', '2023-05-16 21:35:13');
INSERT INTO `lc_error_log` VALUES ('53481', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 21:35:13', '2023-05-16 21:35:13');
INSERT INTO `lc_error_log` VALUES ('53482', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 21:35:13', '2023-05-16 21:35:13');
INSERT INTO `lc_error_log` VALUES ('53483', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-05-16 21:40:17', '2023-05-16 21:40:17');
INSERT INTO `lc_error_log` VALUES ('53484', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:04:22', '2023-05-16 22:04:22');
INSERT INTO `lc_error_log` VALUES ('53485', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:05:33', '2023-05-16 22:05:33');
INSERT INTO `lc_error_log` VALUES ('53486', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:06:39', '2023-05-16 22:06:39');
INSERT INTO `lc_error_log` VALUES ('53487', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:13:16', '2023-05-16 22:13:16');
INSERT INTO `lc_error_log` VALUES ('53488', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:20:00', '2023-05-16 22:20:00');
INSERT INTO `lc_error_log` VALUES ('53489', 'api', '/www/wwwroot/gouziyuan/app/common/lib/Upload.php', '不支持该类型文件，请重新上传', '0', '47', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:20:00', '2023-05-16 22:20:00');
INSERT INTO `lc_error_log` VALUES ('53490', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:20:40', '2023-05-16 22:20:40');
INSERT INTO `lc_error_log` VALUES ('53491', 'api', '/www/wwwroot/gouziyuan/app/common/lib/Upload.php', '不支持该类型文件，请重新上传', '0', '47', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:20:40', '2023-05-16 22:20:40');
INSERT INTO `lc_error_log` VALUES ('53492', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:21:08', '2023-05-16 22:21:08');
INSERT INTO `lc_error_log` VALUES ('53493', 'api', '/www/wwwroot/gouziyuan/app/common/lib/Upload.php', '不支持该类型文件，请重新上传', '0', '47', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:21:08', '2023-05-16 22:21:08');
INSERT INTO `lc_error_log` VALUES ('53494', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:27:49', '2023-05-16 22:27:49');
INSERT INTO `lc_error_log` VALUES ('53495', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:27:59', '2023-05-16 22:27:59');
INSERT INTO `lc_error_log` VALUES ('53496', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-16 22:28:07', '2023-05-16 22:28:07');
INSERT INTO `lc_error_log` VALUES ('53497', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-16 22:38:06', '2023-05-16 22:38:06');
INSERT INTO `lc_error_log` VALUES ('53498', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-16 22:38:06', '2023-05-16 22:38:06');
INSERT INTO `lc_error_log` VALUES ('53499', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.164.148', '2023-05-17 03:47:28', '2023-05-17 03:47:28');
INSERT INTO `lc_error_log` VALUES ('53500', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-05-17 04:44:39', '2023-05-17 04:44:39');
INSERT INTO `lc_error_log` VALUES ('53501', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.154', '2023-05-17 04:44:39', '2023-05-17 04:44:39');
INSERT INTO `lc_error_log` VALUES ('53502', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-17 23:40:52', '2023-05-17 23:40:52');
INSERT INTO `lc_error_log` VALUES ('53503', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-17 23:50:35', '2023-05-17 23:50:35');
INSERT INTO `lc_error_log` VALUES ('53504', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.150', '2023-05-18 00:47:41', '2023-05-18 00:47:41');
INSERT INTO `lc_error_log` VALUES ('53505', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.132.207', '2023-05-18 00:48:47', '2023-05-18 00:48:47');
INSERT INTO `lc_error_log` VALUES ('53506', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.132.207', '2023-05-18 00:48:48', '2023-05-18 00:48:48');
INSERT INTO `lc_error_log` VALUES ('53507', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.51.68', '2023-05-18 00:48:50', '2023-05-18 00:48:50');
INSERT INTO `lc_error_log` VALUES ('53508', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.97.98', '2023-05-18 00:49:26', '2023-05-18 00:49:26');
INSERT INTO `lc_error_log` VALUES ('53509', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-05-18 10:55:36', '2023-05-18 10:55:36');
INSERT INTO `lc_error_log` VALUES ('53510', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-18 13:24:27', '2023-05-18 13:24:27');
INSERT INTO `lc_error_log` VALUES ('53511', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-18 13:24:27', '2023-05-18 13:24:27');
INSERT INTO `lc_error_log` VALUES ('53512', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.10.148.18', '2023-05-18 20:13:12', '2023-05-18 20:13:12');
INSERT INTO `lc_error_log` VALUES ('53513', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.32.154', '2023-05-18 20:20:24', '2023-05-18 20:20:24');
INSERT INTO `lc_error_log` VALUES ('53514', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.91', '2023-05-18 20:20:39', '2023-05-18 20:20:39');
INSERT INTO `lc_error_log` VALUES ('53515', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.230', '2023-05-18 20:20:40', '2023-05-18 20:20:40');
INSERT INTO `lc_error_log` VALUES ('53516', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.25', '2023-05-18 20:53:46', '2023-05-18 20:53:46');
INSERT INTO `lc_error_log` VALUES ('53517', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-18 21:01:26', '2023-05-18 21:01:26');
INSERT INTO `lc_error_log` VALUES ('53518', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-18 21:11:07', '2023-05-18 21:11:07');
INSERT INTO `lc_error_log` VALUES ('53519', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.224.211', '2023-05-18 22:02:47', '2023-05-18 22:02:47');
INSERT INTO `lc_error_log` VALUES ('53520', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.224.211', '2023-05-18 22:02:47', '2023-05-18 22:02:47');
INSERT INTO `lc_error_log` VALUES ('53521', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-18 22:07:00', '2023-05-18 22:07:00');
INSERT INTO `lc_error_log` VALUES ('53522', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请填写描述', '0', '524', '/v1/mp/kecheng', '117.189.224.211', '2023-05-18 22:07:07', '2023-05-18 22:07:07');
INSERT INTO `lc_error_log` VALUES ('53523', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.70', '2023-05-18 22:19:55', '2023-05-18 22:19:55');
INSERT INTO `lc_error_log` VALUES ('53524', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '180.163.220.4', '2023-05-18 22:20:01', '2023-05-18 22:20:01');
INSERT INTO `lc_error_log` VALUES ('53525', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '180.163.220.4', '2023-05-18 22:20:01', '2023-05-18 22:20:01');
INSERT INTO `lc_error_log` VALUES ('53526', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-18 22:36:03', '2023-05-18 22:36:03');
INSERT INTO `lc_error_log` VALUES ('53527', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-18 22:36:40', '2023-05-18 22:36:40');
INSERT INTO `lc_error_log` VALUES ('53528', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-18 22:58:20', '2023-05-18 22:58:20');
INSERT INTO `lc_error_log` VALUES ('53529', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-18 23:09:12', '2023-05-18 23:09:12');
INSERT INTO `lc_error_log` VALUES ('53530', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-18 23:12:43', '2023-05-18 23:12:43');
INSERT INTO `lc_error_log` VALUES ('53531', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.17', '2023-05-18 23:53:00', '2023-05-18 23:53:00');
INSERT INTO `lc_error_log` VALUES ('53532', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '103.43.19.38', '2023-05-19 01:30:32', '2023-05-19 01:30:32');
INSERT INTO `lc_error_log` VALUES ('53533', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.27', '2023-05-19 02:14:35', '2023-05-19 02:14:35');
INSERT INTO `lc_error_log` VALUES ('53534', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.71.157', '2023-05-19 07:40:09', '2023-05-19 07:40:09');
INSERT INTO `lc_error_log` VALUES ('53535', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.198', '2023-05-19 09:30:53', '2023-05-19 09:30:53');
INSERT INTO `lc_error_log` VALUES ('53536', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-05-19 09:32:43', '2023-05-19 09:32:43');
INSERT INTO `lc_error_log` VALUES ('53537', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-05-19 09:32:45', '2023-05-19 09:32:45');
INSERT INTO `lc_error_log` VALUES ('53538', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.100.28', '2023-05-19 13:58:55', '2023-05-19 13:58:55');
INSERT INTO `lc_error_log` VALUES ('53539', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.157.198', '2023-05-19 13:59:09', '2023-05-19 13:59:09');
INSERT INTO `lc_error_log` VALUES ('53540', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-19 22:22:23', '2023-05-19 22:22:23');
INSERT INTO `lc_error_log` VALUES ('53541', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=62&directory_id=549', '116.179.37.17', '2023-05-19 22:25:48', '2023-05-19 22:25:48');
INSERT INTO `lc_error_log` VALUES ('53542', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=62&directory_id=549', '116.179.37.101', '2023-05-19 22:25:51', '2023-05-19 22:25:51');
INSERT INTO `lc_error_log` VALUES ('53543', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 22:46:35', '2023-05-19 22:46:35');
INSERT INTO `lc_error_log` VALUES ('53544', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 22:47:52', '2023-05-19 22:47:52');
INSERT INTO `lc_error_log` VALUES ('53545', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=62&directory_id=551', '116.179.37.42', '2023-05-19 22:49:30', '2023-05-19 22:49:30');
INSERT INTO `lc_error_log` VALUES ('53546', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=62&directory_id=553', '116.179.37.15', '2023-05-19 22:49:32', '2023-05-19 22:49:32');
INSERT INTO `lc_error_log` VALUES ('53547', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=62&directory_id=551', '116.179.37.104', '2023-05-19 22:49:32', '2023-05-19 22:49:32');
INSERT INTO `lc_error_log` VALUES ('53548', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=62&directory_id=553', '116.179.37.123', '2023-05-19 22:49:34', '2023-05-19 22:49:34');
INSERT INTO `lc_error_log` VALUES ('53549', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=63&directory_id=556', '116.179.37.112', '2023-05-19 22:56:06', '2023-05-19 22:56:06');
INSERT INTO `lc_error_log` VALUES ('53550', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23592&order_id=&chapters_id=63&directory_id=556', '116.179.37.252', '2023-05-19 22:56:08', '2023-05-19 22:56:08');
INSERT INTO `lc_error_log` VALUES ('53551', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 22:59:57', '2023-05-19 22:59:57');
INSERT INTO `lc_error_log` VALUES ('53552', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:00:53', '2023-05-19 23:00:53');
INSERT INTO `lc_error_log` VALUES ('53553', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:06:48', '2023-05-19 23:06:48');
INSERT INTO `lc_error_log` VALUES ('53554', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:09:16', '2023-05-19 23:09:16');
INSERT INTO `lc_error_log` VALUES ('53555', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:15:47', '2023-05-19 23:15:47');
INSERT INTO `lc_error_log` VALUES ('53556', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:22:57', '2023-05-19 23:22:57');
INSERT INTO `lc_error_log` VALUES ('53557', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:28:10', '2023-05-19 23:28:10');
INSERT INTO `lc_error_log` VALUES ('53558', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:28:31', '2023-05-19 23:28:31');
INSERT INTO `lc_error_log` VALUES ('53559', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-19 23:29:00', '2023-05-19 23:29:00');
INSERT INTO `lc_error_log` VALUES ('53560', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-20 06:26:53', '2023-05-20 06:26:53');
INSERT INTO `lc_error_log` VALUES ('53561', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-20 06:26:53', '2023-05-20 06:26:53');
INSERT INTO `lc_error_log` VALUES ('53562', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.24', '2023-05-20 11:38:37', '2023-05-20 11:38:37');
INSERT INTO `lc_error_log` VALUES ('53563', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '49.7.205.156', '2023-05-20 11:44:42', '2023-05-20 11:44:42');
INSERT INTO `lc_error_log` VALUES ('53564', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.21', '2023-05-20 13:38:32', '2023-05-20 13:38:32');
INSERT INTO `lc_error_log` VALUES ('53565', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.198', '2023-05-20 14:19:43', '2023-05-20 14:19:43');
INSERT INTO `lc_error_log` VALUES ('53566', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.23', '2023-05-20 14:23:21', '2023-05-20 14:23:21');
INSERT INTO `lc_error_log` VALUES ('53567', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.23', '2023-05-20 14:23:23', '2023-05-20 14:23:23');
INSERT INTO `lc_error_log` VALUES ('53568', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.22', '2023-05-20 16:11:23', '2023-05-20 16:11:23');
INSERT INTO `lc_error_log` VALUES ('53569', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-05-20 18:39:58', '2023-05-20 18:39:58');
INSERT INTO `lc_error_log` VALUES ('53570', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '39.173.105.164', '2023-05-20 20:19:09', '2023-05-20 20:19:09');
INSERT INTO `lc_error_log` VALUES ('53571', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-20 20:43:41', '2023-05-20 20:43:41');
INSERT INTO `lc_error_log` VALUES ('53572', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-20 20:43:42', '2023-05-20 20:43:42');
INSERT INTO `lc_error_log` VALUES ('53573', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '59.41.162.50', '2023-05-20 21:44:52', '2023-05-20 21:44:52');
INSERT INTO `lc_error_log` VALUES ('53574', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.109', '2023-05-20 23:09:06', '2023-05-20 23:09:06');
INSERT INTO `lc_error_log` VALUES ('53575', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.75', '2023-05-20 23:09:11', '2023-05-20 23:09:11');
INSERT INTO `lc_error_log` VALUES ('53576', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-20 23:11:17', '2023-05-20 23:11:17');
INSERT INTO `lc_error_log` VALUES ('53577', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-20 23:17:51', '2023-05-20 23:17:51');
INSERT INTO `lc_error_log` VALUES ('53578', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.106.149', '2023-05-21 08:55:05', '2023-05-21 08:55:05');
INSERT INTO `lc_error_log` VALUES ('53579', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-21 10:51:08', '2023-05-21 10:51:08');
INSERT INTO `lc_error_log` VALUES ('53580', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-21 10:51:08', '2023-05-21 10:51:08');
INSERT INTO `lc_error_log` VALUES ('53581', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.224.211', '2023-05-21 14:49:50', '2023-05-21 14:49:50');
INSERT INTO `lc_error_log` VALUES ('53582', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-21 15:13:34', '2023-05-21 15:13:34');
INSERT INTO `lc_error_log` VALUES ('53583', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.10', '2023-05-21 17:33:48', '2023-05-21 17:33:48');
INSERT INTO `lc_error_log` VALUES ('53584', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-05-21 19:36:18', '2023-05-21 19:36:18');
INSERT INTO `lc_error_log` VALUES ('53585', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-05-21 19:36:19', '2023-05-21 19:36:19');
INSERT INTO `lc_error_log` VALUES ('53586', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.15', '2023-05-21 19:36:52', '2023-05-21 19:36:52');
INSERT INTO `lc_error_log` VALUES ('53587', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.62.198', '2023-05-21 19:37:42', '2023-05-21 19:37:42');
INSERT INTO `lc_error_log` VALUES ('53588', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.129', '2023-05-21 19:37:44', '2023-05-21 19:37:44');
INSERT INTO `lc_error_log` VALUES ('53589', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.155.40', '2023-05-21 19:38:08', '2023-05-21 19:38:08');
INSERT INTO `lc_error_log` VALUES ('53590', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.155.40', '2023-05-21 19:38:08', '2023-05-21 19:38:08');
INSERT INTO `lc_error_log` VALUES ('53591', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-21 20:15:44', '2023-05-21 20:15:44');
INSERT INTO `lc_error_log` VALUES ('53592', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.83.68.12', '2023-05-21 20:19:29', '2023-05-21 20:19:29');
INSERT INTO `lc_error_log` VALUES ('53593', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/index.php/api/index/home', '119.29.204.24', '2023-05-21 20:53:30', '2023-05-21 20:53:30');
INSERT INTO `lc_error_log` VALUES ('53594', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-21 21:36:12', '2023-05-21 21:36:12');
INSERT INTO `lc_error_log` VALUES ('53595', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-21 21:36:13', '2023-05-21 21:36:13');
INSERT INTO `lc_error_log` VALUES ('53596', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.24', '2023-05-21 23:15:08', '2023-05-21 23:15:08');
INSERT INTO `lc_error_log` VALUES ('53597', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '119.29.204.24', '2023-05-21 23:31:34', '2023-05-21 23:31:34');
INSERT INTO `lc_error_log` VALUES ('53598', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-05-21 23:32:53', '2023-05-21 23:32:53');
INSERT INTO `lc_error_log` VALUES ('53599', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-05-21 23:41:21', '2023-05-21 23:41:21');
INSERT INTO `lc_error_log` VALUES ('53600', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-21 23:42:18', '2023-05-21 23:42:18');
INSERT INTO `lc_error_log` VALUES ('53601', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.118', '2023-05-22 00:49:39', '2023-05-22 00:49:39');
INSERT INTO `lc_error_log` VALUES ('53602', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.101', '2023-05-22 00:49:47', '2023-05-22 00:49:47');
INSERT INTO `lc_error_log` VALUES ('53603', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '180.163.220.4', '2023-05-22 00:50:20', '2023-05-22 00:50:20');
INSERT INTO `lc_error_log` VALUES ('53604', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '180.163.220.114', '2023-05-22 00:50:30', '2023-05-22 00:50:30');
INSERT INTO `lc_error_log` VALUES ('53605', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.29', '2023-05-22 03:15:09', '2023-05-22 03:15:09');
INSERT INTO `lc_error_log` VALUES ('53606', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-05-22 07:16:45', '2023-05-22 07:16:45');
INSERT INTO `lc_error_log` VALUES ('53607', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-05-22 07:16:47', '2023-05-22 07:16:47');
INSERT INTO `lc_error_log` VALUES ('53608', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.18', '2023-05-22 07:50:01', '2023-05-22 07:50:01');
INSERT INTO `lc_error_log` VALUES ('53609', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-05-22 09:35:17', '2023-05-22 09:35:17');
INSERT INTO `lc_error_log` VALUES ('53610', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-05-22 09:35:18', '2023-05-22 09:35:18');
INSERT INTO `lc_error_log` VALUES ('53611', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-05-22 11:05:06', '2023-05-22 11:05:06');
INSERT INTO `lc_error_log` VALUES ('53612', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-22 19:50:37', '2023-05-22 19:50:37');
INSERT INTO `lc_error_log` VALUES ('53613', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-22 19:50:37', '2023-05-22 19:50:37');
INSERT INTO `lc_error_log` VALUES ('53614', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-22 19:58:23', '2023-05-22 19:58:23');
INSERT INTO `lc_error_log` VALUES ('53615', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '113.111.230.81', '2023-05-22 20:25:56', '2023-05-22 20:25:56');
INSERT INTO `lc_error_log` VALUES ('53616', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.200.19.170', '2023-05-22 21:20:21', '2023-05-22 21:20:21');
INSERT INTO `lc_error_log` VALUES ('53617', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-22 22:33:06', '2023-05-22 22:33:06');
INSERT INTO `lc_error_log` VALUES ('53618', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-05-22 22:40:34', '2023-05-22 22:40:34');
INSERT INTO `lc_error_log` VALUES ('53619', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.224.211', '2023-05-22 22:41:49', '2023-05-22 22:41:49');
INSERT INTO `lc_error_log` VALUES ('53620', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-23 11:45:59', '2023-05-23 11:45:59');
INSERT INTO `lc_error_log` VALUES ('53621', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-23 11:45:59', '2023-05-23 11:45:59');
INSERT INTO `lc_error_log` VALUES ('53622', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-23 11:46:45', '2023-05-23 11:46:45');
INSERT INTO `lc_error_log` VALUES ('53623', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-23 11:46:47', '2023-05-23 11:46:47');
INSERT INTO `lc_error_log` VALUES ('53624', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.187.173.79', '2023-05-23 12:08:03', '2023-05-23 12:08:03');
INSERT INTO `lc_error_log` VALUES ('53625', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.70.38', '2023-05-23 12:17:05', '2023-05-23 12:17:05');
INSERT INTO `lc_error_log` VALUES ('53626', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '113.117.186.148', '2023-05-23 17:28:26', '2023-05-23 17:28:26');
INSERT INTO `lc_error_log` VALUES ('53627', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-23 18:31:24', '2023-05-23 18:31:24');
INSERT INTO `lc_error_log` VALUES ('53628', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.5', '2023-05-23 22:09:00', '2023-05-23 22:09:00');
INSERT INTO `lc_error_log` VALUES ('53629', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.21', '2023-05-23 22:10:00', '2023-05-23 22:10:00');
INSERT INTO `lc_error_log` VALUES ('53630', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.21', '2023-05-23 22:10:05', '2023-05-23 22:10:05');
INSERT INTO `lc_error_log` VALUES ('53631', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getErrorLog?page=1&pageSize=20', '101.43.98.47', '2023-05-23 22:48:56', '2023-05-23 22:48:56');
INSERT INTO `lc_error_log` VALUES ('53632', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-05-23 22:50:27', '2023-05-23 22:50:27');
INSERT INTO `lc_error_log` VALUES ('53633', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.30', '2023-05-23 23:04:49', '2023-05-23 23:04:49');
INSERT INTO `lc_error_log` VALUES ('53634', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.153.26', '2023-05-24 02:05:52', '2023-05-24 02:05:52');
INSERT INTO `lc_error_log` VALUES ('53635', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-24 07:05:40', '2023-05-24 07:05:40');
INSERT INTO `lc_error_log` VALUES ('53636', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-24 07:05:40', '2023-05-24 07:05:40');
INSERT INTO `lc_error_log` VALUES ('53637', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:32', '2023-05-24 07:55:32');
INSERT INTO `lc_error_log` VALUES ('53638', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:33', '2023-05-24 07:55:33');
INSERT INTO `lc_error_log` VALUES ('53639', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '47.101.149.21', '2023-05-24 07:55:33', '2023-05-24 07:55:33');
INSERT INTO `lc_error_log` VALUES ('53640', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:33', '2023-05-24 07:55:33');
INSERT INTO `lc_error_log` VALUES ('53641', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:33', '2023-05-24 07:55:33');
INSERT INTO `lc_error_log` VALUES ('53642', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:33', '2023-05-24 07:55:33');
INSERT INTO `lc_error_log` VALUES ('53643', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '47.101.149.21', '2023-05-24 07:55:33', '2023-05-24 07:55:33');
INSERT INTO `lc_error_log` VALUES ('53644', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:34', '2023-05-24 07:55:34');
INSERT INTO `lc_error_log` VALUES ('53645', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:34', '2023-05-24 07:55:34');
INSERT INTO `lc_error_log` VALUES ('53646', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:52', '2023-05-24 07:55:52');
INSERT INTO `lc_error_log` VALUES ('53647', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-05-24 07:55:52', '2023-05-24 07:55:52');
INSERT INTO `lc_error_log` VALUES ('53648', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-24 13:56:24', '2023-05-24 13:56:24');
INSERT INTO `lc_error_log` VALUES ('53649', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-05-24 13:56:24', '2023-05-24 13:56:24');
INSERT INTO `lc_error_log` VALUES ('53650', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.229.181', '2023-05-24 19:06:29', '2023-05-24 19:06:29');
INSERT INTO `lc_error_log` VALUES ('53651', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.229.181', '2023-05-24 19:06:29', '2023-05-24 19:06:29');
INSERT INTO `lc_error_log` VALUES ('53652', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/getUserInfo', '113.117.185.128', '2023-05-24 20:05:33', '2023-05-24 20:05:33');
INSERT INTO `lc_error_log` VALUES ('53653', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/getWeChatBindingParams', '113.117.185.128', '2023-05-24 20:05:33', '2023-05-24 20:05:33');
INSERT INTO `lc_error_log` VALUES ('53654', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-05-24 21:35:15', '2023-05-24 21:35:15');
INSERT INTO `lc_error_log` VALUES ('53655', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-05-24 21:35:17', '2023-05-24 21:35:17');
INSERT INTO `lc_error_log` VALUES ('53656', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.106', '2023-05-24 22:54:14', '2023-05-24 22:54:14');
INSERT INTO `lc_error_log` VALUES ('53657', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.117', '2023-05-24 22:54:21', '2023-05-24 22:54:21');
INSERT INTO `lc_error_log` VALUES ('53658', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '66.249.79.11', '2023-05-25 00:33:46', '2023-05-25 00:33:46');
INSERT INTO `lc_error_log` VALUES ('53659', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=19', '117.189.229.181', '2023-05-25 01:08:52', '2023-05-25 01:08:52');
INSERT INTO `lc_error_log` VALUES ('53660', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '117.189.229.181', '2023-05-25 01:08:52', '2023-05-25 01:08:52');
INSERT INTO `lc_error_log` VALUES ('53661', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-25 03:55:34', '2023-05-25 03:55:34');
INSERT INTO `lc_error_log` VALUES ('53662', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-25 03:55:34', '2023-05-25 03:55:34');
INSERT INTO `lc_error_log` VALUES ('53663', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-05-25 07:55:47', '2023-05-25 07:55:47');
INSERT INTO `lc_error_log` VALUES ('53664', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.78.18.106', '2023-05-25 11:55:15', '2023-05-25 11:55:15');
INSERT INTO `lc_error_log` VALUES ('53665', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在或未通过审核1', '0', '94', '/v1/getKeInfo?ke_id=23589&order_id=&chapters_id=48&directory_id=404', '116.179.37.194', '2023-05-25 13:50:01', '2023-05-25 13:50:01');
INSERT INTO `lc_error_log` VALUES ('53666', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-05-25 13:59:40', '2023-05-25 13:59:40');
INSERT INTO `lc_error_log` VALUES ('53667', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-05-25 13:59:42', '2023-05-25 13:59:42');
INSERT INTO `lc_error_log` VALUES ('53668', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-05-25 19:26:46', '2023-05-25 19:26:46');
INSERT INTO `lc_error_log` VALUES ('53669', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-05-25 20:57:37', '2023-05-25 20:57:37');
INSERT INTO `lc_error_log` VALUES ('53670', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-05-25 20:57:38', '2023-05-25 20:57:38');
INSERT INTO `lc_error_log` VALUES ('53671', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '101.87.147.26', '2023-05-25 21:18:39', '2023-05-25 21:18:39');
INSERT INTO `lc_error_log` VALUES ('53672', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.106.189.158', '2023-05-25 22:07:07', '2023-05-25 22:07:07');
INSERT INTO `lc_error_log` VALUES ('53673', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.18.100', '2023-05-25 23:40:08', '2023-05-25 23:40:08');
INSERT INTO `lc_error_log` VALUES ('53674', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-26 00:30:31', '2023-05-26 00:30:31');
INSERT INTO `lc_error_log` VALUES ('53675', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-05-26 04:40:09', '2023-05-26 04:40:09');
INSERT INTO `lc_error_log` VALUES ('53676', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-05-26 04:43:00', '2023-05-26 04:43:00');
INSERT INTO `lc_error_log` VALUES ('53677', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-05-26 04:43:02', '2023-05-26 04:43:02');
INSERT INTO `lc_error_log` VALUES ('53678', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-26 18:08:16', '2023-05-26 18:08:16');
INSERT INTO `lc_error_log` VALUES ('53679', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-26 18:19:32', '2023-05-26 18:19:32');
INSERT INTO `lc_error_log` VALUES ('53680', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-05-26 22:18:15', '2023-05-26 22:18:15');
INSERT INTO `lc_error_log` VALUES ('53681', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-26 23:00:08', '2023-05-26 23:00:08');
INSERT INTO `lc_error_log` VALUES ('53682', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-05-26 23:03:56', '2023-05-26 23:03:56');
INSERT INTO `lc_error_log` VALUES ('53683', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.229.181', '2023-05-26 23:04:44', '2023-05-26 23:04:44');
INSERT INTO `lc_error_log` VALUES ('53684', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.229.181', '2023-05-26 23:04:44', '2023-05-26 23:04:44');
INSERT INTO `lc_error_log` VALUES ('53685', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-26 23:06:45', '2023-05-26 23:06:45');
INSERT INTO `lc_error_log` VALUES ('53686', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-26 23:06:56', '2023-05-26 23:06:56');
INSERT INTO `lc_error_log` VALUES ('53687', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=43&info=true', '42.236.10.75', '2023-05-27 16:52:46', '2023-05-27 16:52:46');
INSERT INTO `lc_error_log` VALUES ('53688', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=43&info=true', '42.236.10.75', '2023-05-27 16:52:48', '2023-05-27 16:52:48');
INSERT INTO `lc_error_log` VALUES ('53689', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-05-27 17:13:09', '2023-05-27 17:13:09');
INSERT INTO `lc_error_log` VALUES ('53690', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-05-27 17:25:39', '2023-05-27 17:25:39');
INSERT INTO `lc_error_log` VALUES ('53691', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-05-27 17:25:42', '2023-05-27 17:25:42');
INSERT INTO `lc_error_log` VALUES ('53692', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-05-27 18:47:50', '2023-05-27 18:47:50');
INSERT INTO `lc_error_log` VALUES ('53693', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-05-27 18:53:44', '2023-05-27 18:53:44');
INSERT INTO `lc_error_log` VALUES ('53694', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-05-27 18:54:24', '2023-05-27 18:54:24');
INSERT INTO `lc_error_log` VALUES ('53695', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-05-27 18:54:36', '2023-05-27 18:54:36');
INSERT INTO `lc_error_log` VALUES ('53696', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-05-27 18:54:45', '2023-05-27 18:54:45');
INSERT INTO `lc_error_log` VALUES ('53697', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-05-27 18:55:11', '2023-05-27 18:55:11');
INSERT INTO `lc_error_log` VALUES ('53698', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-27 18:56:12', '2023-05-27 18:56:12');
INSERT INTO `lc_error_log` VALUES ('53699', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-27 18:58:33', '2023-05-27 18:58:33');
INSERT INTO `lc_error_log` VALUES ('53700', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.229.181', '2023-05-27 20:04:37', '2023-05-27 20:04:37');
INSERT INTO `lc_error_log` VALUES ('53701', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.229.181', '2023-05-27 20:04:37', '2023-05-27 20:04:37');
INSERT INTO `lc_error_log` VALUES ('53702', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-27 20:22:52', '2023-05-27 20:22:52');
INSERT INTO `lc_error_log` VALUES ('53703', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 00:05:32', '2023-05-28 00:05:32');
INSERT INTO `lc_error_log` VALUES ('53704', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 00:08:19', '2023-05-28 00:08:19');
INSERT INTO `lc_error_log` VALUES ('53705', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 00:28:09', '2023-05-28 00:28:09');
INSERT INTO `lc_error_log` VALUES ('53706', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.17', '2023-05-28 06:54:31', '2023-05-28 06:54:31');
INSERT INTO `lc_error_log` VALUES ('53707', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.162', '2023-05-28 09:12:07', '2023-05-28 09:12:07');
INSERT INTO `lc_error_log` VALUES ('53708', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.129', '2023-05-28 09:12:22', '2023-05-28 09:12:22');
INSERT INTO `lc_error_log` VALUES ('53709', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-05-28 09:12:23', '2023-05-28 09:12:23');
INSERT INTO `lc_error_log` VALUES ('53710', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.225.158.236', '2023-05-28 09:15:35', '2023-05-28 09:15:35');
INSERT INTO `lc_error_log` VALUES ('53711', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.30', '2023-05-28 11:07:20', '2023-05-28 11:07:20');
INSERT INTO `lc_error_log` VALUES ('53712', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.30', '2023-05-28 11:07:22', '2023-05-28 11:07:22');
INSERT INTO `lc_error_log` VALUES ('53713', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-28 14:20:19', '2023-05-28 14:20:19');
INSERT INTO `lc_error_log` VALUES ('53714', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:41:45', '2023-05-28 14:41:45');
INSERT INTO `lc_error_log` VALUES ('53715', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:41:46', '2023-05-28 14:41:46');
INSERT INTO `lc_error_log` VALUES ('53716', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:41:46', '2023-05-28 14:41:46');
INSERT INTO `lc_error_log` VALUES ('53717', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:41:46', '2023-05-28 14:41:46');
INSERT INTO `lc_error_log` VALUES ('53718', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:41:46', '2023-05-28 14:41:46');
INSERT INTO `lc_error_log` VALUES ('53719', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '123.57.235.51', '2023-05-28 14:41:47', '2023-05-28 14:41:47');
INSERT INTO `lc_error_log` VALUES ('53720', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:41:47', '2023-05-28 14:41:47');
INSERT INTO `lc_error_log` VALUES ('53721', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '123.57.235.51', '2023-05-28 14:41:47', '2023-05-28 14:41:47');
INSERT INTO `lc_error_log` VALUES ('53722', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:41:47', '2023-05-28 14:41:47');
INSERT INTO `lc_error_log` VALUES ('53723', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:42:02', '2023-05-28 14:42:02');
INSERT INTO `lc_error_log` VALUES ('53724', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-05-28 14:42:02', '2023-05-28 14:42:02');
INSERT INTO `lc_error_log` VALUES ('53725', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.18', '2023-05-28 14:57:58', '2023-05-28 14:57:58');
INSERT INTO `lc_error_log` VALUES ('53726', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-05-28 15:18:35', '2023-05-28 15:18:35');
INSERT INTO `lc_error_log` VALUES ('53727', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-05-28 15:18:36', '2023-05-28 15:18:36');
INSERT INTO `lc_error_log` VALUES ('53728', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.20', '2023-05-28 17:00:08', '2023-05-28 17:00:08');
INSERT INTO `lc_error_log` VALUES ('53729', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.79.11', '2023-05-28 17:22:40', '2023-05-28 17:22:40');
INSERT INTO `lc_error_log` VALUES ('53730', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.229.181', '2023-05-28 20:03:25', '2023-05-28 20:03:25');
INSERT INTO `lc_error_log` VALUES ('53731', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.229.181', '2023-05-28 20:03:25', '2023-05-28 20:03:25');
INSERT INTO `lc_error_log` VALUES ('53732', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 21:55:56', '2023-05-28 21:55:56');
INSERT INTO `lc_error_log` VALUES ('53733', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-28 22:49:06', '2023-05-28 22:49:06');
INSERT INTO `lc_error_log` VALUES ('53734', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.229.181', '2023-05-28 23:05:06', '2023-05-28 23:05:06');
INSERT INTO `lc_error_log` VALUES ('53735', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 23:14:25', '2023-05-28 23:14:25');
INSERT INTO `lc_error_log` VALUES ('53736', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 23:14:39', '2023-05-28 23:14:39');
INSERT INTO `lc_error_log` VALUES ('53737', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 23:14:50', '2023-05-28 23:14:50');
INSERT INTO `lc_error_log` VALUES ('53738', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-28 23:15:08', '2023-05-28 23:15:08');
INSERT INTO `lc_error_log` VALUES ('53739', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-28 23:27:45', '2023-05-28 23:27:45');
INSERT INTO `lc_error_log` VALUES ('53740', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-28 23:31:22', '2023-05-28 23:31:22');
INSERT INTO `lc_error_log` VALUES ('53741', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.198', '2023-05-29 00:57:02', '2023-05-29 00:57:02');
INSERT INTO `lc_error_log` VALUES ('53742', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-05-29 05:29:22', '2023-05-29 05:29:22');
INSERT INTO `lc_error_log` VALUES ('53743', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-05-29 05:33:18', '2023-05-29 05:33:18');
INSERT INTO `lc_error_log` VALUES ('53744', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-05-29 05:33:23', '2023-05-29 05:33:23');
INSERT INTO `lc_error_log` VALUES ('53745', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.79.13', '2023-05-29 07:32:23', '2023-05-29 07:32:23');
INSERT INTO `lc_error_log` VALUES ('53746', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.79.11', '2023-05-29 07:32:29', '2023-05-29 07:32:29');
INSERT INTO `lc_error_log` VALUES ('53747', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.18', '2023-05-29 08:41:42', '2023-05-29 08:41:42');
INSERT INTO `lc_error_log` VALUES ('53748', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.18', '2023-05-29 08:41:43', '2023-05-29 08:41:43');
INSERT INTO `lc_error_log` VALUES ('53749', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-05-29 11:07:42', '2023-05-29 11:07:42');
INSERT INTO `lc_error_log` VALUES ('53750', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '180.98.129.60', '2023-05-29 11:40:18', '2023-05-29 11:40:18');
INSERT INTO `lc_error_log` VALUES ('53751', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-29 18:06:45', '2023-05-29 18:06:45');
INSERT INTO `lc_error_log` VALUES ('53752', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-05-29 18:18:05', '2023-05-29 18:18:05');
INSERT INTO `lc_error_log` VALUES ('53753', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请添加内容', '0', '524', '/v1/mp/keDirectory', '117.189.229.181', '2023-05-29 21:06:12', '2023-05-29 21:06:12');
INSERT INTO `lc_error_log` VALUES ('53754', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称必填', '0', '524', '/v1/mp/keDirectory', '117.189.229.181', '2023-05-29 21:48:45', '2023-05-29 21:48:45');
INSERT INTO `lc_error_log` VALUES ('53755', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.16', '2023-05-30 01:47:18', '2023-05-30 01:47:18');
INSERT INTO `lc_error_log` VALUES ('53756', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '52.167.144.31', '2023-05-30 02:24:37', '2023-05-30 02:24:37');
INSERT INTO `lc_error_log` VALUES ('53757', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.27', '2023-05-30 03:47:13', '2023-05-30 03:47:13');
INSERT INTO `lc_error_log` VALUES ('53758', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-05-30 03:59:11', '2023-05-30 03:59:11');
INSERT INTO `lc_error_log` VALUES ('53759', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-05-30 03:59:12', '2023-05-30 03:59:12');
INSERT INTO `lc_error_log` VALUES ('53760', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-30 13:00:00', '2023-05-30 13:00:00');
INSERT INTO `lc_error_log` VALUES ('53761', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.248.128.82', '2023-05-30 13:00:05', '2023-05-30 13:00:05');
INSERT INTO `lc_error_log` VALUES ('53762', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.143.101', '2023-05-30 14:17:06', '2023-05-30 14:17:06');
INSERT INTO `lc_error_log` VALUES ('53763', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '113.219.255.221', '2023-05-30 14:22:14', '2023-05-30 14:22:14');
INSERT INTO `lc_error_log` VALUES ('53764', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-30 23:41:38', '2023-05-30 23:41:38');
INSERT INTO `lc_error_log` VALUES ('53765', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '218.95.234.107', '2023-05-31 01:13:36', '2023-05-31 01:13:36');
INSERT INTO `lc_error_log` VALUES ('53766', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '120.211.145.96', '2023-05-31 01:14:17', '2023-05-31 01:14:17');
INSERT INTO `lc_error_log` VALUES ('53767', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.60.108.123', '2023-05-31 04:54:12', '2023-05-31 04:54:12');
INSERT INTO `lc_error_log` VALUES ('53768', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-05-31 13:20:45', '2023-05-31 13:20:45');
INSERT INTO `lc_error_log` VALUES ('53769', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 15:16:35', '2023-05-31 15:16:35');
INSERT INTO `lc_error_log` VALUES ('53770', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-05-31 15:36:31', '2023-05-31 15:36:31');
INSERT INTO `lc_error_log` VALUES ('53771', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 15:37:36', '2023-05-31 15:37:36');
INSERT INTO `lc_error_log` VALUES ('53772', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 15:37:36', '2023-05-31 15:37:36');
INSERT INTO `lc_error_log` VALUES ('53773', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-05-31 15:37:45', '2023-05-31 15:37:45');
INSERT INTO `lc_error_log` VALUES ('53774', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-05-31 15:42:28', '2023-05-31 15:42:28');
INSERT INTO `lc_error_log` VALUES ('53775', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.198', '2023-05-31 17:47:20', '2023-05-31 17:47:20');
INSERT INTO `lc_error_log` VALUES ('53776', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-05-31 20:22:42', '2023-05-31 20:22:42');
INSERT INTO `lc_error_log` VALUES ('53777', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.8', '2023-05-31 21:27:03', '2023-05-31 21:27:03');
INSERT INTO `lc_error_log` VALUES ('53778', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:44:35', '2023-05-31 23:44:35');
INSERT INTO `lc_error_log` VALUES ('53779', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:44:45', '2023-05-31 23:44:45');
INSERT INTO `lc_error_log` VALUES ('53780', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:44:54', '2023-05-31 23:44:54');
INSERT INTO `lc_error_log` VALUES ('53781', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-05-31 23:51:11', '2023-05-31 23:51:11');
INSERT INTO `lc_error_log` VALUES ('53782', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:52:01', '2023-05-31 23:52:01');
INSERT INTO `lc_error_log` VALUES ('53783', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:52:12', '2023-05-31 23:52:12');
INSERT INTO `lc_error_log` VALUES ('53784', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:56:11', '2023-05-31 23:56:11');
INSERT INTO `lc_error_log` VALUES ('53785', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:56:20', '2023-05-31 23:56:20');
INSERT INTO `lc_error_log` VALUES ('53786', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:56:30', '2023-05-31 23:56:30');
INSERT INTO `lc_error_log` VALUES ('53787', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-05-31 23:57:25', '2023-05-31 23:57:25');
INSERT INTO `lc_error_log` VALUES ('53788', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-06-01 00:02:48', '2023-06-01 00:02:48');
INSERT INTO `lc_error_log` VALUES ('53789', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-06-01 00:08:50', '2023-06-01 00:08:50');
INSERT INTO `lc_error_log` VALUES ('53790', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-06-01 00:08:57', '2023-06-01 00:08:57');
INSERT INTO `lc_error_log` VALUES ('53791', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.181', '2023-06-01 00:09:06', '2023-06-01 00:09:06');
INSERT INTO `lc_error_log` VALUES ('53792', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-01 00:24:46', '2023-06-01 00:24:46');
INSERT INTO `lc_error_log` VALUES ('53793', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-01 00:27:07', '2023-06-01 00:27:07');
INSERT INTO `lc_error_log` VALUES ('53794', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-06-01 02:22:08', '2023-06-01 02:22:08');
INSERT INTO `lc_error_log` VALUES ('53795', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '118.123.105.86', '2023-06-01 02:42:11', '2023-06-01 02:42:11');
INSERT INTO `lc_error_log` VALUES ('53796', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-01 04:12:35', '2023-06-01 04:12:35');
INSERT INTO `lc_error_log` VALUES ('53797', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-01 04:12:38', '2023-06-01 04:12:38');
INSERT INTO `lc_error_log` VALUES ('53798', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=19', '116.179.37.173', '2023-06-01 14:51:14', '2023-06-01 14:51:14');
INSERT INTO `lc_error_log` VALUES ('53799', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.198', '2023-06-01 14:51:14', '2023-06-01 14:51:14');
INSERT INTO `lc_error_log` VALUES ('53800', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=19', '116.179.37.125', '2023-06-01 14:51:15', '2023-06-01 14:51:15');
INSERT INTO `lc_error_log` VALUES ('53801', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.141', '2023-06-01 14:51:16', '2023-06-01 14:51:16');
INSERT INTO `lc_error_log` VALUES ('53802', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.21', '2023-06-01 19:57:32', '2023-06-01 19:57:32');
INSERT INTO `lc_error_log` VALUES ('53803', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.129', '2023-06-01 19:59:13', '2023-06-01 19:59:13');
INSERT INTO `lc_error_log` VALUES ('53804', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-06-01 19:59:23', '2023-06-01 19:59:23');
INSERT INTO `lc_error_log` VALUES ('53805', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-06-01 19:59:29', '2023-06-01 19:59:29');
INSERT INTO `lc_error_log` VALUES ('53806', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.101.229', '2023-06-01 20:01:16', '2023-06-01 20:01:16');
INSERT INTO `lc_error_log` VALUES ('53807', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-01 22:17:56', '2023-06-01 22:17:56');
INSERT INTO `lc_error_log` VALUES ('53808', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-01 22:26:57', '2023-06-01 22:26:57');
INSERT INTO `lc_error_log` VALUES ('53809', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '157.55.39.220', '2023-06-02 02:48:16', '2023-06-02 02:48:16');
INSERT INTO `lc_error_log` VALUES ('53810', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitKechengOrder', '183.221.109.135', '2023-06-02 14:22:45', '2023-06-02 14:22:45');
INSERT INTO `lc_error_log` VALUES ('53811', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-02 18:59:32', '2023-06-02 18:59:32');
INSERT INTO `lc_error_log` VALUES ('53812', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-02 19:01:00', '2023-06-02 19:01:00');
INSERT INTO `lc_error_log` VALUES ('53813', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-02 19:02:44', '2023-06-02 19:02:44');
INSERT INTO `lc_error_log` VALUES ('53814', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-02 19:04:28', '2023-06-02 19:04:28');
INSERT INTO `lc_error_log` VALUES ('53815', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-02 19:04:37', '2023-06-02 19:04:37');
INSERT INTO `lc_error_log` VALUES ('53816', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-02 19:04:50', '2023-06-02 19:04:50');
INSERT INTO `lc_error_log` VALUES ('53817', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '180.163.220.67', '2023-06-02 21:05:42', '2023-06-02 21:05:42');
INSERT INTO `lc_error_log` VALUES ('53818', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.38', '2023-06-02 21:05:51', '2023-06-02 21:05:51');
INSERT INTO `lc_error_log` VALUES ('53819', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.75.171', '2023-06-02 21:41:54', '2023-06-02 21:41:54');
INSERT INTO `lc_error_log` VALUES ('53820', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.75.173', '2023-06-02 21:41:58', '2023-06-02 21:41:58');
INSERT INTO `lc_error_log` VALUES ('53821', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.198', '2023-06-02 21:43:41', '2023-06-02 21:43:41');
INSERT INTO `lc_error_log` VALUES ('53822', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getInfo?id=17', '101.43.98.47', '2023-06-02 22:52:06', '2023-06-02 22:52:06');
INSERT INTO `lc_error_log` VALUES ('53823', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-02 22:52:43', '2023-06-02 22:52:43');
INSERT INTO `lc_error_log` VALUES ('53824', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-02 23:51:27', '2023-06-02 23:51:27');
INSERT INTO `lc_error_log` VALUES ('53825', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//article/article', '101.43.98.47', '2023-06-02 23:53:01', '2023-06-02 23:53:01');
INSERT INTO `lc_error_log` VALUES ('53826', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-03 00:15:37', '2023-06-03 00:15:37');
INSERT INTO `lc_error_log` VALUES ('53827', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-03 00:25:12', '2023-06-03 00:25:12');
INSERT INTO `lc_error_log` VALUES ('53828', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.198', '2023-06-03 02:05:56', '2023-06-03 02:05:56');
INSERT INTO `lc_error_log` VALUES ('53829', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-06-03 02:06:54', '2023-06-03 02:06:54');
INSERT INTO `lc_error_log` VALUES ('53830', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-06-03 02:06:55', '2023-06-03 02:06:55');
INSERT INTO `lc_error_log` VALUES ('53831', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.28', '2023-06-03 04:14:23', '2023-06-03 04:14:23');
INSERT INTO `lc_error_log` VALUES ('53832', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.75.173', '2023-06-03 07:57:29', '2023-06-03 07:57:29');
INSERT INTO `lc_error_log` VALUES ('53833', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-06-03 11:28:21', '2023-06-03 11:28:21');
INSERT INTO `lc_error_log` VALUES ('53834', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '125.124.54.158', '2023-06-03 11:28:40', '2023-06-03 11:28:40');
INSERT INTO `lc_error_log` VALUES ('53835', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-03 13:35:01', '2023-06-03 13:35:01');
INSERT INTO `lc_error_log` VALUES ('53836', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-03 21:08:07', '2023-06-03 21:08:07');
INSERT INTO `lc_error_log` VALUES ('53837', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-03 21:33:03', '2023-06-03 21:33:03');
INSERT INTO `lc_error_log` VALUES ('53838', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-03 21:35:04', '2023-06-03 21:35:04');
INSERT INTO `lc_error_log` VALUES ('53839', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getInfo?id=17', '101.43.98.47', '2023-06-03 21:36:40', '2023-06-03 21:36:40');
INSERT INTO `lc_error_log` VALUES ('53840', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//article/article', '101.43.98.47', '2023-06-03 21:37:16', '2023-06-03 21:37:16');
INSERT INTO `lc_error_log` VALUES ('53841', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-03 21:40:07', '2023-06-03 21:40:07');
INSERT INTO `lc_error_log` VALUES ('53842', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-03 21:42:48', '2023-06-03 21:42:48');
INSERT INTO `lc_error_log` VALUES ('53843', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-03 23:15:38', '2023-06-03 23:15:38');
INSERT INTO `lc_error_log` VALUES ('53844', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-03 23:21:15', '2023-06-03 23:21:15');
INSERT INTO `lc_error_log` VALUES ('53845', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-03 23:33:13', '2023-06-03 23:33:13');
INSERT INTO `lc_error_log` VALUES ('53846', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-03 23:35:47', '2023-06-03 23:35:47');
INSERT INTO `lc_error_log` VALUES ('53847', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-04 00:01:26', '2023-06-04 00:01:26');
INSERT INTO `lc_error_log` VALUES ('53848', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-04 01:04:06', '2023-06-04 01:04:06');
INSERT INTO `lc_error_log` VALUES ('53849', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '183.197.19.115', '2023-06-04 02:50:00', '2023-06-04 02:50:00');
INSERT INTO `lc_error_log` VALUES ('53850', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-04 04:06:57', '2023-06-04 04:06:57');
INSERT INTO `lc_error_log` VALUES ('53851', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/wp-content/uploads/wp_dndcf7_uploads/wpcf7-files/2Qi7Bkh8Y6aDL0eEQ0BbK9TzfGP.svg', '114.132.203.61', '2023-06-04 06:21:37', '2023-06-04 06:21:37');
INSERT INTO `lc_error_log` VALUES ('53852', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/wp-content/uploads/wp_dndcf7_uploads/wpcf7-files/2Qi7BhSt3N0LRJsMql41tNOwlzn.svg', '114.132.203.195', '2023-06-04 06:27:39', '2023-06-04 06:27:39');
INSERT INTO `lc_error_log` VALUES ('53853', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.198', '2023-06-04 07:19:47', '2023-06-04 07:19:47');
INSERT INTO `lc_error_log` VALUES ('53854', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.29', '2023-06-04 07:20:48', '2023-06-04 07:20:48');
INSERT INTO `lc_error_log` VALUES ('53855', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.29', '2023-06-04 07:20:50', '2023-06-04 07:20:50');
INSERT INTO `lc_error_log` VALUES ('53856', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=23583&info=true', '223.102.211.151', '2023-06-04 09:40:41', '2023-06-04 09:40:41');
INSERT INTO `lc_error_log` VALUES ('53857', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=23583&info=true', '223.102.211.151', '2023-06-04 09:40:43', '2023-06-04 09:40:43');
INSERT INTO `lc_error_log` VALUES ('53858', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=23583&info=true', '223.102.211.151', '2023-06-04 09:41:11', '2023-06-04 09:41:11');
INSERT INTO `lc_error_log` VALUES ('53859', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=23583&info=true', '223.102.211.151', '2023-06-04 09:41:12', '2023-06-04 09:41:12');
INSERT INTO `lc_error_log` VALUES ('53860', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Url.php', 'controller not exists:wp-content', '0', '62', '/wp-content/uploads/wp_dndcf7_uploads/wpcf7-files/2Qi7Ba8mudUwI4V4TVGAWhxnxBW.svg', '114.132.203.162', '2023-06-04 11:24:04', '2023-06-04 11:24:04');
INSERT INTO `lc_error_log` VALUES ('53861', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/wp-content/uploads/wp_dndcf7_uploads/wpcf7-files/2Qi7BhSt3N0LRJsMql41tNOwlzn.svg', '114.132.203.195', '2023-06-04 12:32:04', '2023-06-04 12:32:04');
INSERT INTO `lc_error_log` VALUES ('53862', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '183.197.19.115', '2023-06-04 12:37:33', '2023-06-04 12:37:33');
INSERT INTO `lc_error_log` VALUES ('53863', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '183.197.19.115', '2023-06-04 12:37:33', '2023-06-04 12:37:33');
INSERT INTO `lc_error_log` VALUES ('53864', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.23', '2023-06-04 16:20:04', '2023-06-04 16:20:04');
INSERT INTO `lc_error_log` VALUES ('53865', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.23', '2023-06-04 16:20:06', '2023-06-04 16:20:06');
INSERT INTO `lc_error_log` VALUES ('53866', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '36.24.210.254', '2023-06-04 16:52:50', '2023-06-04 16:52:50');
INSERT INTO `lc_error_log` VALUES ('53867', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Url.php', 'controller not exists:wp-content', '0', '62', '/wp-content/uploads/wp_dndcf7_uploads/wpcf7-files/2Qi7Ba8mudUwI4V4TVGAWhxnxBW.svg', '114.132.203.153', '2023-06-04 17:36:15', '2023-06-04 17:36:15');
INSERT INTO `lc_error_log` VALUES ('53868', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-04 18:31:25', '2023-06-04 18:31:25');
INSERT INTO `lc_error_log` VALUES ('53869', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-04 18:33:23', '2023-06-04 18:33:23');
INSERT INTO `lc_error_log` VALUES ('53870', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-04 18:37:56', '2023-06-04 18:37:56');
INSERT INTO `lc_error_log` VALUES ('53871', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//mpAudit', '101.43.98.47', '2023-06-04 18:41:43', '2023-06-04 18:41:43');
INSERT INTO `lc_error_log` VALUES ('53872', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-04 19:25:40', '2023-06-04 19:25:40');
INSERT INTO `lc_error_log` VALUES ('53873', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-06-04 19:31:43', '2023-06-04 19:31:43');
INSERT INTO `lc_error_log` VALUES ('53874', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-06-04 19:32:33', '2023-06-04 19:32:33');
INSERT INTO `lc_error_log` VALUES ('53875', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-04 19:32:36', '2023-06-04 19:32:36');
INSERT INTO `lc_error_log` VALUES ('53876', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//article/article', '101.43.98.47', '2023-06-04 19:37:56', '2023-06-04 19:37:56');
INSERT INTO `lc_error_log` VALUES ('53877', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-04 19:43:26', '2023-06-04 19:43:26');
INSERT INTO `lc_error_log` VALUES ('53878', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-04 19:44:39', '2023-06-04 19:44:39');
INSERT INTO `lc_error_log` VALUES ('53879', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getInfo?id=17', '101.43.98.47', '2023-06-04 19:50:04', '2023-06-04 19:50:04');
INSERT INTO `lc_error_log` VALUES ('53880', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-04 19:50:21', '2023-06-04 19:50:21');
INSERT INTO `lc_error_log` VALUES ('53881', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.65.84.215', '2023-06-04 21:26:28', '2023-06-04 21:26:28');
INSERT INTO `lc_error_log` VALUES ('53882', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-04 21:50:23', '2023-06-04 21:50:23');
INSERT INTO `lc_error_log` VALUES ('53883', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.106.149', '2023-06-04 23:58:49', '2023-06-04 23:58:49');
INSERT INTO `lc_error_log` VALUES ('53884', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.23', '2023-06-05 00:20:01', '2023-06-05 00:20:01');
INSERT INTO `lc_error_log` VALUES ('53885', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.109', '2023-06-05 00:21:48', '2023-06-05 00:21:48');
INSERT INTO `lc_error_log` VALUES ('53886', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '180.163.220.3', '2023-06-05 00:21:52', '2023-06-05 00:21:52');
INSERT INTO `lc_error_log` VALUES ('53887', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-06-05 11:13:18', '2023-06-05 11:13:18');
INSERT INTO `lc_error_log` VALUES ('53888', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-05 12:41:11', '2023-06-05 12:41:11');
INSERT INTO `lc_error_log` VALUES ('53889', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-05 12:43:54', '2023-06-05 12:43:54');
INSERT INTO `lc_error_log` VALUES ('53890', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '112.13.112.90', '2023-06-05 13:21:41', '2023-06-05 13:21:41');
INSERT INTO `lc_error_log` VALUES ('53891', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-06-05 14:21:35', '2023-06-05 14:21:35');
INSERT INTO `lc_error_log` VALUES ('53892', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-05 14:55:45', '2023-06-05 14:55:45');
INSERT INTO `lc_error_log` VALUES ('53893', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-05 15:11:40', '2023-06-05 15:11:40');
INSERT INTO `lc_error_log` VALUES ('53894', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-05 15:11:43', '2023-06-05 15:11:43');
INSERT INTO `lc_error_log` VALUES ('53895', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.58.165.157', '2023-06-05 19:47:16', '2023-06-05 19:47:16');
INSERT INTO `lc_error_log` VALUES ('53896', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.15', '2023-06-05 19:58:37', '2023-06-05 19:58:37');
INSERT INTO `lc_error_log` VALUES ('53897', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.158', '2023-06-05 19:59:25', '2023-06-05 19:59:25');
INSERT INTO `lc_error_log` VALUES ('53898', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.137.113', '2023-06-05 20:02:49', '2023-06-05 20:02:49');
INSERT INTO `lc_error_log` VALUES ('53899', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.227.34.166', '2023-06-05 20:02:53', '2023-06-05 20:02:53');
INSERT INTO `lc_error_log` VALUES ('53900', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '175.6.137.113', '2023-06-05 20:03:15', '2023-06-05 20:03:15');
INSERT INTO `lc_error_log` VALUES ('53901', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '223.111.175.108', '2023-06-05 21:54:25', '2023-06-05 21:54:25');
INSERT INTO `lc_error_log` VALUES ('53902', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.63.211', '2023-06-05 22:03:40', '2023-06-05 22:03:40');
INSERT INTO `lc_error_log` VALUES ('53903', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-05 22:07:13', '2023-06-05 22:07:13');
INSERT INTO `lc_error_log` VALUES ('53904', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-05 22:14:41', '2023-06-05 22:14:41');
INSERT INTO `lc_error_log` VALUES ('53905', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-05 23:01:30', '2023-06-05 23:01:30');
INSERT INTO `lc_error_log` VALUES ('53906', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '32', '/v1/getArticleInfo?id=www.gouziyuan.cn', '66.249.75.175', '2023-06-06 00:50:58', '2023-06-06 00:50:58');
INSERT INTO `lc_error_log` VALUES ('53907', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '66.249.75.173', '2023-06-06 00:50:59', '2023-06-06 00:50:59');
INSERT INTO `lc_error_log` VALUES ('53908', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 02:01:56', '2023-06-06 02:01:56');
INSERT INTO `lc_error_log` VALUES ('53909', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.176.225', '2023-06-06 02:04:37', '2023-06-06 02:04:37');
INSERT INTO `lc_error_log` VALUES ('53910', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-06 02:17:13', '2023-06-06 02:17:13');
INSERT INTO `lc_error_log` VALUES ('53911', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-06 02:30:34', '2023-06-06 02:30:34');
INSERT INTO `lc_error_log` VALUES ('53912', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-06 16:01:56', '2023-06-06 16:01:56');
INSERT INTO `lc_error_log` VALUES ('53913', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-06-06 16:03:08', '2023-06-06 16:03:08');
INSERT INTO `lc_error_log` VALUES ('53914', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-06 16:05:25', '2023-06-06 16:05:25');
INSERT INTO `lc_error_log` VALUES ('53915', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-06 18:49:02', '2023-06-06 18:49:02');
INSERT INTO `lc_error_log` VALUES ('53916', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-06 18:49:02', '2023-06-06 18:49:02');
INSERT INTO `lc_error_log` VALUES ('53917', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.29', '2023-06-06 20:02:04', '2023-06-06 20:02:04');
INSERT INTO `lc_error_log` VALUES ('53918', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-06 21:01:25', '2023-06-06 21:01:25');
INSERT INTO `lc_error_log` VALUES ('53919', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:17:25', '2023-06-06 21:17:25');
INSERT INTO `lc_error_log` VALUES ('53920', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:17:25', '2023-06-06 21:17:25');
INSERT INTO `lc_error_log` VALUES ('53921', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:22:24', '2023-06-06 21:22:24');
INSERT INTO `lc_error_log` VALUES ('53922', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:05', '2023-06-06 21:35:05');
INSERT INTO `lc_error_log` VALUES ('53923', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:05', '2023-06-06 21:35:05');
INSERT INTO `lc_error_log` VALUES ('53924', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:05', '2023-06-06 21:35:05');
INSERT INTO `lc_error_log` VALUES ('53925', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanInfo?id=27734', '101.43.98.47', '2023-06-06 21:35:22', '2023-06-06 21:35:22');
INSERT INTO `lc_error_log` VALUES ('53926', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:27', '2023-06-06 21:35:27');
INSERT INTO `lc_error_log` VALUES ('53927', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:33', '2023-06-06 21:35:33');
INSERT INTO `lc_error_log` VALUES ('53928', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:40', '2023-06-06 21:35:40');
INSERT INTO `lc_error_log` VALUES ('53929', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:46', '2023-06-06 21:35:46');
INSERT INTO `lc_error_log` VALUES ('53930', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:51', '2023-06-06 21:35:51');
INSERT INTO `lc_error_log` VALUES ('53931', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:35:56', '2023-06-06 21:35:56');
INSERT INTO `lc_error_log` VALUES ('53932', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:36:09', '2023-06-06 21:36:09');
INSERT INTO `lc_error_log` VALUES ('53933', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:36:23', '2023-06-06 21:36:23');
INSERT INTO `lc_error_log` VALUES ('53934', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:36:28', '2023-06-06 21:36:28');
INSERT INTO `lc_error_log` VALUES ('53935', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:36:34', '2023-06-06 21:36:34');
INSERT INTO `lc_error_log` VALUES ('53936', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:36:43', '2023-06-06 21:36:43');
INSERT INTO `lc_error_log` VALUES ('53937', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:36:49', '2023-06-06 21:36:49');
INSERT INTO `lc_error_log` VALUES ('53938', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-06 21:43:37', '2023-06-06 21:43:37');
INSERT INTO `lc_error_log` VALUES ('53939', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.111.174.96', '2023-06-06 23:40:21', '2023-06-06 23:40:21');
INSERT INTO `lc_error_log` VALUES ('53940', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.19', '2023-06-07 00:33:04', '2023-06-07 00:33:04');
INSERT INTO `lc_error_log` VALUES ('53941', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-06-07 01:28:02', '2023-06-07 01:28:02');
INSERT INTO `lc_error_log` VALUES ('53942', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.27', '2023-06-07 02:33:13', '2023-06-07 02:33:13');
INSERT INTO `lc_error_log` VALUES ('53943', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-07 07:21:22', '2023-06-07 07:21:22');
INSERT INTO `lc_error_log` VALUES ('53944', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-06-07 08:35:09', '2023-06-07 08:35:09');
INSERT INTO `lc_error_log` VALUES ('53945', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '123.134.28.193', '2023-06-07 10:14:33', '2023-06-07 10:14:33');
INSERT INTO `lc_error_log` VALUES ('53946', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '222.128.118.7', '2023-06-07 10:18:01', '2023-06-07 10:18:01');
INSERT INTO `lc_error_log` VALUES ('53947', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-06-07 13:38:41', '2023-06-07 13:38:41');
INSERT INTO `lc_error_log` VALUES ('53948', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-06-07 13:38:41', '2023-06-07 13:38:41');
INSERT INTO `lc_error_log` VALUES ('53949', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '119.123.56.240', '2023-06-07 18:20:01', '2023-06-07 18:20:01');
INSERT INTO `lc_error_log` VALUES ('53950', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-07 20:33:58', '2023-06-07 20:33:58');
INSERT INTO `lc_error_log` VALUES ('53951', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-07 20:43:00', '2023-06-07 20:43:00');
INSERT INTO `lc_error_log` VALUES ('53952', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-07 21:29:26', '2023-06-07 21:29:26');
INSERT INTO `lc_error_log` VALUES ('53953', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-07 21:31:23', '2023-06-07 21:31:23');
INSERT INTO `lc_error_log` VALUES ('53954', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-07 21:31:52', '2023-06-07 21:31:52');
INSERT INTO `lc_error_log` VALUES ('53955', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-07 21:36:17', '2023-06-07 21:36:17');
INSERT INTO `lc_error_log` VALUES ('53956', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-07 21:38:48', '2023-06-07 21:38:48');
INSERT INTO `lc_error_log` VALUES ('53957', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-07 21:41:17', '2023-06-07 21:41:17');
INSERT INTO `lc_error_log` VALUES ('53958', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-07 21:46:06', '2023-06-07 21:46:06');
INSERT INTO `lc_error_log` VALUES ('53959', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-07 23:21:24', '2023-06-07 23:21:24');
INSERT INTO `lc_error_log` VALUES ('53960', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-07 23:25:17', '2023-06-07 23:25:17');
INSERT INTO `lc_error_log` VALUES ('53961', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '66.249.65.165', '2023-06-08 00:06:36', '2023-06-08 00:06:36');
INSERT INTO `lc_error_log` VALUES ('53962', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-08 09:01:30', '2023-06-08 09:01:30');
INSERT INTO `lc_error_log` VALUES ('53963', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-06-08 22:21:13', '2023-06-08 22:21:13');
INSERT INTO `lc_error_log` VALUES ('53964', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.29', '2023-06-09 00:35:14', '2023-06-09 00:35:14');
INSERT INTO `lc_error_log` VALUES ('53965', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-09 00:35:27', '2023-06-09 00:35:27');
INSERT INTO `lc_error_log` VALUES ('53966', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.112', '2023-06-09 03:45:24', '2023-06-09 03:45:24');
INSERT INTO `lc_error_log` VALUES ('53967', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.111.168.190', '2023-06-09 03:50:43', '2023-06-09 03:50:43');
INSERT INTO `lc_error_log` VALUES ('53968', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-09 03:58:37', '2023-06-09 03:58:37');
INSERT INTO `lc_error_log` VALUES ('53969', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-09 03:58:37', '2023-06-09 03:58:37');
INSERT INTO `lc_error_log` VALUES ('53970', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.74', '2023-06-09 05:27:12', '2023-06-09 05:27:12');
INSERT INTO `lc_error_log` VALUES ('53971', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.39', '2023-06-09 05:42:12', '2023-06-09 05:42:12');
INSERT INTO `lc_error_log` VALUES ('53972', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.77', '2023-06-09 05:57:13', '2023-06-09 05:57:13');
INSERT INTO `lc_error_log` VALUES ('53973', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '223.111.175.24', '2023-06-09 07:31:50', '2023-06-09 07:31:50');
INSERT INTO `lc_error_log` VALUES ('53974', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.101.48', '2023-06-09 07:41:47', '2023-06-09 07:41:47');
INSERT INTO `lc_error_log` VALUES ('53975', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.206.94.99', '2023-06-09 10:23:54', '2023-06-09 10:23:54');
INSERT INTO `lc_error_log` VALUES ('53976', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '1.206.94.99', '2023-06-09 11:26:09', '2023-06-09 11:26:09');
INSERT INTO `lc_error_log` VALUES ('53977', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '1.206.94.99', '2023-06-09 11:26:09', '2023-06-09 11:26:09');
INSERT INTO `lc_error_log` VALUES ('53978', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '1.206.94.99', '2023-06-09 11:26:39', '2023-06-09 11:26:39');
INSERT INTO `lc_error_log` VALUES ('53979', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '1.206.94.99', '2023-06-09 11:26:39', '2023-06-09 11:26:39');
INSERT INTO `lc_error_log` VALUES ('53980', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '1.206.94.99', '2023-06-09 11:26:45', '2023-06-09 11:26:45');
INSERT INTO `lc_error_log` VALUES ('53981', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '1.206.94.99', '2023-06-09 11:26:45', '2023-06-09 11:26:45');
INSERT INTO `lc_error_log` VALUES ('53982', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '1.206.94.99', '2023-06-09 11:26:50', '2023-06-09 11:26:50');
INSERT INTO `lc_error_log` VALUES ('53983', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '1.206.94.99', '2023-06-09 11:26:50', '2023-06-09 11:26:50');
INSERT INTO `lc_error_log` VALUES ('53984', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '1.206.94.99', '2023-06-09 11:26:54', '2023-06-09 11:26:54');
INSERT INTO `lc_error_log` VALUES ('53985', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '1.206.94.99', '2023-06-09 11:26:54', '2023-06-09 11:26:54');
INSERT INTO `lc_error_log` VALUES ('53986', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '1.206.94.99', '2023-06-09 11:27:26', '2023-06-09 11:27:26');
INSERT INTO `lc_error_log` VALUES ('53987', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '1.206.94.99', '2023-06-09 11:27:26', '2023-06-09 11:27:26');
INSERT INTO `lc_error_log` VALUES ('53988', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '61.157.25.14', '2023-06-09 14:14:12', '2023-06-09 14:14:12');
INSERT INTO `lc_error_log` VALUES ('53989', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '1.192.195.8', '2023-06-09 16:01:43', '2023-06-09 16:01:43');
INSERT INTO `lc_error_log` VALUES ('53990', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '1.192.195.5', '2023-06-09 16:01:46', '2023-06-09 16:01:46');
INSERT INTO `lc_error_log` VALUES ('53991', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '1.192.195.5', '2023-06-09 16:06:14', '2023-06-09 16:06:14');
INSERT INTO `lc_error_log` VALUES ('53992', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '1.192.195.5', '2023-06-09 16:06:32', '2023-06-09 16:06:32');
INSERT INTO `lc_error_log` VALUES ('53993', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\sitemap\\Xml', '0', '76', '/sitemap.xml', '101.199.254.230', '2023-06-09 16:07:15', '2023-06-09 16:07:15');
INSERT INTO `lc_error_log` VALUES ('53994', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '101.198.0.150', '2023-06-09 16:12:39', '2023-06-09 16:12:39');
INSERT INTO `lc_error_log` VALUES ('53995', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '101.198.0.152', '2023-06-09 16:12:40', '2023-06-09 16:12:40');
INSERT INTO `lc_error_log` VALUES ('53996', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '101.198.0.187', '2023-06-09 16:38:14', '2023-06-09 16:38:14');
INSERT INTO `lc_error_log` VALUES ('53997', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '101.198.0.181', '2023-06-09 16:38:33', '2023-06-09 16:38:33');
INSERT INTO `lc_error_log` VALUES ('53998', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\sitemap\\Xml', '0', '76', '/sitemap.xml', '101.199.254.199', '2023-06-09 16:44:44', '2023-06-09 16:44:44');
INSERT INTO `lc_error_log` VALUES ('53999', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-09 17:31:20', '2023-06-09 17:31:20');
INSERT INTO `lc_error_log` VALUES ('54000', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-09 17:43:58', '2023-06-09 17:43:58');
INSERT INTO `lc_error_log` VALUES ('54001', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-06-09 19:44:29', '2023-06-09 19:44:29');
INSERT INTO `lc_error_log` VALUES ('54002', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.197', '2023-06-09 19:49:48', '2023-06-09 19:49:48');
INSERT INTO `lc_error_log` VALUES ('54003', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.31', '2023-06-09 20:27:10', '2023-06-09 20:27:10');
INSERT INTO `lc_error_log` VALUES ('54004', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.23', '2023-06-09 22:05:33', '2023-06-09 22:05:33');
INSERT INTO `lc_error_log` VALUES ('54005', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.148.96.120', '2023-06-09 22:44:51', '2023-06-09 22:44:51');
INSERT INTO `lc_error_log` VALUES ('54006', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-10 02:07:17', '2023-06-10 02:07:17');
INSERT INTO `lc_error_log` VALUES ('54007', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-10 02:10:19', '2023-06-10 02:10:19');
INSERT INTO `lc_error_log` VALUES ('54008', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-10 02:11:40', '2023-06-10 02:11:40');
INSERT INTO `lc_error_log` VALUES ('54009', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.147.108', '2023-06-10 02:51:36', '2023-06-10 02:51:36');
INSERT INTO `lc_error_log` VALUES ('54010', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.157.131', '2023-06-10 02:51:38', '2023-06-10 02:51:38');
INSERT INTO `lc_error_log` VALUES ('54011', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-10 08:03:13', '2023-06-10 08:03:13');
INSERT INTO `lc_error_log` VALUES ('54012', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-10 08:03:14', '2023-06-10 08:03:14');
INSERT INTO `lc_error_log` VALUES ('54013', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-10 08:15:48', '2023-06-10 08:15:48');
INSERT INTO `lc_error_log` VALUES ('54014', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.20', '2023-06-10 15:09:14', '2023-06-10 15:09:14');
INSERT INTO `lc_error_log` VALUES ('54015', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-10 15:25:12', '2023-06-10 15:25:12');
INSERT INTO `lc_error_log` VALUES ('54016', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.26', '2023-06-10 17:11:32', '2023-06-10 17:11:32');
INSERT INTO `lc_error_log` VALUES ('54017', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-10 21:06:30', '2023-06-10 21:06:30');
INSERT INTO `lc_error_log` VALUES ('54018', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-10 21:08:18', '2023-06-10 21:08:18');
INSERT INTO `lc_error_log` VALUES ('54019', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-10 21:08:46', '2023-06-10 21:08:46');
INSERT INTO `lc_error_log` VALUES ('54020', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-10 21:59:33', '2023-06-10 21:59:33');
INSERT INTO `lc_error_log` VALUES ('54021', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.163', '2023-06-10 22:10:24', '2023-06-10 22:10:24');
INSERT INTO `lc_error_log` VALUES ('54022', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '117.189.229.163', '2023-06-10 22:10:40', '2023-06-10 22:10:40');
INSERT INTO `lc_error_log` VALUES ('54023', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '117.189.229.163', '2023-06-10 22:10:49', '2023-06-10 22:10:49');
INSERT INTO `lc_error_log` VALUES ('54024', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '117.189.229.163', '2023-06-10 22:10:55', '2023-06-10 22:10:55');
INSERT INTO `lc_error_log` VALUES ('54025', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '不能购买自己的资源', '0', '48', '/v1/submitOrder', '117.189.229.163', '2023-06-10 22:12:42', '2023-06-10 22:12:42');
INSERT INTO `lc_error_log` VALUES ('54026', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-10 22:42:59', '2023-06-10 22:42:59');
INSERT INTO `lc_error_log` VALUES ('54027', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-11 00:50:20', '2023-06-11 00:50:20');
INSERT INTO `lc_error_log` VALUES ('54028', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-11 01:01:35', '2023-06-11 01:01:35');
INSERT INTO `lc_error_log` VALUES ('54029', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-06-11 03:14:55', '2023-06-11 03:14:55');
INSERT INTO `lc_error_log` VALUES ('54030', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-06-11 03:14:55', '2023-06-11 03:14:55');
INSERT INTO `lc_error_log` VALUES ('54031', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-06-11 03:27:16', '2023-06-11 03:27:16');
INSERT INTO `lc_error_log` VALUES ('54032', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.16', '2023-06-11 03:32:06', '2023-06-11 03:32:06');
INSERT INTO `lc_error_log` VALUES ('54033', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-06-11 04:44:15', '2023-06-11 04:44:15');
INSERT INTO `lc_error_log` VALUES ('54034', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.44', '2023-06-11 04:46:04', '2023-06-11 04:46:04');
INSERT INTO `lc_error_log` VALUES ('54035', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.198', '2023-06-11 07:32:04', '2023-06-11 07:32:04');
INSERT INTO `lc_error_log` VALUES ('54036', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.125.21.149', '2023-06-11 17:11:42', '2023-06-11 17:11:42');
INSERT INTO `lc_error_log` VALUES ('54037', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-11 19:42:02', '2023-06-11 19:42:02');
INSERT INTO `lc_error_log` VALUES ('54038', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-11 19:42:02', '2023-06-11 19:42:02');
INSERT INTO `lc_error_log` VALUES ('54039', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-11 22:51:10', '2023-06-11 22:51:10');
INSERT INTO `lc_error_log` VALUES ('54040', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-11 22:54:13', '2023-06-11 22:54:13');
INSERT INTO `lc_error_log` VALUES ('54041', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-11 22:55:54', '2023-06-11 22:55:54');
INSERT INTO `lc_error_log` VALUES ('54042', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-11 22:56:23', '2023-06-11 22:56:23');
INSERT INTO `lc_error_log` VALUES ('54043', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.199', '2023-06-11 23:13:15', '2023-06-11 23:13:15');
INSERT INTO `lc_error_log` VALUES ('54044', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.32.19', '2023-06-12 07:01:11', '2023-06-12 07:01:11');
INSERT INTO `lc_error_log` VALUES ('54045', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.156', '2023-06-12 07:01:13', '2023-06-12 07:01:13');
INSERT INTO `lc_error_log` VALUES ('54046', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '220.181.108.149', '2023-06-12 09:56:49', '2023-06-12 09:56:49');
INSERT INTO `lc_error_log` VALUES ('54047', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.24', '2023-06-12 09:56:51', '2023-06-12 09:56:51');
INSERT INTO `lc_error_log` VALUES ('54048', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.32.106', '2023-06-12 10:50:49', '2023-06-12 10:50:49');
INSERT INTO `lc_error_log` VALUES ('54049', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.161', '2023-06-12 10:50:50', '2023-06-12 10:50:50');
INSERT INTO `lc_error_log` VALUES ('54050', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.32.163', '2023-06-12 11:08:50', '2023-06-12 11:08:50');
INSERT INTO `lc_error_log` VALUES ('54051', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.39', '2023-06-12 11:08:55', '2023-06-12 11:08:55');
INSERT INTO `lc_error_log` VALUES ('54052', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-06-12 11:14:57', '2023-06-12 11:14:57');
INSERT INTO `lc_error_log` VALUES ('54053', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '220.181.108.92', '2023-06-12 11:52:05', '2023-06-12 11:52:05');
INSERT INTO `lc_error_log` VALUES ('54054', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '113.24.224.89', '2023-06-12 14:22:49', '2023-06-12 14:22:49');
INSERT INTO `lc_error_log` VALUES ('54055', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.78', '2023-06-12 14:22:50', '2023-06-12 14:22:50');
INSERT INTO `lc_error_log` VALUES ('54056', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '120.37.188.111', '2023-06-12 15:23:07', '2023-06-12 15:23:07');
INSERT INTO `lc_error_log` VALUES ('54057', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '120.37.188.111', '2023-06-12 15:23:14', '2023-06-12 15:23:14');
INSERT INTO `lc_error_log` VALUES ('54058', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-12 16:49:37', '2023-06-12 16:49:37');
INSERT INTO `lc_error_log` VALUES ('54059', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-12 17:00:19', '2023-06-12 17:00:19');
INSERT INTO `lc_error_log` VALUES ('54060', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-12 17:45:38', '2023-06-12 17:45:38');
INSERT INTO `lc_error_log` VALUES ('54061', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-12 18:05:50', '2023-06-12 18:05:50');
INSERT INTO `lc_error_log` VALUES ('54062', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-12 18:07:37', '2023-06-12 18:07:37');
INSERT INTO `lc_error_log` VALUES ('54063', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-12 19:27:12', '2023-06-12 19:27:12');
INSERT INTO `lc_error_log` VALUES ('54064', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-12 19:38:16', '2023-06-12 19:38:16');
INSERT INTO `lc_error_log` VALUES ('54065', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.94', '2023-06-12 20:23:35', '2023-06-12 20:23:35');
INSERT INTO `lc_error_log` VALUES ('54066', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.79', '2023-06-12 20:23:37', '2023-06-12 20:23:37');
INSERT INTO `lc_error_log` VALUES ('54067', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.103', '2023-06-12 20:51:16', '2023-06-12 20:51:16');
INSERT INTO `lc_error_log` VALUES ('54068', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.137', '2023-06-12 20:51:22', '2023-06-12 20:51:22');
INSERT INTO `lc_error_log` VALUES ('54069', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-12 21:00:46', '2023-06-12 21:00:46');
INSERT INTO `lc_error_log` VALUES ('54070', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.8', '2023-06-12 21:25:22', '2023-06-12 21:25:22');
INSERT INTO `lc_error_log` VALUES ('54071', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.25', '2023-06-12 21:26:49', '2023-06-12 21:26:49');
INSERT INTO `lc_error_log` VALUES ('54072', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.25', '2023-06-12 21:26:50', '2023-06-12 21:26:50');
INSERT INTO `lc_error_log` VALUES ('54073', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-12 21:26:52', '2023-06-12 21:26:52');
INSERT INTO `lc_error_log` VALUES ('54074', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-12 21:28:17', '2023-06-12 21:28:17');
INSERT INTO `lc_error_log` VALUES ('54075', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-12 21:40:11', '2023-06-12 21:40:11');
INSERT INTO `lc_error_log` VALUES ('54076', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-12 21:42:32', '2023-06-12 21:42:32');
INSERT INTO `lc_error_log` VALUES ('54077', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.32.213', '2023-06-12 22:14:29', '2023-06-12 22:14:29');
INSERT INTO `lc_error_log` VALUES ('54078', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.119', '2023-06-12 22:14:36', '2023-06-12 22:14:36');
INSERT INTO `lc_error_log` VALUES ('54079', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '220.181.108.166', '2023-06-12 22:42:14', '2023-06-12 22:42:14');
INSERT INTO `lc_error_log` VALUES ('54080', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.152', '2023-06-12 22:42:19', '2023-06-12 22:42:19');
INSERT INTO `lc_error_log` VALUES ('54081', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.94', '2023-06-12 23:37:43', '2023-06-12 23:37:43');
INSERT INTO `lc_error_log` VALUES ('54082', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '220.181.108.178', '2023-06-12 23:37:43', '2023-06-12 23:37:43');
INSERT INTO `lc_error_log` VALUES ('54083', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.158', '2023-06-12 23:37:44', '2023-06-12 23:37:44');
INSERT INTO `lc_error_log` VALUES ('54084', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.222', '2023-06-12 23:37:47', '2023-06-12 23:37:47');
INSERT INTO `lc_error_log` VALUES ('54085', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.151', '2023-06-12 23:37:48', '2023-06-12 23:37:48');
INSERT INTO `lc_error_log` VALUES ('54086', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.75', '2023-06-12 23:37:50', '2023-06-12 23:37:50');
INSERT INTO `lc_error_log` VALUES ('54087', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '220.181.108.165', '2023-06-12 23:41:03', '2023-06-12 23:41:03');
INSERT INTO `lc_error_log` VALUES ('54088', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.165', '2023-06-12 23:41:06', '2023-06-12 23:41:06');
INSERT INTO `lc_error_log` VALUES ('54089', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '124.90.57.57', '2023-06-13 00:02:37', '2023-06-13 00:02:37');
INSERT INTO `lc_error_log` VALUES ('54090', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.20', '2023-06-13 02:32:05', '2023-06-13 02:32:05');
INSERT INTO `lc_error_log` VALUES ('54091', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-06-13 03:17:30', '2023-06-13 03:17:30');
INSERT INTO `lc_error_log` VALUES ('54092', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-13 05:27:17', '2023-06-13 05:27:17');
INSERT INTO `lc_error_log` VALUES ('54093', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-13 05:27:21', '2023-06-13 05:27:21');
INSERT INTO `lc_error_log` VALUES ('54094', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-06-13 06:06:11', '2023-06-13 06:06:11');
INSERT INTO `lc_error_log` VALUES ('54095', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '114.96.82.196', '2023-06-13 09:09:36', '2023-06-13 09:09:36');
INSERT INTO `lc_error_log` VALUES ('54096', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-13 11:07:26', '2023-06-13 11:07:26');
INSERT INTO `lc_error_log` VALUES ('54097', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-13 11:07:27', '2023-06-13 11:07:27');
INSERT INTO `lc_error_log` VALUES ('54098', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-13 18:40:55', '2023-06-13 18:40:55');
INSERT INTO `lc_error_log` VALUES ('54099', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-13 18:52:16', '2023-06-13 18:52:16');
INSERT INTO `lc_error_log` VALUES ('54100', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请输入用户名', '0', '524', '/v1/login', '117.189.229.139', '2023-06-13 21:02:08', '2023-06-13 21:02:08');
INSERT INTO `lc_error_log` VALUES ('54101', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '108', '/v1/login', '117.189.229.139', '2023-06-13 21:02:43', '2023-06-13 21:02:43');
INSERT INTO `lc_error_log` VALUES ('54102', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '108', '/v1/login', '117.189.229.139', '2023-06-13 21:02:52', '2023-06-13 21:02:52');
INSERT INTO `lc_error_log` VALUES ('54103', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '108', '/v1/login', '117.189.229.139', '2023-06-13 21:03:02', '2023-06-13 21:03:02');
INSERT INTO `lc_error_log` VALUES ('54104', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '108', '/v1/login', '117.189.229.139', '2023-06-13 21:03:16', '2023-06-13 21:03:16');
INSERT INTO `lc_error_log` VALUES ('54105', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '108', '/v1/login', '117.189.229.139', '2023-06-13 21:03:25', '2023-06-13 21:03:25');
INSERT INTO `lc_error_log` VALUES ('54106', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '108', '/v1/login', '117.189.229.139', '2023-06-13 21:04:22', '2023-06-13 21:04:22');
INSERT INTO `lc_error_log` VALUES ('54107', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-13 21:34:17', '2023-06-13 21:34:17');
INSERT INTO `lc_error_log` VALUES ('54108', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-06-13 21:43:15', '2023-06-13 21:43:15');
INSERT INTO `lc_error_log` VALUES ('54109', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-13 21:43:15', '2023-06-13 21:43:15');
INSERT INTO `lc_error_log` VALUES ('54110', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-13 21:43:18', '2023-06-13 21:43:18');
INSERT INTO `lc_error_log` VALUES ('54111', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-13 21:44:12', '2023-06-13 21:44:12');
INSERT INTO `lc_error_log` VALUES ('54112', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-13 21:44:40', '2023-06-13 21:44:40');
INSERT INTO `lc_error_log` VALUES ('54113', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-13 21:45:24', '2023-06-13 21:45:24');
INSERT INTO `lc_error_log` VALUES ('54114', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-13 21:45:54', '2023-06-13 21:45:54');
INSERT INTO `lc_error_log` VALUES ('54115', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-06-13 21:46:50', '2023-06-13 21:46:50');
INSERT INTO `lc_error_log` VALUES ('54116', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-13 21:46:51', '2023-06-13 21:46:51');
INSERT INTO `lc_error_log` VALUES ('54117', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-13 21:51:21', '2023-06-13 21:51:21');
INSERT INTO `lc_error_log` VALUES ('54118', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-13 21:52:07', '2023-06-13 21:52:07');
INSERT INTO `lc_error_log` VALUES ('54119', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-13 21:52:27', '2023-06-13 21:52:27');
INSERT INTO `lc_error_log` VALUES ('54120', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-13 22:07:08', '2023-06-13 22:07:08');
INSERT INTO `lc_error_log` VALUES ('54121', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-13 22:19:23', '2023-06-13 22:19:23');
INSERT INTO `lc_error_log` VALUES ('54122', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.229.139', '2023-06-13 22:20:48', '2023-06-13 22:20:48');
INSERT INTO `lc_error_log` VALUES ('54123', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '180.163.220.68', '2023-06-13 23:14:29', '2023-06-13 23:14:29');
INSERT INTO `lc_error_log` VALUES ('54124', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.70', '2023-06-13 23:14:35', '2023-06-13 23:14:35');
INSERT INTO `lc_error_log` VALUES ('54125', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '118.123.105.90', '2023-06-14 02:00:00', '2023-06-14 02:00:00');
INSERT INTO `lc_error_log` VALUES ('54126', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.113', '2023-06-14 04:32:55', '2023-06-14 04:32:55');
INSERT INTO `lc_error_log` VALUES ('54127', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.119', '2023-06-14 04:32:56', '2023-06-14 04:32:56');
INSERT INTO `lc_error_log` VALUES ('54128', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '218.93.39.82', '2023-06-14 06:26:56', '2023-06-14 06:26:56');
INSERT INTO `lc_error_log` VALUES ('54129', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '121.4.134.241', '2023-06-14 09:19:45', '2023-06-14 09:19:45');
INSERT INTO `lc_error_log` VALUES ('54130', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/templets/system/channel_list.htm', '121.4.134.241', '2023-06-14 09:19:45', '2023-06-14 09:19:45');
INSERT INTO `lc_error_log` VALUES ('54131', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/comp/portalRouter', '121.4.134.241', '2023-06-14 09:19:46', '2023-06-14 09:19:46');
INSERT INTO `lc_error_log` VALUES ('54132', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/A/t/tpl/admin.html', '121.4.134.241', '2023-06-14 09:19:47', '2023-06-14 09:19:47');
INSERT INTO `lc_error_log` VALUES ('54133', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/min/?f=/res/js/login.js', '121.4.134.241', '2023-06-14 09:19:48', '2023-06-14 09:19:48');
INSERT INTO `lc_error_log` VALUES ('54134', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/template/web/', '121.4.134.241', '2023-06-14 09:19:50', '2023-06-14 09:19:50');
INSERT INTO `lc_error_log` VALUES ('54135', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/services/platform/validateCode.aspx', '121.4.134.241', '2023-06-14 09:19:50', '2023-06-14 09:19:50');
INSERT INTO `lc_error_log` VALUES ('54136', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '112.27.238.182', '2023-06-14 10:51:10', '2023-06-14 10:51:10');
INSERT INTO `lc_error_log` VALUES ('54137', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '112.27.238.182', '2023-06-14 10:51:49', '2023-06-14 10:51:49');
INSERT INTO `lc_error_log` VALUES ('54138', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '118.123.105.86', '2023-06-14 14:11:26', '2023-06-14 14:11:26');
INSERT INTO `lc_error_log` VALUES ('54139', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-14 15:54:08', '2023-06-14 15:54:08');
INSERT INTO `lc_error_log` VALUES ('54140', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-14 15:57:18', '2023-06-14 15:57:18');
INSERT INTO `lc_error_log` VALUES ('54141', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-14 15:59:20', '2023-06-14 15:59:20');
INSERT INTO `lc_error_log` VALUES ('54142', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-14 16:01:03', '2023-06-14 16:01:03');
INSERT INTO `lc_error_log` VALUES ('54143', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-14 21:58:54', '2023-06-14 21:58:54');
INSERT INTO `lc_error_log` VALUES ('54144', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-14 22:23:43', '2023-06-14 22:23:43');
INSERT INTO `lc_error_log` VALUES ('54145', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-14 22:26:27', '2023-06-14 22:26:27');
INSERT INTO `lc_error_log` VALUES ('54146', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-14 22:26:59', '2023-06-14 22:26:59');
INSERT INTO `lc_error_log` VALUES ('54147', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-14 22:28:08', '2023-06-14 22:28:08');
INSERT INTO `lc_error_log` VALUES ('54148', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/mp/getZiyuanInfo?ziyuan_id=27744', '27.115.124.6', '2023-06-14 22:30:43', '2023-06-14 22:30:43');
INSERT INTO `lc_error_log` VALUES ('54149', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '39.105.112.126', '2023-06-15 07:52:15', '2023-06-15 07:52:15');
INSERT INTO `lc_error_log` VALUES ('54150', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-15 09:25:30', '2023-06-15 09:25:30');
INSERT INTO `lc_error_log` VALUES ('54151', 'api', '/www/wwwroot/gouziyuan/app/api/business/LogRegister.php', '用户名或密码错误', '0', '113', '/v1/login', '220.171.231.42', '2023-06-15 09:32:42', '2023-06-15 09:32:42');
INSERT INTO `lc_error_log` VALUES ('54152', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.6.90', '2023-06-15 09:38:14', '2023-06-15 09:38:14');
INSERT INTO `lc_error_log` VALUES ('54153', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '175.0.123.121', '2023-06-15 09:38:37', '2023-06-15 09:38:37');
INSERT INTO `lc_error_log` VALUES ('54154', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '175.0.123.121', '2023-06-15 09:40:01', '2023-06-15 09:40:01');
INSERT INTO `lc_error_log` VALUES ('54155', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '175.0.123.121', '2023-06-15 09:40:06', '2023-06-15 09:40:06');
INSERT INTO `lc_error_log` VALUES ('54156', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '175.0.123.121', '2023-06-15 09:40:15', '2023-06-15 09:40:15');
INSERT INTO `lc_error_log` VALUES ('54157', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-15 10:09:14', '2023-06-15 10:09:14');
INSERT INTO `lc_error_log` VALUES ('54158', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-15 10:11:52', '2023-06-15 10:11:52');
INSERT INTO `lc_error_log` VALUES ('54159', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-15 10:12:42', '2023-06-15 10:12:42');
INSERT INTO `lc_error_log` VALUES ('54160', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-15 10:14:58', '2023-06-15 10:14:58');
INSERT INTO `lc_error_log` VALUES ('54161', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '125.44.70.141', '2023-06-15 11:12:11', '2023-06-15 11:12:11');
INSERT INTO `lc_error_log` VALUES ('54162', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '40.77.167.156', '2023-06-15 11:57:51', '2023-06-15 11:57:51');
INSERT INTO `lc_error_log` VALUES ('54163', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '39.105.185.179', '2023-06-15 12:27:53', '2023-06-15 12:27:53');
INSERT INTO `lc_error_log` VALUES ('54164', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-15 12:47:16', '2023-06-15 12:47:16');
INSERT INTO `lc_error_log` VALUES ('54165', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-15 12:47:19', '2023-06-15 12:47:19');
INSERT INTO `lc_error_log` VALUES ('54166', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '119.134.240.47', '2023-06-15 13:29:19', '2023-06-15 13:29:19');
INSERT INTO `lc_error_log` VALUES ('54167', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '125.84.110.3', '2023-06-15 13:37:09', '2023-06-15 13:37:09');
INSERT INTO `lc_error_log` VALUES ('54168', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.5', '2023-06-15 18:02:41', '2023-06-15 18:02:41');
INSERT INTO `lc_error_log` VALUES ('54169', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-06-15 18:04:24', '2023-06-15 18:04:24');
INSERT INTO `lc_error_log` VALUES ('54170', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-06-15 18:04:25', '2023-06-15 18:04:25');
INSERT INTO `lc_error_log` VALUES ('54171', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-15 19:30:10', '2023-06-15 19:30:10');
INSERT INTO `lc_error_log` VALUES ('54172', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-06-15 20:47:58', '2023-06-15 20:47:58');
INSERT INTO `lc_error_log` VALUES ('54173', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-06-15 20:47:59', '2023-06-15 20:47:59');
INSERT INTO `lc_error_log` VALUES ('54174', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '61.182.95.250', '2023-06-15 21:33:22', '2023-06-15 21:33:22');
INSERT INTO `lc_error_log` VALUES ('54175', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-15 21:47:02', '2023-06-15 21:47:02');
INSERT INTO `lc_error_log` VALUES ('54176', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-15 21:49:11', '2023-06-15 21:49:11');
INSERT INTO `lc_error_log` VALUES ('54177', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-15 23:52:56', '2023-06-15 23:52:56');
INSERT INTO `lc_error_log` VALUES ('54178', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-15 23:53:50', '2023-06-15 23:53:50');
INSERT INTO `lc_error_log` VALUES ('54179', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-15 23:55:12', '2023-06-15 23:55:12');
INSERT INTO `lc_error_log` VALUES ('54180', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-06-15 23:56:16', '2023-06-15 23:56:16');
INSERT INTO `lc_error_log` VALUES ('54181', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '182.126.220.126', '2023-06-16 10:20:44', '2023-06-16 10:20:44');
INSERT INTO `lc_error_log` VALUES ('54182', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-16 10:36:32', '2023-06-16 10:36:32');
INSERT INTO `lc_error_log` VALUES ('54183', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-16 10:36:32', '2023-06-16 10:36:32');
INSERT INTO `lc_error_log` VALUES ('54184', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '40.77.167.156', '2023-06-16 12:38:28', '2023-06-16 12:38:28');
INSERT INTO `lc_error_log` VALUES ('54185', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.69.3', '2023-06-16 12:55:26', '2023-06-16 12:55:26');
INSERT INTO `lc_error_log` VALUES ('54186', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.69.5', '2023-06-16 13:15:59', '2023-06-16 13:15:59');
INSERT INTO `lc_error_log` VALUES ('54187', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-16 18:29:15', '2023-06-16 18:29:15');
INSERT INTO `lc_error_log` VALUES ('54188', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.25', '2023-06-16 18:52:12', '2023-06-16 18:52:12');
INSERT INTO `lc_error_log` VALUES ('54189', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-16 19:54:11', '2023-06-16 19:54:11');
INSERT INTO `lc_error_log` VALUES ('54190', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-16 19:58:47', '2023-06-16 19:58:47');
INSERT INTO `lc_error_log` VALUES ('54191', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-16 19:59:39', '2023-06-16 19:59:39');
INSERT INTO `lc_error_log` VALUES ('54192', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-16 19:59:45', '2023-06-16 19:59:45');
INSERT INTO `lc_error_log` VALUES ('54193', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-16 20:04:24', '2023-06-16 20:04:24');
INSERT INTO `lc_error_log` VALUES ('54194', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-06-17 00:20:35', '2023-06-17 00:20:35');
INSERT INTO `lc_error_log` VALUES ('54195', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-06-17 00:21:33', '2023-06-17 00:21:33');
INSERT INTO `lc_error_log` VALUES ('54196', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-17 00:21:48', '2023-06-17 00:21:48');
INSERT INTO `lc_error_log` VALUES ('54197', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-17 00:22:09', '2023-06-17 00:22:09');
INSERT INTO `lc_error_log` VALUES ('54198', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.150', '2023-06-17 01:51:30', '2023-06-17 01:51:30');
INSERT INTO `lc_error_log` VALUES ('54199', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '211.95.50.8', '2023-06-17 01:52:18', '2023-06-17 01:52:18');
INSERT INTO `lc_error_log` VALUES ('54200', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '211.95.50.8', '2023-06-17 01:52:27', '2023-06-17 01:52:27');
INSERT INTO `lc_error_log` VALUES ('54201', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-06-17 01:52:38', '2023-06-17 01:52:38');
INSERT INTO `lc_error_log` VALUES ('54202', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-06-17 01:53:13', '2023-06-17 01:53:13');
INSERT INTO `lc_error_log` VALUES ('54203', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.61.238', '2023-06-17 04:43:59', '2023-06-17 04:43:59');
INSERT INTO `lc_error_log` VALUES ('54204', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:42', '2023-06-17 10:30:42');
INSERT INTO `lc_error_log` VALUES ('54205', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:43', '2023-06-17 10:30:43');
INSERT INTO `lc_error_log` VALUES ('54206', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:43', '2023-06-17 10:30:43');
INSERT INTO `lc_error_log` VALUES ('54207', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:43', '2023-06-17 10:30:43');
INSERT INTO `lc_error_log` VALUES ('54208', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '123.57.235.51', '2023-06-17 10:30:43', '2023-06-17 10:30:43');
INSERT INTO `lc_error_log` VALUES ('54209', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:43', '2023-06-17 10:30:43');
INSERT INTO `lc_error_log` VALUES ('54210', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '123.57.235.51', '2023-06-17 10:30:44', '2023-06-17 10:30:44');
INSERT INTO `lc_error_log` VALUES ('54211', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:44', '2023-06-17 10:30:44');
INSERT INTO `lc_error_log` VALUES ('54212', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/index.php', '123.57.235.51', '2023-06-17 10:30:47', '2023-06-17 10:30:47');
INSERT INTO `lc_error_log` VALUES ('54213', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:54', '2023-06-17 10:30:54');
INSERT INTO `lc_error_log` VALUES ('54214', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.57.235.51', '2023-06-17 10:30:54', '2023-06-17 10:30:54');
INSERT INTO `lc_error_log` VALUES ('54215', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '140.246.72.81', '2023-06-17 10:56:57', '2023-06-17 10:56:57');
INSERT INTO `lc_error_log` VALUES ('54216', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-17 16:32:03', '2023-06-17 16:32:03');
INSERT INTO `lc_error_log` VALUES ('54217', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.28', '2023-06-17 21:40:47', '2023-06-17 21:40:47');
INSERT INTO `lc_error_log` VALUES ('54218', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.73.200.203', '2023-06-17 22:19:22', '2023-06-17 22:19:22');
INSERT INTO `lc_error_log` VALUES ('54219', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.73.200.203', '2023-06-17 22:20:48', '2023-06-17 22:20:48');
INSERT INTO `lc_error_log` VALUES ('54220', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.73.200.203', '2023-06-17 22:23:15', '2023-06-17 22:23:15');
INSERT INTO `lc_error_log` VALUES ('54221', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.73.200.203', '2023-06-17 22:24:53', '2023-06-17 22:24:53');
INSERT INTO `lc_error_log` VALUES ('54222', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-17 23:19:08', '2023-06-17 23:19:08');
INSERT INTO `lc_error_log` VALUES ('54223', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-17 23:22:17', '2023-06-17 23:22:17');
INSERT INTO `lc_error_log` VALUES ('54224', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-17 23:25:40', '2023-06-17 23:25:40');
INSERT INTO `lc_error_log` VALUES ('54225', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-18 00:00:56', '2023-06-18 00:00:56');
INSERT INTO `lc_error_log` VALUES ('54226', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.6.90', '2023-06-18 07:17:56', '2023-06-18 07:17:56');
INSERT INTO `lc_error_log` VALUES ('54227', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-18 08:54:56', '2023-06-18 08:54:56');
INSERT INTO `lc_error_log` VALUES ('54228', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-18 08:54:59', '2023-06-18 08:54:59');
INSERT INTO `lc_error_log` VALUES ('54229', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.116.68.249', '2023-06-18 14:15:49', '2023-06-18 14:15:49');
INSERT INTO `lc_error_log` VALUES ('54230', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '207.46.13.218', '2023-06-18 15:53:48', '2023-06-18 15:53:48');
INSERT INTO `lc_error_log` VALUES ('54231', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-18 16:16:40', '2023-06-18 16:16:40');
INSERT INTO `lc_error_log` VALUES ('54232', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-18 16:19:51', '2023-06-18 16:19:51');
INSERT INTO `lc_error_log` VALUES ('54233', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-18 16:24:19', '2023-06-18 16:24:19');
INSERT INTO `lc_error_log` VALUES ('54234', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-18 17:31:44', '2023-06-18 17:31:44');
INSERT INTO `lc_error_log` VALUES ('54235', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-18 23:40:49', '2023-06-18 23:40:49');
INSERT INTO `lc_error_log` VALUES ('54236', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.109', '2023-06-18 23:51:20', '2023-06-18 23:51:20');
INSERT INTO `lc_error_log` VALUES ('54237', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.78', '2023-06-18 23:51:30', '2023-06-18 23:51:30');
INSERT INTO `lc_error_log` VALUES ('54238', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-18 23:52:27', '2023-06-18 23:52:27');
INSERT INTO `lc_error_log` VALUES ('54239', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=27369&info=true', '116.179.32.177', '2023-06-19 03:07:24', '2023-06-19 03:07:24');
INSERT INTO `lc_error_log` VALUES ('54240', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=27369&info=true', '116.179.37.49', '2023-06-19 03:07:27', '2023-06-19 03:07:27');
INSERT INTO `lc_error_log` VALUES ('54241', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '121.5.252.171', '2023-06-19 03:15:14', '2023-06-19 03:15:14');
INSERT INTO `lc_error_log` VALUES ('54242', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/templets/system/channel_list.htm', '121.5.252.171', '2023-06-19 03:15:14', '2023-06-19 03:15:14');
INSERT INTO `lc_error_log` VALUES ('54243', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/comp/portalRouter', '121.5.252.171', '2023-06-19 03:15:15', '2023-06-19 03:15:15');
INSERT INTO `lc_error_log` VALUES ('54244', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/A/t/tpl/admin.html', '121.5.252.171', '2023-06-19 03:15:16', '2023-06-19 03:15:16');
INSERT INTO `lc_error_log` VALUES ('54245', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/min/?f=/res/js/login.js', '121.5.252.171', '2023-06-19 03:15:17', '2023-06-19 03:15:17');
INSERT INTO `lc_error_log` VALUES ('54246', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/template/web/', '121.5.252.171', '2023-06-19 03:15:19', '2023-06-19 03:15:19');
INSERT INTO `lc_error_log` VALUES ('54247', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/services/platform/validateCode.aspx', '121.5.252.171', '2023-06-19 03:15:19', '2023-06-19 03:15:19');
INSERT INTO `lc_error_log` VALUES ('54248', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.25', '2023-06-19 04:43:35', '2023-06-19 04:43:35');
INSERT INTO `lc_error_log` VALUES ('54249', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-06-19 04:49:34', '2023-06-19 04:49:34');
INSERT INTO `lc_error_log` VALUES ('54250', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-06-19 04:49:34', '2023-06-19 04:49:34');
INSERT INTO `lc_error_log` VALUES ('54251', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-06-19 08:43:14', '2023-06-19 08:43:14');
INSERT INTO `lc_error_log` VALUES ('54252', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-19 09:05:38', '2023-06-19 09:05:38');
INSERT INTO `lc_error_log` VALUES ('54253', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-06-19 11:12:28', '2023-06-19 11:12:28');
INSERT INTO `lc_error_log` VALUES ('54254', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '66.249.79.5', '2023-06-19 12:54:50', '2023-06-19 12:54:50');
INSERT INTO `lc_error_log` VALUES ('54255', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '66.249.79.7', '2023-06-19 13:01:31', '2023-06-19 13:01:31');
INSERT INTO `lc_error_log` VALUES ('54256', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-19 15:42:52', '2023-06-19 15:42:52');
INSERT INTO `lc_error_log` VALUES ('54257', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '222.174.144.10', '2023-06-19 16:54:27', '2023-06-19 16:54:27');
INSERT INTO `lc_error_log` VALUES ('54258', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '39.103.188.154', '2023-06-19 18:41:43', '2023-06-19 18:41:43');
INSERT INTO `lc_error_log` VALUES ('54259', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '218.73.240.8', '2023-06-19 20:58:01', '2023-06-19 20:58:01');
INSERT INTO `lc_error_log` VALUES ('54260', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '218.73.240.8', '2023-06-19 20:59:06', '2023-06-19 20:59:06');
INSERT INTO `lc_error_log` VALUES ('54261', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '218.73.240.8', '2023-06-19 20:59:18', '2023-06-19 20:59:18');
INSERT INTO `lc_error_log` VALUES ('54262', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '218.73.240.8', '2023-06-19 20:59:28', '2023-06-19 20:59:28');
INSERT INTO `lc_error_log` VALUES ('54263', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '218.73.240.8', '2023-06-19 20:59:34', '2023-06-19 20:59:34');
INSERT INTO `lc_error_log` VALUES ('54264', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-19 21:03:29', '2023-06-19 21:03:29');
INSERT INTO `lc_error_log` VALUES ('54265', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-06-19 21:03:37', '2023-06-19 21:03:37');
INSERT INTO `lc_error_log` VALUES ('54266', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-19 21:04:38', '2023-06-19 21:04:38');
INSERT INTO `lc_error_log` VALUES ('54267', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '115.238.44.237', '2023-06-19 21:17:07', '2023-06-19 21:17:07');
INSERT INTO `lc_error_log` VALUES ('54268', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-19 22:23:44', '2023-06-19 22:23:44');
INSERT INTO `lc_error_log` VALUES ('54269', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-19 22:30:30', '2023-06-19 22:30:30');
INSERT INTO `lc_error_log` VALUES ('54270', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-06-19 22:31:51', '2023-06-19 22:31:51');
INSERT INTO `lc_error_log` VALUES ('54271', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.151', '2023-06-20 09:32:34', '2023-06-20 09:32:34');
INSERT INTO `lc_error_log` VALUES ('54272', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:29', '2023-06-20 10:18:29');
INSERT INTO `lc_error_log` VALUES ('54273', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/index.php', '47.101.149.21', '2023-06-20 10:18:29', '2023-06-20 10:18:29');
INSERT INTO `lc_error_log` VALUES ('54274', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:29', '2023-06-20 10:18:29');
INSERT INTO `lc_error_log` VALUES ('54275', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '47.101.149.21', '2023-06-20 10:18:29', '2023-06-20 10:18:29');
INSERT INTO `lc_error_log` VALUES ('54276', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:29', '2023-06-20 10:18:29');
INSERT INTO `lc_error_log` VALUES ('54277', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:29', '2023-06-20 10:18:29');
INSERT INTO `lc_error_log` VALUES ('54278', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:30', '2023-06-20 10:18:30');
INSERT INTO `lc_error_log` VALUES ('54279', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '47.101.149.21', '2023-06-20 10:18:30', '2023-06-20 10:18:30');
INSERT INTO `lc_error_log` VALUES ('54280', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:30', '2023-06-20 10:18:30');
INSERT INTO `lc_error_log` VALUES ('54281', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:30', '2023-06-20 10:18:30');
INSERT INTO `lc_error_log` VALUES ('54282', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:40', '2023-06-20 10:18:40');
INSERT INTO `lc_error_log` VALUES ('54283', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.149.21', '2023-06-20 10:18:41', '2023-06-20 10:18:41');
INSERT INTO `lc_error_log` VALUES ('54284', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.206.83.231', '2023-06-20 10:32:38', '2023-06-20 10:32:38');
INSERT INTO `lc_error_log` VALUES ('54285', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '218.73.240.8', '2023-06-20 10:33:24', '2023-06-20 10:33:24');
INSERT INTO `lc_error_log` VALUES ('54286', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '1.206.83.231', '2023-06-20 10:33:30', '2023-06-20 10:33:30');
INSERT INTO `lc_error_log` VALUES ('54287', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '1.206.83.231', '2023-06-20 10:33:39', '2023-06-20 10:33:39');
INSERT INTO `lc_error_log` VALUES ('54288', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '1.206.83.231', '2023-06-20 10:33:47', '2023-06-20 10:33:47');
INSERT INTO `lc_error_log` VALUES ('54289', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '1.206.83.231', '2023-06-20 10:33:53', '2023-06-20 10:33:53');
INSERT INTO `lc_error_log` VALUES ('54290', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-20 11:40:26', '2023-06-20 11:40:26');
INSERT INTO `lc_error_log` VALUES ('54291', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-20 11:41:49', '2023-06-20 11:41:49');
INSERT INTO `lc_error_log` VALUES ('54292', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-06-20 11:43:55', '2023-06-20 11:43:55');
INSERT INTO `lc_error_log` VALUES ('54293', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-20 11:53:29', '2023-06-20 11:53:29');
INSERT INTO `lc_error_log` VALUES ('54294', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-20 16:53:13', '2023-06-20 16:53:13');
INSERT INTO `lc_error_log` VALUES ('54295', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '40.77.167.201', '2023-06-20 18:30:22', '2023-06-20 18:30:22');
INSERT INTO `lc_error_log` VALUES ('54296', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '112.13.112.175', '2023-06-20 19:33:20', '2023-06-20 19:33:20');
INSERT INTO `lc_error_log` VALUES ('54297', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '223.111.175.111', '2023-06-20 20:57:45', '2023-06-20 20:57:45');
INSERT INTO `lc_error_log` VALUES ('54298', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.101.67', '2023-06-20 21:09:31', '2023-06-20 21:09:31');
INSERT INTO `lc_error_log` VALUES ('54299', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-20 22:50:22', '2023-06-20 22:50:22');
INSERT INTO `lc_error_log` VALUES ('54300', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-20 22:55:08', '2023-06-20 22:55:08');
INSERT INTO `lc_error_log` VALUES ('54301', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '66.249.79.6', '2023-06-20 22:59:42', '2023-06-20 22:59:42');
INSERT INTO `lc_error_log` VALUES ('54302', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.147.108', '2023-06-21 02:05:09', '2023-06-21 02:05:09');
INSERT INTO `lc_error_log` VALUES ('54303', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.192.79.197', '2023-06-21 02:14:53', '2023-06-21 02:14:53');
INSERT INTO `lc_error_log` VALUES ('54304', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '112.36.251.133', '2023-06-21 08:51:36', '2023-06-21 08:51:36');
INSERT INTO `lc_error_log` VALUES ('54305', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.206.83.231', '2023-06-21 10:17:39', '2023-06-21 10:17:39');
INSERT INTO `lc_error_log` VALUES ('54306', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '1.206.83.231', '2023-06-21 10:20:02', '2023-06-21 10:20:02');
INSERT INTO `lc_error_log` VALUES ('54307', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '1.206.83.231', '2023-06-21 10:23:13', '2023-06-21 10:23:13');
INSERT INTO `lc_error_log` VALUES ('54308', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-21 10:47:11', '2023-06-21 10:47:11');
INSERT INTO `lc_error_log` VALUES ('54309', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '42.180.2.94', '2023-06-21 11:01:02', '2023-06-21 11:01:02');
INSERT INTO `lc_error_log` VALUES ('54310', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-21 12:49:36', '2023-06-21 12:49:36');
INSERT INTO `lc_error_log` VALUES ('54311', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-06-21 13:11:19', '2023-06-21 13:11:19');
INSERT INTO `lc_error_log` VALUES ('54312', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '112.8.223.21', '2023-06-21 13:12:00', '2023-06-21 13:12:00');
INSERT INTO `lc_error_log` VALUES ('54313', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-21 15:29:29', '2023-06-21 15:29:29');
INSERT INTO `lc_error_log` VALUES ('54314', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '39.171.148.234', '2023-06-21 15:59:31', '2023-06-21 15:59:31');
INSERT INTO `lc_error_log` VALUES ('54315', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-21 16:10:24', '2023-06-21 16:10:24');
INSERT INTO `lc_error_log` VALUES ('54316', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-21 16:10:27', '2023-06-21 16:10:27');
INSERT INTO `lc_error_log` VALUES ('54317', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=1&order_id=&chapters_id=&directory_id=', '116.179.37.178', '2023-06-21 17:21:06', '2023-06-21 17:21:06');
INSERT INTO `lc_error_log` VALUES ('54318', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.24', '2023-06-21 17:55:11', '2023-06-21 17:55:11');
INSERT INTO `lc_error_log` VALUES ('54319', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.18', '2023-06-21 20:17:16', '2023-06-21 20:17:16');
INSERT INTO `lc_error_log` VALUES ('54320', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-21 20:51:47', '2023-06-21 20:51:47');
INSERT INTO `lc_error_log` VALUES ('54321', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-21 21:05:18', '2023-06-21 21:05:18');
INSERT INTO `lc_error_log` VALUES ('54322', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-21 21:10:03', '2023-06-21 21:10:03');
INSERT INTO `lc_error_log` VALUES ('54323', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-21 21:56:21', '2023-06-21 21:56:21');
INSERT INTO `lc_error_log` VALUES ('54324', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.19', '2023-06-21 21:59:54', '2023-06-21 21:59:54');
INSERT INTO `lc_error_log` VALUES ('54325', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.19', '2023-06-21 21:59:58', '2023-06-21 21:59:58');
INSERT INTO `lc_error_log` VALUES ('54326', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '39.171.148.234', '2023-06-21 22:06:39', '2023-06-21 22:06:39');
INSERT INTO `lc_error_log` VALUES ('54327', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-21 23:02:09', '2023-06-21 23:02:09');
INSERT INTO `lc_error_log` VALUES ('54328', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-21 23:05:51', '2023-06-21 23:05:51');
INSERT INTO `lc_error_log` VALUES ('54329', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-21 23:14:54', '2023-06-21 23:14:54');
INSERT INTO `lc_error_log` VALUES ('54330', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=1&order_id=&chapters_id=&directory_id=', '116.179.37.109', '2023-06-21 23:21:06', '2023-06-21 23:21:06');
INSERT INTO `lc_error_log` VALUES ('54331', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=1&order_id=&chapters_id=&directory_id=', '116.179.37.202', '2023-06-22 02:21:06', '2023-06-22 02:21:06');
INSERT INTO `lc_error_log` VALUES ('54332', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=1&order_id=&chapters_id=&directory_id=', '116.179.37.145', '2023-06-22 02:21:08', '2023-06-22 02:21:08');
INSERT INTO `lc_error_log` VALUES ('54333', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.23', '2023-06-22 06:28:55', '2023-06-22 06:28:55');
INSERT INTO `lc_error_log` VALUES ('54334', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '请输入关键词', '0', '465', '/v1/search?keyWord=&category_id=23&page=1&pageSize=20', '112.97.201.169', '2023-06-22 07:40:51', '2023-06-22 07:40:51');
INSERT INTO `lc_error_log` VALUES ('54335', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '请输入关键词', '0', '465', '/v1/search?keyWord=&category_id=23&page=undefined&pageSize=undefined', '112.97.201.169', '2023-06-22 07:40:53', '2023-06-22 07:40:53');
INSERT INTO `lc_error_log` VALUES ('54336', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=1&order_id=&chapters_id=&directory_id=', '116.179.37.20', '2023-06-22 08:21:06', '2023-06-22 08:21:06');
INSERT INTO `lc_error_log` VALUES ('54337', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '36.99.62.46', '2023-06-22 12:37:32', '2023-06-22 12:37:32');
INSERT INTO `lc_error_log` VALUES ('54338', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-22 13:25:40', '2023-06-22 13:25:40');
INSERT INTO `lc_error_log` VALUES ('54339', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.25', '2023-06-22 18:28:34', '2023-06-22 18:28:34');
INSERT INTO `lc_error_log` VALUES ('54340', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '207.46.13.208', '2023-06-22 20:51:07', '2023-06-22 20:51:07');
INSERT INTO `lc_error_log` VALUES ('54341', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-22 21:04:45', '2023-06-22 21:04:45');
INSERT INTO `lc_error_log` VALUES ('54342', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '不能购买自己的资源', '0', '48', '/v1/submitOrder', '117.189.225.244', '2023-06-22 23:06:36', '2023-06-22 23:06:36');
INSERT INTO `lc_error_log` VALUES ('54343', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '113.245.27.87', '2023-06-22 23:13:43', '2023-06-22 23:13:43');
INSERT INTO `lc_error_log` VALUES ('54344', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-06-22 23:21:20', '2023-06-22 23:21:20');
INSERT INTO `lc_error_log` VALUES ('54345', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-22 23:22:56', '2023-06-22 23:22:56');
INSERT INTO `lc_error_log` VALUES ('54346', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-22 23:23:06', '2023-06-22 23:23:06');
INSERT INTO `lc_error_log` VALUES ('54347', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.147', '2023-06-22 23:48:57', '2023-06-22 23:48:57');
INSERT INTO `lc_error_log` VALUES ('54348', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-22 23:58:46', '2023-06-22 23:58:46');
INSERT INTO `lc_error_log` VALUES ('54349', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-23 00:01:40', '2023-06-23 00:01:40');
INSERT INTO `lc_error_log` VALUES ('54350', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '58.59.90.50', '2023-06-23 01:29:54', '2023-06-23 01:29:54');
INSERT INTO `lc_error_log` VALUES ('54351', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.177', '2023-06-23 02:11:23', '2023-06-23 02:11:23');
INSERT INTO `lc_error_log` VALUES ('54352', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-06-23 02:11:54', '2023-06-23 02:11:54');
INSERT INTO `lc_error_log` VALUES ('54353', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.41.161.209', '2023-06-23 02:13:41', '2023-06-23 02:13:41');
INSERT INTO `lc_error_log` VALUES ('54354', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.41.161.209', '2023-06-23 02:14:03', '2023-06-23 02:14:03');
INSERT INTO `lc_error_log` VALUES ('54355', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.30', '2023-06-23 14:59:39', '2023-06-23 14:59:39');
INSERT INTO `lc_error_log` VALUES ('54356', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.5', '2023-06-23 17:20:07', '2023-06-23 17:20:07');
INSERT INTO `lc_error_log` VALUES ('54357', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.16', '2023-06-23 17:21:05', '2023-06-23 17:21:05');
INSERT INTO `lc_error_log` VALUES ('54358', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.16', '2023-06-23 17:21:10', '2023-06-23 17:21:10');
INSERT INTO `lc_error_log` VALUES ('54359', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-23 20:16:54', '2023-06-23 20:16:54');
INSERT INTO `lc_error_log` VALUES ('54360', 'platform', '/www/wwwroot/gouziyuan/app/platform/service/AdminUser.php', '用户名或密码错误', '0', '43', '//login', '101.43.98.47', '2023-06-23 20:17:09', '2023-06-23 20:17:09');
INSERT INTO `lc_error_log` VALUES ('54361', 'platform', '/www/wwwroot/gouziyuan/app/platform/service/AdminUser.php', '用户名或密码错误', '0', '43', '//login', '101.43.98.47', '2023-06-23 20:17:17', '2023-06-23 20:17:17');
INSERT INTO `lc_error_log` VALUES ('54362', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-23 20:23:02', '2023-06-23 20:23:02');
INSERT INTO `lc_error_log` VALUES ('54363', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-23 20:23:44', '2023-06-23 20:23:44');
INSERT INTO `lc_error_log` VALUES ('54364', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-23 20:26:12', '2023-06-23 20:26:12');
INSERT INTO `lc_error_log` VALUES ('54365', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-23 20:29:56', '2023-06-23 20:29:56');
INSERT INTO `lc_error_log` VALUES ('54366', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-06-23 20:31:52', '2023-06-23 20:31:52');
INSERT INTO `lc_error_log` VALUES ('54367', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-06-23 20:33:28', '2023-06-23 20:33:28');
INSERT INTO `lc_error_log` VALUES ('54368', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-06-23 20:33:29', '2023-06-23 20:33:29');
INSERT INTO `lc_error_log` VALUES ('54369', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-06-23 20:33:39', '2023-06-23 20:33:39');
INSERT INTO `lc_error_log` VALUES ('54370', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.187.173.45', '2023-06-23 22:42:30', '2023-06-23 22:42:30');
INSERT INTO `lc_error_log` VALUES ('54371', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-23 23:06:36', '2023-06-23 23:06:36');
INSERT INTO `lc_error_log` VALUES ('54372', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.2.214', '2023-06-23 23:11:46', '2023-06-23 23:11:46');
INSERT INTO `lc_error_log` VALUES ('54373', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-23 23:22:13', '2023-06-23 23:22:13');
INSERT INTO `lc_error_log` VALUES ('54374', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.54.63.40', '2023-06-24 01:38:51', '2023-06-24 01:38:51');
INSERT INTO `lc_error_log` VALUES ('54375', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.164.14', '2023-06-24 02:04:12', '2023-06-24 02:04:12');
INSERT INTO `lc_error_log` VALUES ('54376', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/scrape/kube-system', '124.221.253.185', '2023-06-24 05:05:56', '2023-06-24 05:05:56');
INSERT INTO `lc_error_log` VALUES ('54377', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '不能购买自己的资源', '0', '48', '/v1/submitOrder', '220.171.228.254', '2023-06-24 10:35:20', '2023-06-24 10:35:20');
INSERT INTO `lc_error_log` VALUES ('54378', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '不能购买自己的资源', '0', '48', '/v1/submitOrder', '220.171.228.254', '2023-06-24 11:02:23', '2023-06-24 11:02:23');
INSERT INTO `lc_error_log` VALUES ('54379', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '220.171.228.254', '2023-06-24 11:02:56', '2023-06-24 11:02:56');
INSERT INTO `lc_error_log` VALUES ('54380', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '不能购买自己的资源', '0', '48', '/v1/submitOrder', '220.171.228.254', '2023-06-24 11:11:25', '2023-06-24 11:11:25');
INSERT INTO `lc_error_log` VALUES ('54381', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '183.158.243.48', '2023-06-24 11:29:20', '2023-06-24 11:29:20');
INSERT INTO `lc_error_log` VALUES ('54382', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '183.158.243.48', '2023-06-24 11:29:20', '2023-06-24 11:29:20');
INSERT INTO `lc_error_log` VALUES ('54383', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-06-24 13:25:03', '2023-06-24 13:25:03');
INSERT INTO `lc_error_log` VALUES ('54384', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.100.30', '2023-06-24 13:25:03', '2023-06-24 13:25:03');
INSERT INTO `lc_error_log` VALUES ('54385', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-24 13:55:13', '2023-06-24 13:55:13');
INSERT INTO `lc_error_log` VALUES ('54386', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-24 13:56:27', '2023-06-24 13:56:27');
INSERT INTO `lc_error_log` VALUES ('54387', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-24 13:57:19', '2023-06-24 13:57:19');
INSERT INTO `lc_error_log` VALUES ('54388', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.21', '2023-06-24 14:01:12', '2023-06-24 14:01:12');
INSERT INTO `lc_error_log` VALUES ('54389', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.24', '2023-06-24 16:19:08', '2023-06-24 16:19:08');
INSERT INTO `lc_error_log` VALUES ('54390', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.24', '2023-06-24 16:19:09', '2023-06-24 16:19:09');
INSERT INTO `lc_error_log` VALUES ('54391', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '119.39.140.9', '2023-06-24 16:29:14', '2023-06-24 16:29:14');
INSERT INTO `lc_error_log` VALUES ('54392', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.207.132.109', '2023-06-24 20:54:30', '2023-06-24 20:54:30');
INSERT INTO `lc_error_log` VALUES ('54393', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.207.132.109', '2023-06-24 20:55:12', '2023-06-24 20:55:12');
INSERT INTO `lc_error_log` VALUES ('54394', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.207.132.109', '2023-06-24 20:56:46', '2023-06-24 20:56:46');
INSERT INTO `lc_error_log` VALUES ('54395', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-24 21:32:59', '2023-06-24 21:32:59');
INSERT INTO `lc_error_log` VALUES ('54396', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-24 21:34:18', '2023-06-24 21:34:18');
INSERT INTO `lc_error_log` VALUES ('54397', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.53', '2023-06-24 21:38:35', '2023-06-24 21:38:35');
INSERT INTO `lc_error_log` VALUES ('54398', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.101', '2023-06-24 21:38:39', '2023-06-24 21:38:39');
INSERT INTO `lc_error_log` VALUES ('54399', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-24 23:17:49', '2023-06-24 23:17:49');
INSERT INTO `lc_error_log` VALUES ('54400', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-06-24 23:31:23', '2023-06-24 23:31:23');
INSERT INTO `lc_error_log` VALUES ('54401', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-25 08:04:05', '2023-06-25 08:04:05');
INSERT INTO `lc_error_log` VALUES ('54402', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-25 08:04:09', '2023-06-25 08:04:09');
INSERT INTO `lc_error_log` VALUES ('54403', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.5', '2023-06-25 18:50:42', '2023-06-25 18:50:42');
INSERT INTO `lc_error_log` VALUES ('54404', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-06-25 18:54:27', '2023-06-25 18:54:27');
INSERT INTO `lc_error_log` VALUES ('54405', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-06-25 18:54:29', '2023-06-25 18:54:29');
INSERT INTO `lc_error_log` VALUES ('54406', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.19', '2023-06-25 20:51:38', '2023-06-25 20:51:38');
INSERT INTO `lc_error_log` VALUES ('54407', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '118.125.192.169', '2023-06-25 22:26:44', '2023-06-25 22:26:44');
INSERT INTO `lc_error_log` VALUES ('54408', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请输入用户名', '0', '524', '/v1/login', '118.125.192.169', '2023-06-25 22:26:50', '2023-06-25 22:26:50');
INSERT INTO `lc_error_log` VALUES ('54409', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '118.125.192.169', '2023-06-25 22:27:50', '2023-06-25 22:27:50');
INSERT INTO `lc_error_log` VALUES ('54410', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\Templets', '0', '76', '/templets/system/channel_list.htm', '121.5.252.171', '2023-06-26 03:57:49', '2023-06-26 03:57:49');
INSERT INTO `lc_error_log` VALUES ('54411', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\Comp', '0', '76', '/comp/portalRouter', '121.5.252.171', '2023-06-26 03:57:50', '2023-06-26 03:57:50');
INSERT INTO `lc_error_log` VALUES ('54412', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\A', '0', '76', '/A/t/tpl/admin.html', '121.5.252.171', '2023-06-26 03:57:51', '2023-06-26 03:57:51');
INSERT INTO `lc_error_log` VALUES ('54413', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\Min', '0', '76', '/min/?f=/res/js/login.js', '121.5.252.171', '2023-06-26 03:57:51', '2023-06-26 03:57:51');
INSERT INTO `lc_error_log` VALUES ('54414', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\Template', '0', '76', '/template/web/', '121.5.252.171', '2023-06-26 03:57:54', '2023-06-26 03:57:54');
INSERT INTO `lc_error_log` VALUES ('54415', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\Services', '0', '76', '/services/platform/validateCode.aspx', '121.5.252.171', '2023-06-26 03:57:54', '2023-06-26 03:57:54');
INSERT INTO `lc_error_log` VALUES ('54416', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '113.125.46.37', '2023-06-26 04:11:16', '2023-06-26 04:11:16');
INSERT INTO `lc_error_log` VALUES ('54417', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.32.72', '2023-06-26 05:34:57', '2023-06-26 05:34:57');
INSERT INTO `lc_error_log` VALUES ('54418', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.32.230', '2023-06-26 05:34:57', '2023-06-26 05:34:57');
INSERT INTO `lc_error_log` VALUES ('54419', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.32.218', '2023-06-26 05:34:57', '2023-06-26 05:34:57');
INSERT INTO `lc_error_log` VALUES ('54420', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.229', '2023-06-26 05:34:58', '2023-06-26 05:34:58');
INSERT INTO `lc_error_log` VALUES ('54421', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.107', '2023-06-26 05:34:58', '2023-06-26 05:34:58');
INSERT INTO `lc_error_log` VALUES ('54422', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.239', '2023-06-26 05:34:58', '2023-06-26 05:34:58');
INSERT INTO `lc_error_log` VALUES ('54423', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.161', '2023-06-26 05:34:59', '2023-06-26 05:34:59');
INSERT INTO `lc_error_log` VALUES ('54424', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.92', '2023-06-26 05:35:01', '2023-06-26 05:35:01');
INSERT INTO `lc_error_log` VALUES ('54425', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '220.181.108.80', '2023-06-26 05:38:17', '2023-06-26 05:38:17');
INSERT INTO `lc_error_log` VALUES ('54426', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.246', '2023-06-26 05:38:20', '2023-06-26 05:38:20');
INSERT INTO `lc_error_log` VALUES ('54427', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.9', '2023-06-26 08:50:56', '2023-06-26 08:50:56');
INSERT INTO `lc_error_log` VALUES ('54428', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.176', '2023-06-26 08:59:40', '2023-06-26 08:59:40');
INSERT INTO `lc_error_log` VALUES ('54429', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.32.164', '2023-06-26 08:59:40', '2023-06-26 08:59:40');
INSERT INTO `lc_error_log` VALUES ('54430', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.32.213', '2023-06-26 09:03:41', '2023-06-26 09:03:41');
INSERT INTO `lc_error_log` VALUES ('54431', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.212', '2023-06-26 09:03:44', '2023-06-26 09:03:44');
INSERT INTO `lc_error_log` VALUES ('54432', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.32.43', '2023-06-26 10:08:46', '2023-06-26 10:08:46');
INSERT INTO `lc_error_log` VALUES ('54433', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.135', '2023-06-26 10:08:49', '2023-06-26 10:08:49');
INSERT INTO `lc_error_log` VALUES ('54434', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-06-26 11:18:21', '2023-06-26 11:18:21');
INSERT INTO `lc_error_log` VALUES ('54435', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-26 14:13:47', '2023-06-26 14:13:47');
INSERT INTO `lc_error_log` VALUES ('54436', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '60.22.157.10', '2023-06-26 14:53:36', '2023-06-26 14:53:36');
INSERT INTO `lc_error_log` VALUES ('54437', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '60.22.157.10', '2023-06-26 16:28:24', '2023-06-26 16:28:24');
INSERT INTO `lc_error_log` VALUES ('54438', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-06-26 16:39:22', '2023-06-26 16:39:22');
INSERT INTO `lc_error_log` VALUES ('54439', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-06-26 16:39:23', '2023-06-26 16:39:23');
INSERT INTO `lc_error_log` VALUES ('54440', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '59.42.201.175', '2023-06-26 18:20:45', '2023-06-26 18:20:45');
INSERT INTO `lc_error_log` VALUES ('54441', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '58.211.23.183', '2023-06-26 20:19:17', '2023-06-26 20:19:17');
INSERT INTO `lc_error_log` VALUES ('54442', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-26 22:06:59', '2023-06-26 22:06:59');
INSERT INTO `lc_error_log` VALUES ('54443', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-26 22:07:05', '2023-06-26 22:07:05');
INSERT INTO `lc_error_log` VALUES ('54444', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-26 22:13:36', '2023-06-26 22:13:36');
INSERT INTO `lc_error_log` VALUES ('54445', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-06-27 00:53:15', '2023-06-27 00:53:15');
INSERT INTO `lc_error_log` VALUES ('54446', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.22', '2023-06-27 00:53:16', '2023-06-27 00:53:16');
INSERT INTO `lc_error_log` VALUES ('54447', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.20', '2023-06-27 00:59:57', '2023-06-27 00:59:57');
INSERT INTO `lc_error_log` VALUES ('54448', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.197', '2023-06-27 02:38:06', '2023-06-27 02:38:06');
INSERT INTO `lc_error_log` VALUES ('54449', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.84', '2023-06-27 08:09:23', '2023-06-27 08:09:23');
INSERT INTO `lc_error_log` VALUES ('54450', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.58.165.157', '2023-06-27 09:59:53', '2023-06-27 09:59:53');
INSERT INTO `lc_error_log` VALUES ('54451', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '122.224.147.141', '2023-06-27 11:55:26', '2023-06-27 11:55:26');
INSERT INTO `lc_error_log` VALUES ('54452', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '122.224.147.141', '2023-06-27 11:57:01', '2023-06-27 11:57:01');
INSERT INTO `lc_error_log` VALUES ('54453', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '122.224.147.141', '2023-06-27 12:00:48', '2023-06-27 12:00:48');
INSERT INTO `lc_error_log` VALUES ('54454', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '122.224.147.141', '2023-06-27 12:02:08', '2023-06-27 12:02:08');
INSERT INTO `lc_error_log` VALUES ('54455', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '60.176.169.153', '2023-06-27 15:58:53', '2023-06-27 15:58:53');
INSERT INTO `lc_error_log` VALUES ('54456', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.198', '2023-06-27 18:49:29', '2023-06-27 18:49:29');
INSERT INTO `lc_error_log` VALUES ('54457', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=81&info=true', '116.179.32.26', '2023-06-27 19:32:36', '2023-06-27 19:32:36');
INSERT INTO `lc_error_log` VALUES ('54458', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=81&info=true', '116.179.37.239', '2023-06-27 19:32:38', '2023-06-27 19:32:38');
INSERT INTO `lc_error_log` VALUES ('54459', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=81&info=true', '116.179.37.42', '2023-06-27 19:32:40', '2023-06-27 19:32:40');
INSERT INTO `lc_error_log` VALUES ('54460', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.32.96', '2023-06-27 20:47:33', '2023-06-27 20:47:33');
INSERT INTO `lc_error_log` VALUES ('54461', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.233', '2023-06-27 20:47:39', '2023-06-27 20:47:39');
INSERT INTO `lc_error_log` VALUES ('54462', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.148', '2023-06-27 22:11:01', '2023-06-27 22:11:01');
INSERT INTO `lc_error_log` VALUES ('54463', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-27 22:33:24', '2023-06-27 22:33:24');
INSERT INTO `lc_error_log` VALUES ('54464', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-27 22:34:15', '2023-06-27 22:34:15');
INSERT INTO `lc_error_log` VALUES ('54465', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-27 22:40:06', '2023-06-27 22:40:06');
INSERT INTO `lc_error_log` VALUES ('54466', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.240', '2023-06-27 22:45:46', '2023-06-27 22:45:46');
INSERT INTO `lc_error_log` VALUES ('54467', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.209', '2023-06-27 23:26:51', '2023-06-27 23:26:51');
INSERT INTO `lc_error_log` VALUES ('54468', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.245', '2023-06-28 01:30:54', '2023-06-28 01:30:54');
INSERT INTO `lc_error_log` VALUES ('54469', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.254.108.14', '2023-06-28 11:21:21', '2023-06-28 11:21:21');
INSERT INTO `lc_error_log` VALUES ('54470', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-28 12:19:32', '2023-06-28 12:19:32');
INSERT INTO `lc_error_log` VALUES ('54471', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-28 13:23:46', '2023-06-28 13:23:46');
INSERT INTO `lc_error_log` VALUES ('54472', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.17', '2023-06-28 14:07:19', '2023-06-28 14:07:19');
INSERT INTO `lc_error_log` VALUES ('54473', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.21', '2023-06-28 14:07:22', '2023-06-28 14:07:22');
INSERT INTO `lc_error_log` VALUES ('54474', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.18', '2023-06-28 14:58:25', '2023-06-28 14:58:25');
INSERT INTO `lc_error_log` VALUES ('54475', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-28 15:24:57', '2023-06-28 15:24:57');
INSERT INTO `lc_error_log` VALUES ('54476', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '117.44.93.85', '2023-06-28 15:28:26', '2023-06-28 15:28:26');
INSERT INTO `lc_error_log` VALUES ('54477', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-28 16:39:18', '2023-06-28 16:39:18');
INSERT INTO `lc_error_log` VALUES ('54478', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-28 16:41:35', '2023-06-28 16:41:35');
INSERT INTO `lc_error_log` VALUES ('54479', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '120.216.180.112', '2023-06-28 16:51:59', '2023-06-28 16:51:59');
INSERT INTO `lc_error_log` VALUES ('54480', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '名称长度需大于等于3个字符', '0', '524', '/v1/mp/keDirectory', '117.189.225.244', '2023-06-28 16:52:38', '2023-06-28 16:52:38');
INSERT INTO `lc_error_log` VALUES ('54481', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-28 17:04:10', '2023-06-28 17:04:10');
INSERT INTO `lc_error_log` VALUES ('54482', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 17:12:19', '2023-06-28 17:12:19');
INSERT INTO `lc_error_log` VALUES ('54483', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 17:15:47', '2023-06-28 17:15:47');
INSERT INTO `lc_error_log` VALUES ('54484', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-28 18:03:54', '2023-06-28 18:03:54');
INSERT INTO `lc_error_log` VALUES ('54485', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-06-28 18:03:57', '2023-06-28 18:03:57');
INSERT INTO `lc_error_log` VALUES ('54486', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-06-28 18:04:27', '2023-06-28 18:04:27');
INSERT INTO `lc_error_log` VALUES ('54487', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-06-28 18:31:39', '2023-06-28 18:31:39');
INSERT INTO `lc_error_log` VALUES ('54488', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-28 22:08:24', '2023-06-28 22:08:24');
INSERT INTO `lc_error_log` VALUES ('54489', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:38:36', '2023-06-28 22:38:36');
INSERT INTO `lc_error_log` VALUES ('54490', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:38:42', '2023-06-28 22:38:42');
INSERT INTO `lc_error_log` VALUES ('54491', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:38:49', '2023-06-28 22:38:49');
INSERT INTO `lc_error_log` VALUES ('54492', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:39:01', '2023-06-28 22:39:01');
INSERT INTO `lc_error_log` VALUES ('54493', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:39:09', '2023-06-28 22:39:09');
INSERT INTO `lc_error_log` VALUES ('54494', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:39:17', '2023-06-28 22:39:17');
INSERT INTO `lc_error_log` VALUES ('54495', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:39:24', '2023-06-28 22:39:24');
INSERT INTO `lc_error_log` VALUES ('54496', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:39:31', '2023-06-28 22:39:31');
INSERT INTO `lc_error_log` VALUES ('54497', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:39:38', '2023-06-28 22:39:38');
INSERT INTO `lc_error_log` VALUES ('54498', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:39:45', '2023-06-28 22:39:45');
INSERT INTO `lc_error_log` VALUES ('54499', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.225.244', '2023-06-28 22:47:16', '2023-06-28 22:47:16');
INSERT INTO `lc_error_log` VALUES ('54500', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-06-28 23:22:38', '2023-06-28 23:22:38');
INSERT INTO `lc_error_log` VALUES ('54501', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.42', '2023-06-29 01:46:58', '2023-06-29 01:46:58');
INSERT INTO `lc_error_log` VALUES ('54502', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.220', '2023-06-29 04:10:04', '2023-06-29 04:10:04');
INSERT INTO `lc_error_log` VALUES ('54503', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '139.170.202.61', '2023-06-29 09:40:55', '2023-06-29 09:40:55');
INSERT INTO `lc_error_log` VALUES ('54504', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '124.117.199.96', '2023-06-29 09:54:11', '2023-06-29 09:54:11');
INSERT INTO `lc_error_log` VALUES ('54505', 'platform', '/www/wwwroot/gouziyuan/app/platform/service/AdminUser.php', '手机号错误', '0', '47', '//login', '101.43.98.47', '2023-06-29 13:31:53', '2023-06-29 13:31:53');
INSERT INTO `lc_error_log` VALUES ('54506', 'platform', '/www/wwwroot/gouziyuan/app/platform/service/AdminUser.php', '手机号错误', '0', '47', '//login', '101.43.98.47', '2023-06-29 13:32:28', '2023-06-29 13:32:28');
INSERT INTO `lc_error_log` VALUES ('54507', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '117.28.135.66', '2023-06-29 14:09:31', '2023-06-29 14:09:31');
INSERT INTO `lc_error_log` VALUES ('54508', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-06-29 14:16:10', '2023-06-29 14:16:10');
INSERT INTO `lc_error_log` VALUES ('54509', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.23', '2023-06-29 14:16:49', '2023-06-29 14:16:49');
INSERT INTO `lc_error_log` VALUES ('54510', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '36.154.28.122', '2023-06-29 14:20:37', '2023-06-29 14:20:37');
INSERT INTO `lc_error_log` VALUES ('54511', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '36.154.28.122', '2023-06-29 14:21:01', '2023-06-29 14:21:01');
INSERT INTO `lc_error_log` VALUES ('54512', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '60.176.169.153', '2023-06-29 14:22:47', '2023-06-29 14:22:47');
INSERT INTO `lc_error_log` VALUES ('54513', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '117.28.135.66', '2023-06-29 15:16:06', '2023-06-29 15:16:06');
INSERT INTO `lc_error_log` VALUES ('54514', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '117.28.135.66', '2023-06-29 15:16:43', '2023-06-29 15:16:43');
INSERT INTO `lc_error_log` VALUES ('54515', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.32.48', '2023-06-29 15:54:29', '2023-06-29 15:54:29');
INSERT INTO `lc_error_log` VALUES ('54516', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=82&info=true', '116.179.37.244', '2023-06-29 15:54:31', '2023-06-29 15:54:31');
INSERT INTO `lc_error_log` VALUES ('54517', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-29 17:20:10', '2023-06-29 17:20:10');
INSERT INTO `lc_error_log` VALUES ('54518', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-06-29 17:30:56', '2023-06-29 17:30:56');
INSERT INTO `lc_error_log` VALUES ('54519', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=78&info=true', '220.181.108.112', '2023-06-29 21:08:35', '2023-06-29 21:08:35');
INSERT INTO `lc_error_log` VALUES ('54520', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=78&info=true', '116.179.37.146', '2023-06-29 21:08:40', '2023-06-29 21:08:40');
INSERT INTO `lc_error_log` VALUES ('54521', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitKechengOrder', '117.189.225.244', '2023-06-29 22:03:34', '2023-06-29 22:03:34');
INSERT INTO `lc_error_log` VALUES ('54522', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-06-29 22:11:54', '2023-06-29 22:11:54');
INSERT INTO `lc_error_log` VALUES ('54523', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-06-29 22:16:08', '2023-06-29 22:16:08');
INSERT INTO `lc_error_log` VALUES ('54524', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-29 22:17:47', '2023-06-29 22:17:47');
INSERT INTO `lc_error_log` VALUES ('54525', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-29 22:18:18', '2023-06-29 22:18:18');
INSERT INTO `lc_error_log` VALUES ('54526', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-06-29 22:43:10', '2023-06-29 22:43:10');
INSERT INTO `lc_error_log` VALUES ('54527', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-06-29 23:25:38', '2023-06-29 23:25:38');
INSERT INTO `lc_error_log` VALUES ('54528', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:23', '2023-06-29 23:28:23');
INSERT INTO `lc_error_log` VALUES ('54529', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:24', '2023-06-29 23:28:24');
INSERT INTO `lc_error_log` VALUES ('54530', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:24', '2023-06-29 23:28:24');
INSERT INTO `lc_error_log` VALUES ('54531', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:24', '2023-06-29 23:28:24');
INSERT INTO `lc_error_log` VALUES ('54532', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '123.56.155.157', '2023-06-29 23:28:24', '2023-06-29 23:28:24');
INSERT INTO `lc_error_log` VALUES ('54533', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:24', '2023-06-29 23:28:24');
INSERT INTO `lc_error_log` VALUES ('54534', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '123.56.155.157', '2023-06-29 23:28:25', '2023-06-29 23:28:25');
INSERT INTO `lc_error_log` VALUES ('54535', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:25', '2023-06-29 23:28:25');
INSERT INTO `lc_error_log` VALUES ('54536', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:39', '2023-06-29 23:28:39');
INSERT INTO `lc_error_log` VALUES ('54537', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-06-29 23:28:40', '2023-06-29 23:28:40');
INSERT INTO `lc_error_log` VALUES ('54538', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/users/search', '124.221.253.185', '2023-06-30 04:39:09', '2023-06-30 04:39:09');
INSERT INTO `lc_error_log` VALUES ('54539', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/v1/users/search', '124.221.253.185', '2023-06-30 04:39:18', '2023-06-30 04:39:18');
INSERT INTO `lc_error_log` VALUES ('54540', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.30', '2023-06-30 05:36:39', '2023-06-30 05:36:39');
INSERT INTO `lc_error_log` VALUES ('54541', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-06-30 08:01:31', '2023-06-30 08:01:31');
INSERT INTO `lc_error_log` VALUES ('54542', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-06-30 08:01:33', '2023-06-30 08:01:33');
INSERT INTO `lc_error_log` VALUES ('54543', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.199', '2023-06-30 08:11:35', '2023-06-30 08:11:35');
INSERT INTO `lc_error_log` VALUES ('54544', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=8&info=true', '116.179.32.101', '2023-06-30 10:26:49', '2023-06-30 10:26:49');
INSERT INTO `lc_error_log` VALUES ('54545', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=8&info=true', '116.179.37.86', '2023-06-30 10:26:52', '2023-06-30 10:26:52');
INSERT INTO `lc_error_log` VALUES ('54546', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=36', '116.179.37.219', '2023-06-30 19:06:09', '2023-06-30 19:06:09');
INSERT INTO `lc_error_log` VALUES ('54547', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.95', '2023-06-30 19:06:09', '2023-06-30 19:06:09');
INSERT INTO `lc_error_log` VALUES ('54548', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=36', '116.179.37.98', '2023-06-30 19:06:12', '2023-06-30 19:06:12');
INSERT INTO `lc_error_log` VALUES ('54549', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.133', '2023-06-30 19:06:12', '2023-06-30 19:06:12');
INSERT INTO `lc_error_log` VALUES ('54550', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.53', '2023-06-30 20:52:43', '2023-06-30 20:52:43');
INSERT INTO `lc_error_log` VALUES ('54551', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.38', '2023-06-30 20:52:48', '2023-06-30 20:52:48');
INSERT INTO `lc_error_log` VALUES ('54552', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.38', '2023-06-30 20:52:48', '2023-06-30 20:52:48');
INSERT INTO `lc_error_log` VALUES ('54553', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-06-30 21:06:32', '2023-06-30 21:06:32');
INSERT INTO `lc_error_log` VALUES ('54554', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-06-30 21:30:49', '2023-06-30 21:30:49');
INSERT INTO `lc_error_log` VALUES ('54555', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=23', '116.179.37.245', '2023-06-30 21:39:22', '2023-06-30 21:39:22');
INSERT INTO `lc_error_log` VALUES ('54556', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.86', '2023-06-30 21:39:22', '2023-06-30 21:39:22');
INSERT INTO `lc_error_log` VALUES ('54557', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=23', '116.179.37.8', '2023-06-30 21:39:24', '2023-06-30 21:39:24');
INSERT INTO `lc_error_log` VALUES ('54558', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.186', '2023-06-30 21:39:24', '2023-06-30 21:39:24');
INSERT INTO `lc_error_log` VALUES ('54559', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=32', '116.179.37.45', '2023-06-30 22:05:05', '2023-06-30 22:05:05');
INSERT INTO `lc_error_log` VALUES ('54560', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.146', '2023-06-30 22:05:06', '2023-06-30 22:05:06');
INSERT INTO `lc_error_log` VALUES ('54561', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=32', '116.179.37.27', '2023-06-30 22:05:08', '2023-06-30 22:05:08');
INSERT INTO `lc_error_log` VALUES ('54562', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.117', '2023-06-30 22:05:08', '2023-06-30 22:05:08');
INSERT INTO `lc_error_log` VALUES ('54563', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//getUserLoginLog?page=1&pageSize=20', '101.43.98.47', '2023-06-30 23:20:39', '2023-06-30 23:20:39');
INSERT INTO `lc_error_log` VALUES ('54564', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-06-30 23:33:40', '2023-06-30 23:33:40');
INSERT INTO `lc_error_log` VALUES ('54565', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.67.29.173', '2023-07-01 09:23:58', '2023-07-01 09:23:58');
INSERT INTO `lc_error_log` VALUES ('54566', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-07-01 09:50:48', '2023-07-01 09:50:48');
INSERT INTO `lc_error_log` VALUES ('54567', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-07-01 09:50:48', '2023-07-01 09:50:48');
INSERT INTO `lc_error_log` VALUES ('54568', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-07-01 09:51:06', '2023-07-01 09:51:06');
INSERT INTO `lc_error_log` VALUES ('54569', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-07-01 09:51:17', '2023-07-01 09:51:17');
INSERT INTO `lc_error_log` VALUES ('54570', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.29', '2023-07-01 11:26:05', '2023-07-01 11:26:05');
INSERT INTO `lc_error_log` VALUES ('54571', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=34', '116.179.37.235', '2023-07-01 12:59:34', '2023-07-01 12:59:34');
INSERT INTO `lc_error_log` VALUES ('54572', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.51', '2023-07-01 12:59:35', '2023-07-01 12:59:35');
INSERT INTO `lc_error_log` VALUES ('54573', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=34', '116.179.37.178', '2023-07-01 12:59:36', '2023-07-01 12:59:36');
INSERT INTO `lc_error_log` VALUES ('54574', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.5', '2023-07-01 12:59:37', '2023-07-01 12:59:37');
INSERT INTO `lc_error_log` VALUES ('54575', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.27', '2023-07-01 13:26:20', '2023-07-01 13:26:20');
INSERT INTO `lc_error_log` VALUES ('54576', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitKechengOrder', '125.122.108.131', '2023-07-01 14:30:22', '2023-07-01 14:30:22');
INSERT INTO `lc_error_log` VALUES ('54577', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.158', '2023-07-01 15:04:04', '2023-07-01 15:04:04');
INSERT INTO `lc_error_log` VALUES ('54578', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-07-01 15:04:20', '2023-07-01 15:04:20');
INSERT INTO `lc_error_log` VALUES ('54579', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=31', '116.179.37.29', '2023-07-01 15:47:32', '2023-07-01 15:47:32');
INSERT INTO `lc_error_log` VALUES ('54580', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.157', '2023-07-01 15:47:32', '2023-07-01 15:47:32');
INSERT INTO `lc_error_log` VALUES ('54581', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=31', '116.179.37.55', '2023-07-01 15:47:34', '2023-07-01 15:47:34');
INSERT INTO `lc_error_log` VALUES ('54582', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.37', '2023-07-01 15:47:34', '2023-07-01 15:47:34');
INSERT INTO `lc_error_log` VALUES ('54583', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=29', '116.179.37.194', '2023-07-01 16:31:10', '2023-07-01 16:31:10');
INSERT INTO `lc_error_log` VALUES ('54584', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.82', '2023-07-01 16:31:10', '2023-07-01 16:31:10');
INSERT INTO `lc_error_log` VALUES ('54585', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=29', '116.179.37.160', '2023-07-01 16:31:12', '2023-07-01 16:31:12');
INSERT INTO `lc_error_log` VALUES ('54586', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.80', '2023-07-01 16:31:12', '2023-07-01 16:31:12');
INSERT INTO `lc_error_log` VALUES ('54587', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-01 18:11:20', '2023-07-01 18:11:20');
INSERT INTO `lc_error_log` VALUES ('54588', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.111.166.96', '2023-07-01 18:14:29', '2023-07-01 18:14:29');
INSERT INTO `lc_error_log` VALUES ('54589', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '43.226.53.242', '2023-07-01 18:35:33', '2023-07-01 18:35:33');
INSERT INTO `lc_error_log` VALUES ('54590', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '218.24.113.14', '2023-07-01 18:35:33', '2023-07-01 18:35:33');
INSERT INTO `lc_error_log` VALUES ('54591', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.60.108.123', '2023-07-01 18:35:53', '2023-07-01 18:35:53');
INSERT INTO `lc_error_log` VALUES ('54592', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-01 21:39:56', '2023-07-01 21:39:56');
INSERT INTO `lc_error_log` VALUES ('54593', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-01 22:40:47', '2023-07-01 22:40:47');
INSERT INTO `lc_error_log` VALUES ('54594', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-01 23:03:04', '2023-07-01 23:03:04');
INSERT INTO `lc_error_log` VALUES ('54595', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=22', '116.179.37.83', '2023-07-01 23:55:08', '2023-07-01 23:55:08');
INSERT INTO `lc_error_log` VALUES ('54596', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.111', '2023-07-01 23:55:09', '2023-07-01 23:55:09');
INSERT INTO `lc_error_log` VALUES ('54597', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=22', '116.179.37.211', '2023-07-01 23:55:10', '2023-07-01 23:55:10');
INSERT INTO `lc_error_log` VALUES ('54598', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.78', '2023-07-01 23:55:11', '2023-07-01 23:55:11');
INSERT INTO `lc_error_log` VALUES ('54599', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '36.148.85.134', '2023-07-01 23:55:18', '2023-07-01 23:55:18');
INSERT INTO `lc_error_log` VALUES ('54600', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-07-02 00:03:41', '2023-07-02 00:03:41');
INSERT INTO `lc_error_log` VALUES ('54601', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=37', '116.179.37.177', '2023-07-02 00:49:53', '2023-07-02 00:49:53');
INSERT INTO `lc_error_log` VALUES ('54602', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.160', '2023-07-02 00:49:53', '2023-07-02 00:49:53');
INSERT INTO `lc_error_log` VALUES ('54603', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=37', '116.179.37.186', '2023-07-02 00:49:55', '2023-07-02 00:49:55');
INSERT INTO `lc_error_log` VALUES ('54604', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.186', '2023-07-02 00:49:55', '2023-07-02 00:49:55');
INSERT INTO `lc_error_log` VALUES ('54605', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=33', '116.179.37.172', '2023-07-02 03:06:41', '2023-07-02 03:06:41');
INSERT INTO `lc_error_log` VALUES ('54606', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.146', '2023-07-02 03:06:41', '2023-07-02 03:06:41');
INSERT INTO `lc_error_log` VALUES ('54607', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=33', '116.179.37.163', '2023-07-02 03:06:43', '2023-07-02 03:06:43');
INSERT INTO `lc_error_log` VALUES ('54608', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.58', '2023-07-02 03:06:43', '2023-07-02 03:06:43');
INSERT INTO `lc_error_log` VALUES ('54609', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=30', '116.179.37.205', '2023-07-02 08:05:50', '2023-07-02 08:05:50');
INSERT INTO `lc_error_log` VALUES ('54610', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.181', '2023-07-02 08:05:51', '2023-07-02 08:05:51');
INSERT INTO `lc_error_log` VALUES ('54611', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=30', '116.179.37.163', '2023-07-02 08:05:53', '2023-07-02 08:05:53');
INSERT INTO `lc_error_log` VALUES ('54612', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.229', '2023-07-02 08:05:53', '2023-07-02 08:05:53');
INSERT INTO `lc_error_log` VALUES ('54613', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=38', '116.179.37.215', '2023-07-02 09:11:23', '2023-07-02 09:11:23');
INSERT INTO `lc_error_log` VALUES ('54614', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.83', '2023-07-02 09:11:23', '2023-07-02 09:11:23');
INSERT INTO `lc_error_log` VALUES ('54615', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=38', '116.179.37.120', '2023-07-02 09:11:28', '2023-07-02 09:11:28');
INSERT INTO `lc_error_log` VALUES ('54616', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.68', '2023-07-02 09:11:29', '2023-07-02 09:11:29');
INSERT INTO `lc_error_log` VALUES ('54617', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-02 11:26:09', '2023-07-02 11:26:09');
INSERT INTO `lc_error_log` VALUES ('54618', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-02 11:33:33', '2023-07-02 11:33:33');
INSERT INTO `lc_error_log` VALUES ('54619', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '118.213.143.247', '2023-07-02 11:54:06', '2023-07-02 11:54:06');
INSERT INTO `lc_error_log` VALUES ('54620', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '118.213.143.247', '2023-07-02 11:54:20', '2023-07-02 11:54:20');
INSERT INTO `lc_error_log` VALUES ('54621', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.26', '2023-07-02 13:19:15', '2023-07-02 13:19:15');
INSERT INTO `lc_error_log` VALUES ('54622', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=27', '116.179.37.98', '2023-07-02 14:33:20', '2023-07-02 14:33:20');
INSERT INTO `lc_error_log` VALUES ('54623', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.33', '2023-07-02 14:33:21', '2023-07-02 14:33:21');
INSERT INTO `lc_error_log` VALUES ('54624', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=27', '116.179.37.98', '2023-07-02 14:33:22', '2023-07-02 14:33:22');
INSERT INTO `lc_error_log` VALUES ('54625', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.244', '2023-07-02 14:33:22', '2023-07-02 14:33:22');
INSERT INTO `lc_error_log` VALUES ('54626', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '32', '/v1/getArticleInfo?id=www.gouziyuan.cn', '66.249.79.5', '2023-07-02 14:40:10', '2023-07-02 14:40:10');
INSERT INTO `lc_error_log` VALUES ('54627', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '66.249.79.6', '2023-07-02 14:40:13', '2023-07-02 14:40:13');
INSERT INTO `lc_error_log` VALUES ('54628', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.198', '2023-07-02 16:14:21', '2023-07-02 16:14:21');
INSERT INTO `lc_error_log` VALUES ('54629', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-07-02 16:17:04', '2023-07-02 16:17:04');
INSERT INTO `lc_error_log` VALUES ('54630', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.196', '2023-07-02 16:17:06', '2023-07-02 16:17:06');
INSERT INTO `lc_error_log` VALUES ('54631', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.34', '2023-07-02 19:03:10', '2023-07-02 19:03:10');
INSERT INTO `lc_error_log` VALUES ('54632', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '27.115.124.49', '2023-07-02 19:03:10', '2023-07-02 19:03:10');
INSERT INTO `lc_error_log` VALUES ('54633', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.5', '2023-07-02 19:04:52', '2023-07-02 19:04:52');
INSERT INTO `lc_error_log` VALUES ('54634', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '27.115.124.40', '2023-07-02 19:05:11', '2023-07-02 19:05:11');
INSERT INTO `lc_error_log` VALUES ('54635', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.6.49.14', '2023-07-02 19:06:05', '2023-07-02 19:06:05');
INSERT INTO `lc_error_log` VALUES ('54636', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '123.6.49.10', '2023-07-02 19:06:23', '2023-07-02 19:06:23');
INSERT INTO `lc_error_log` VALUES ('54637', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.6.49.49', '2023-07-02 19:14:36', '2023-07-02 19:14:36');
INSERT INTO `lc_error_log` VALUES ('54638', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/sitemap.xml', '123.6.49.17', '2023-07-02 19:14:37', '2023-07-02 19:14:37');
INSERT INTO `lc_error_log` VALUES ('54639', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\sitemap\\Xml', '0', '76', '/sitemap.xml', '101.198.0.187', '2023-07-02 19:16:08', '2023-07-02 19:16:08');
INSERT INTO `lc_error_log` VALUES ('54640', 'wx', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/route/dispatch/Controller.php', 'controller not exists:app\\wx\\controller\\sitemap\\Xml', '0', '76', '/sitemap.xml', '101.198.0.151', '2023-07-02 19:17:08', '2023-07-02 19:17:08');
INSERT INTO `lc_error_log` VALUES ('54641', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-07-02 20:42:15', '2023-07-02 20:42:15');
INSERT INTO `lc_error_log` VALUES ('54642', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-02 21:12:34', '2023-07-02 21:12:34');
INSERT INTO `lc_error_log` VALUES ('54643', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getErrorLog?page=1&pageSize=20', '101.43.98.47', '2023-07-02 22:47:35', '2023-07-02 22:47:35');
INSERT INTO `lc_error_log` VALUES ('54644', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-03 00:11:12', '2023-07-03 00:11:12');
INSERT INTO `lc_error_log` VALUES ('54645', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-03 00:14:49', '2023-07-03 00:14:49');
INSERT INTO `lc_error_log` VALUES ('54646', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '39.105.5.9', '2023-07-03 01:55:49', '2023-07-03 01:55:49');
INSERT INTO `lc_error_log` VALUES ('54647', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=8&info=true', '116.179.32.88', '2023-07-03 09:02:29', '2023-07-03 09:02:29');
INSERT INTO `lc_error_log` VALUES ('54648', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在', '0', '96', '/v1/getZiyuanInfo?id=8&info=true', '116.179.37.111', '2023-07-03 09:02:32', '2023-07-03 09:02:32');
INSERT INTO `lc_error_log` VALUES ('54649', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '42.180.2.94', '2023-07-03 10:10:30', '2023-07-03 10:10:30');
INSERT INTO `lc_error_log` VALUES ('54650', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '116.19.201.133', '2023-07-03 10:42:50', '2023-07-03 10:42:50');
INSERT INTO `lc_error_log` VALUES ('54651', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.19.201.133', '2023-07-03 10:51:59', '2023-07-03 10:51:59');
INSERT INTO `lc_error_log` VALUES ('54652', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '116.19.201.133', '2023-07-03 10:52:08', '2023-07-03 10:52:08');
INSERT INTO `lc_error_log` VALUES ('54653', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '116.19.201.133', '2023-07-03 10:52:08', '2023-07-03 10:52:08');
INSERT INTO `lc_error_log` VALUES ('54654', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-07-03 11:06:07', '2023-07-03 11:06:07');
INSERT INTO `lc_error_log` VALUES ('54655', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '153.0.83.185', '2023-07-03 11:51:14', '2023-07-03 11:51:14');
INSERT INTO `lc_error_log` VALUES ('54656', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-03 11:55:54', '2023-07-03 11:55:54');
INSERT INTO `lc_error_log` VALUES ('54657', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-03 11:58:03', '2023-07-03 11:58:03');
INSERT INTO `lc_error_log` VALUES ('54658', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-07-03 12:02:15', '2023-07-03 12:02:15');
INSERT INTO `lc_error_log` VALUES ('54659', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '112.66.109.171', '2023-07-03 12:18:13', '2023-07-03 12:18:13');
INSERT INTO `lc_error_log` VALUES ('54660', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-07-03 12:43:44', '2023-07-03 12:43:44');
INSERT INTO `lc_error_log` VALUES ('54661', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.75', '2023-07-03 17:17:34', '2023-07-03 17:17:34');
INSERT INTO `lc_error_log` VALUES ('54662', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '42.236.10.78', '2023-07-03 17:17:45', '2023-07-03 17:17:45');
INSERT INTO `lc_error_log` VALUES ('54663', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-03 17:34:32', '2023-07-03 17:34:32');
INSERT INTO `lc_error_log` VALUES ('54664', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=1&keyword=', '101.43.98.47', '2023-07-03 18:21:34', '2023-07-03 18:21:34');
INSERT INTO `lc_error_log` VALUES ('54665', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.25', '2023-07-03 19:40:34', '2023-07-03 19:40:34');
INSERT INTO `lc_error_log` VALUES ('54666', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.25', '2023-07-03 19:40:36', '2023-07-03 19:40:36');
INSERT INTO `lc_error_log` VALUES ('54667', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.149', '2023-07-04 00:58:41', '2023-07-04 00:58:41');
INSERT INTO `lc_error_log` VALUES ('54668', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.166', '2023-07-04 00:59:56', '2023-07-04 00:59:56');
INSERT INTO `lc_error_log` VALUES ('54669', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.129', '2023-07-04 01:00:05', '2023-07-04 01:00:05');
INSERT INTO `lc_error_log` VALUES ('54670', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.129', '2023-07-04 01:00:15', '2023-07-04 01:00:15');
INSERT INTO `lc_error_log` VALUES ('54671', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-07-04 01:00:27', '2023-07-04 01:00:27');
INSERT INTO `lc_error_log` VALUES ('54672', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-07-04 03:37:11', '2023-07-04 03:37:11');
INSERT INTO `lc_error_log` VALUES ('54673', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '220.171.201.131', '2023-07-04 09:09:18', '2023-07-04 09:09:18');
INSERT INTO `lc_error_log` VALUES ('54674', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-04 10:42:30', '2023-07-04 10:42:30');
INSERT INTO `lc_error_log` VALUES ('54675', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-04 10:42:57', '2023-07-04 10:42:57');
INSERT INTO `lc_error_log` VALUES ('54676', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-04 10:44:28', '2023-07-04 10:44:28');
INSERT INTO `lc_error_log` VALUES ('54677', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-04 10:46:51', '2023-07-04 10:46:51');
INSERT INTO `lc_error_log` VALUES ('54678', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '66.249.79.7', '2023-07-04 13:04:58', '2023-07-04 13:04:58');
INSERT INTO `lc_error_log` VALUES ('54679', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-04 14:15:54', '2023-07-04 14:15:54');
INSERT INTO `lc_error_log` VALUES ('54680', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '218.83.27.70', '2023-07-04 14:22:55', '2023-07-04 14:22:55');
INSERT INTO `lc_error_log` VALUES ('54681', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-07-04 17:00:12', '2023-07-04 17:00:12');
INSERT INTO `lc_error_log` VALUES ('54682', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '117.62.7.224', '2023-07-04 17:48:11', '2023-07-04 17:48:11');
INSERT INTO `lc_error_log` VALUES ('54683', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '117.62.7.224', '2023-07-04 17:52:52', '2023-07-04 17:52:52');
INSERT INTO `lc_error_log` VALUES ('54684', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '117.62.7.224', '2023-07-04 17:54:38', '2023-07-04 17:54:38');
INSERT INTO `lc_error_log` VALUES ('54685', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-04 20:19:20', '2023-07-04 20:19:20');
INSERT INTO `lc_error_log` VALUES ('54686', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-07-04 21:02:27', '2023-07-04 21:02:27');
INSERT INTO `lc_error_log` VALUES ('54687', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-07-04 21:02:30', '2023-07-04 21:02:30');
INSERT INTO `lc_error_log` VALUES ('54688', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=29', '116.179.37.137', '2023-07-05 01:46:33', '2023-07-05 01:46:33');
INSERT INTO `lc_error_log` VALUES ('54689', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.203', '2023-07-05 01:46:34', '2023-07-05 01:46:34');
INSERT INTO `lc_error_log` VALUES ('54690', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=32', '116.179.37.16', '2023-07-05 01:49:55', '2023-07-05 01:49:55');
INSERT INTO `lc_error_log` VALUES ('54691', 'api', '/www/wwwroot/gouziyuan/app/api/business/Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '116.179.37.137', '2023-07-05 01:49:55', '2023-07-05 01:49:55');
INSERT INTO `lc_error_log` VALUES ('54692', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '220.181.108.158', '2023-07-05 02:48:25', '2023-07-05 02:48:25');
INSERT INTO `lc_error_log` VALUES ('54693', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.50', '2023-07-05 02:48:28', '2023-07-05 02:48:28');
INSERT INTO `lc_error_log` VALUES ('54694', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.148', '2023-07-05 06:24:18', '2023-07-05 06:24:18');
INSERT INTO `lc_error_log` VALUES ('54695', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.129', '2023-07-05 06:25:28', '2023-07-05 06:25:28');
INSERT INTO `lc_error_log` VALUES ('54696', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-07-05 06:25:29', '2023-07-05 06:25:29');
INSERT INTO `lc_error_log` VALUES ('54697', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-07-05 06:25:37', '2023-07-05 06:25:37');
INSERT INTO `lc_error_log` VALUES ('54698', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-07-05 06:25:54', '2023-07-05 06:25:54');
INSERT INTO `lc_error_log` VALUES ('54699', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.70.50', '2023-07-05 07:56:42', '2023-07-05 07:56:42');
INSERT INTO `lc_error_log` VALUES ('54700', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.154.169', '2023-07-05 07:56:58', '2023-07-05 07:56:58');
INSERT INTO `lc_error_log` VALUES ('54701', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.184.217', '2023-07-05 07:57:45', '2023-07-05 07:57:45');
INSERT INTO `lc_error_log` VALUES ('54702', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.132.214', '2023-07-05 07:57:45', '2023-07-05 07:57:45');
INSERT INTO `lc_error_log` VALUES ('54703', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-05 09:44:12', '2023-07-05 09:44:12');
INSERT INTO `lc_error_log` VALUES ('54704', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '122.227.162.222', '2023-07-05 11:38:23', '2023-07-05 11:38:23');
INSERT INTO `lc_error_log` VALUES ('54705', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-05 12:26:41', '2023-07-05 12:26:41');
INSERT INTO `lc_error_log` VALUES ('54706', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-07-05 12:34:55', '2023-07-05 12:34:55');
INSERT INTO `lc_error_log` VALUES ('54707', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '122.227.162.222', '2023-07-05 12:53:34', '2023-07-05 12:53:34');
INSERT INTO `lc_error_log` VALUES ('54708', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '36.24.78.87', '2023-07-05 13:01:45', '2023-07-05 13:01:45');
INSERT INTO `lc_error_log` VALUES ('54709', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '1.206.93.113', '2023-07-05 14:06:08', '2023-07-05 14:06:08');
INSERT INTO `lc_error_log` VALUES ('54710', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-05 14:27:18', '2023-07-05 14:27:18');
INSERT INTO `lc_error_log` VALUES ('54711', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-05 14:37:43', '2023-07-05 14:37:43');
INSERT INTO `lc_error_log` VALUES ('54712', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getErrorLog?page=1&pageSize=20', '101.43.98.47', '2023-07-05 15:33:29', '2023-07-05 15:33:29');
INSERT INTO `lc_error_log` VALUES ('54713', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-07-05 16:04:11', '2023-07-05 16:04:11');
INSERT INTO `lc_error_log` VALUES ('54714', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-07-05 16:04:14', '2023-07-05 16:04:14');
INSERT INTO `lc_error_log` VALUES ('54715', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-05 16:10:52', '2023-07-05 16:10:52');
INSERT INTO `lc_error_log` VALUES ('54716', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-05 16:16:20', '2023-07-05 16:16:20');
INSERT INTO `lc_error_log` VALUES ('54717', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-05 16:17:03', '2023-07-05 16:17:03');
INSERT INTO `lc_error_log` VALUES ('54718', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-05 16:25:27', '2023-07-05 16:25:27');
INSERT INTO `lc_error_log` VALUES ('54719', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-05 16:26:06', '2023-07-05 16:26:06');
INSERT INTO `lc_error_log` VALUES ('54720', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-05 16:26:25', '2023-07-05 16:26:25');
INSERT INTO `lc_error_log` VALUES ('54721', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-07-05 16:36:49', '2023-07-05 16:36:49');
INSERT INTO `lc_error_log` VALUES ('54722', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-05 17:07:51', '2023-07-05 17:07:51');
INSERT INTO `lc_error_log` VALUES ('54723', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-07-05 18:23:33', '2023-07-05 18:23:33');
INSERT INTO `lc_error_log` VALUES ('54724', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-07-05 18:27:16', '2023-07-05 18:27:16');
INSERT INTO `lc_error_log` VALUES ('54725', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-07-05 18:27:19', '2023-07-05 18:27:19');
INSERT INTO `lc_error_log` VALUES ('54726', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.159', '2023-07-05 18:57:35', '2023-07-05 18:57:35');
INSERT INTO `lc_error_log` VALUES ('54727', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.146', '2023-07-05 18:57:38', '2023-07-05 18:57:38');
INSERT INTO `lc_error_log` VALUES ('54728', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.194.158.2', '2023-07-05 22:48:28', '2023-07-05 22:48:28');
INSERT INTO `lc_error_log` VALUES ('54729', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-05 23:17:25', '2023-07-05 23:17:25');
INSERT INTO `lc_error_log` VALUES ('54730', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-06 00:57:30', '2023-07-06 00:57:30');
INSERT INTO `lc_error_log` VALUES ('54731', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.28', '2023-07-06 02:26:00', '2023-07-06 02:26:00');
INSERT INTO `lc_error_log` VALUES ('54732', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-06 08:06:03', '2023-07-06 08:06:03');
INSERT INTO `lc_error_log` VALUES ('54733', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.32.138', '2023-07-06 12:27:45', '2023-07-06 12:27:45');
INSERT INTO `lc_error_log` VALUES ('54734', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.47', '2023-07-06 12:27:46', '2023-07-06 12:27:46');
INSERT INTO `lc_error_log` VALUES ('54735', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.32.42', '2023-07-06 13:06:51', '2023-07-06 13:06:51');
INSERT INTO `lc_error_log` VALUES ('54736', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.161', '2023-07-06 13:06:56', '2023-07-06 13:06:56');
INSERT INTO `lc_error_log` VALUES ('54737', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-06 16:08:29', '2023-07-06 16:08:29');
INSERT INTO `lc_error_log` VALUES ('54738', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getErrorLog?page=1&pageSize=20', '101.43.98.47', '2023-07-06 16:34:51', '2023-07-06 16:34:51');
INSERT INTO `lc_error_log` VALUES ('54739', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '66.249.79.7', '2023-07-06 17:25:19', '2023-07-06 17:25:19');
INSERT INTO `lc_error_log` VALUES ('54740', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-07-06 17:40:52', '2023-07-06 17:40:52');
INSERT INTO `lc_error_log` VALUES ('54741', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.26', '2023-07-06 17:40:54', '2023-07-06 17:40:54');
INSERT INTO `lc_error_log` VALUES ('54742', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.100.31', '2023-07-06 19:51:43', '2023-07-06 19:51:43');
INSERT INTO `lc_error_log` VALUES ('54743', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-07-06 19:52:09', '2023-07-06 19:52:09');
INSERT INTO `lc_error_log` VALUES ('54744', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-06 21:16:41', '2023-07-06 21:16:41');
INSERT INTO `lc_error_log` VALUES ('54745', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '111.18.3.151', '2023-07-06 22:23:56', '2023-07-06 22:23:56');
INSERT INTO `lc_error_log` VALUES ('54746', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-07 00:37:41', '2023-07-07 00:37:41');
INSERT INTO `lc_error_log` VALUES ('54747', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-07 00:40:21', '2023-07-07 00:40:21');
INSERT INTO `lc_error_log` VALUES ('54748', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-07 00:46:41', '2023-07-07 00:46:41');
INSERT INTO `lc_error_log` VALUES ('54749', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.19', '2023-07-07 02:33:34', '2023-07-07 02:33:34');
INSERT INTO `lc_error_log` VALUES ('54750', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.187.173.47', '2023-07-07 07:09:46', '2023-07-07 07:09:46');
INSERT INTO `lc_error_log` VALUES ('54751', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-07-07 09:39:29', '2023-07-07 09:39:29');
INSERT INTO `lc_error_log` VALUES ('54752', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-07 11:12:10', '2023-07-07 11:12:10');
INSERT INTO `lc_error_log` VALUES ('54753', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-07 12:14:17', '2023-07-07 12:14:17');
INSERT INTO `lc_error_log` VALUES ('54754', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-07-07 12:28:15', '2023-07-07 12:28:15');
INSERT INTO `lc_error_log` VALUES ('54755', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.50.170.125', '2023-07-07 13:52:59', '2023-07-07 13:52:59');
INSERT INTO `lc_error_log` VALUES ('54756', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.209.254.179', '2023-07-07 14:48:02', '2023-07-07 14:48:02');
INSERT INTO `lc_error_log` VALUES ('54757', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-07 16:27:19', '2023-07-07 16:27:19');
INSERT INTO `lc_error_log` VALUES ('54758', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-07 16:30:43', '2023-07-07 16:30:43');
INSERT INTO `lc_error_log` VALUES ('54759', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-07 16:34:51', '2023-07-07 16:34:51');
INSERT INTO `lc_error_log` VALUES ('54760', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getErrorLog?page=1&pageSize=20', '101.43.98.47', '2023-07-07 16:35:30', '2023-07-07 16:35:30');
INSERT INTO `lc_error_log` VALUES ('54761', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '42.180.2.94', '2023-07-07 16:49:49', '2023-07-07 16:49:49');
INSERT INTO `lc_error_log` VALUES ('54762', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-07-07 19:27:25', '2023-07-07 19:27:25');
INSERT INTO `lc_error_log` VALUES ('54763', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-07 21:26:45', '2023-07-07 21:26:45');
INSERT INTO `lc_error_log` VALUES ('54764', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-07 22:07:53', '2023-07-07 22:07:53');
INSERT INTO `lc_error_log` VALUES ('54765', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请填写描述', '0', '524', '/v1/mp/kecheng', '117.189.228.217', '2023-07-07 22:08:14', '2023-07-07 22:08:14');
INSERT INTO `lc_error_log` VALUES ('54766', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-07-07 22:23:40', '2023-07-07 22:23:40');
INSERT INTO `lc_error_log` VALUES ('54767', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-07 22:26:17', '2023-07-07 22:26:17');
INSERT INTO `lc_error_log` VALUES ('54768', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.174', '2023-07-07 22:33:22', '2023-07-07 22:33:22');
INSERT INTO `lc_error_log` VALUES ('54769', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.110', '2023-07-07 22:33:29', '2023-07-07 22:33:29');
INSERT INTO `lc_error_log` VALUES ('54770', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-07 22:43:23', '2023-07-07 22:43:23');
INSERT INTO `lc_error_log` VALUES ('54771', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-07 22:53:35', '2023-07-07 22:53:35');
INSERT INTO `lc_error_log` VALUES ('54772', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.239', '2023-07-08 00:03:22', '2023-07-08 00:03:22');
INSERT INTO `lc_error_log` VALUES ('54773', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.52', '2023-07-08 00:03:24', '2023-07-08 00:03:24');
INSERT INTO `lc_error_log` VALUES ('54774', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '220.181.108.167', '2023-07-08 00:31:40', '2023-07-08 00:31:40');
INSERT INTO `lc_error_log` VALUES ('54775', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.240', '2023-07-08 00:31:43', '2023-07-08 00:31:43');
INSERT INTO `lc_error_log` VALUES ('54776', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '116.179.37.248', '2023-07-08 00:31:44', '2023-07-08 00:31:44');
INSERT INTO `lc_error_log` VALUES ('54777', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.145.229', '2023-07-08 02:05:07', '2023-07-08 02:05:07');
INSERT INTO `lc_error_log` VALUES ('54778', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '218.95.226.151', '2023-07-08 04:52:51', '2023-07-08 04:52:51');
INSERT INTO `lc_error_log` VALUES ('54779', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-08 12:07:22', '2023-07-08 12:07:22');
INSERT INTO `lc_error_log` VALUES ('54780', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-08 12:28:57', '2023-07-08 12:28:57');
INSERT INTO `lc_error_log` VALUES ('54781', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-08 13:41:16', '2023-07-08 13:41:16');
INSERT INTO `lc_error_log` VALUES ('54782', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-08 13:45:05', '2023-07-08 13:45:05');
INSERT INTO `lc_error_log` VALUES ('54783', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-08 13:51:26', '2023-07-08 13:51:26');
INSERT INTO `lc_error_log` VALUES ('54784', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.32.106', '2023-07-08 13:51:29', '2023-07-08 13:51:29');
INSERT INTO `lc_error_log` VALUES ('54785', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.103', '2023-07-08 13:51:37', '2023-07-08 13:51:37');
INSERT INTO `lc_error_log` VALUES ('54786', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '116.179.37.148', '2023-07-08 13:51:39', '2023-07-08 13:51:39');
INSERT INTO `lc_error_log` VALUES ('54787', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-08 13:52:48', '2023-07-08 13:52:48');
INSERT INTO `lc_error_log` VALUES ('54788', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-07-08 14:04:58', '2023-07-08 14:04:58');
INSERT INTO `lc_error_log` VALUES ('54789', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-08 16:43:24', '2023-07-08 16:43:24');
INSERT INTO `lc_error_log` VALUES ('54790', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '113.125.35.168', '2023-07-08 19:35:11', '2023-07-08 19:35:11');
INSERT INTO `lc_error_log` VALUES ('54791', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.26', '2023-07-08 19:42:25', '2023-07-08 19:42:25');
INSERT INTO `lc_error_log` VALUES ('54792', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.198', '2023-07-08 20:54:45', '2023-07-08 20:54:45');
INSERT INTO `lc_error_log` VALUES ('54793', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-09 07:02:22', '2023-07-09 07:02:22');
INSERT INTO `lc_error_log` VALUES ('54794', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:30', '2023-07-09 09:50:30');
INSERT INTO `lc_error_log` VALUES ('54795', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:30', '2023-07-09 09:50:30');
INSERT INTO `lc_error_log` VALUES ('54796', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:31', '2023-07-09 09:50:31');
INSERT INTO `lc_error_log` VALUES ('54797', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '123.56.155.157', '2023-07-09 09:50:31', '2023-07-09 09:50:31');
INSERT INTO `lc_error_log` VALUES ('54798', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:31', '2023-07-09 09:50:31');
INSERT INTO `lc_error_log` VALUES ('54799', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:31', '2023-07-09 09:50:31');
INSERT INTO `lc_error_log` VALUES ('54800', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '123.56.155.157', '2023-07-09 09:50:31', '2023-07-09 09:50:31');
INSERT INTO `lc_error_log` VALUES ('54801', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:31', '2023-07-09 09:50:31');
INSERT INTO `lc_error_log` VALUES ('54802', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:32', '2023-07-09 09:50:32');
INSERT INTO `lc_error_log` VALUES ('54803', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:46', '2023-07-09 09:50:46');
INSERT INTO `lc_error_log` VALUES ('54804', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.56.155.157', '2023-07-09 09:50:47', '2023-07-09 09:50:47');
INSERT INTO `lc_error_log` VALUES ('54805', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.27', '2023-07-09 12:41:05', '2023-07-09 12:41:05');
INSERT INTO `lc_error_log` VALUES ('54806', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.228.217', '2023-07-09 14:48:13', '2023-07-09 14:48:13');
INSERT INTO `lc_error_log` VALUES ('54807', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请填写描述', '0', '524', '/v1/mp/kecheng', '117.189.228.217', '2023-07-09 14:49:16', '2023-07-09 14:49:16');
INSERT INTO `lc_error_log` VALUES ('54808', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-09 14:56:43', '2023-07-09 14:56:43');
INSERT INTO `lc_error_log` VALUES ('54809', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.4', '2023-07-09 15:32:54', '2023-07-09 15:32:54');
INSERT INTO `lc_error_log` VALUES ('54810', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-07-09 15:35:28', '2023-07-09 15:35:28');
INSERT INTO `lc_error_log` VALUES ('54811', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-07-09 15:35:29', '2023-07-09 15:35:29');
INSERT INTO `lc_error_log` VALUES ('54812', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '101.68.211.2', '2023-07-09 16:56:00', '2023-07-09 16:56:00');
INSERT INTO `lc_error_log` VALUES ('54813', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '101.68.211.2', '2023-07-09 16:56:00', '2023-07-09 16:56:00');
INSERT INTO `lc_error_log` VALUES ('54814', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '101.68.211.2', '2023-07-09 16:58:45', '2023-07-09 16:58:45');
INSERT INTO `lc_error_log` VALUES ('54815', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '101.68.211.2', '2023-07-09 16:58:45', '2023-07-09 16:58:45');
INSERT INTO `lc_error_log` VALUES ('54816', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-07-09 20:38:31', '2023-07-09 20:38:31');
INSERT INTO `lc_error_log` VALUES ('54817', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-09 23:28:48', '2023-07-09 23:28:48');
INSERT INTO `lc_error_log` VALUES ('54818', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-09 23:31:57', '2023-07-09 23:31:57');
INSERT INTO `lc_error_log` VALUES ('54819', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.187.173.72', '2023-07-09 23:45:12', '2023-07-09 23:45:12');
INSERT INTO `lc_error_log` VALUES ('54820', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-10 01:03:34', '2023-07-10 01:03:34');
INSERT INTO `lc_error_log` VALUES ('54821', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.50.170.109', '2023-07-10 03:10:54', '2023-07-10 03:10:54');
INSERT INTO `lc_error_log` VALUES ('54822', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-07-10 03:44:23', '2023-07-10 03:44:23');
INSERT INTO `lc_error_log` VALUES ('54823', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.3.146.202', '2023-07-10 03:44:27', '2023-07-10 03:44:27');
INSERT INTO `lc_error_log` VALUES ('54824', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-07-10 11:05:00', '2023-07-10 11:05:00');
INSERT INTO `lc_error_log` VALUES ('54825', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-07-10 12:57:15', '2023-07-10 12:57:15');
INSERT INTO `lc_error_log` VALUES ('54826', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '125.36.101.73', '2023-07-10 14:03:45', '2023-07-10 14:03:45');
INSERT INTO `lc_error_log` VALUES ('54827', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-10 15:51:56', '2023-07-10 15:51:56');
INSERT INTO `lc_error_log` VALUES ('54828', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-10 15:53:16', '2023-07-10 15:53:16');
INSERT INTO `lc_error_log` VALUES ('54829', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-10 15:55:25', '2023-07-10 15:55:25');
INSERT INTO `lc_error_log` VALUES ('54830', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-10 19:30:07', '2023-07-10 19:30:07');
INSERT INTO `lc_error_log` VALUES ('54831', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-10 19:39:03', '2023-07-10 19:39:03');
INSERT INTO `lc_error_log` VALUES ('54832', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '39.173.107.107', '2023-07-10 19:50:36', '2023-07-10 19:50:36');
INSERT INTO `lc_error_log` VALUES ('54833', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '117.152.73.162', '2023-07-10 20:30:12', '2023-07-10 20:30:12');
INSERT INTO `lc_error_log` VALUES ('54834', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-10 20:46:55', '2023-07-10 20:46:55');
INSERT INTO `lc_error_log` VALUES ('54835', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-10 20:47:25', '2023-07-10 20:47:25');
INSERT INTO `lc_error_log` VALUES ('54836', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-07-10 20:53:27', '2023-07-10 20:53:27');
INSERT INTO `lc_error_log` VALUES ('54837', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-07-10 20:54:09', '2023-07-10 20:54:09');
INSERT INTO `lc_error_log` VALUES ('54838', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-07-10 20:55:19', '2023-07-10 20:55:19');
INSERT INTO `lc_error_log` VALUES ('54839', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-10 21:29:09', '2023-07-10 21:29:09');
INSERT INTO `lc_error_log` VALUES ('54840', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-07-10 22:57:19', '2023-07-10 22:57:19');
INSERT INTO `lc_error_log` VALUES ('54841', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanInfo?id=27765', '101.43.98.47', '2023-07-10 23:10:24', '2023-07-10 23:10:24');
INSERT INTO `lc_error_log` VALUES ('54842', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getErrorLog?page=1&pageSize=20', '101.43.98.47', '2023-07-10 23:51:44', '2023-07-10 23:51:44');
INSERT INTO `lc_error_log` VALUES ('54843', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-07-10 23:52:35', '2023-07-10 23:52:35');
INSERT INTO `lc_error_log` VALUES ('54844', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '202.120.37.109', '2023-07-11 00:12:52', '2023-07-11 00:12:52');
INSERT INTO `lc_error_log` VALUES ('54845', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.176.99', '2023-07-11 02:06:40', '2023-07-11 02:06:40');
INSERT INTO `lc_error_log` VALUES ('54846', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.31.6', '2023-07-11 04:29:33', '2023-07-11 04:29:33');
INSERT INTO `lc_error_log` VALUES ('54847', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-11 10:37:38', '2023-07-11 10:37:38');
INSERT INTO `lc_error_log` VALUES ('54848', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-11 10:42:58', '2023-07-11 10:42:58');
INSERT INTO `lc_error_log` VALUES ('54849', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-07-11 10:43:42', '2023-07-11 10:43:42');
INSERT INTO `lc_error_log` VALUES ('54850', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-11 10:44:27', '2023-07-11 10:44:27');
INSERT INTO `lc_error_log` VALUES ('54851', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-11 10:46:58', '2023-07-11 10:46:58');
INSERT INTO `lc_error_log` VALUES ('54852', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.171.21.15', '2023-07-11 12:42:29', '2023-07-11 12:42:29');
INSERT INTO `lc_error_log` VALUES ('54853', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-11 13:30:39', '2023-07-11 13:30:39');
INSERT INTO `lc_error_log` VALUES ('54854', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '120.230.23.255', '2023-07-11 14:07:19', '2023-07-11 14:07:19');
INSERT INTO `lc_error_log` VALUES ('54855', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '120.230.23.255', '2023-07-11 14:07:34', '2023-07-11 14:07:34');
INSERT INTO `lc_error_log` VALUES ('54856', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '120.230.23.255', '2023-07-11 14:30:15', '2023-07-11 14:30:15');
INSERT INTO `lc_error_log` VALUES ('54857', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '120.230.23.255', '2023-07-11 14:30:19', '2023-07-11 14:30:19');
INSERT INTO `lc_error_log` VALUES ('54858', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '请勿重复提交订单', '0', '210', '/v1/submitKechengOrder', '120.230.23.255', '2023-07-11 14:42:53', '2023-07-11 14:42:53');
INSERT INTO `lc_error_log` VALUES ('54859', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '120.230.23.255', '2023-07-11 14:46:45', '2023-07-11 14:46:45');
INSERT INTO `lc_error_log` VALUES ('54860', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '120.230.23.255', '2023-07-11 14:46:45', '2023-07-11 14:46:45');
INSERT INTO `lc_error_log` VALUES ('54861', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '120.230.23.255', '2023-07-11 14:46:55', '2023-07-11 14:46:55');
INSERT INTO `lc_error_log` VALUES ('54862', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '120.230.23.255', '2023-07-11 14:46:55', '2023-07-11 14:46:55');
INSERT INTO `lc_error_log` VALUES ('54863', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '120.230.23.255', '2023-07-11 14:46:57', '2023-07-11 14:46:57');
INSERT INTO `lc_error_log` VALUES ('54864', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '120.230.23.255', '2023-07-11 14:46:57', '2023-07-11 14:46:57');
INSERT INTO `lc_error_log` VALUES ('54865', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.44.118.160', '2023-07-11 15:04:11', '2023-07-11 15:04:11');
INSERT INTO `lc_error_log` VALUES ('54866', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-11 15:16:14', '2023-07-11 15:16:14');
INSERT INTO `lc_error_log` VALUES ('54867', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-11 15:29:50', '2023-07-11 15:29:50');
INSERT INTO `lc_error_log` VALUES ('54868', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-07-11 15:37:21', '2023-07-11 15:37:21');
INSERT INTO `lc_error_log` VALUES ('54869', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-11 15:44:01', '2023-07-11 15:44:01');
INSERT INTO `lc_error_log` VALUES ('54870', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.215.113.210', '2023-07-11 18:29:59', '2023-07-11 18:29:59');
INSERT INTO `lc_error_log` VALUES ('54871', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.227.131', '2023-07-11 21:11:18', '2023-07-11 21:11:18');
INSERT INTO `lc_error_log` VALUES ('54872', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.227.131', '2023-07-11 21:11:18', '2023-07-11 21:11:18');
INSERT INTO `lc_error_log` VALUES ('54873', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-11 21:22:54', '2023-07-11 21:22:54');
INSERT INTO `lc_error_log` VALUES ('54874', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.227.131', '2023-07-11 21:28:00', '2023-07-11 21:28:00');
INSERT INTO `lc_error_log` VALUES ('54875', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.227.131', '2023-07-11 21:28:00', '2023-07-11 21:28:00');
INSERT INTO `lc_error_log` VALUES ('54876', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:38:42', '2023-07-11 23:38:42');
INSERT INTO `lc_error_log` VALUES ('54877', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:42:31', '2023-07-11 23:42:31');
INSERT INTO `lc_error_log` VALUES ('54878', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:45:14', '2023-07-11 23:45:14');
INSERT INTO `lc_error_log` VALUES ('54879', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:47:13', '2023-07-11 23:47:13');
INSERT INTO `lc_error_log` VALUES ('54880', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:48:12', '2023-07-11 23:48:12');
INSERT INTO `lc_error_log` VALUES ('54881', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:48:25', '2023-07-11 23:48:25');
INSERT INTO `lc_error_log` VALUES ('54882', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:49:16', '2023-07-11 23:49:16');
INSERT INTO `lc_error_log` VALUES ('54883', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:49:24', '2023-07-11 23:49:24');
INSERT INTO `lc_error_log` VALUES ('54884', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-11 23:50:58', '2023-07-11 23:50:58');
INSERT INTO `lc_error_log` VALUES ('54885', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-12 06:21:10', '2023-07-12 06:21:10');
INSERT INTO `lc_error_log` VALUES ('54886', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-12 06:28:42', '2023-07-12 06:28:42');
INSERT INTO `lc_error_log` VALUES ('54887', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-07-12 08:14:58', '2023-07-12 08:14:58');
INSERT INTO `lc_error_log` VALUES ('54888', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-12 09:02:17', '2023-07-12 09:02:17');
INSERT INTO `lc_error_log` VALUES ('54889', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-12 16:45:10', '2023-07-12 16:45:10');
INSERT INTO `lc_error_log` VALUES ('54890', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-12 16:47:15', '2023-07-12 16:47:15');
INSERT INTO `lc_error_log` VALUES ('54891', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-12 18:06:52', '2023-07-12 18:06:52');
INSERT INTO `lc_error_log` VALUES ('54892', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.21', '2023-07-12 18:25:24', '2023-07-12 18:25:24');
INSERT INTO `lc_error_log` VALUES ('54893', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '112.49.97.7', '2023-07-12 23:43:47', '2023-07-12 23:43:47');
INSERT INTO `lc_error_log` VALUES ('54894', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '112.49.97.7', '2023-07-12 23:47:52', '2023-07-12 23:47:52');
INSERT INTO `lc_error_log` VALUES ('54895', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.31', '2023-07-13 00:30:21', '2023-07-13 00:30:21');
INSERT INTO `lc_error_log` VALUES ('54896', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-13 00:51:08', '2023-07-13 00:51:08');
INSERT INTO `lc_error_log` VALUES ('54897', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-13 00:51:17', '2023-07-13 00:51:17');
INSERT INTO `lc_error_log` VALUES ('54898', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.15', '2023-07-13 06:21:32', '2023-07-13 06:21:32');
INSERT INTO `lc_error_log` VALUES ('54899', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '125.93.110.107', '2023-07-13 08:07:41', '2023-07-13 08:07:41');
INSERT INTO `lc_error_log` VALUES ('54900', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-13 09:18:16', '2023-07-13 09:18:16');
INSERT INTO `lc_error_log` VALUES ('54901', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-13 09:18:35', '2023-07-13 09:18:35');
INSERT INTO `lc_error_log` VALUES ('54902', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-07-13 09:23:38', '2023-07-13 09:23:38');
INSERT INTO `lc_error_log` VALUES ('54903', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getList?category_id=&title=&page=1&pageSize=20', '101.43.98.47', '2023-07-13 09:25:45', '2023-07-13 09:25:45');
INSERT INTO `lc_error_log` VALUES ('54904', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-07-13 09:26:07', '2023-07-13 09:26:07');
INSERT INTO `lc_error_log` VALUES ('54905', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-13 09:26:19', '2023-07-13 09:26:19');
INSERT INTO `lc_error_log` VALUES ('54906', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//article/getCategorys', '101.43.98.47', '2023-07-13 09:26:49', '2023-07-13 09:26:49');
INSERT INTO `lc_error_log` VALUES ('54907', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.100.35', '2023-07-13 09:31:42', '2023-07-13 09:31:42');
INSERT INTO `lc_error_log` VALUES ('54908', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.161', '2023-07-13 09:31:42', '2023-07-13 09:31:42');
INSERT INTO `lc_error_log` VALUES ('54909', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.161', '2023-07-13 09:31:54', '2023-07-13 09:31:54');
INSERT INTO `lc_error_log` VALUES ('54910', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.163', '2023-07-13 09:31:55', '2023-07-13 09:31:55');
INSERT INTO `lc_error_log` VALUES ('54911', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-07-13 09:44:25', '2023-07-13 09:44:25');
INSERT INTO `lc_error_log` VALUES ('54912', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '101.82.113.211', '2023-07-13 12:26:15', '2023-07-13 12:26:15');
INSERT INTO `lc_error_log` VALUES ('54913', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-13 13:48:25', '2023-07-13 13:48:25');
INSERT INTO `lc_error_log` VALUES ('54914', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-13 13:50:29', '2023-07-13 13:50:29');
INSERT INTO `lc_error_log` VALUES ('54915', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '125.36.101.73', '2023-07-13 15:45:58', '2023-07-13 15:45:58');
INSERT INTO `lc_error_log` VALUES ('54916', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-13 16:24:44', '2023-07-13 16:24:44');
INSERT INTO `lc_error_log` VALUES ('54917', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-07-13 17:32:03', '2023-07-13 17:32:03');
INSERT INTO `lc_error_log` VALUES ('54918', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-07-13 17:32:05', '2023-07-13 17:32:05');
INSERT INTO `lc_error_log` VALUES ('54919', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-07-13 19:19:06', '2023-07-13 19:19:06');
INSERT INTO `lc_error_log` VALUES ('54920', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.28', '2023-07-13 19:19:07', '2023-07-13 19:19:07');
INSERT INTO `lc_error_log` VALUES ('54921', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '124.162.243.24', '2023-07-13 21:17:59', '2023-07-13 21:17:59');
INSERT INTO `lc_error_log` VALUES ('54922', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '40.77.189.210', '2023-07-13 21:37:56', '2023-07-13 21:37:56');
INSERT INTO `lc_error_log` VALUES ('54923', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-13 22:40:06', '2023-07-13 22:40:06');
INSERT INTO `lc_error_log` VALUES ('54924', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-07-13 22:49:49', '2023-07-13 22:49:49');
INSERT INTO `lc_error_log` VALUES ('54925', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-13 22:51:03', '2023-07-13 22:51:03');
INSERT INTO `lc_error_log` VALUES ('54926', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-13 22:56:15', '2023-07-13 22:56:15');
INSERT INTO `lc_error_log` VALUES ('54927', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=2&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-13 23:38:46', '2023-07-13 23:38:46');
INSERT INTO `lc_error_log` VALUES ('54928', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/getCollectList?page=1&pageSize=20', '120.230.23.255', '2023-07-13 23:39:59', '2023-07-13 23:39:59');
INSERT INTO `lc_error_log` VALUES ('54929', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-13 23:56:32', '2023-07-13 23:56:32');
INSERT INTO `lc_error_log` VALUES ('54930', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-14 00:01:08', '2023-07-14 00:01:08');
INSERT INTO `lc_error_log` VALUES ('54931', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.7', '2023-07-14 01:04:33', '2023-07-14 01:04:33');
INSERT INTO `lc_error_log` VALUES ('54932', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.25', '2023-07-14 03:33:53', '2023-07-14 03:33:53');
INSERT INTO `lc_error_log` VALUES ('54933', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-14 08:06:45', '2023-07-14 08:06:45');
INSERT INTO `lc_error_log` VALUES ('54934', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-14 08:16:43', '2023-07-14 08:16:43');
INSERT INTO `lc_error_log` VALUES ('54935', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-14 09:02:38', '2023-07-14 09:02:38');
INSERT INTO `lc_error_log` VALUES ('54936', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:16:19', '2023-07-14 09:16:19');
INSERT INTO `lc_error_log` VALUES ('54937', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:17:46', '2023-07-14 09:17:46');
INSERT INTO `lc_error_log` VALUES ('54938', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:18:04', '2023-07-14 09:18:04');
INSERT INTO `lc_error_log` VALUES ('54939', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:18:22', '2023-07-14 09:18:22');
INSERT INTO `lc_error_log` VALUES ('54940', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:19:03', '2023-07-14 09:19:03');
INSERT INTO `lc_error_log` VALUES ('54941', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:28:15', '2023-07-14 09:28:15');
INSERT INTO `lc_error_log` VALUES ('54942', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.6.49.18', '2023-07-14 09:29:58', '2023-07-14 09:29:58');
INSERT INTO `lc_error_log` VALUES ('54943', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.6.49.18', '2023-07-14 09:29:58', '2023-07-14 09:29:58');
INSERT INTO `lc_error_log` VALUES ('54944', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '27.115.124.101', '2023-07-14 09:30:05', '2023-07-14 09:30:05');
INSERT INTO `lc_error_log` VALUES ('54945', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:32:17', '2023-07-14 09:32:17');
INSERT INTO `lc_error_log` VALUES ('54946', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:32:39', '2023-07-14 09:32:39');
INSERT INTO `lc_error_log` VALUES ('54947', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:33:39', '2023-07-14 09:33:39');
INSERT INTO `lc_error_log` VALUES ('54948', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:47:37', '2023-07-14 09:47:37');
INSERT INTO `lc_error_log` VALUES ('54949', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 09:51:25', '2023-07-14 09:51:25');
INSERT INTO `lc_error_log` VALUES ('54950', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-07-14 10:04:04', '2023-07-14 10:04:04');
INSERT INTO `lc_error_log` VALUES ('54951', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '113.125.177.184', '2023-07-14 12:09:36', '2023-07-14 12:09:36');
INSERT INTO `lc_error_log` VALUES ('54952', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '183.254.5.149', '2023-07-14 13:16:07', '2023-07-14 13:16:07');
INSERT INTO `lc_error_log` VALUES ('54953', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-14 14:23:46', '2023-07-14 14:23:46');
INSERT INTO `lc_error_log` VALUES ('54954', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 14:45:22', '2023-07-14 14:45:22');
INSERT INTO `lc_error_log` VALUES ('54955', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '117.61.30.220', '2023-07-14 14:50:03', '2023-07-14 14:50:03');
INSERT INTO `lc_error_log` VALUES ('54956', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 14:53:28', '2023-07-14 14:53:28');
INSERT INTO `lc_error_log` VALUES ('54957', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-14 16:06:41', '2023-07-14 16:06:41');
INSERT INTO `lc_error_log` VALUES ('54958', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '125.36.101.73', '2023-07-14 16:27:54', '2023-07-14 16:27:54');
INSERT INTO `lc_error_log` VALUES ('54959', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.4', '2023-07-14 17:14:07', '2023-07-14 17:14:07');
INSERT INTO `lc_error_log` VALUES ('54960', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-14 17:30:50', '2023-07-14 17:30:50');
INSERT INTO `lc_error_log` VALUES ('54961', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-14 19:35:53', '2023-07-14 19:35:53');
INSERT INTO `lc_error_log` VALUES ('54962', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.165', '2023-07-14 19:47:48', '2023-07-14 19:47:48');
INSERT INTO `lc_error_log` VALUES ('54963', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-07-14 19:47:49', '2023-07-14 19:47:49');
INSERT INTO `lc_error_log` VALUES ('54964', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-14 19:52:15', '2023-07-14 19:52:15');
INSERT INTO `lc_error_log` VALUES ('54965', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '117.189.227.131', '2023-07-14 19:56:57', '2023-07-14 19:56:57');
INSERT INTO `lc_error_log` VALUES ('54966', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.227.131', '2023-07-14 19:56:57', '2023-07-14 19:56:57');
INSERT INTO `lc_error_log` VALUES ('54967', 'api', '/www/wwwroot/gouziyuan/app/api/business/MpUser.php', '身份证号已被使用', '0', '70', '/v1/registerMp', '117.189.227.131', '2023-07-14 19:58:08', '2023-07-14 19:58:08');
INSERT INTO `lc_error_log` VALUES ('54968', 'api', '/www/wwwroot/gouziyuan/app/api/business/MpUser.php', '手机号与注册时的手机号不一致', '0', '82', '/v1/registerMp', '117.189.227.131', '2023-07-14 19:58:16', '2023-07-14 19:58:16');
INSERT INTO `lc_error_log` VALUES ('54969', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/tongji/index', '117.189.227.131', '2023-07-14 20:05:09', '2023-07-14 20:05:09');
INSERT INTO `lc_error_log` VALUES ('54970', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthMpMiddleware.php', '无权限', '2', '18', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.227.131', '2023-07-14 20:05:09', '2023-07-14 20:05:09');
INSERT INTO `lc_error_log` VALUES ('54971', 'api', '/www/wwwroot/gouziyuan/app/api/business/MpUser.php', '已是创作者', '5', '137', '/v1/getMpStatus', '117.189.227.131', '2023-07-14 20:05:40', '2023-07-14 20:05:40');
INSERT INTO `lc_error_log` VALUES ('54972', 'api', '/www/wwwroot/gouziyuan/app/api/business/MpUser.php', '已是创作者', '5', '137', '/v1/getMpStatus', '117.189.227.131', '2023-07-14 20:05:45', '2023-07-14 20:05:45');
INSERT INTO `lc_error_log` VALUES ('54973', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.131', '2023-07-14 20:09:00', '2023-07-14 20:09:00');
INSERT INTO `lc_error_log` VALUES ('54974', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//mpAudit', '101.43.98.47', '2023-07-14 20:11:45', '2023-07-14 20:11:45');
INSERT INTO `lc_error_log` VALUES ('54975', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getMpUser?page=1&pageSize=10', '101.43.98.47', '2023-07-14 20:25:25', '2023-07-14 20:25:25');
INSERT INTO `lc_error_log` VALUES ('54976', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-14 21:00:01', '2023-07-14 21:00:01');
INSERT INTO `lc_error_log` VALUES ('54977', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:19', '2023-07-14 21:36:19');
INSERT INTO `lc_error_log` VALUES ('54978', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:19', '2023-07-14 21:36:19');
INSERT INTO `lc_error_log` VALUES ('54979', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:19', '2023-07-14 21:36:19');
INSERT INTO `lc_error_log` VALUES ('54980', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPE9568F36-D428-11d2-A769-00AA001ACF42', '47.101.154.149', '2023-07-14 21:36:20', '2023-07-14 21:36:20');
INSERT INTO `lc_error_log` VALUES ('54981', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:20', '2023-07-14 21:36:20');
INSERT INTO `lc_error_log` VALUES ('54982', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:20', '2023-07-14 21:36:20');
INSERT INTO `lc_error_log` VALUES ('54983', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/?=PHPB8B5F2A0-3C92-11d3-A3A9-4C7B08C10000', '47.101.154.149', '2023-07-14 21:36:20', '2023-07-14 21:36:20');
INSERT INTO `lc_error_log` VALUES ('54984', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:20', '2023-07-14 21:36:20');
INSERT INTO `lc_error_log` VALUES ('54985', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:20', '2023-07-14 21:36:20');
INSERT INTO `lc_error_log` VALUES ('54986', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:37', '2023-07-14 21:36:37');
INSERT INTO `lc_error_log` VALUES ('54987', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '47.101.154.149', '2023-07-14 21:36:37', '2023-07-14 21:36:37');
INSERT INTO `lc_error_log` VALUES ('54988', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserTixian?page=1&pageSize=20', '101.43.98.47', '2023-07-14 22:27:35', '2023-07-14 22:27:35');
INSERT INTO `lc_error_log` VALUES ('54989', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-14 22:38:15', '2023-07-14 22:38:15');
INSERT INTO `lc_error_log` VALUES ('54990', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-14 22:38:15', '2023-07-14 22:38:15');
INSERT INTO `lc_error_log` VALUES ('54991', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.18', '2023-07-15 09:43:25', '2023-07-15 09:43:25');
INSERT INTO `lc_error_log` VALUES ('54992', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '175.16.225.84', '2023-07-15 10:02:52', '2023-07-15 10:02:52');
INSERT INTO `lc_error_log` VALUES ('54993', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-07-15 14:02:02', '2023-07-15 14:02:02');
INSERT INTO `lc_error_log` VALUES ('54994', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.46', '2023-07-15 14:02:02', '2023-07-15 14:02:02');
INSERT INTO `lc_error_log` VALUES ('54995', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '106.47.8.96', '2023-07-15 14:46:17', '2023-07-15 14:46:17');
INSERT INTO `lc_error_log` VALUES ('54996', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '119.123.153.5', '2023-07-15 15:39:50', '2023-07-15 15:39:50');
INSERT INTO `lc_error_log` VALUES ('54997', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-07-15 21:25:57', '2023-07-15 21:25:57');
INSERT INTO `lc_error_log` VALUES ('54998', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-07-15 21:25:58', '2023-07-15 21:25:58');
INSERT INTO `lc_error_log` VALUES ('54999', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-16 00:31:25', '2023-07-16 00:31:25');
INSERT INTO `lc_error_log` VALUES ('55000', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-16 00:36:03', '2023-07-16 00:36:03');
INSERT INTO `lc_error_log` VALUES ('55001', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-16 00:37:24', '2023-07-16 00:37:24');
INSERT INTO `lc_error_log` VALUES ('55002', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-16 00:41:17', '2023-07-16 00:41:17');
INSERT INTO `lc_error_log` VALUES ('55003', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.79.7', '2023-07-16 00:41:46', '2023-07-16 00:41:46');
INSERT INTO `lc_error_log` VALUES ('55004', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=79&info=true', '66.249.79.5', '2023-07-16 00:41:51', '2023-07-16 00:41:51');
INSERT INTO `lc_error_log` VALUES ('55005', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-16 01:18:48', '2023-07-16 01:18:48');
INSERT INTO `lc_error_log` VALUES ('55006', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-16 04:50:27', '2023-07-16 04:50:27');
INSERT INTO `lc_error_log` VALUES ('55007', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-16 04:50:27', '2023-07-16 04:50:27');
INSERT INTO `lc_error_log` VALUES ('55008', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.19', '2023-07-16 07:41:53', '2023-07-16 07:41:53');
INSERT INTO `lc_error_log` VALUES ('55009', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.19', '2023-07-16 07:41:55', '2023-07-16 07:41:55');
INSERT INTO `lc_error_log` VALUES ('55010', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-07-16 09:08:40', '2023-07-16 09:08:40');
INSERT INTO `lc_error_log` VALUES ('55011', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-16 09:21:13', '2023-07-16 09:21:13');
INSERT INTO `lc_error_log` VALUES ('55012', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-16 12:17:04', '2023-07-16 12:17:04');
INSERT INTO `lc_error_log` VALUES ('55013', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-16 12:17:04', '2023-07-16 12:17:04');
INSERT INTO `lc_error_log` VALUES ('55014', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitKechengOrder', '117.189.215.253', '2023-07-16 17:12:03', '2023-07-16 17:12:03');
INSERT INTO `lc_error_log` VALUES ('55015', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-16 17:51:51', '2023-07-16 17:51:51');
INSERT INTO `lc_error_log` VALUES ('55016', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-16 17:52:48', '2023-07-16 17:52:48');
INSERT INTO `lc_error_log` VALUES ('55017', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-16 17:57:28', '2023-07-16 17:57:28');
INSERT INTO `lc_error_log` VALUES ('55018', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '124.240.53.111', '2023-07-16 22:15:45', '2023-07-16 22:15:45');
INSERT INTO `lc_error_log` VALUES ('55019', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '124.240.53.111', '2023-07-16 22:16:44', '2023-07-16 22:16:44');
INSERT INTO `lc_error_log` VALUES ('55020', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '124.240.53.111', '2023-07-16 22:16:54', '2023-07-16 22:16:54');
INSERT INTO `lc_error_log` VALUES ('55021', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '124.240.53.111', '2023-07-16 22:17:07', '2023-07-16 22:17:07');
INSERT INTO `lc_error_log` VALUES ('55022', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '124.240.53.111', '2023-07-16 22:17:09', '2023-07-16 22:17:09');
INSERT INTO `lc_error_log` VALUES ('55023', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '124.240.53.111', '2023-07-16 22:17:42', '2023-07-16 22:17:42');
INSERT INTO `lc_error_log` VALUES ('55024', 'api', '/www/wwwroot/gouziyuan/app/api/business/Order.php', '请勿重复提交订单', '0', '59', '/v1/submitOrder', '124.240.53.111', '2023-07-16 22:18:01', '2023-07-16 22:18:01');
INSERT INTO `lc_error_log` VALUES ('55025', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-07-17 01:15:34', '2023-07-17 01:15:34');
INSERT INTO `lc_error_log` VALUES ('55026', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-17 01:40:36', '2023-07-17 01:40:36');
INSERT INTO `lc_error_log` VALUES ('55027', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-17 01:47:04', '2023-07-17 01:47:04');
INSERT INTO `lc_error_log` VALUES ('55028', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '66.249.79.6', '2023-07-17 02:30:00', '2023-07-17 02:30:00');
INSERT INTO `lc_error_log` VALUES ('55029', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.70.50', '2023-07-17 03:20:17', '2023-07-17 03:20:17');
INSERT INTO `lc_error_log` VALUES ('55030', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-07-17 04:06:28', '2023-07-17 04:06:28');
INSERT INTO `lc_error_log` VALUES ('55031', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '111.7.96.170', '2023-07-17 04:06:37', '2023-07-17 04:06:37');
INSERT INTO `lc_error_log` VALUES ('55032', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '106.75.117.86', '2023-07-17 06:23:20', '2023-07-17 06:23:20');
INSERT INTO `lc_error_log` VALUES ('55033', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/setCollect', '223.95.31.104', '2023-07-17 06:42:38', '2023-07-17 06:42:38');
INSERT INTO `lc_error_log` VALUES ('55034', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '223.95.31.104', '2023-07-17 06:46:09', '2023-07-17 06:46:09');
INSERT INTO `lc_error_log` VALUES ('55035', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.54', '2023-07-17 07:54:52', '2023-07-17 07:54:52');
INSERT INTO `lc_error_log` VALUES ('55036', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-17 09:09:28', '2023-07-17 09:09:28');
INSERT INTO `lc_error_log` VALUES ('55037', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//smsLog?page=1&pageSize=20&phone=', '101.43.98.47', '2023-07-17 09:11:09', '2023-07-17 09:11:09');
INSERT INTO `lc_error_log` VALUES ('55038', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getErrorLog?page=1&pageSize=20', '101.43.98.47', '2023-07-17 09:17:57', '2023-07-17 09:17:57');
INSERT INTO `lc_error_log` VALUES ('55039', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-17 09:23:29', '2023-07-17 09:23:29');
INSERT INTO `lc_error_log` VALUES ('55040', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '58.214.121.126', '2023-07-17 10:36:36', '2023-07-17 10:36:36');
INSERT INTO `lc_error_log` VALUES ('55041', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '182.140.132.119', '2023-07-17 11:13:09', '2023-07-17 11:13:09');
INSERT INTO `lc_error_log` VALUES ('55042', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-17 11:46:12', '2023-07-17 11:46:12');
INSERT INTO `lc_error_log` VALUES ('55043', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-17 14:54:34', '2023-07-17 14:54:34');
INSERT INTO `lc_error_log` VALUES ('55044', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '101.227.1.199', '2023-07-17 15:16:33', '2023-07-17 15:16:33');
INSERT INTO `lc_error_log` VALUES ('55045', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-17 20:40:39', '2023-07-17 20:40:39');
INSERT INTO `lc_error_log` VALUES ('55046', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-17 21:04:21', '2023-07-17 21:04:21');
INSERT INTO `lc_error_log` VALUES ('55047', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-17 21:04:21', '2023-07-17 21:04:21');
INSERT INTO `lc_error_log` VALUES ('55048', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-17 23:01:10', '2023-07-17 23:01:10');
INSERT INTO `lc_error_log` VALUES ('55049', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-17 23:01:19', '2023-07-17 23:01:19');
INSERT INTO `lc_error_log` VALUES ('55050', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-17 23:06:00', '2023-07-17 23:06:00');
INSERT INTO `lc_error_log` VALUES ('55051', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '222.186.48.204', '2023-07-18 05:41:38', '2023-07-18 05:41:38');
INSERT INTO `lc_error_log` VALUES ('55052', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.44', '2023-07-18 06:44:28', '2023-07-18 06:44:28');
INSERT INTO `lc_error_log` VALUES ('55053', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-18 06:55:37', '2023-07-18 06:55:37');
INSERT INTO `lc_error_log` VALUES ('55054', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-18 06:55:37', '2023-07-18 06:55:37');
INSERT INTO `lc_error_log` VALUES ('55055', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-07-18 07:57:52', '2023-07-18 07:57:52');
INSERT INTO `lc_error_log` VALUES ('55056', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-18 10:13:45', '2023-07-18 10:13:45');
INSERT INTO `lc_error_log` VALUES ('55057', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-18 11:28:27', '2023-07-18 11:28:27');
INSERT INTO `lc_error_log` VALUES ('55058', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-18 11:28:40', '2023-07-18 11:28:40');
INSERT INTO `lc_error_log` VALUES ('55059', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-18 11:28:51', '2023-07-18 11:28:51');
INSERT INTO `lc_error_log` VALUES ('55060', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-07-18 11:48:32', '2023-07-18 11:48:32');
INSERT INTO `lc_error_log` VALUES ('55061', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-18 14:57:18', '2023-07-18 14:57:18');
INSERT INTO `lc_error_log` VALUES ('55062', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-18 15:03:43', '2023-07-18 15:03:43');
INSERT INTO `lc_error_log` VALUES ('55063', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-18 18:35:15', '2023-07-18 18:35:15');
INSERT INTO `lc_error_log` VALUES ('55064', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '183.136.225.32', '2023-07-18 18:47:50', '2023-07-18 18:47:50');
INSERT INTO `lc_error_log` VALUES ('55065', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.52', '2023-07-18 21:49:41', '2023-07-18 21:49:41');
INSERT INTO `lc_error_log` VALUES ('55066', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.6.49.47', '2023-07-18 22:08:26', '2023-07-18 22:08:26');
INSERT INTO `lc_error_log` VALUES ('55067', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.6.49.42', '2023-07-18 22:08:33', '2023-07-18 22:08:33');
INSERT INTO `lc_error_log` VALUES ('55068', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-18 22:26:49', '2023-07-18 22:26:49');
INSERT INTO `lc_error_log` VALUES ('55069', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=78&info=true', '117.189.227.240', '2023-07-18 22:50:31', '2023-07-18 22:50:31');
INSERT INTO `lc_error_log` VALUES ('55070', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '99', '/v1/getZiyuanInfo?id=78&info=true', '117.189.227.240', '2023-07-18 22:50:31', '2023-07-18 22:50:31');
INSERT INTO `lc_error_log` VALUES ('55071', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//kecheng/list?page=1&pageSize=20&keyword=', '101.43.98.47', '2023-07-18 23:09:57', '2023-07-18 23:09:57');
INSERT INTO `lc_error_log` VALUES ('55072', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-18 23:09:57', '2023-07-18 23:09:57');
INSERT INTO `lc_error_log` VALUES ('55073', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/tongji/index', '117.189.227.240', '2023-07-18 23:35:42', '2023-07-18 23:35:42');
INSERT INTO `lc_error_log` VALUES ('55074', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '未登录', '-1', '25', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '117.189.227.240', '2023-07-18 23:35:42', '2023-07-18 23:35:42');
INSERT INTO `lc_error_log` VALUES ('55075', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-19 00:36:14', '2023-07-19 00:36:14');
INSERT INTO `lc_error_log` VALUES ('55076', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanInfo?id=27776', '101.43.98.47', '2023-07-19 01:30:53', '2023-07-19 01:30:53');
INSERT INTO `lc_error_log` VALUES ('55077', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '110.251.52.154', '2023-07-19 02:00:57', '2023-07-19 02:00:57');
INSERT INTO `lc_error_log` VALUES ('55078', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.133', '2023-07-19 03:08:34', '2023-07-19 03:08:34');
INSERT INTO `lc_error_log` VALUES ('55079', 'api', '/www/wwwroot/gouziyuan/app/api/business/Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=2&order_id=&chapters_id=&directory_id=', '116.179.37.231', '2023-07-19 03:08:36', '2023-07-19 03:08:36');
INSERT INTO `lc_error_log` VALUES ('55080', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.4', '2023-07-19 07:52:12', '2023-07-19 07:52:12');
INSERT INTO `lc_error_log` VALUES ('55081', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-07-19 07:54:09', '2023-07-19 07:54:09');
INSERT INTO `lc_error_log` VALUES ('55082', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.27', '2023-07-19 07:54:10', '2023-07-19 07:54:10');
INSERT INTO `lc_error_log` VALUES ('55083', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '117.62.218.192', '2023-07-19 09:19:09', '2023-07-19 09:19:09');
INSERT INTO `lc_error_log` VALUES ('55084', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-19 09:56:05', '2023-07-19 09:56:05');
INSERT INTO `lc_error_log` VALUES ('55085', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '183.254.5.128', '2023-07-19 11:13:37', '2023-07-19 11:13:37');
INSERT INTO `lc_error_log` VALUES ('55086', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '183.254.5.128', '2023-07-19 11:26:46', '2023-07-19 11:26:46');
INSERT INTO `lc_error_log` VALUES ('55087', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '183.254.5.128', '2023-07-19 11:28:32', '2023-07-19 11:28:32');
INSERT INTO `lc_error_log` VALUES ('55088', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.254.5.128', '2023-07-19 11:35:44', '2023-07-19 11:35:44');
INSERT INTO `lc_error_log` VALUES ('55089', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.254.5.128', '2023-07-19 11:36:07', '2023-07-19 11:36:07');
INSERT INTO `lc_error_log` VALUES ('55090', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.254.5.128', '2023-07-19 11:36:20', '2023-07-19 11:36:20');
INSERT INTO `lc_error_log` VALUES ('55091', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '183.254.5.128', '2023-07-19 11:36:33', '2023-07-19 11:36:33');
INSERT INTO `lc_error_log` VALUES ('55092', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-19 11:46:31', '2023-07-19 11:46:31');
INSERT INTO `lc_error_log` VALUES ('55093', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getDownloadLog?page=1&pageSize=20', '101.43.98.47', '2023-07-19 11:47:28', '2023-07-19 11:47:28');
INSERT INTO `lc_error_log` VALUES ('55094', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '//ziyuanAudit', '101.43.98.47', '2023-07-19 13:36:40', '2023-07-19 13:36:40');
INSERT INTO `lc_error_log` VALUES ('55095', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '123.160.221.17', '2023-07-19 13:51:30', '2023-07-19 13:51:30');
INSERT INTO `lc_error_log` VALUES ('55096', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.136', '2023-07-19 13:53:08', '2023-07-19 13:53:08');
INSERT INTO `lc_error_log` VALUES ('55097', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.128', '2023-07-19 13:53:19', '2023-07-19 13:53:19');
INSERT INTO `lc_error_log` VALUES ('55098', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-07-19 13:53:26', '2023-07-19 13:53:26');
INSERT INTO `lc_error_log` VALUES ('55099', 'platform', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/', '36.99.136.137', '2023-07-19 13:53:29', '2023-07-19 13:53:29');
INSERT INTO `lc_error_log` VALUES ('55100', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.198', '2023-07-19 15:44:57', '2023-07-19 15:44:57');
INSERT INTO `lc_error_log` VALUES ('55101', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '60.27.28.4', '2023-07-19 16:09:14', '2023-07-19 16:09:14');
INSERT INTO `lc_error_log` VALUES ('55102', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '请输入关键词', '0', '465', '/v1/search?keyWord=&category_id=17&page=1&pageSize=20', '60.27.28.4', '2023-07-19 16:16:59', '2023-07-19 16:16:59');
INSERT INTO `lc_error_log` VALUES ('55103', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '请输入关键词', '0', '465', '/v1/search?keyWord=&category_id=17&page=undefined&pageSize=undefined', '60.27.28.4', '2023-07-19 16:17:03', '2023-07-19 16:17:03');
INSERT INTO `lc_error_log` VALUES ('55104', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '115.206.116.30', '2023-07-19 16:22:21', '2023-07-19 16:22:21');
INSERT INTO `lc_error_log` VALUES ('55105', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '60.27.28.4', '2023-07-19 16:23:39', '2023-07-19 16:23:39');
INSERT INTO `lc_error_log` VALUES ('55106', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-19 16:28:48', '2023-07-19 16:28:48');
INSERT INTO `lc_error_log` VALUES ('55107', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '36.113.146.241', '2023-07-19 17:12:05', '2023-07-19 17:12:05');
INSERT INTO `lc_error_log` VALUES ('55108', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '60.27.28.4', '2023-07-19 17:17:26', '2023-07-19 17:17:26');
INSERT INTO `lc_error_log` VALUES ('55109', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.22', '2023-07-19 18:23:16', '2023-07-19 18:23:16');
INSERT INTO `lc_error_log` VALUES ('55110', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/submitOrder', '122.247.70.222', '2023-07-19 19:31:13', '2023-07-19 19:31:13');
INSERT INTO `lc_error_log` VALUES ('55111', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '112.13.112.133', '2023-07-19 23:14:04', '2023-07-19 23:14:04');
INSERT INTO `lc_error_log` VALUES ('55112', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-19 23:53:24', '2023-07-19 23:53:24');
INSERT INTO `lc_error_log` VALUES ('55113', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-20 00:00:41', '2023-07-20 00:00:41');
INSERT INTO `lc_error_log` VALUES ('55114', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Route.php', 'Route Not Found', '0', '799', '/v1/whoami', '101.37.31.36', '2023-07-20 03:14:32', '2023-07-20 03:14:32');
INSERT INTO `lc_error_log` VALUES ('55115', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '210.52.224.27', '2023-07-20 05:38:06', '2023-07-20 05:38:06');
INSERT INTO `lc_error_log` VALUES ('55116', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '211.95.50.5', '2023-07-20 06:04:38', '2023-07-20 06:04:38');
INSERT INTO `lc_error_log` VALUES ('55117', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.30', '2023-07-20 06:05:15', '2023-07-20 06:05:15');
INSERT INTO `lc_error_log` VALUES ('55118', 'api', '/www/wwwroot/gouziyuan/app/api/business/Ziyuan.php', '当前资源不存在或未通过审核', '0', '111', '/v1/getZiyuanInfo?id=27486&info=true', '210.52.224.30', '2023-07-20 06:05:17', '2023-07-20 06:05:17');
INSERT INTO `lc_error_log` VALUES ('55119', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/getOrderList?page=1&pageSize=20', '101.227.1.196', '2023-07-20 07:37:37', '2023-07-20 07:37:37');
INSERT INTO `lc_error_log` VALUES ('55120', 'api', '/www/wwwroot/gouziyuan/vendor/topthink/framework/src/think/Validate.php', '请输入用户名', '0', '524', '/v1/login', '113.116.29.11', '2023-07-20 10:24:36', '2023-07-20 10:24:36');
INSERT INTO `lc_error_log` VALUES ('55121', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-07-20 13:22:38', '2023-07-20 13:22:38');
INSERT INTO `lc_error_log` VALUES ('55122', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:07:20', '2023-07-20 17:07:20');
INSERT INTO `lc_error_log` VALUES ('55123', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:07:27', '2023-07-20 17:07:27');
INSERT INTO `lc_error_log` VALUES ('55124', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:07:33', '2023-07-20 17:07:33');
INSERT INTO `lc_error_log` VALUES ('55125', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:07:38', '2023-07-20 17:07:38');
INSERT INTO `lc_error_log` VALUES ('55126', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:07:43', '2023-07-20 17:07:43');
INSERT INTO `lc_error_log` VALUES ('55127', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:07:49', '2023-07-20 17:07:49');
INSERT INTO `lc_error_log` VALUES ('55128', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:08:00', '2023-07-20 17:08:00');
INSERT INTO `lc_error_log` VALUES ('55129', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:08:06', '2023-07-20 17:08:06');
INSERT INTO `lc_error_log` VALUES ('55130', 'api', '/www/wwwroot/gouziyuan/app/api/middleware/AuthTokenMiddleware.php', '请登录', '-1', '62', '/v1/imgUpload', '117.189.227.240', '2023-07-20 17:08:11', '2023-07-20 17:08:11');
INSERT INTO `lc_error_log` VALUES ('55131', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanCategorys', '101.43.98.47', '2023-07-20 17:18:20', '2023-07-20 17:18:20');
INSERT INTO `lc_error_log` VALUES ('55132', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getZiyuanList?page=1&pageSize=20&status=&category_id=&ziyuan_title=&user_name=', '101.43.98.47', '2023-07-20 17:35:07', '2023-07-20 17:35:07');
INSERT INTO `lc_error_log` VALUES ('55133', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '未登录', '-1', '24', '//indexData', '101.43.98.47', '2023-07-20 19:49:39', '2023-07-20 19:49:39');
INSERT INTO `lc_error_log` VALUES ('55134', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//indexData', '101.43.98.47', '2023-07-20 20:06:23', '2023-07-20 20:06:23');
INSERT INTO `lc_error_log` VALUES ('55135', 'platform', '/www/wwwroot/gouziyuan/app/platform/middleware/AuthTokenMiddleware.php', '请登录', '-1', '60', '//getUserList?page=1&pageSize=20&user_name=&type=&phone=', '101.43.98.47', '2023-07-20 23:03:29', '2023-07-20 23:03:29');
INSERT INTO `lc_error_log` VALUES ('55136', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/v1/getKechengOrderList', '127.0.0.1', '2023-08-03 20:28:28', '2023-08-03 20:28:28');
INSERT INTO `lc_error_log` VALUES ('55137', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'controller not exists:app\\mobile\\controller\\Test', '0', '76', '/test', '127.0.0.1', '2023-08-08 11:16:16', '2023-08-08 11:16:16');
INSERT INTO `lc_error_log` VALUES ('55138', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\controller\\v1\\Test.php', 'Class \'ApiBase\' not found', '0', '6', '/v1/test', '127.0.0.1', '2023-08-08 11:16:29', '2023-08-08 11:16:29');
INSERT INTO `lc_error_log` VALUES ('55139', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\controller\\v1\\Test.php', 'Class \'ApiBase\' not found', '0', '6', '/v1/test', '127.0.0.1', '2023-08-08 11:16:53', '2023-08-08 11:16:53');
INSERT INTO `lc_error_log` VALUES ('55140', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\controller\\v1\\Test.php', 'Class \'ApiBase\' not found', '0', '6', '/v1/test', '127.0.0.1', '2023-08-08 11:16:54', '2023-08-08 11:16:54');
INSERT INTO `lc_error_log` VALUES ('55141', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/smsLog?page=1&pageSize=20&phone=', '127.0.0.1', '2023-08-17 14:45:32', '2023-08-17 14:45:32');
INSERT INTO `lc_error_log` VALUES ('55142', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/indexData', '127.0.0.1', '2023-08-17 15:09:34', '2023-08-17 15:09:34');
INSERT INTO `lc_error_log` VALUES ('55143', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/indexData', '127.0.0.1', '2023-08-17 15:09:35', '2023-08-17 15:09:35');
INSERT INTO `lc_error_log` VALUES ('55144', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/indexData', '127.0.0.1', '2023-08-17 15:09:52', '2023-08-17 15:09:52');
INSERT INTO `lc_error_log` VALUES ('55145', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/indexData', '127.0.0.1', '2023-08-17 15:10:09', '2023-08-17 15:10:09');
INSERT INTO `lc_error_log` VALUES ('55146', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/indexData', '127.0.0.1', '2023-08-17 15:10:35', '2023-08-17 15:10:35');
INSERT INTO `lc_error_log` VALUES ('55147', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/indexData', '127.0.0.1', '2023-08-17 15:10:53', '2023-08-17 15:10:53');
INSERT INTO `lc_error_log` VALUES ('55148', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getUserLoginLog?page=1&pageSize=20', '127.0.0.1', '2023-08-17 15:17:59', '2023-08-17 15:17:59');
INSERT INTO `lc_error_log` VALUES ('55149', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getUserLoginLog?page=1&pageSize=20', '127.0.0.1', '2023-08-17 15:38:38', '2023-08-17 15:38:38');
INSERT INTO `lc_error_log` VALUES ('55150', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getErrorLog?page=1&pageSize=20', '127.0.0.1', '2023-08-17 15:38:40', '2023-08-17 15:38:40');
INSERT INTO `lc_error_log` VALUES ('55151', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getUserLoginLog?page=1&pageSize=20', '127.0.0.1', '2023-08-17 15:38:42', '2023-08-17 15:38:42');
INSERT INTO `lc_error_log` VALUES ('55152', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getUserLoginLog?page=1&pageSize=20', '127.0.0.1', '2023-08-17 15:38:59', '2023-08-17 15:38:59');
INSERT INTO `lc_error_log` VALUES ('55153', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-18 01:27:42', '2023-08-18 01:27:42');
INSERT INTO `lc_error_log` VALUES ('55154', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-18 02:09:44', '2023-08-18 02:09:44');
INSERT INTO `lc_error_log` VALUES ('55155', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-18 10:06:53', '2023-08-18 10:06:53');
INSERT INTO `lc_error_log` VALUES ('55156', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-18 10:37:39', '2023-08-18 10:37:39');
INSERT INTO `lc_error_log` VALUES ('55157', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-18 10:38:12', '2023-08-18 10:38:12');
INSERT INTO `lc_error_log` VALUES ('55158', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-18 10:38:13', '2023-08-18 10:38:13');
INSERT INTO `lc_error_log` VALUES ('55159', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-18 13:03:09', '2023-08-18 13:03:09');
INSERT INTO `lc_error_log` VALUES ('55160', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-21 09:43:04', '2023-08-21 09:43:04');
INSERT INTO `lc_error_log` VALUES ('55161', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-21 09:43:04', '2023-08-21 09:43:04');
INSERT INTO `lc_error_log` VALUES ('55162', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-21 15:39:28', '2023-08-21 15:39:28');
INSERT INTO `lc_error_log` VALUES ('55163', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-21 15:39:28', '2023-08-21 15:39:28');
INSERT INTO `lc_error_log` VALUES ('55164', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-26 11:05:16', '2023-08-26 11:05:16');
INSERT INTO `lc_error_log` VALUES ('55165', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-08-26 11:05:16', '2023-08-26 11:05:16');
INSERT INTO `lc_error_log` VALUES ('55166', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\WeChat.php', '参数错误', '0', '285', '/v1/weChat/getQrCode', '127.0.0.1', '2023-08-27 16:58:26', '2023-08-27 16:58:26');
INSERT INTO `lc_error_log` VALUES ('55167', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\WeChat.php', '参数错误', '0', '285', '/v1/weChat/getQrCode', '127.0.0.1', '2023-08-27 16:58:51', '2023-08-27 16:58:51');
INSERT INTO `lc_error_log` VALUES ('55168', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '请输入关键词', '0', '467', '/v1/search?keyWord=&category_id=1&page=1&pageSize=20', '127.0.0.1', '2023-09-19 23:44:03', '2023-09-19 23:44:03');
INSERT INTO `lc_error_log` VALUES ('55169', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '请输入关键词', '0', '467', '/v1/search?keyWord=&category_id=1&page=undefined&pageSize=undefined', '127.0.0.1', '2023-09-19 23:44:08', '2023-09-19 23:44:08');
INSERT INTO `lc_error_log` VALUES ('55170', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '请输入关键词', '0', '467', '/v1/search?keyWord=&category_id=1&page=1&pageSize=20', '127.0.0.1', '2023-09-19 23:44:38', '2023-09-19 23:44:38');
INSERT INTO `lc_error_log` VALUES ('55171', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '请输入关键词', '0', '467', '/v1/search?keyWord=&category_id=1&page=undefined&pageSize=undefined', '127.0.0.1', '2023-09-19 23:44:52', '2023-09-19 23:44:52');
INSERT INTO `lc_error_log` VALUES ('55172', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-09-20 00:41:20', '2023-09-20 00:41:20');
INSERT INTO `lc_error_log` VALUES ('55173', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-09-20 00:41:20', '2023-09-20 00:41:20');
INSERT INTO `lc_error_log` VALUES ('55174', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/http://api.gzy.com/v1//weChat/getQrCode?type=binding', '127.0.0.1', '2023-09-22 16:36:19', '2023-09-22 16:36:19');
INSERT INTO `lc_error_log` VALUES ('55175', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\api\\controller\\v1\\User->getQrCodeLogin()', '0', '107', '/v1/weChat/getQrCodeLogin', '127.0.0.1', '2023-09-22 16:39:28', '2023-09-22 16:39:28');
INSERT INTO `lc_error_log` VALUES ('55176', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\api\\controller\\v1\\User->getQrCodeLogin()', '0', '107', '/v1/weChat/getQrCodeLogin', '127.0.0.1', '2023-09-22 16:40:17', '2023-09-22 16:40:17');
INSERT INTO `lc_error_log` VALUES ('55177', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/user/getQrCodeLogin', '127.0.0.1', '2023-09-22 16:40:39', '2023-09-22 16:40:39');
INSERT INTO `lc_error_log` VALUES ('55178', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\api\\controller\\v1\\User->getQrCodeLogin()', '0', '107', '/v1/user/getQrCodeLogin', '127.0.0.1', '2023-09-22 16:41:18', '2023-09-22 16:41:18');
INSERT INTO `lc_error_log` VALUES ('55179', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/getQrCodeLogin', '127.0.0.1', '2023-09-22 16:42:06', '2023-09-22 16:42:06');
INSERT INTO `lc_error_log` VALUES ('55180', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '29', '/v1/getQrCodeLogin', '127.0.0.1', '2023-09-22 16:47:43', '2023-09-22 16:47:43');
INSERT INTO `lc_error_log` VALUES ('55181', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '29', '/v1/getQrCodeLogin', '127.0.0.1', '2023-09-24 02:14:47', '2023-09-24 02:14:47');
INSERT INTO `lc_error_log` VALUES ('55182', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/getQrCodeLogin', '127.0.0.1', '2023-09-24 02:17:17', '2023-09-24 02:17:17');
INSERT INTO `lc_error_log` VALUES ('55183', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:29:51', '2023-09-24 02:29:51');
INSERT INTO `lc_error_log` VALUES ('55184', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:29:53', '2023-09-24 02:29:53');
INSERT INTO `lc_error_log` VALUES ('55185', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:29:56', '2023-09-24 02:29:56');
INSERT INTO `lc_error_log` VALUES ('55186', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:29:58', '2023-09-24 02:29:58');
INSERT INTO `lc_error_log` VALUES ('55187', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:01', '2023-09-24 02:30:01');
INSERT INTO `lc_error_log` VALUES ('55188', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:03', '2023-09-24 02:30:03');
INSERT INTO `lc_error_log` VALUES ('55189', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:06', '2023-09-24 02:30:06');
INSERT INTO `lc_error_log` VALUES ('55190', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:08', '2023-09-24 02:30:08');
INSERT INTO `lc_error_log` VALUES ('55191', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:11', '2023-09-24 02:30:11');
INSERT INTO `lc_error_log` VALUES ('55192', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:13', '2023-09-24 02:30:13');
INSERT INTO `lc_error_log` VALUES ('55193', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:16', '2023-09-24 02:30:16');
INSERT INTO `lc_error_log` VALUES ('55194', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:18', '2023-09-24 02:30:18');
INSERT INTO `lc_error_log` VALUES ('55195', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:21', '2023-09-24 02:30:21');
INSERT INTO `lc_error_log` VALUES ('55196', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:23', '2023-09-24 02:30:23');
INSERT INTO `lc_error_log` VALUES ('55197', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:26', '2023-09-24 02:30:26');
INSERT INTO `lc_error_log` VALUES ('55198', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:28', '2023-09-24 02:30:28');
INSERT INTO `lc_error_log` VALUES ('55199', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:31', '2023-09-24 02:30:31');
INSERT INTO `lc_error_log` VALUES ('55200', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:33', '2023-09-24 02:30:33');
INSERT INTO `lc_error_log` VALUES ('55201', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:36', '2023-09-24 02:30:36');
INSERT INTO `lc_error_log` VALUES ('55202', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:38', '2023-09-24 02:30:38');
INSERT INTO `lc_error_log` VALUES ('55203', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:41', '2023-09-24 02:30:41');
INSERT INTO `lc_error_log` VALUES ('55204', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:43', '2023-09-24 02:30:43');
INSERT INTO `lc_error_log` VALUES ('55205', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:46', '2023-09-24 02:30:46');
INSERT INTO `lc_error_log` VALUES ('55206', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:48', '2023-09-24 02:30:48');
INSERT INTO `lc_error_log` VALUES ('55207', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:51', '2023-09-24 02:30:51');
INSERT INTO `lc_error_log` VALUES ('55208', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:53', '2023-09-24 02:30:53');
INSERT INTO `lc_error_log` VALUES ('55209', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:56', '2023-09-24 02:30:56');
INSERT INTO `lc_error_log` VALUES ('55210', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:30:58', '2023-09-24 02:30:58');
INSERT INTO `lc_error_log` VALUES ('55211', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:01', '2023-09-24 02:31:01');
INSERT INTO `lc_error_log` VALUES ('55212', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:03', '2023-09-24 02:31:03');
INSERT INTO `lc_error_log` VALUES ('55213', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:06', '2023-09-24 02:31:06');
INSERT INTO `lc_error_log` VALUES ('55214', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:08', '2023-09-24 02:31:08');
INSERT INTO `lc_error_log` VALUES ('55215', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:11', '2023-09-24 02:31:11');
INSERT INTO `lc_error_log` VALUES ('55216', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:13', '2023-09-24 02:31:13');
INSERT INTO `lc_error_log` VALUES ('55217', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:16', '2023-09-24 02:31:16');
INSERT INTO `lc_error_log` VALUES ('55218', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:18', '2023-09-24 02:31:18');
INSERT INTO `lc_error_log` VALUES ('55219', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:21', '2023-09-24 02:31:21');
INSERT INTO `lc_error_log` VALUES ('55220', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:23', '2023-09-24 02:31:23');
INSERT INTO `lc_error_log` VALUES ('55221', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:26', '2023-09-24 02:31:26');
INSERT INTO `lc_error_log` VALUES ('55222', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:28', '2023-09-24 02:31:28');
INSERT INTO `lc_error_log` VALUES ('55223', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:31', '2023-09-24 02:31:31');
INSERT INTO `lc_error_log` VALUES ('55224', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:33', '2023-09-24 02:31:33');
INSERT INTO `lc_error_log` VALUES ('55225', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:36', '2023-09-24 02:31:36');
INSERT INTO `lc_error_log` VALUES ('55226', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:38', '2023-09-24 02:31:38');
INSERT INTO `lc_error_log` VALUES ('55227', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:41', '2023-09-24 02:31:41');
INSERT INTO `lc_error_log` VALUES ('55228', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:43', '2023-09-24 02:31:43');
INSERT INTO `lc_error_log` VALUES ('55229', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:46', '2023-09-24 02:31:46');
INSERT INTO `lc_error_log` VALUES ('55230', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:48', '2023-09-24 02:31:48');
INSERT INTO `lc_error_log` VALUES ('55231', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:51', '2023-09-24 02:31:51');
INSERT INTO `lc_error_log` VALUES ('55232', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:31:53', '2023-09-24 02:31:53');
INSERT INTO `lc_error_log` VALUES ('55233', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '619', '/v1/getWxLoginData', '127.0.0.1', '2023-09-24 02:32:23', '2023-09-24 02:32:23');
INSERT INTO `lc_error_log` VALUES ('55234', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/mp/tongji/index', '127.0.0.1', '2023-09-24 02:34:31', '2023-09-24 02:34:31');
INSERT INTO `lc_error_log` VALUES ('55235', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2023-09-24 02:34:31', '2023-09-24 02:34:31');
INSERT INTO `lc_error_log` VALUES ('55236', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/mp/tongji/index', '127.0.0.1', '2023-09-24 02:35:31', '2023-09-24 02:35:31');
INSERT INTO `lc_error_log` VALUES ('55237', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2023-09-24 02:35:31', '2023-09-24 02:35:31');
INSERT INTO `lc_error_log` VALUES ('55238', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2023-09-24 02:36:36', '2023-09-24 02:36:36');
INSERT INTO `lc_error_log` VALUES ('55239', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/v1/mp/tongji/index', '127.0.0.1', '2023-09-24 02:36:36', '2023-09-24 02:36:36');
INSERT INTO `lc_error_log` VALUES ('55240', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\mobile\\controller\\v1\\Common->smsSend()', '0', '107', '/v1/smsSend', '127.0.0.1', '2023-09-25 23:47:54', '2023-09-25 23:47:54');
INSERT INTO `lc_error_log` VALUES ('55241', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/', '127.0.0.1', '2023-09-25 23:47:57', '2023-09-25 23:47:57');
INSERT INTO `lc_error_log` VALUES ('55242', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\controller\\v1\\Common.php', 'Class \'app\\mobile\\business\\Common\' not found', '0', '25', '/v1/smsSend', '127.0.0.1', '2023-09-25 23:50:10', '2023-09-25 23:50:10');
INSERT INTO `lc_error_log` VALUES ('55243', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\controller\\v1\\Common.php', 'Class \'app\\mobile\\business\\Common\' not found', '0', '25', '/v1/smsSend', '127.0.0.1', '2023-09-26 00:03:17', '2023-09-26 00:03:17');
INSERT INTO `lc_error_log` VALUES ('55244', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\controller\\v1\\Common.php', 'Class \'app\\mobile\\business\\Common\' not found', '0', '25', '/v1/smsSend', '127.0.0.1', '2023-09-26 00:03:19', '2023-09-26 00:03:19');
INSERT INTO `lc_error_log` VALUES ('55245', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\helper.php', 'Class \'app\\mobile\\validate\\User\' not found', '0', '531', '/v1/smsSend', '127.0.0.1', '2023-09-26 00:03:34', '2023-09-26 00:03:34');
INSERT INTO `lc_error_log` VALUES ('55246', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '请填写手机号', '0', '524', '/v1/smsSend', '127.0.0.1', '2023-09-26 00:04:15', '2023-09-26 00:04:15');
INSERT INTO `lc_error_log` VALUES ('55247', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\Common.php', 'Call to undefined function app\\mobile\\service\\v1\\smsPrefix()', '0', '69', '/v1/smsSend', '127.0.0.1', '2023-09-26 00:07:33', '2023-09-26 00:07:33');
INSERT INTO `lc_error_log` VALUES ('55248', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/register', '127.0.0.1', '2023-09-26 00:11:13', '2023-09-26 00:11:13');
INSERT INTO `lc_error_log` VALUES ('55249', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\mobile\\controller\\v1\\LogRegister->register()', '0', '107', '/v1/register', '127.0.0.1', '2023-09-26 00:11:17', '2023-09-26 00:11:17');
INSERT INTO `lc_error_log` VALUES ('55250', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'controller not exists:app\\mobile\\service\\v1\\LogRegister', '0', '76', '/v1/register', '127.0.0.1', '2023-09-26 00:27:29', '2023-09-26 00:27:29');
INSERT INTO `lc_error_log` VALUES ('55251', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '请输入用户名', '0', '524', '/v1/register', '127.0.0.1', '2023-09-26 00:28:12', '2023-09-26 00:28:12');
INSERT INTO `lc_error_log` VALUES ('55252', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2023-09-26 00:29:07', '2023-09-26 00:29:07');
INSERT INTO `lc_error_log` VALUES ('55253', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2023-09-26 00:30:33', '2023-09-26 00:30:33');
INSERT INTO `lc_error_log` VALUES ('55254', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2023-09-26 00:30:38', '2023-09-26 00:30:38');
INSERT INTO `lc_error_log` VALUES ('55255', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\Common.php', '手机号已存在', '0', '49', '/v1/smsSend', '127.0.0.1', '2023-09-26 00:31:51', '2023-09-26 00:31:51');
INSERT INTO `lc_error_log` VALUES ('55256', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\LogRegister.php', 'Undefined index: password', '0', '59', '/v1/register', '127.0.0.1', '2023-09-26 00:33:04', '2023-09-26 00:33:04');
INSERT INTO `lc_error_log` VALUES ('55257', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/login', '127.0.0.1', '2023-09-26 00:45:08', '2023-09-26 00:45:08');
INSERT INTO `lc_error_log` VALUES ('55258', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '请输入验证码', '0', '524', '/v1/login', '127.0.0.1', '2023-09-26 00:48:46', '2023-09-26 00:48:46');
INSERT INTO `lc_error_log` VALUES ('55259', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\Common.php', '手机号已存在', '0', '49', '/v1/smsSend', '127.0.0.1', '2023-09-26 00:49:00', '2023-09-26 00:49:00');
INSERT INTO `lc_error_log` VALUES ('55260', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\LogRegister.php', '验证码错误', '0', '105', '/v1/login', '127.0.0.1', '2023-09-26 00:49:48', '2023-09-26 00:49:48');
INSERT INTO `lc_error_log` VALUES ('55261', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\LogRegister.php', '验证码错误', '0', '105', '/v1/login', '127.0.0.1', '2023-09-26 01:08:03', '2023-09-26 01:08:03');
INSERT INTO `lc_error_log` VALUES ('55262', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->getCategorys', '10500', '117', '/v1/getZiyuanCategorys', '127.0.0.1', '2023-09-26 01:17:32', '2023-09-26 01:17:32');
INSERT INTO `lc_error_log` VALUES ('55263', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\Ziyuan.php', 'Undefined index: subclass', '0', '41', '/v1/getZiyuanCategorys', '127.0.0.1', '2023-09-26 01:18:14', '2023-09-26 01:18:14');
INSERT INTO `lc_error_log` VALUES ('55264', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\controller\\v1\\Kecheng.php', 'Call to undefined method app\\mobile\\service\\v1\\Kecheng::getKeList()', '0', '25', '/v1/getKechengList', '127.0.0.1', '2023-09-28 00:28:47', '2023-09-28 00:28:47');
INSERT INTO `lc_error_log` VALUES ('55265', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\Kecheng.php', 'Undefined variable: model', '0', '33', '/v1/getKechengList?page=1&pageSize=15&type=tuijian', '127.0.0.1', '2023-09-28 00:39:56', '2023-09-28 00:39:56');
INSERT INTO `lc_error_log` VALUES ('55266', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\mobile\\service\\v1\\Kecheng.php', 'Undefined index: id', '0', '40', '/v1/getKechengList?page=1&pageSize=15&type=tuijian', '127.0.0.1', '2023-09-28 00:47:00', '2023-09-28 00:47:00');
INSERT INTO `lc_error_log` VALUES ('55267', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->getListByKeId', '10500', '117', '/v1/getKechengList?page=1&pageSize=15&type=tuijian', '127.0.0.1', '2023-09-28 00:47:53', '2023-09-28 00:47:53');
INSERT INTO `lc_error_log` VALUES ('55268', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->getListByKeId', '10500', '117', '/v1/getKechengList?page=1&pageSize=15&type=tuijian', '127.0.0.1', '2023-09-28 00:48:10', '2023-09-28 00:48:10');
INSERT INTO `lc_error_log` VALUES ('55269', 'mobile', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->getListByKeId', '10500', '117', '/v1/getKechengList?page=1&pageSize=15&type=tuijian', '127.0.0.1', '2023-09-28 00:48:30', '2023-09-28 00:48:30');
INSERT INTO `lc_error_log` VALUES ('55270', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-10-14 13:05:46', '2023-10-14 13:05:46');
INSERT INTO `lc_error_log` VALUES ('55271', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-10-14 13:05:46', '2023-10-14 13:05:46');
INSERT INTO `lc_error_log` VALUES ('55272', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '581', '/v1/getZhuanjiInfo', '127.0.0.1', '2023-10-19 20:14:38', '2023-10-19 20:14:38');
INSERT INTO `lc_error_log` VALUES ('55273', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=Mac1', '127.0.0.1', '2023-10-19 20:58:37', '2023-10-19 20:58:37');
INSERT INTO `lc_error_log` VALUES ('55274', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=abc', '127.0.0.1', '2023-10-19 21:11:17', '2023-10-19 21:11:17');
INSERT INTO `lc_error_log` VALUES ('55275', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=abc', '127.0.0.1', '2023-10-19 21:12:46', '2023-10-19 21:12:46');
INSERT INTO `lc_error_log` VALUES ('55276', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=abc', '127.0.0.1', '2023-10-19 21:12:52', '2023-10-19 21:12:52');
INSERT INTO `lc_error_log` VALUES ('55277', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=abc', '127.0.0.1', '2023-10-19 21:13:34', '2023-10-19 21:13:34');
INSERT INTO `lc_error_log` VALUES ('55278', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=abc', '127.0.0.1', '2023-10-19 21:14:41', '2023-10-19 21:14:41');
INSERT INTO `lc_error_log` VALUES ('55279', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=abc', '127.0.0.1', '2023-10-19 21:14:41', '2023-10-19 21:14:41');
INSERT INTO `lc_error_log` VALUES ('55280', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=aa', '127.0.0.1', '2023-10-19 21:15:07', '2023-10-19 21:15:07');
INSERT INTO `lc_error_log` VALUES ('55281', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=aa', '127.0.0.1', '2023-10-19 21:15:24', '2023-10-19 21:15:24');
INSERT INTO `lc_error_log` VALUES ('55282', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=aa', '127.0.0.1', '2023-10-19 21:15:47', '2023-10-19 21:15:47');
INSERT INTO `lc_error_log` VALUES ('55283', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=aa', '127.0.0.1', '2023-10-19 21:15:58', '2023-10-19 21:15:58');
INSERT INTO `lc_error_log` VALUES ('55284', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=aa', '127.0.0.1', '2023-10-19 21:16:37', '2023-10-19 21:16:37');
INSERT INTO `lc_error_log` VALUES ('55285', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '参数错误', '0', '587', '/v1/getZhuanjiInfo?zhuanji=aa', '127.0.0.1', '2023-10-19 21:16:55', '2023-10-19 21:16:55');
INSERT INTO `lc_error_log` VALUES ('55286', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-04 17:50:33', '2023-11-04 17:50:33');
INSERT INTO `lc_error_log` VALUES ('55287', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-04 17:55:58', '2023-11-04 17:55:58');
INSERT INTO `lc_error_log` VALUES ('55288', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-04 17:55:58', '2023-11-04 17:55:58');
INSERT INTO `lc_error_log` VALUES ('55289', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-05 21:50:34', '2023-11-05 21:50:34');
INSERT INTO `lc_error_log` VALUES ('55290', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-05 21:50:34', '2023-11-05 21:50:34');
INSERT INTO `lc_error_log` VALUES ('55291', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 18:54:07', '2023-11-06 18:54:07');
INSERT INTO `lc_error_log` VALUES ('55292', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 18:54:07', '2023-11-06 18:54:07');
INSERT INTO `lc_error_log` VALUES ('55293', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 22:38:13', '2023-11-06 22:38:13');
INSERT INTO `lc_error_log` VALUES ('55294', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 22:38:13', '2023-11-06 22:38:13');
INSERT INTO `lc_error_log` VALUES ('55295', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 22:53:13', '2023-11-06 22:53:13');
INSERT INTO `lc_error_log` VALUES ('55296', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 22:53:13', '2023-11-06 22:53:13');
INSERT INTO `lc_error_log` VALUES ('55297', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 23:23:32', '2023-11-06 23:23:32');
INSERT INTO `lc_error_log` VALUES ('55298', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-06 23:23:32', '2023-11-06 23:23:32');
INSERT INTO `lc_error_log` VALUES ('55299', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-14 17:41:48', '2023-11-14 17:41:48');
INSERT INTO `lc_error_log` VALUES ('55300', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-14 17:41:48', '2023-11-14 17:41:48');
INSERT INTO `lc_error_log` VALUES ('55301', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-14 17:42:26', '2023-11-14 17:42:26');
INSERT INTO `lc_error_log` VALUES ('55302', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2023-11-14 17:42:26', '2023-11-14 17:42:26');
INSERT INTO `lc_error_log` VALUES ('55303', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '192.168.2.108', '2023-11-14 18:21:51', '2023-11-14 18:21:51');
INSERT INTO `lc_error_log` VALUES ('55304', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\ErrorLog.php', '数据操作失败', '0', '49', '/v1/getSystemConfig', '127.0.0.1', '2023-11-26 22:33:17', '2023-11-26 22:33:17');
INSERT INTO `lc_error_log` VALUES ('55305', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\ErrorLog.php', '数据操作失败', '0', '49', '/v1/getCategorys?subclass=true', '127.0.0.1', '2023-11-26 22:33:17', '2023-11-26 22:33:17');
INSERT INTO `lc_error_log` VALUES ('55306', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\ErrorLog.php', '数据操作失败', '0', '49', '/v1/getKeTryList?page=1&pageSize=20', '127.0.0.1', '2023-11-26 22:33:17', '2023-11-26 22:33:17');
INSERT INTO `lc_error_log` VALUES ('55307', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\ErrorLog.php', '数据操作失败', '0', '49', '/v1/getArticleInfo?id=16', '127.0.0.1', '2023-11-28 11:46:52', '2023-11-28 11:46:52');
INSERT INTO `lc_error_log` VALUES ('55308', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\ErrorLog.php', '数据操作失败', '0', '49', '/v1/getSystemConfig', '127.0.0.1', '2023-11-28 11:46:52', '2023-11-28 11:46:52');
INSERT INTO `lc_error_log` VALUES ('55309', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '127.0.0.1', '2023-11-28 11:46:52', '2023-11-28 11:46:52');
INSERT INTO `lc_error_log` VALUES ('55310', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined variable: result', '0', '39', '/v1/getZiyuanListRandom', '127.0.0.1', '2023-11-28 13:30:18', '2023-11-28 13:30:18');
INSERT INTO `lc_error_log` VALUES ('55311', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '数据操作失败', '0', '44', '/v1/getZiyuanListRandom', '127.0.0.1', '2023-11-28 13:31:14', '2023-11-28 13:31:14');
INSERT INTO `lc_error_log` VALUES ('55312', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined variable: paging', '0', '49', '/v1/getZiyuanListRandom', '127.0.0.1', '2023-11-28 13:32:20', '2023-11-28 13:32:20');
INSERT INTO `lc_error_log` VALUES ('55313', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined variable: paging', '0', '49', '/v1/getZiyuanListRandom', '127.0.0.1', '2023-11-28 13:32:23', '2023-11-28 13:32:23');
INSERT INTO `lc_error_log` VALUES ('55314', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined index: user_id', '0', '49', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2023-11-28 13:50:51', '2023-11-28 13:50:51');
INSERT INTO `lc_error_log` VALUES ('55315', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined variable: user', '0', '50', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2023-11-28 13:51:13', '2023-11-28 13:51:13');
INSERT INTO `lc_error_log` VALUES ('55316', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\api\\controller\\v1\\Ziyuan->getZiyuanListBrowse()', '0', '107', '/v1/getZiyuanListBrowse', '127.0.0.1', '2023-11-28 14:02:38', '2023-11-28 14:02:38');
INSERT INTO `lc_error_log` VALUES ('55317', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Cannot redeclare app\\api\\business\\Ziyuan::getRandomList()', '0', '70', '/v1/getZiyuanListBrowse', '127.0.0.1', '2023-11-28 14:07:27', '2023-11-28 14:07:27');
INSERT INTO `lc_error_log` VALUES ('55318', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Cannot redeclare app\\api\\business\\Ziyuan::getRandomList()', '0', '70', '/v1/getZiyuanListBrowse', '127.0.0.1', '2023-11-28 14:07:53', '2023-11-28 14:07:53');
INSERT INTO `lc_error_log` VALUES ('55319', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Cannot redeclare app\\api\\business\\Ziyuan::getRandomList()', '0', '70', '/v1/getZiyuanListBrowse', '127.0.0.1', '2023-11-28 14:07:56', '2023-11-28 14:07:56');
INSERT INTO `lc_error_log` VALUES ('55320', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Cannot redeclare app\\api\\business\\Ziyuan::getRandomList()', '0', '70', '/v1/getZiyuanListBrowse', '127.0.0.1', '2023-11-28 14:07:57', '2023-11-28 14:07:57');
INSERT INTO `lc_error_log` VALUES ('55321', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', 'syntax error, unexpected \'/\'', '0', '201', '/v1/getIndexTongji', '127.0.0.1', '2023-11-28 14:37:24', '2023-11-28 14:37:24');
INSERT INTO `lc_error_log` VALUES ('55322', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', 'Class \'app\\api\\business\\app\' not found', '0', '201', '/v1/getIndexTongji', '127.0.0.1', '2023-11-28 14:37:33', '2023-11-28 14:37:33');
INSERT INTO `lc_error_log` VALUES ('55323', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', 'Undefined variable: num', '0', '217', '/v1/getIndexTongji', '127.0.0.1', '2023-11-28 15:09:58', '2023-11-28 15:09:58');
INSERT INTO `lc_error_log` VALUES ('55324', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', 'Undefined variable: num', '0', '222', '/v1/getIndexTongji', '127.0.0.1', '2023-11-28 15:10:44', '2023-11-28 15:10:44');
INSERT INTO `lc_error_log` VALUES ('55325', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/http://api.gzy.com/v1//getZiyuanList?category_id=1', '127.0.0.1', '2023-11-28 16:05:16', '2023-11-28 16:05:16');
INSERT INTO `lc_error_log` VALUES ('55326', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '内容不存在或审核未通过', '0', '36', '/v1/getArticleInfo?id=1', '127.0.0.1', '2023-11-28 16:48:16', '2023-11-28 16:48:16');
INSERT INTO `lc_error_log` VALUES ('55327', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '127.0.0.1', '2023-11-28 16:48:17', '2023-11-28 16:48:17');
INSERT INTO `lc_error_log` VALUES ('55328', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '32', '/v1/getArticleInfo?id=index', '127.0.0.1', '2023-11-28 20:44:32', '2023-11-28 20:44:32');
INSERT INTO `lc_error_log` VALUES ('55329', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '127.0.0.1', '2023-11-28 20:44:32', '2023-11-28 20:44:32');
INSERT INTO `lc_error_log` VALUES ('55330', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '32', '/v1/getArticleInfo?id=index', '127.0.0.1', '2023-11-28 20:45:29', '2023-11-28 20:45:29');
INSERT INTO `lc_error_log` VALUES ('55331', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '127.0.0.1', '2023-11-28 20:45:29', '2023-11-28 20:45:29');
INSERT INTO `lc_error_log` VALUES ('55332', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '32', '/v1/getArticleInfo?id=index', '127.0.0.1', '2023-11-28 20:46:07', '2023-11-28 20:46:07');
INSERT INTO `lc_error_log` VALUES ('55333', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '参数错误', '0', '57', '/v1/getColumnArticleList?category_id=undefined&page=1&pageSize=5', '127.0.0.1', '2023-11-28 20:46:08', '2023-11-28 20:46:08');
INSERT INTO `lc_error_log` VALUES ('55334', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '请输入关键词', '0', '46', '/v1/getArticleSearch', '127.0.0.1', '2023-11-29 13:49:06', '2023-11-29 13:49:06');
INSERT INTO `lc_error_log` VALUES ('55335', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', 'Undefined variable: keyword', '0', '49', '/v1/getArticleSearch?page=1&pageSize=20&keyWord=%E5%85%B3%E4%BA%8E', '127.0.0.1', '2023-11-29 13:53:02', '2023-11-29 13:53:02');
INSERT INTO `lc_error_log` VALUES ('55336', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '请输入关键词', '0', '46', '/v1/getArticleSearch?page=1&pageSize=20&keyWord=', '127.0.0.1', '2023-11-29 14:03:17', '2023-11-29 14:03:17');
INSERT INTO `lc_error_log` VALUES ('55337', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '请输入关键词', '0', '46', '/v1/getArticleSearch?page=1&pageSize=20&keyWord=', '127.0.0.1', '2023-11-29 14:03:34', '2023-11-29 14:03:34');
INSERT INTO `lc_error_log` VALUES ('55338', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Article.php', '请输入关键词', '0', '46', '/v1/getArticleSearch?page=1&pageSize=20&keyWord=', '127.0.0.1', '2023-11-29 14:17:51', '2023-11-29 14:17:51');
INSERT INTO `lc_error_log` VALUES ('55339', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getUserLoginLog?page=1&pageSize=20', '127.0.0.1', '2023-11-30 09:34:10', '2023-11-30 09:34:10');
INSERT INTO `lc_error_log` VALUES ('55340', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getErrorLog?page=1&pageSize=20', '127.0.0.1', '2023-11-30 09:34:15', '2023-11-30 09:34:15');
INSERT INTO `lc_error_log` VALUES ('55341', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getErrorLog?page=1&pageSize=20', '127.0.0.1', '2023-11-30 09:56:00', '2023-11-30 09:56:00');
INSERT INTO `lc_error_log` VALUES ('55342', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/api/getIndexTongji', '127.0.0.1', '2023-12-18 11:38:58', '2023-12-18 11:38:58');
INSERT INTO `lc_error_log` VALUES ('55343', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '29', '/v1/getQrCodeLogin', '127.0.0.1', '2023-12-29 17:41:31', '2023-12-29 17:41:31');
INSERT INTO `lc_error_log` VALUES ('55344', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:34', '2023-12-29 17:41:34');
INSERT INTO `lc_error_log` VALUES ('55345', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:37', '2023-12-29 17:41:37');
INSERT INTO `lc_error_log` VALUES ('55346', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:39', '2023-12-29 17:41:39');
INSERT INTO `lc_error_log` VALUES ('55347', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:42', '2023-12-29 17:41:42');
INSERT INTO `lc_error_log` VALUES ('55348', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:44', '2023-12-29 17:41:44');
INSERT INTO `lc_error_log` VALUES ('55349', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:47', '2023-12-29 17:41:47');
INSERT INTO `lc_error_log` VALUES ('55350', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:49', '2023-12-29 17:41:49');
INSERT INTO `lc_error_log` VALUES ('55351', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:52', '2023-12-29 17:41:52');
INSERT INTO `lc_error_log` VALUES ('55352', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:54', '2023-12-29 17:41:54');
INSERT INTO `lc_error_log` VALUES ('55353', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:57', '2023-12-29 17:41:57');
INSERT INTO `lc_error_log` VALUES ('55354', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:41:59', '2023-12-29 17:41:59');
INSERT INTO `lc_error_log` VALUES ('55355', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:02', '2023-12-29 17:42:02');
INSERT INTO `lc_error_log` VALUES ('55356', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:04', '2023-12-29 17:42:04');
INSERT INTO `lc_error_log` VALUES ('55357', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '29', '/v1/getQrCodeLogin', '127.0.0.1', '2023-12-29 17:42:07', '2023-12-29 17:42:07');
INSERT INTO `lc_error_log` VALUES ('55358', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:07', '2023-12-29 17:42:07');
INSERT INTO `lc_error_log` VALUES ('55359', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:09', '2023-12-29 17:42:09');
INSERT INTO `lc_error_log` VALUES ('55360', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:09', '2023-12-29 17:42:09');
INSERT INTO `lc_error_log` VALUES ('55361', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:12', '2023-12-29 17:42:12');
INSERT INTO `lc_error_log` VALUES ('55362', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:12', '2023-12-29 17:42:12');
INSERT INTO `lc_error_log` VALUES ('55363', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:14', '2023-12-29 17:42:14');
INSERT INTO `lc_error_log` VALUES ('55364', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:14', '2023-12-29 17:42:14');
INSERT INTO `lc_error_log` VALUES ('55365', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:17', '2023-12-29 17:42:17');
INSERT INTO `lc_error_log` VALUES ('55366', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:17', '2023-12-29 17:42:17');
INSERT INTO `lc_error_log` VALUES ('55367', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:19', '2023-12-29 17:42:19');
INSERT INTO `lc_error_log` VALUES ('55368', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:19', '2023-12-29 17:42:19');
INSERT INTO `lc_error_log` VALUES ('55369', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:22', '2023-12-29 17:42:22');
INSERT INTO `lc_error_log` VALUES ('55370', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:22', '2023-12-29 17:42:22');
INSERT INTO `lc_error_log` VALUES ('55371', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:25', '2023-12-29 17:42:25');
INSERT INTO `lc_error_log` VALUES ('55372', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:25', '2023-12-29 17:42:25');
INSERT INTO `lc_error_log` VALUES ('55373', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:27', '2023-12-29 17:42:27');
INSERT INTO `lc_error_log` VALUES ('55374', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:27', '2023-12-29 17:42:27');
INSERT INTO `lc_error_log` VALUES ('55375', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:30', '2023-12-29 17:42:30');
INSERT INTO `lc_error_log` VALUES ('55376', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:30', '2023-12-29 17:42:30');
INSERT INTO `lc_error_log` VALUES ('55377', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:32', '2023-12-29 17:42:32');
INSERT INTO `lc_error_log` VALUES ('55378', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:32', '2023-12-29 17:42:32');
INSERT INTO `lc_error_log` VALUES ('55379', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:35', '2023-12-29 17:42:35');
INSERT INTO `lc_error_log` VALUES ('55380', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:35', '2023-12-29 17:42:35');
INSERT INTO `lc_error_log` VALUES ('55381', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:37', '2023-12-29 17:42:37');
INSERT INTO `lc_error_log` VALUES ('55382', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:37', '2023-12-29 17:42:37');
INSERT INTO `lc_error_log` VALUES ('55383', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:40', '2023-12-29 17:42:40');
INSERT INTO `lc_error_log` VALUES ('55384', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:40', '2023-12-29 17:42:40');
INSERT INTO `lc_error_log` VALUES ('55385', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:42', '2023-12-29 17:42:42');
INSERT INTO `lc_error_log` VALUES ('55386', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:42', '2023-12-29 17:42:42');
INSERT INTO `lc_error_log` VALUES ('55387', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:45', '2023-12-29 17:42:45');
INSERT INTO `lc_error_log` VALUES ('55388', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:45', '2023-12-29 17:42:45');
INSERT INTO `lc_error_log` VALUES ('55389', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:47', '2023-12-29 17:42:47');
INSERT INTO `lc_error_log` VALUES ('55390', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:47', '2023-12-29 17:42:47');
INSERT INTO `lc_error_log` VALUES ('55391', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:50', '2023-12-29 17:42:50');
INSERT INTO `lc_error_log` VALUES ('55392', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:50', '2023-12-29 17:42:50');
INSERT INTO `lc_error_log` VALUES ('55393', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:52', '2023-12-29 17:42:52');
INSERT INTO `lc_error_log` VALUES ('55394', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:52', '2023-12-29 17:42:52');
INSERT INTO `lc_error_log` VALUES ('55395', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:55', '2023-12-29 17:42:55');
INSERT INTO `lc_error_log` VALUES ('55396', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:55', '2023-12-29 17:42:55');
INSERT INTO `lc_error_log` VALUES ('55397', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:57', '2023-12-29 17:42:57');
INSERT INTO `lc_error_log` VALUES ('55398', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:42:57', '2023-12-29 17:42:57');
INSERT INTO `lc_error_log` VALUES ('55399', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:00', '2023-12-29 17:43:00');
INSERT INTO `lc_error_log` VALUES ('55400', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:00', '2023-12-29 17:43:00');
INSERT INTO `lc_error_log` VALUES ('55401', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:02', '2023-12-29 17:43:02');
INSERT INTO `lc_error_log` VALUES ('55402', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:02', '2023-12-29 17:43:02');
INSERT INTO `lc_error_log` VALUES ('55403', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:05', '2023-12-29 17:43:05');
INSERT INTO `lc_error_log` VALUES ('55404', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:05', '2023-12-29 17:43:05');
INSERT INTO `lc_error_log` VALUES ('55405', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:07', '2023-12-29 17:43:07');
INSERT INTO `lc_error_log` VALUES ('55406', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:07', '2023-12-29 17:43:07');
INSERT INTO `lc_error_log` VALUES ('55407', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:10', '2023-12-29 17:43:10');
INSERT INTO `lc_error_log` VALUES ('55408', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:10', '2023-12-29 17:43:10');
INSERT INTO `lc_error_log` VALUES ('55409', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:12', '2023-12-29 17:43:12');
INSERT INTO `lc_error_log` VALUES ('55410', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:12', '2023-12-29 17:43:12');
INSERT INTO `lc_error_log` VALUES ('55411', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:15', '2023-12-29 17:43:15');
INSERT INTO `lc_error_log` VALUES ('55412', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:15', '2023-12-29 17:43:15');
INSERT INTO `lc_error_log` VALUES ('55413', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:17', '2023-12-29 17:43:17');
INSERT INTO `lc_error_log` VALUES ('55414', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:17', '2023-12-29 17:43:17');
INSERT INTO `lc_error_log` VALUES ('55415', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:20', '2023-12-29 17:43:20');
INSERT INTO `lc_error_log` VALUES ('55416', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:20', '2023-12-29 17:43:20');
INSERT INTO `lc_error_log` VALUES ('55417', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:25', '2023-12-29 17:43:25');
INSERT INTO `lc_error_log` VALUES ('55418', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:25', '2023-12-29 17:43:25');
INSERT INTO `lc_error_log` VALUES ('55419', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:27', '2023-12-29 17:43:27');
INSERT INTO `lc_error_log` VALUES ('55420', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:27', '2023-12-29 17:43:27');
INSERT INTO `lc_error_log` VALUES ('55421', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:29', '2023-12-29 17:43:29');
INSERT INTO `lc_error_log` VALUES ('55422', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:29', '2023-12-29 17:43:29');
INSERT INTO `lc_error_log` VALUES ('55423', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:32', '2023-12-29 17:43:32');
INSERT INTO `lc_error_log` VALUES ('55424', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:32', '2023-12-29 17:43:32');
INSERT INTO `lc_error_log` VALUES ('55425', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:34', '2023-12-29 17:43:34');
INSERT INTO `lc_error_log` VALUES ('55426', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:34', '2023-12-29 17:43:34');
INSERT INTO `lc_error_log` VALUES ('55427', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:37', '2023-12-29 17:43:37');
INSERT INTO `lc_error_log` VALUES ('55428', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:37', '2023-12-29 17:43:37');
INSERT INTO `lc_error_log` VALUES ('55429', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:39', '2023-12-29 17:43:39');
INSERT INTO `lc_error_log` VALUES ('55430', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:39', '2023-12-29 17:43:39');
INSERT INTO `lc_error_log` VALUES ('55431', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:42', '2023-12-29 17:43:42');
INSERT INTO `lc_error_log` VALUES ('55432', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:44', '2023-12-29 17:43:44');
INSERT INTO `lc_error_log` VALUES ('55433', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:47', '2023-12-29 17:43:47');
INSERT INTO `lc_error_log` VALUES ('55434', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:49', '2023-12-29 17:43:49');
INSERT INTO `lc_error_log` VALUES ('55435', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:52', '2023-12-29 17:43:52');
INSERT INTO `lc_error_log` VALUES ('55436', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:54', '2023-12-29 17:43:54');
INSERT INTO `lc_error_log` VALUES ('55437', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:43:57', '2023-12-29 17:43:57');
INSERT INTO `lc_error_log` VALUES ('55438', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:22', '2023-12-29 17:45:22');
INSERT INTO `lc_error_log` VALUES ('55439', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:22', '2023-12-29 17:45:22');
INSERT INTO `lc_error_log` VALUES ('55440', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:22', '2023-12-29 17:45:22');
INSERT INTO `lc_error_log` VALUES ('55441', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:22', '2023-12-29 17:45:22');
INSERT INTO `lc_error_log` VALUES ('55442', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:23', '2023-12-29 17:45:23');
INSERT INTO `lc_error_log` VALUES ('55443', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:23', '2023-12-29 17:45:23');
INSERT INTO `lc_error_log` VALUES ('55444', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:23', '2023-12-29 17:45:23');
INSERT INTO `lc_error_log` VALUES ('55445', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:23', '2023-12-29 17:45:23');
INSERT INTO `lc_error_log` VALUES ('55446', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:23', '2023-12-29 17:45:23');
INSERT INTO `lc_error_log` VALUES ('55447', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:45:23', '2023-12-29 17:45:23');
INSERT INTO `lc_error_log` VALUES ('55448', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:46:05', '2023-12-29 17:46:05');
INSERT INTO `lc_error_log` VALUES ('55449', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:46:05', '2023-12-29 17:46:05');
INSERT INTO `lc_error_log` VALUES ('55450', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:46:55', '2023-12-29 17:46:55');
INSERT INTO `lc_error_log` VALUES ('55451', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:46:55', '2023-12-29 17:46:55');
INSERT INTO `lc_error_log` VALUES ('55452', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:48:01', '2023-12-29 17:48:01');
INSERT INTO `lc_error_log` VALUES ('55453', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:48:01', '2023-12-29 17:48:01');
INSERT INTO `lc_error_log` VALUES ('55454', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '登录失败', '0', '612', '/v1/getWxLoginData', '127.0.0.1', '2023-12-29 17:48:57', '2023-12-29 17:48:57');
INSERT INTO `lc_error_log` VALUES ('55455', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/imgUploadQiniu', '127.0.0.1', '2024-01-10 12:51:40', '2024-01-10 12:51:40');
INSERT INTO `lc_error_log` VALUES ('55456', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-13 22:58:00', '2024-01-13 22:58:00');
INSERT INTO `lc_error_log` VALUES ('55457', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-13 22:58:00', '2024-01-13 22:58:00');
INSERT INTO `lc_error_log` VALUES ('55458', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-17 22:05:26', '2024-01-17 22:05:26');
INSERT INTO `lc_error_log` VALUES ('55459', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-17 22:05:26', '2024-01-17 22:05:26');
INSERT INTO `lc_error_log` VALUES ('55460', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\UserWalletLog.php', '数据操作失败', '0', '23', '/tixianAudit', '127.0.0.1', '2024-01-17 22:11:17', '2024-01-17 22:11:17');
INSERT INTO `lc_error_log` VALUES ('55461', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'fields not exists:[before_data]', '10500', '164', '/tixianAudit', '127.0.0.1', '2024-01-17 22:12:22', '2024-01-17 22:12:22');
INSERT INTO `lc_error_log` VALUES ('55462', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-18 10:41:27', '2024-01-18 10:41:27');
INSERT INTO `lc_error_log` VALUES ('55463', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-18 10:41:28', '2024-01-18 10:41:28');
INSERT INTO `lc_error_log` VALUES ('55464', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'controller not exists:app\\platform\\controller\\:version\\Common', '0', '76', '/smsSend', '127.0.0.1', '2024-01-19 21:18:59', '2024-01-19 21:18:59');
INSERT INTO `lc_error_log` VALUES ('55465', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\controller\\Common.php', 'Class \'app\\platform\\controller\\commonBus\' not found', '0', '27', '/smsSend', '127.0.0.1', '2024-01-19 21:20:04', '2024-01-19 21:20:04');
INSERT INTO `lc_error_log` VALUES ('55466', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Common.php', 'syntax error, unexpected \'$logModel\' (T_VARIABLE)', '0', '61', '/smsSend', '127.0.0.1', '2024-01-19 21:21:18', '2024-01-19 21:21:18');
INSERT INTO `lc_error_log` VALUES ('55467', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '请填写手机号', '0', '524', '/smsSend', '127.0.0.1', '2024-01-19 21:21:43', '2024-01-19 21:21:43');
INSERT INTO `lc_error_log` VALUES ('55468', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '验证码类型错误', '0', '524', '/smsSend', '127.0.0.1', '2024-01-19 21:21:57', '2024-01-19 21:21:57');
INSERT INTO `lc_error_log` VALUES ('55469', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Common.php', '短信发送失败', '0', '32', '/smsSend', '127.0.0.1', '2024-01-19 21:22:47', '2024-01-19 21:22:47');
INSERT INTO `lc_error_log` VALUES ('55470', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Common.php', '短信发送失败', '0', '32', '/smsSend', '127.0.0.1', '2024-01-19 21:24:33', '2024-01-19 21:24:33');
INSERT INTO `lc_error_log` VALUES ('55471', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Common.php', 'Undefined variable: config', '0', '31', '/smsSend', '127.0.0.1', '2024-01-19 21:25:06', '2024-01-19 21:25:06');
INSERT INTO `lc_error_log` VALUES ('55472', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Common.php', '短信发送失败', '0', '34', '/smsSend', '127.0.0.1', '2024-01-19 21:25:18', '2024-01-19 21:25:18');
INSERT INTO `lc_error_log` VALUES ('55473', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\SmsCode.php', '数据操作失败', '0', '17', '/smsSend', '127.0.0.1', '2024-01-19 21:25:38', '2024-01-19 21:25:38');
INSERT INTO `lc_error_log` VALUES ('55474', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[22001]: String data, right truncated: 1406 Data too long for column \'type\' at row 1', '10501', '796', '/smsSend', '127.0.0.1', '2024-01-19 21:26:37', '2024-01-19 21:26:37');
INSERT INTO `lc_error_log` VALUES ('55475', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\Sms.php', 'Undefined index: formlc_login', '0', '31', '/smsSend', '127.0.0.1', '2024-01-19 21:31:17', '2024-01-19 21:31:17');
INSERT INTO `lc_error_log` VALUES ('55476', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-19 21:45:36', '2024-01-19 21:45:36');
INSERT INTO `lc_error_log` VALUES ('55477', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-19 21:45:37', '2024-01-19 21:45:37');
INSERT INTO `lc_error_log` VALUES ('55478', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', 'Undefined variable: prefix', '0', '40', '/login', '127.0.0.1', '2024-01-19 21:47:45', '2024-01-19 21:47:45');
INSERT INTO `lc_error_log` VALUES ('55479', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '验证码不正确', '0', '45', '/login', '127.0.0.1', '2024-01-19 21:48:31', '2024-01-19 21:48:31');
INSERT INTO `lc_error_log` VALUES ('55480', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '62', '/login', '127.0.0.1', '2024-01-19 21:48:44', '2024-01-19 21:48:44');
INSERT INTO `lc_error_log` VALUES ('55481', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '验证码不正确', '0', '45', '/login', '127.0.0.1', '2024-01-19 21:49:18', '2024-01-19 21:49:18');
INSERT INTO `lc_error_log` VALUES ('55482', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '验证码不正确', '0', '45', '/login', '127.0.0.1', '2024-01-19 22:10:04', '2024-01-19 22:10:04');
INSERT INTO `lc_error_log` VALUES ('55483', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '62', '/login', '127.0.0.1', '2024-01-19 22:11:36', '2024-01-19 22:11:36');
INSERT INTO `lc_error_log` VALUES ('55484', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '65', '/login', '127.0.0.1', '2024-01-19 22:12:46', '2024-01-19 22:12:46');
INSERT INTO `lc_error_log` VALUES ('55485', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '验证码不正确', '0', '45', '/login', '127.0.0.1', '2024-01-19 22:17:08', '2024-01-19 22:17:08');
INSERT INTO `lc_error_log` VALUES ('55486', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Common.php', 'Undefined variable: phone', '0', '31', '/smsSend', '127.0.0.1', '2024-01-19 22:24:12', '2024-01-19 22:24:12');
INSERT INTO `lc_error_log` VALUES ('55487', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '73', '/login', '127.0.0.1', '2024-01-19 22:30:16', '2024-01-19 22:30:16');
INSERT INTO `lc_error_log` VALUES ('55488', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '73', '/login', '127.0.0.1', '2024-01-19 22:30:33', '2024-01-19 22:30:33');
INSERT INTO `lc_error_log` VALUES ('55489', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '73', '/login', '127.0.0.1', '2024-01-19 22:38:06', '2024-01-19 22:38:06');
INSERT INTO `lc_error_log` VALUES ('55490', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '73', '/login', '127.0.0.1', '2024-01-19 22:38:45', '2024-01-19 22:38:45');
INSERT INTO `lc_error_log` VALUES ('55491', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '73', '/login', '127.0.0.1', '2024-01-19 22:40:34', '2024-01-19 22:40:34');
INSERT INTO `lc_error_log` VALUES ('55492', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '验证码类型错误', '0', '524', '/smsSend', '127.0.0.1', '2024-01-20 22:07:53', '2024-01-20 22:07:53');
INSERT INTO `lc_error_log` VALUES ('55493', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '验证码类型错误', '0', '524', '/smsSend', '127.0.0.1', '2024-01-20 22:10:38', '2024-01-20 22:10:38');
INSERT INTO `lc_error_log` VALUES ('55494', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-21 14:10:40', '2024-01-21 14:10:40');
INSERT INTO `lc_error_log` VALUES ('55495', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-01-21 14:10:41', '2024-01-21 14:10:41');
INSERT INTO `lc_error_log` VALUES ('55496', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '验证码不正确', '0', '45', '/login', '127.0.0.1', '2024-01-21 14:11:19', '2024-01-21 14:11:19');
INSERT INTO `lc_error_log` VALUES ('55497', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-02 20:38:25', '2024-02-02 20:38:25');
INSERT INTO `lc_error_log` VALUES ('55498', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '请上传图标', '0', '524', '/ziyuanCategory', '127.0.0.1', '2024-02-02 22:22:13', '2024-02-02 22:22:13');
INSERT INTO `lc_error_log` VALUES ('55499', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '请填写关键词', '0', '524', '/ziyuanCategory', '127.0.0.1', '2024-02-02 22:23:07', '2024-02-02 22:23:07');
INSERT INTO `lc_error_log` VALUES ('55500', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\ZiyuanCategory.php', '数据操作失败', '0', '64', '/ziyuanCategory', '127.0.0.1', '2024-02-02 22:23:23', '2024-02-02 22:23:23');
INSERT INTO `lc_error_log` VALUES ('55501', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\ZiyuanCategory.php', '数据操作失败', '0', '64', '/ziyuanCategory', '127.0.0.1', '2024-02-02 22:23:35', '2024-02-02 22:23:35');
INSERT INTO `lc_error_log` VALUES ('55502', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'fields not exists:[subclass]', '10500', '164', '/ziyuanCategory', '127.0.0.1', '2024-02-02 22:25:06', '2024-02-02 22:25:06');
INSERT INTO `lc_error_log` VALUES ('55503', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-03 13:04:40', '2024-02-03 13:04:40');
INSERT INTO `lc_error_log` VALUES ('55504', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-03 13:04:41', '2024-02-03 13:04:41');
INSERT INTO `lc_error_log` VALUES ('55505', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/editPassword', '127.0.0.1', '2024-02-03 13:46:35', '2024-02-03 13:46:35');
INSERT INTO `lc_error_log` VALUES ('55506', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\controller\\AdminUser.php', 'Class \'app\\platform\\controller\\AnotherClass\' not found', '0', '10', '/editPassword', '127.0.0.1', '2024-02-03 13:47:05', '2024-02-03 13:47:05');
INSERT INTO `lc_error_log` VALUES ('55507', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '请输入新密码', '0', '524', '/editPassword', '127.0.0.1', '2024-02-03 14:07:33', '2024-02-03 14:07:33');
INSERT INTO `lc_error_log` VALUES ('55508', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', 'newPassword和比较字段不能相同', '0', '524', '/editPassword', '127.0.0.1', '2024-02-03 14:08:50', '2024-02-03 14:08:50');
INSERT INTO `lc_error_log` VALUES ('55509', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '新密码不能和原密码一致', '0', '524', '/editPassword', '127.0.0.1', '2024-02-03 14:10:27', '2024-02-03 14:10:27');
INSERT INTO `lc_error_log` VALUES ('55510', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', 'rPassword和确认字段不一致', '0', '524', '/editPassword', '127.0.0.1', '2024-02-03 14:10:42', '2024-02-03 14:10:42');
INSERT INTO `lc_error_log` VALUES ('55511', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '确认密码不正确', '0', '524', '/editPassword', '127.0.0.1', '2024-02-03 14:11:02', '2024-02-03 14:11:02');
INSERT INTO `lc_error_log` VALUES ('55512', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '确认密码不正确', '0', '524', '/editPassword', '127.0.0.1', '2024-02-03 14:15:13', '2024-02-03 14:15:13');
INSERT INTO `lc_error_log` VALUES ('55513', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\model\\AdminUser.php', 'syntax error, unexpected \'}\', expecting \';\'', '0', '79', '/editPassword', '127.0.0.1', '2024-02-03 14:37:53', '2024-02-03 14:37:53');
INSERT INTO `lc_error_log` VALUES ('55514', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '原密码不正确', '0', '137', '/editPassword', '127.0.0.1', '2024-02-03 15:40:22', '2024-02-03 15:40:22');
INSERT INTO `lc_error_log` VALUES ('55515', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '原密码不正确', '0', '137', '/editPassword', '127.0.0.1', '2024-02-03 15:40:42', '2024-02-03 15:40:42');
INSERT INTO `lc_error_log` VALUES ('55516', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', 'syntax error, unexpected \'.\'', '0', '130', '/loginOut', '127.0.0.1', '2024-02-03 16:44:27', '2024-02-03 16:44:27');
INSERT INTO `lc_error_log` VALUES ('55517', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/loginOut', '127.0.0.1', '2024-02-03 16:46:32', '2024-02-03 16:46:32');
INSERT INTO `lc_error_log` VALUES ('55518', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', 'syntax error, unexpected \'.\'', '0', '133', '/loginOut', '127.0.0.1', '2024-02-03 16:46:45', '2024-02-03 16:46:45');
INSERT INTO `lc_error_log` VALUES ('55519', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', 'syntax error, unexpected \'.\'', '0', '133', '/login', '127.0.0.1', '2024-02-03 16:47:19', '2024-02-03 16:47:19');
INSERT INTO `lc_error_log` VALUES ('55520', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '63', '/login', '127.0.0.1', '2024-02-03 16:49:05', '2024-02-03 16:49:05');
INSERT INTO `lc_error_log` VALUES ('55521', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', 'Undefined variable: user', '0', '130', '/login', '127.0.0.1', '2024-02-03 16:49:13', '2024-02-03 16:49:13');
INSERT INTO `lc_error_log` VALUES ('55522', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-03 16:54:56', '2024-02-03 16:54:56');
INSERT INTO `lc_error_log` VALUES ('55523', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/ad/getAds?id=undefined', '127.0.0.1', '2024-02-03 17:38:05', '2024-02-03 17:38:05');
INSERT INTO `lc_error_log` VALUES ('55524', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/ad/getAds?id=undefined', '127.0.0.1', '2024-02-03 17:38:12', '2024-02-03 17:38:12');
INSERT INTO `lc_error_log` VALUES ('55525', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/ad/getAds?id=undefined', '127.0.0.1', '2024-02-03 18:29:01', '2024-02-03 18:29:01');
INSERT INTO `lc_error_log` VALUES ('55526', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/ad/getAds?id=undefined', '127.0.0.1', '2024-02-03 18:31:43', '2024-02-03 18:31:43');
INSERT INTO `lc_error_log` VALUES ('55527', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/ad/getAds?id=undefined', '127.0.0.1', '2024-02-03 18:32:30', '2024-02-03 18:32:30');
INSERT INTO `lc_error_log` VALUES ('55528', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/ad/getAds?id=undefined', '127.0.0.1', '2024-02-03 18:33:29', '2024-02-03 18:33:29');
INSERT INTO `lc_error_log` VALUES ('55529', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/test', '127.0.0.1', '2024-02-12 14:18:43', '2024-02-12 14:18:43');
INSERT INTO `lc_error_log` VALUES ('55530', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/test', '127.0.0.1', '2024-02-12 14:20:18', '2024-02-12 14:20:18');
INSERT INTO `lc_error_log` VALUES ('55531', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\controller\\Test.php', 'Cannot use [] for reading', '0', '14', '/test', '127.0.0.1', '2024-02-12 14:21:35', '2024-02-12 14:21:35');
INSERT INTO `lc_error_log` VALUES ('55532', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-20 09:11:03', '2024-02-20 09:11:03');
INSERT INTO `lc_error_log` VALUES ('55533', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-20 09:11:03', '2024-02-20 09:11:03');
INSERT INTO `lc_error_log` VALUES ('55534', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '63', '/login', '127.0.0.1', '2024-02-20 09:12:07', '2024-02-20 09:12:07');
INSERT INTO `lc_error_log` VALUES ('55535', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '63', '/login', '127.0.0.1', '2024-02-20 09:12:28', '2024-02-20 09:12:28');
INSERT INTO `lc_error_log` VALUES ('55536', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-27 14:11:12', '2024-02-27 14:11:12');
INSERT INTO `lc_error_log` VALUES ('55537', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-27 14:11:13', '2024-02-27 14:11:13');
INSERT INTO `lc_error_log` VALUES ('55538', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '63', '/login', '127.0.0.1', '2024-02-27 18:53:59', '2024-02-27 18:53:59');
INSERT INTO `lc_error_log` VALUES ('55539', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Tongji.php', 'Cannot use isset() on the result of an expression (you can use \"null !== expression\" instead)', '0', '79', '/indexData', '127.0.0.1', '2024-02-27 19:18:23', '2024-02-27 19:18:23');
INSERT INTO `lc_error_log` VALUES ('55540', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-28 13:17:57', '2024-02-28 13:17:57');
INSERT INTO `lc_error_log` VALUES ('55541', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-28 13:17:58', '2024-02-28 13:17:58');
INSERT INTO `lc_error_log` VALUES ('55542', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-29 15:11:13', '2024-02-29 15:11:13');
INSERT INTO `lc_error_log` VALUES ('55543', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-02-29 15:11:13', '2024-02-29 15:11:13');
INSERT INTO `lc_error_log` VALUES ('55544', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-03-03 14:24:10', '2024-03-03 14:24:10');
INSERT INTO `lc_error_log` VALUES ('55545', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-03-03 14:31:18', '2024-03-03 14:31:18');
INSERT INTO `lc_error_log` VALUES ('55546', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-03-03 14:31:19', '2024-03-03 14:31:19');
INSERT INTO `lc_error_log` VALUES ('55547', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-03-03 14:47:02', '2024-03-03 14:47:02');
INSERT INTO `lc_error_log` VALUES ('55548', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\service\\service\\Tengxun.php', 'syntax error, unexpected \'error_reporting\' (T_STRING), expecting function (T_FUNCTION) or const (T_CONST)', '0', '16', '/test', '127.0.0.1', '2024-03-03 15:08:09', '2024-03-03 15:08:09');
INSERT INTO `lc_error_log` VALUES ('55549', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[HY000] [2002] 由于目标计算机积极拒绝，无法连接。\r\n', '10501', '796', '/v1/getIndexTongji', '127.0.0.1', '2024-03-06 13:16:38', '2024-03-06 13:16:38');
INSERT INTO `lc_error_log` VALUES ('55550', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[HY000] [2002] 由于目标计算机积极拒绝，无法连接。\r\n', '10501', '796', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-03-06 13:16:38', '2024-03-06 13:16:38');
INSERT INTO `lc_error_log` VALUES ('55551', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[HY000] [2002] 由于目标计算机积极拒绝，无法连接。\r\n', '10501', '796', '/v1/getZiyuanListBrowse?page=1&pageSize=3', '127.0.0.1', '2024-03-06 13:16:38', '2024-03-06 13:16:38');
INSERT INTO `lc_error_log` VALUES ('55552', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\log\\driver\\File.php', 'mkdir(): File exists', '0', '70', '/v1/getIndexTongji', '127.0.0.1', '2024-03-06 13:16:38', '2024-03-06 13:16:38');
INSERT INTO `lc_error_log` VALUES ('55553', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[HY000] [2002] 由于目标计算机积极拒绝，无法连接。\r\n', '10501', '796', '/v1/getKeTryList?page=1&pageSize=12', '127.0.0.1', '2024-03-06 13:16:38', '2024-03-06 13:16:38');
INSERT INTO `lc_error_log` VALUES ('55554', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[HY000] [2002] 由于目标计算机积极拒绝，无法连接。\r\n', '10501', '796', '/v1/getSystemConfig', '127.0.0.1', '2024-03-06 13:16:38', '2024-03-06 13:16:38');
INSERT INTO `lc_error_log` VALUES ('55555', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[HY000] [2002] 由于目标计算机积极拒绝，无法连接。\r\n', '10501', '796', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2024-03-06 13:16:38', '2024-03-06 13:16:38');
INSERT INTO `lc_error_log` VALUES ('55556', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1', '127.0.0.1', '2024-03-06 13:17:13', '2024-03-06 13:17:13');
INSERT INTO `lc_error_log` VALUES ('55557', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2024-03-06 13:19:58', '2024-03-06 13:19:58');
INSERT INTO `lc_error_log` VALUES ('55558', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2024-03-06 13:29:12', '2024-03-06 13:29:12');
INSERT INTO `lc_error_log` VALUES ('55559', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2024-03-06 13:33:11', '2024-03-06 13:33:11');
INSERT INTO `lc_error_log` VALUES ('55560', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Validate.php', '验证码错误', '0', '524', '/v1/register', '127.0.0.1', '2024-03-06 13:34:24', '2024-03-06 13:34:24');
INSERT INTO `lc_error_log` VALUES ('55561', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2024-03-06 13:34:57', '2024-03-06 13:34:57');
INSERT INTO `lc_error_log` VALUES ('55562', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2024-03-06 13:35:06', '2024-03-06 13:35:06');
INSERT INTO `lc_error_log` VALUES ('55563', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2024-03-06 13:35:40', '2024-03-06 13:35:40');
INSERT INTO `lc_error_log` VALUES ('55564', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '47', '/v1/register', '127.0.0.1', '2024-03-06 13:35:52', '2024-03-06 13:35:52');
INSERT INTO `lc_error_log` VALUES ('55565', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '49', '/v1/register', '127.0.0.1', '2024-03-06 13:36:46', '2024-03-06 13:36:46');
INSERT INTO `lc_error_log` VALUES ('55566', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '验证码错误', '0', '49', '/v1/register', '127.0.0.1', '2024-03-06 13:36:52', '2024-03-06 13:36:52');
INSERT INTO `lc_error_log` VALUES ('55567', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '验证码错误', '0', '518', '/v1/findPwd', '127.0.0.1', '2024-03-06 13:48:08', '2024-03-06 13:48:08');
INSERT INTO `lc_error_log` VALUES ('55568', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '验证码错误', '0', '522', '/v1/findPwd', '127.0.0.1', '2024-03-06 13:53:04', '2024-03-06 13:53:04');
INSERT INTO `lc_error_log` VALUES ('55569', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\User.php', '验证码错误', '0', '522', '/v1/findPwd', '127.0.0.1', '2024-03-06 13:53:11', '2024-03-06 13:53:11');
INSERT INTO `lc_error_log` VALUES ('55570', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '58', '/v1/submitOrder', '127.0.0.1', '2024-03-06 15:57:43', '2024-03-06 15:57:43');
INSERT INTO `lc_error_log` VALUES ('55571', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '用户名或密码错误', '0', '113', '/v1/login', '127.0.0.1', '2024-03-06 16:24:25', '2024-03-06 16:24:25');
INSERT INTO `lc_error_log` VALUES ('55572', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '63', '/login', '127.0.0.1', '2024-03-19 21:23:42', '2024-03-19 21:23:42');
INSERT INTO `lc_error_log` VALUES ('55573', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-03-21 12:33:16', '2024-03-21 12:33:16');
INSERT INTO `lc_error_log` VALUES ('55574', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\AdminUser.php', '用户名或密码错误！', '0', '63', '/login', '127.0.0.1', '2024-03-21 12:34:15', '2024-03-21 12:34:15');
INSERT INTO `lc_error_log` VALUES ('55575', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/%7B%7Bapi%7D%7D/getZiyuanInfo?id=21', '127.0.0.1', '2024-03-21 12:36:56', '2024-03-21 12:36:56');
INSERT INTO `lc_error_log` VALUES ('55576', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/getZiyuanInfo?id=21', '127.0.0.1', '2024-03-21 12:37:04', '2024-03-21 12:37:04');
INSERT INTO `lc_error_log` VALUES ('55577', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Request.php', 'filter_var() expects parameter 2 to be int, bool given', '0', '1431', '/getZiyuanInfo?id=21&info=true', '127.0.0.1', '2024-03-21 12:38:09', '2024-03-21 12:38:09');
INSERT INTO `lc_error_log` VALUES ('55578', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\controller\\Ziyuan.php', 'Use of undefined constant bool - assumed \'bool\' (this will throw an Error in a future version of PHP)', '0', '111', '/getZiyuanInfo?id=21&info=true', '127.0.0.1', '2024-03-21 12:38:25', '2024-03-21 12:38:25');
INSERT INTO `lc_error_log` VALUES ('55579', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Request.php', 'filter_var() expects parameter 2 to be int, bool given', '0', '1431', '/getZiyuanInfo?id=21&info=true', '127.0.0.1', '2024-03-21 12:38:48', '2024-03-21 12:38:48');
INSERT INTO `lc_error_log` VALUES ('55580', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/test', '127.0.0.1', '2024-03-21 20:25:22', '2024-03-21 20:25:22');
INSERT INTO `lc_error_log` VALUES ('55581', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/test', '127.0.0.1', '2024-03-21 20:25:49', '2024-03-21 20:25:49');
INSERT INTO `lc_error_log` VALUES ('55582', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-03-21 20:25:52', '2024-03-21 20:25:52');
INSERT INTO `lc_error_log` VALUES ('55583', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/submitVipOrder', '127.0.0.1', '2024-03-22 11:38:51', '2024-03-22 11:38:51');
INSERT INTO `lc_error_log` VALUES ('55584', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '58', '/v1/submitVipOrder', '127.0.0.1', '2024-03-22 11:38:56', '2024-03-22 11:38:56');
INSERT INTO `lc_error_log` VALUES ('55585', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\User.php', 'syntax error, unexpected \'=\'', '0', '210', '/v1/submitVipOrder', '127.0.0.1', '2024-03-22 11:59:34', '2024-03-22 11:59:34');
INSERT INTO `lc_error_log` VALUES ('55586', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=32322222&info=true', '127.0.0.1', '2024-03-23 10:09:35', '2024-03-23 10:09:35');
INSERT INTO `lc_error_log` VALUES ('55587', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=32322222&info=true', '127.0.0.1', '2024-03-23 10:09:39', '2024-03-23 10:09:39');
INSERT INTO `lc_error_log` VALUES ('55588', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=32322222&info=true', '127.0.0.1', '2024-03-23 10:11:57', '2024-03-23 10:11:57');
INSERT INTO `lc_error_log` VALUES ('55589', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=32322222&info=true', '127.0.0.1', '2024-03-23 10:13:23', '2024-03-23 10:13:23');
INSERT INTO `lc_error_log` VALUES ('55590', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=ewewwww&info=true', '127.0.0.1', '2024-03-23 10:14:30', '2024-03-23 10:14:30');
INSERT INTO `lc_error_log` VALUES ('55591', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=ewewwww&info=true', '127.0.0.1', '2024-03-23 10:14:31', '2024-03-23 10:14:31');
INSERT INTO `lc_error_log` VALUES ('55592', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=ewewwww&info=true', '127.0.0.1', '2024-03-23 10:18:07', '2024-03-23 10:18:07');
INSERT INTO `lc_error_log` VALUES ('55593', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在', '0', '165', '/v1/getZiyuanInfo?id=ewewwww&info=true', '127.0.0.1', '2024-03-23 10:18:08', '2024-03-23 10:18:08');
INSERT INTO `lc_error_log` VALUES ('55594', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=235989999&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-23 10:22:32', '2024-03-23 10:22:32');
INSERT INTO `lc_error_log` VALUES ('55595', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=235989999&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-23 10:25:15', '2024-03-23 10:25:15');
INSERT INTO `lc_error_log` VALUES ('55596', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=235989999&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-23 10:26:20', '2024-03-23 10:26:20');
INSERT INTO `lc_error_log` VALUES ('55597', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', '参数错误', '0', '76', '/v1/getKeInfo?ke_id=fwww&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-23 10:30:38', '2024-03-23 10:30:38');
INSERT INTO `lc_error_log` VALUES ('55598', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus', '127.0.0.1', '2024-03-23 17:10:33', '2024-03-23 17:10:33');
INSERT INTO `lc_error_log` VALUES ('55599', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/submitVipOrder', '127.0.0.1', '2024-03-23 17:21:25', '2024-03-23 17:21:25');
INSERT INTO `lc_error_log` VALUES ('55600', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\UserVip.php', 'Cannot redeclare app\\api\\model\\UserVip::getOrderInfo()', '0', '70', '/v1/submitVipOrder', '127.0.0.1', '2024-03-23 17:30:44', '2024-03-23 17:30:44');
INSERT INTO `lc_error_log` VALUES ('55601', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', 'Trying to access array offset on value of type null', '0', '51', '/v1/submitVipOrder', '127.0.0.1', '2024-03-23 17:37:15', '2024-03-23 17:37:15');
INSERT INTO `lc_error_log` VALUES ('55602', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\UserVip.php', '数据操作失败', '0', '23', '/v1/submitVipOrder', '127.0.0.1', '2024-03-23 17:41:31', '2024-03-23 17:41:31');
INSERT INTO `lc_error_log` VALUES ('55603', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:41:54', '2024-03-23 17:41:54');
INSERT INTO `lc_error_log` VALUES ('55604', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:41:56', '2024-03-23 17:41:56');
INSERT INTO `lc_error_log` VALUES ('55605', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:41:59', '2024-03-23 17:41:59');
INSERT INTO `lc_error_log` VALUES ('55606', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:42:01', '2024-03-23 17:42:01');
INSERT INTO `lc_error_log` VALUES ('55607', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:42:04', '2024-03-23 17:42:04');
INSERT INTO `lc_error_log` VALUES ('55608', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:42:06', '2024-03-23 17:42:06');
INSERT INTO `lc_error_log` VALUES ('55609', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:42:09', '2024-03-23 17:42:09');
INSERT INTO `lc_error_log` VALUES ('55610', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id1', '127.0.0.1', '2024-03-23 17:42:12', '2024-03-23 17:42:12');
INSERT INTO `lc_error_log` VALUES ('55611', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:42:25', '2024-03-23 17:42:25');
INSERT INTO `lc_error_log` VALUES ('55612', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:42:27', '2024-03-23 17:42:27');
INSERT INTO `lc_error_log` VALUES ('55613', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:42:30', '2024-03-23 17:42:30');
INSERT INTO `lc_error_log` VALUES ('55614', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:42:32', '2024-03-23 17:42:32');
INSERT INTO `lc_error_log` VALUES ('55615', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '参数错误', '0', '47', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:43:43', '2024-03-23 17:43:43');
INSERT INTO `lc_error_log` VALUES ('55616', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '数据操作失败', '0', '152', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:44:50', '2024-03-23 17:44:50');
INSERT INTO `lc_error_log` VALUES ('55617', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '数据操作失败', '0', '152', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:44:53', '2024-03-23 17:44:53');
INSERT INTO `lc_error_log` VALUES ('55618', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\UserVip.php', 'Call to a member function where() on null', '0', '91', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:47:08', '2024-03-23 17:47:08');
INSERT INTO `lc_error_log` VALUES ('55619', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '订单不存在', '0', '150', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:48:34', '2024-03-23 17:48:34');
INSERT INTO `lc_error_log` VALUES ('55620', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '订单不存在', '0', '150', '/v1/getVipOrderStatus?order_id=20240323612217487227879424', '127.0.0.1', '2024-03-23 17:48:53', '2024-03-23 17:48:53');
INSERT INTO `lc_error_log` VALUES ('55621', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', '订单不存在', '0', '156', '/v1/getVipOrderStatus?order_id=20240323612217487227879424a', '127.0.0.1', '2024-03-23 17:51:08', '2024-03-23 17:51:08');
INSERT INTO `lc_error_log` VALUES ('55622', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '58', '/v1/getUserVipList', '127.0.0.1', '2024-03-23 18:09:08', '2024-03-23 18:09:08');
INSERT INTO `lc_error_log` VALUES ('55623', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\User.php', 'Call to undefined method app\\api\\business\\Vip::getUserVipList()', '0', '251', '/v1/getUserVipList', '127.0.0.1', '2024-03-23 18:09:19', '2024-03-23 18:09:19');
INSERT INTO `lc_error_log` VALUES ('55624', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Vip.php', 'Undefined variable: model', '0', '46', '/v1/submitVipOrder', '127.0.0.1', '2024-03-23 20:54:57', '2024-03-23 20:54:57');
INSERT INTO `lc_error_log` VALUES ('55625', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Undefined variable: time', '0', '38', '/v1/test', '127.0.0.1', '2024-03-24 09:34:00', '2024-03-24 09:34:00');
INSERT INTO `lc_error_log` VALUES ('55626', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Order.php', '当前资源不存在', '0', '41', '/v1/submitOrder', '127.0.0.1', '2024-03-24 09:45:01', '2024-03-24 09:45:01');
INSERT INTO `lc_error_log` VALUES ('55627', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Order.php', '不能购买自己的资源', '0', '48', '/v1/submitOrder', '127.0.0.1', '2024-03-24 09:54:16', '2024-03-24 09:54:16');
INSERT INTO `lc_error_log` VALUES ('55628', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Order.php', 'Call to undefined function app\\api\\business\\Vip()', '0', '89', '/v1/submitOrder', '127.0.0.1', '2024-03-24 09:54:56', '2024-03-24 09:54:56');
INSERT INTO `lc_error_log` VALUES ('55629', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Order.php', 'Undefined property: app\\api\\business\\Order::$user_id', '0', '91', '/v1/submitOrder', '127.0.0.1', '2024-03-24 09:57:51', '2024-03-24 09:57:51');
INSERT INTO `lc_error_log` VALUES ('55630', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\UserVip.php', '数据操作失败', '0', '145', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:10:17', '2024-03-24 10:10:17');
INSERT INTO `lc_error_log` VALUES ('55631', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\UserVip.php', 'Undefined variable: type', '0', '143', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:10:32', '2024-03-24 10:10:32');
INSERT INTO `lc_error_log` VALUES ('55632', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\UserVip.php', 'Undefined variable: type', '0', '150', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:10:52', '2024-03-24 10:10:52');
INSERT INTO `lc_error_log` VALUES ('55633', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\UserVip.php', 'Undefined variable: type', '0', '150', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:11:27', '2024-03-24 10:11:27');
INSERT INTO `lc_error_log` VALUES ('55634', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined index: pid', '0', '713', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:25:42', '2024-03-24 10:25:42');
INSERT INTO `lc_error_log` VALUES ('55635', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'syntax error, unexpected \')\'', '0', '717', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:31:04', '2024-03-24 10:31:04');
INSERT INTO `lc_error_log` VALUES ('55636', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined variable: result', '0', '718', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:31:32', '2024-03-24 10:31:32');
INSERT INTO `lc_error_log` VALUES ('55637', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/submitOrder', '127.0.0.1', '2024-03-24 10:32:47', '2024-03-24 10:32:47');
INSERT INTO `lc_error_log` VALUES ('55638', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Order.php', 'syntax error, unexpected \'if\' (T_IF)', '0', '81', '/v1/submitOrder', '127.0.0.1', '2024-03-24 13:00:12', '2024-03-24 13:00:12');
INSERT INTO `lc_error_log` VALUES ('55639', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Order.php', 'Undefined variable: start_time', '0', '76', '/v1/submitOrder', '127.0.0.1', '2024-03-24 13:06:50', '2024-03-24 13:06:50');
INSERT INTO `lc_error_log` VALUES ('55640', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\Order.php', '数据操作失败', '0', '180', '/v1/submitOrder', '127.0.0.1', '2024-03-24 13:07:12', '2024-03-24 13:07:12');
INSERT INTO `lc_error_log` VALUES ('55641', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pay_time\' in \'where clause\'', '10501', '796', '/v1/submitOrder', '127.0.0.1', '2024-03-24 13:07:36', '2024-03-24 13:07:36');
INSERT INTO `lc_error_log` VALUES ('55642', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', '当前课程不存在', '0', '81', '/v1/getKeInfo?ke_id=1&chapters_id=2&directory_id=4&order_id=20230415487886240171950081', '127.0.0.1', '2024-03-24 13:32:04', '2024-03-24 13:32:04');
INSERT INTO `lc_error_log` VALUES ('55643', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', 'Undefined variable: ziyuan', '0', '98', '/v1/getKeInfo?ke_id=23584&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-24 13:40:04', '2024-03-24 13:40:04');
INSERT INTO `lc_error_log` VALUES ('55644', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined index: pid', '0', '712', '/v1/getKeInfo?ke_id=23584&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-24 13:40:25', '2024-03-24 13:40:25');
INSERT INTO `lc_error_log` VALUES ('55645', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', '参数错误', '0', '107', '/v1/getKeInfo?ke_id=23584&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-24 13:45:30', '2024-03-24 13:45:30');
INSERT INTO `lc_error_log` VALUES ('55646', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Kecheng.php', '参数错误', '0', '108', '/v1/getKeInfo?ke_id=23584&order_id=&chapters_id=&directory_id=', '127.0.0.1', '2024-03-24 13:46:12', '2024-03-24 13:46:12');
INSERT INTO `lc_error_log` VALUES ('55647', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-03-26 09:18:21', '2024-03-26 09:18:21');
INSERT INTO `lc_error_log` VALUES ('55648', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-03-26 09:18:21', '2024-03-26 09:18:21');
INSERT INTO `lc_error_log` VALUES ('55649', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '60', '/getVipOrderList', '127.0.0.1', '2024-03-26 09:20:39', '2024-03-26 09:20:39');
INSERT INTO `lc_error_log` VALUES ('55650', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\platform\\controller\\User->getVipOrderList()', '0', '107', '/getVipOrderList', '127.0.0.1', '2024-03-26 09:21:10', '2024-03-26 09:21:10');
INSERT INTO `lc_error_log` VALUES ('55651', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->getVipOrderList', '10500', '117', '/getVipOrderList', '127.0.0.1', '2024-03-26 09:25:52', '2024-03-26 09:25:52');
INSERT INTO `lc_error_log` VALUES ('55652', 'platform', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '24', '/indexData', '127.0.0.1', '2024-03-27 13:03:02', '2024-03-27 13:03:02');
INSERT INTO `lc_error_log` VALUES ('55653', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '58', '/v1/mp/getZiyuanInfo?ziyuan_id=27765', '127.0.0.1', '2024-03-29 21:15:32', '2024-03-29 21:15:32');
INSERT INTO `lc_error_log` VALUES ('55654', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'json_decode() expects parameter 1 to be string, object given', '0', '58', '/v1/test', '127.0.0.1', '2024-03-29 22:10:29', '2024-03-29 22:10:29');
INSERT INTO `lc_error_log` VALUES ('55655', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Cannot use object of type stdClass as array', '0', '62', '/v1/test', '127.0.0.1', '2024-03-29 22:11:45', '2024-03-29 22:11:45');
INSERT INTO `lc_error_log` VALUES ('55656', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Too few arguments to function Firebase\\JWT\\JWT::encode(), 2 passed in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php on line 55 and at least 3 expected', '0', '183', '/v1/test', '127.0.0.1', '2024-03-29 22:30:25', '2024-03-29 22:30:25');
INSERT INTO `lc_error_log` VALUES ('55657', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', 'syntax error, unexpected \'$md5Token\' (T_VARIABLE)', '0', '135', '/v1/login', '127.0.0.1', '2024-03-29 22:45:23', '2024-03-29 22:45:23');
INSERT INTO `lc_error_log` VALUES ('55658', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '用户名或密码错误', '0', '116', '/v1/login', '127.0.0.1', '2024-03-29 22:45:50', '2024-03-29 22:45:50');
INSERT INTO `lc_error_log` VALUES ('55659', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '用户名或密码错误', '0', '116', '/v1/login', '127.0.0.1', '2024-03-29 22:46:00', '2024-03-29 22:46:00');
INSERT INTO `lc_error_log` VALUES ('55660', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', 'Undefined variable: prefix', '0', '133', '/v1/login', '127.0.0.1', '2024-03-29 22:46:05', '2024-03-29 22:46:05');
INSERT INTO `lc_error_log` VALUES ('55661', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '23', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:49:20', '2024-03-29 22:49:20');
INSERT INTO `lc_error_log` VALUES ('55662', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Class \'app\\api\\controller\\v1\\JwT\' not found', '0', '35', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:51:19', '2024-03-29 22:51:19');
INSERT INTO `lc_error_log` VALUES ('55663', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Wrong number of segments', '0', '102', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:51:53', '2024-03-29 22:51:53');
INSERT INTO `lc_error_log` VALUES ('55664', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Wrong number of segments', '0', '102', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:55:16', '2024-03-29 22:55:16');
INSERT INTO `lc_error_log` VALUES ('55665', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Wrong number of segments', '0', '102', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:55:19', '2024-03-29 22:55:19');
INSERT INTO `lc_error_log` VALUES ('55666', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Wrong number of segments', '0', '102', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:55:20', '2024-03-29 22:55:20');
INSERT INTO `lc_error_log` VALUES ('55667', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '23', '/v1/loginOut', '127.0.0.1', '2024-03-29 22:56:02', '2024-03-29 22:56:02');
INSERT INTO `lc_error_log` VALUES ('55668', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Wrong number of segments', '0', '102', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:56:46', '2024-03-29 22:56:46');
INSERT INTO `lc_error_log` VALUES ('55669', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Wrong number of segments', '0', '102', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 22:56:48', '2024-03-29 22:56:48');
INSERT INTO `lc_error_log` VALUES ('55670', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', 'Undefined variable: jwt', '0', '140', '/v1/login', '127.0.0.1', '2024-03-29 23:03:58', '2024-03-29 23:03:58');
INSERT INTO `lc_error_log` VALUES ('55671', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '49', '/v1/getKeTryList?page=1&pageSize=12', '127.0.0.1', '2024-03-29 23:04:16', '2024-03-29 23:04:16');
INSERT INTO `lc_error_log` VALUES ('55672', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '49', '/v1/getZiyuanListBrowse?page=1&pageSize=3', '127.0.0.1', '2024-03-29 23:04:16', '2024-03-29 23:04:16');
INSERT INTO `lc_error_log` VALUES ('55673', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '49', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-03-29 23:04:16', '2024-03-29 23:04:16');
INSERT INTO `lc_error_log` VALUES ('55674', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '49', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2024-03-29 23:04:16', '2024-03-29 23:04:16');
INSERT INTO `lc_error_log` VALUES ('55675', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '26', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:04:41', '2024-03-29 23:04:41');
INSERT INTO `lc_error_log` VALUES ('55676', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '26', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:04:56', '2024-03-29 23:04:56');
INSERT INTO `lc_error_log` VALUES ('55677', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '26', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:05:20', '2024-03-29 23:05:20');
INSERT INTO `lc_error_log` VALUES ('55678', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '26', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:05:21', '2024-03-29 23:05:21');
INSERT INTO `lc_error_log` VALUES ('55679', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '26', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:05:22', '2024-03-29 23:05:22');
INSERT INTO `lc_error_log` VALUES ('55680', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', 'Undefined variable: jwt', '0', '55', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:09:41', '2024-03-29 23:09:41');
INSERT INTO `lc_error_log` VALUES ('55681', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', 'Cannot use object of type stdClass as array', '0', '89', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:11:08', '2024-03-29 23:11:08');
INSERT INTO `lc_error_log` VALUES ('55682', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', 'Undefined property: stdClass::$login_name', '0', '91', '/v1/getUserInfo', '127.0.0.1', '2024-03-29 23:14:13', '2024-03-29 23:14:13');
INSERT INTO `lc_error_log` VALUES ('55683', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Argument 1 passed to Firebase\\JWT\\JWT::decode() must be of the type string, null given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php on line 54', '0', '90', '/v1/loginOut', '127.0.0.1', '2024-03-29 23:21:52', '2024-03-29 23:21:52');
INSERT INTO `lc_error_log` VALUES ('55684', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', 'Undefined index: id', '0', '156', '/v1/login', '127.0.0.1', '2024-03-29 23:25:12', '2024-03-29 23:25:12');
INSERT INTO `lc_error_log` VALUES ('55685', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Argument 1 passed to Firebase\\JWT\\JWT::decode() must be of the type string, null given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php on line 54', '0', '90', '/v1/loginOut', '127.0.0.1', '2024-03-29 23:25:54', '2024-03-29 23:25:54');
INSERT INTO `lc_error_log` VALUES ('55686', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Argument 1 passed to Firebase\\JWT\\JWT::decode() must be of the type string, null given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php on line 54', '0', '90', '/v1/loginOut', '127.0.0.1', '2024-03-29 23:26:13', '2024-03-29 23:26:13');
INSERT INTO `lc_error_log` VALUES ('55687', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Argument 1 passed to Firebase\\JWT\\JWT::decode() must be of the type string, null given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php on line 54', '0', '90', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-29 23:26:44', '2024-03-29 23:26:44');
INSERT INTO `lc_error_log` VALUES ('55688', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Argument 1 passed to Firebase\\JWT\\JWT::decode() must be of the type string, null given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php on line 54', '0', '90', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-29 23:26:44', '2024-03-29 23:26:44');
INSERT INTO `lc_error_log` VALUES ('55689', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Argument 1 passed to Firebase\\JWT\\JWT::decode() must be of the type string, null given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php on line 53', '0', '90', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-29 23:28:29', '2024-03-29 23:28:29');
INSERT INTO `lc_error_log` VALUES ('55690', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\firebase\\php-jwt\\src\\JWT.php', 'Argument 1 passed to Firebase\\JWT\\JWT::decode() must be of the type string, null given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php on line 53', '0', '90', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-29 23:28:29', '2024-03-29 23:28:29');
INSERT INTO `lc_error_log` VALUES ('55691', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-29 23:29:39', '2024-03-29 23:29:39');
INSERT INTO `lc_error_log` VALUES ('55692', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-29 23:29:39', '2024-03-29 23:29:39');
INSERT INTO `lc_error_log` VALUES ('55693', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-29 23:29:45', '2024-03-29 23:29:45');
INSERT INTO `lc_error_log` VALUES ('55694', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-29 23:29:45', '2024-03-29 23:29:45');
INSERT INTO `lc_error_log` VALUES ('55695', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\LogRegister.php', '用户名或密码错误', '0', '116', '/v1/login', '127.0.0.1', '2024-03-29 23:34:35', '2024-03-29 23:34:35');
INSERT INTO `lc_error_log` VALUES ('55696', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-29 23:36:13', '2024-03-29 23:36:13');
INSERT INTO `lc_error_log` VALUES ('55697', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-29 23:36:13', '2024-03-29 23:36:13');
INSERT INTO `lc_error_log` VALUES ('55698', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-29 23:36:36', '2024-03-29 23:36:36');
INSERT INTO `lc_error_log` VALUES ('55699', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-29 23:36:36', '2024-03-29 23:36:36');
INSERT INTO `lc_error_log` VALUES ('55700', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-29 23:37:50', '2024-03-29 23:37:50');
INSERT INTO `lc_error_log` VALUES ('55701', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-29 23:37:50', '2024-03-29 23:37:50');
INSERT INTO `lc_error_log` VALUES ('55702', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-30 08:18:04', '2024-03-30 08:18:04');
INSERT INTO `lc_error_log` VALUES ('55703', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-30 08:18:04', '2024-03-30 08:18:04');
INSERT INTO `lc_error_log` VALUES ('55704', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '29', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-30 08:19:20', '2024-03-30 08:19:20');
INSERT INTO `lc_error_log` VALUES ('55705', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '29', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-30 08:19:20', '2024-03-30 08:19:20');
INSERT INTO `lc_error_log` VALUES ('55706', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '29', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-30 08:19:28', '2024-03-30 08:19:28');
INSERT INTO `lc_error_log` VALUES ('55707', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '29', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-30 08:19:28', '2024-03-30 08:19:28');
INSERT INTO `lc_error_log` VALUES ('55708', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-30 08:19:37', '2024-03-30 08:19:37');
INSERT INTO `lc_error_log` VALUES ('55709', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-30 08:19:37', '2024-03-30 08:19:37');
INSERT INTO `lc_error_log` VALUES ('55710', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-03-30 08:20:29', '2024-03-30 08:20:29');
INSERT INTO `lc_error_log` VALUES ('55711', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthMpMiddleware.php', 'Illegal string offset \'type\'', '0', '17', '/v1/mp/tongji/index', '127.0.0.1', '2024-03-30 08:20:29', '2024-03-30 08:20:29');
INSERT INTO `lc_error_log` VALUES ('55712', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getKeTryList?page=1&pageSize=12', '127.0.0.1', '2024-03-30 08:38:47', '2024-03-30 08:38:47');
INSERT INTO `lc_error_log` VALUES ('55713', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getZiyuanListBrowse?page=1&pageSize=3', '127.0.0.1', '2024-03-30 08:38:47', '2024-03-30 08:38:47');
INSERT INTO `lc_error_log` VALUES ('55714', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-03-30 08:38:47', '2024-03-30 08:38:47');
INSERT INTO `lc_error_log` VALUES ('55715', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2024-03-30 08:38:47', '2024-03-30 08:38:47');
INSERT INTO `lc_error_log` VALUES ('55716', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getZiyuanListBrowse?page=1&pageSize=3', '127.0.0.1', '2024-03-30 08:39:05', '2024-03-30 08:39:05');
INSERT INTO `lc_error_log` VALUES ('55717', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2024-03-30 08:39:05', '2024-03-30 08:39:05');
INSERT INTO `lc_error_log` VALUES ('55718', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getKeTryList?page=1&pageSize=12', '127.0.0.1', '2024-03-30 08:39:05', '2024-03-30 08:39:05');
INSERT INTO `lc_error_log` VALUES ('55719', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', 'Undefined variable: jwt', '0', '46', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-03-30 08:39:05', '2024-03-30 08:39:05');
INSERT INTO `lc_error_log` VALUES ('55720', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-03-31 09:30:38', '2024-03-31 09:30:38');
INSERT INTO `lc_error_log` VALUES ('55721', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanListBrowse?page=1&pageSize=3', '127.0.0.1', '2024-03-31 09:30:38', '2024-03-31 09:30:38');
INSERT INTO `lc_error_log` VALUES ('55722', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2024-03-31 09:30:38', '2024-03-31 09:30:38');
INSERT INTO `lc_error_log` VALUES ('55723', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getKeTryList?page=1&pageSize=12', '127.0.0.1', '2024-03-31 09:30:38', '2024-03-31 09:30:38');
INSERT INTO `lc_error_log` VALUES ('55724', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/getZiyuanInfo1?info=1&id=18', '127.0.0.1', '2024-03-31 09:35:33', '2024-03-31 09:35:33');
INSERT INTO `lc_error_log` VALUES ('55725', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/getZiyuanInfo1?info=1&id=18', '127.0.0.1', '2024-03-31 09:35:47', '2024-03-31 09:35:47');
INSERT INTO `lc_error_log` VALUES ('55726', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/getZiyuanInfo1?info=1&id=18', '127.0.0.1', '2024-03-31 09:35:48', '2024-03-31 09:35:48');
INSERT INTO `lc_error_log` VALUES ('55727', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/getZiyuanInfo1?info=1&id=18', '127.0.0.1', '2024-03-31 09:35:49', '2024-03-31 09:35:49');
INSERT INTO `lc_error_log` VALUES ('55728', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanInfo?id=27736&info=true', '127.0.0.1', '2024-03-31 09:36:39', '2024-03-31 09:36:39');
INSERT INTO `lc_error_log` VALUES ('55729', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanInfo?id=27736', '127.0.0.1', '2024-03-31 09:36:53', '2024-03-31 09:36:53');
INSERT INTO `lc_error_log` VALUES ('55730', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanInfo?id=27736', '127.0.0.1', '2024-03-31 09:36:55', '2024-03-31 09:36:55');
INSERT INTO `lc_error_log` VALUES ('55731', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanInfo?id=27736', '127.0.0.1', '2024-03-31 09:37:07', '2024-03-31 09:37:07');
INSERT INTO `lc_error_log` VALUES ('55732', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-03-31 09:37:17', '2024-03-31 09:37:17');
INSERT INTO `lc_error_log` VALUES ('55733', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanListBrowse?page=1&pageSize=3', '127.0.0.1', '2024-03-31 09:37:17', '2024-03-31 09:37:17');
INSERT INTO `lc_error_log` VALUES ('55734', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getKeTryList?page=1&pageSize=12', '127.0.0.1', '2024-03-31 09:37:17', '2024-03-31 09:37:17');
INSERT INTO `lc_error_log` VALUES ('55735', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2024-03-31 09:37:17', '2024-03-31 09:37:17');
INSERT INTO `lc_error_log` VALUES ('55736', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanListBrowse?page=1&pageSize=3', '127.0.0.1', '2024-03-31 09:37:27', '2024-03-31 09:37:27');
INSERT INTO `lc_error_log` VALUES ('55737', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-03-31 09:37:27', '2024-03-31 09:37:27');
INSERT INTO `lc_error_log` VALUES ('55738', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getKeTryList?page=1&pageSize=12', '127.0.0.1', '2024-03-31 09:37:27', '2024-03-31 09:37:27');
INSERT INTO `lc_error_log` VALUES ('55739', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\ApiBase.php', '未登录', '-1', '37', '/v1/getZiyuanListRandom?page=1&pageSize=20', '127.0.0.1', '2024-03-31 09:37:27', '2024-03-31 09:37:27');
INSERT INTO `lc_error_log` VALUES ('55740', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/submitOrder', '127.0.0.1', '2024-04-03 08:43:35', '2024-04-03 08:43:35');
INSERT INTO `lc_error_log` VALUES ('55741', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/submitOrder', '127.0.0.1', '2024-04-03 09:09:28', '2024-04-03 09:09:28');
INSERT INTO `lc_error_log` VALUES ('55742', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/getZiyuanInfo?info=true&id=27736', '127.0.0.1', '2024-04-03 09:11:55', '2024-04-03 09:11:55');
INSERT INTO `lc_error_log` VALUES ('55743', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/getZiyuanInfo?id=27736', '127.0.0.1', '2024-04-03 09:12:08', '2024-04-03 09:12:08');
INSERT INTO `lc_error_log` VALUES ('55744', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/getZiyuanInfo?id=27736', '127.0.0.1', '2024-04-03 09:12:09', '2024-04-03 09:12:09');
INSERT INTO `lc_error_log` VALUES ('55745', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:12:45', '2024-04-03 09:12:45');
INSERT INTO `lc_error_log` VALUES ('55746', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:12:47', '2024-04-03 09:12:47');
INSERT INTO `lc_error_log` VALUES ('55747', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:13:40', '2024-04-03 09:13:40');
INSERT INTO `lc_error_log` VALUES ('55748', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:13:53', '2024-04-03 09:13:53');
INSERT INTO `lc_error_log` VALUES ('55749', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:13:55', '2024-04-03 09:13:55');
INSERT INTO `lc_error_log` VALUES ('55750', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:14:38', '2024-04-03 09:14:38');
INSERT INTO `lc_error_log` VALUES ('55751', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:14:40', '2024-04-03 09:14:40');
INSERT INTO `lc_error_log` VALUES ('55752', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:15:21', '2024-04-03 09:15:21');
INSERT INTO `lc_error_log` VALUES ('55753', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:15:23', '2024-04-03 09:15:23');
INSERT INTO `lc_error_log` VALUES ('55754', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:15:56', '2024-04-03 09:15:56');
INSERT INTO `lc_error_log` VALUES ('55755', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getZiyuanInfo?id=38&info=true', '127.0.0.1', '2024-04-03 09:16:00', '2024-04-03 09:16:00');
INSERT INTO `lc_error_log` VALUES ('55756', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->values', '10500', '117', '/v1/test', '127.0.0.1', '2024-04-06 13:06:07', '2024-04-06 13:06:07');
INSERT INTO `lc_error_log` VALUES ('55757', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'array_push() expects parameter 1 to be array, int given', '0', '48', '/v1/test', '127.0.0.1', '2024-04-06 13:09:22', '2024-04-06 13:09:22');
INSERT INTO `lc_error_log` VALUES ('55758', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Undefined index: _thumbnail_id', '0', '70', '/v1/test', '127.0.0.1', '2024-04-06 14:03:55', '2024-04-06 14:03:55');
INSERT INTO `lc_error_log` VALUES ('55759', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=27786&info=true', '127.0.0.1', '2024-04-06 16:16:51', '2024-04-06 16:16:51');
INSERT INTO `lc_error_log` VALUES ('55760', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=27786&info=true', '127.0.0.1', '2024-04-06 16:16:51', '2024-04-06 16:16:51');
INSERT INTO `lc_error_log` VALUES ('55761', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'gouziyuan.lc_posts\' doesn\'t exist', '10501', '796', '/v1/test', '127.0.0.1', '2024-04-06 17:48:30', '2024-04-06 17:48:30');
INSERT INTO `lc_error_log` VALUES ('55762', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\MpZiyuan.php', '下载地址必填', '0', '104', '/v1/mp/fabu', '127.0.0.1', '2024-04-06 23:39:59', '2024-04-06 23:39:59');
INSERT INTO `lc_error_log` VALUES ('55763', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\MpZiyuan.php', '下载地址必填', '0', '104', '/v1/mp/fabu', '127.0.0.1', '2024-04-06 23:40:46', '2024-04-06 23:40:46');
INSERT INTO `lc_error_log` VALUES ('55764', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'miss update condition', '10500', '1049', '/v1/test?page=1', '127.0.0.1', '2024-04-07 09:16:28', '2024-04-07 09:16:28');
INSERT INTO `lc_error_log` VALUES ('55765', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined variable: prefix', '0', '305', '/v1/getCategorys?subclass=true', '127.0.0.1', '2024-04-07 09:31:07', '2024-04-07 09:31:07');
INSERT INTO `lc_error_log` VALUES ('55766', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\MpZiyuan.php', '下载地址必填', '0', '110', '/v1/mp/fabu', '127.0.0.1', '2024-04-07 09:39:29', '2024-04-07 09:39:29');
INSERT INTO `lc_error_log` VALUES ('55767', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=28528&info=true', '127.0.0.1', '2024-04-07 10:03:55', '2024-04-07 10:03:55');
INSERT INTO `lc_error_log` VALUES ('55768', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=28528&info=true', '127.0.0.1', '2024-04-07 10:04:01', '2024-04-07 10:04:01');
INSERT INTO `lc_error_log` VALUES ('55769', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=29644&info=true', '127.0.0.1', '2024-04-07 10:31:08', '2024-04-07 10:31:08');
INSERT INTO `lc_error_log` VALUES ('55770', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=29644&info=true', '127.0.0.1', '2024-04-07 10:31:08', '2024-04-07 10:31:08');
INSERT INTO `lc_error_log` VALUES ('55771', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=31712&info=true', '127.0.0.1', '2024-04-07 14:10:11', '2024-04-07 14:10:11');
INSERT INTO `lc_error_log` VALUES ('55772', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=31712&info=true', '127.0.0.1', '2024-04-07 14:10:12', '2024-04-07 14:10:12');
INSERT INTO `lc_error_log` VALUES ('55773', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=28792&info=true', '127.0.0.1', '2024-04-07 14:10:50', '2024-04-07 14:10:50');
INSERT INTO `lc_error_log` VALUES ('55774', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', '当前资源不存在或未通过审核', '0', '167', '/v1/getZiyuanInfo?id=28792&info=true', '127.0.0.1', '2024-04-07 14:10:51', '2024-04-07 14:10:51');
INSERT INTO `lc_error_log` VALUES ('55775', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'fields not exists:[price]', '10500', '164', '/v1/test?page=1', '127.0.0.1', '2024-04-07 15:38:10', '2024-04-07 15:38:10');
INSERT INTO `lc_error_log` VALUES ('55776', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'fields not exists:[category_id]', '10500', '164', '/v1/test?page=1', '127.0.0.1', '2024-04-07 15:38:41', '2024-04-07 15:38:41');
INSERT INTO `lc_error_log` VALUES ('55777', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'strpos() expects parameter 1 to be string, int given', '0', '379', '/v1/test?page=1', '127.0.0.1', '2024-04-07 15:58:57', '2024-04-07 15:58:57');
INSERT INTO `lc_error_log` VALUES ('55778', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'fields not exists:[download_url1]', '10500', '164', '/v1/test?page=1', '127.0.0.1', '2024-04-07 16:15:39', '2024-04-07 16:15:39');
INSERT INTO `lc_error_log` VALUES ('55779', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'fields not exists:[download_url1]', '10500', '164', '/v1/test?page=1', '127.0.0.1', '2024-04-07 16:16:14', '2024-04-07 16:16:14');
INSERT INTO `lc_error_log` VALUES ('55780', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Undefined variable: var', '0', '49', '/v1/test?page=1', '127.0.0.1', '2024-04-08 12:16:05', '2024-04-08 12:16:05');
INSERT INTO `lc_error_log` VALUES ('55781', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Undefined index: post_content', '0', '51', '/v1/test?page=1', '127.0.0.1', '2024-04-08 12:17:53', '2024-04-08 12:17:53');
INSERT INTO `lc_error_log` VALUES ('55782', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'unserialize(): Error at offset 116 of 118 bytes', '0', '68', '/v1/test?page=1', '127.0.0.1', '2024-04-08 14:53:20', '2024-04-08 14:53:20');
INSERT INTO `lc_error_log` VALUES ('55783', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Undefined index: download_url', '0', '76', '/v1/test?page=1', '127.0.0.1', '2024-04-08 15:01:42', '2024-04-08 15:01:42');
INSERT INTO `lc_error_log` VALUES ('55784', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'title\' in \'field list\'', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-08 15:13:05', '2024-04-08 15:13:05');
INSERT INTO `lc_error_log` VALUES ('55785', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'download_url\' in \'field list\'', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-08 15:22:10', '2024-04-08 15:22:10');
INSERT INTO `lc_error_log` VALUES ('55786', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'ziyuan_cover\' in \'field list\'', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-08 15:40:21', '2024-04-08 15:40:21');
INSERT INTO `lc_error_log` VALUES ('55787', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'content\' in \'field list\'', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-08 16:02:00', '2024-04-08 16:02:00');
INSERT INTO `lc_error_log` VALUES ('55788', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'gouziyuan.lc_ke_cover\' doesn\'t exist', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-08 16:02:20', '2024-04-08 16:02:20');
INSERT INTO `lc_error_log` VALUES ('55789', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'content\' in \'field list\'', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-08 16:03:11', '2024-04-08 16:03:11');
INSERT INTO `lc_error_log` VALUES ('55790', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Undefined variable: vke_cover', '0', '56', '/v1/test?page=1', '127.0.0.1', '2024-04-08 18:26:58', '2024-04-08 18:26:58');
INSERT INTO `lc_error_log` VALUES ('55791', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'id\' in \'field list\'', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-08 20:13:08', '2024-04-08 20:13:08');
INSERT INTO `lc_error_log` VALUES ('55792', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\route\\dispatch\\Controller.php', 'method not exists:app\\api\\controller\\v1\\Ziyuan->getLabelList()', '0', '107', '/v1/getLabelList', '127.0.0.1', '2024-04-09 19:01:53', '2024-04-09 19:01:53');
INSERT INTO `lc_error_log` VALUES ('55793', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Ziyuan.php', 'Undefined variable: ziyuanBus', '0', '254', '/v1/getLabelList', '127.0.0.1', '2024-04-09 19:05:03', '2024-04-09 19:05:03');
INSERT INTO `lc_error_log` VALUES ('55794', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\Ziyuan.php', '数据操作失败', '0', '200', '/v1/getLabelList', '127.0.0.1', '2024-04-09 19:07:08', '2024-04-09 19:07:08');
INSERT INTO `lc_error_log` VALUES ('55795', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Ziyuan.php', 'Undefined index: label', '0', '758', '/v1/getLabelList', '127.0.0.1', '2024-04-09 19:14:11', '2024-04-09 19:14:11');
INSERT INTO `lc_error_log` VALUES ('55796', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\model\\Ziyuan.php', '数据操作失败', '0', '205', '/v1/getLabelList', '127.0.0.1', '2024-04-09 21:31:16', '2024-04-09 21:31:16');
INSERT INTO `lc_error_log` VALUES ('55797', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'order express error:rand()', '10500', '913', '/v1/getLabelList', '127.0.0.1', '2024-04-09 21:31:37', '2024-04-09 21:31:37');
INSERT INTO `lc_error_log` VALUES ('55798', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'order express error:rand()', '10500', '913', '/v1/getLabelList', '127.0.0.1', '2024-04-09 21:32:07', '2024-04-09 21:32:07');
INSERT INTO `lc_error_log` VALUES ('55799', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'order express error:rand()', '10500', '913', '/v1/getLabelList', '127.0.0.1', '2024-04-09 21:32:32', '2024-04-09 21:32:32');
INSERT INTO `lc_error_log` VALUES ('55800', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'order express error:rand()', '10500', '913', '/v1/getLabelList', '127.0.0.1', '2024-04-09 21:33:20', '2024-04-09 21:33:20');
INSERT INTO `lc_error_log` VALUES ('55801', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'order express error:rand()', '10500', '913', '/v1/getLabelList', '127.0.0.1', '2024-04-09 21:33:22', '2024-04-09 21:33:22');
INSERT INTO `lc_error_log` VALUES ('55802', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\Builder.php', 'order express error:rand()', '10500', '913', '/v1/getLabelList', '127.0.0.1', '2024-04-09 21:33:32', '2024-04-09 21:33:32');
INSERT INTO `lc_error_log` VALUES ('55803', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Undefined variable: res', '0', '52', '/v1/test?page=1', '127.0.0.1', '2024-04-10 12:24:40', '2024-04-10 12:24:40');
INSERT INTO `lc_error_log` VALUES ('55804', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'gouziyuan.lc_posts\' doesn\'t exist', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-10 12:32:16', '2024-04-10 12:32:16');
INSERT INTO `lc_error_log` VALUES ('55805', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'gouziyuan.lc_posts\' doesn\'t exist', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-10 12:34:58', '2024-04-10 12:34:58');
INSERT INTO `lc_error_log` VALUES ('55806', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\DbManager.php', 'Undefined db config:zy_gouziyuan', '0', '248', '/v1/test?page=1', '127.0.0.1', '2024-04-10 12:40:57', '2024-04-10 12:40:57');
INSERT INTO `lc_error_log` VALUES ('55807', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Class \'app\\api\\controller\\v1\\Pinyin\' not found', '0', '42', '/v1/test?page=1', '127.0.0.1', '2024-04-10 13:56:06', '2024-04-10 13:56:06');
INSERT INTO `lc_error_log` VALUES ('55808', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Class \'overtrue\\Pinyin\' not found', '0', '44', '/v1/test?page=1', '127.0.0.1', '2024-04-10 13:57:47', '2024-04-10 13:57:47');
INSERT INTO `lc_error_log` VALUES ('55809', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Class \'app\\api\\controller\\v1\\Pinyin\' not found', '0', '44', '/v1/test?page=1', '127.0.0.1', '2024-04-10 13:58:45', '2024-04-10 13:58:45');
INSERT INTO `lc_error_log` VALUES ('55810', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'require(vendor/autoload.php): failed to open stream: No such file or directory', '0', '12', '/v1/test?page=1', '127.0.0.1', '2024-04-10 13:59:20', '2024-04-10 13:59:20');
INSERT INTO `lc_error_log` VALUES ('55811', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Non-static method Overtrue\\Pinyin\\Pinyin::sentence() should not be called statically', '0', '44', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:01:39', '2024-04-10 14:01:39');
INSERT INTO `lc_error_log` VALUES ('55812', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Non-static method Overtrue\\Pinyin\\Pinyin::sentence() should not be called statically', '0', '44', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:01:52', '2024-04-10 14:01:52');
INSERT INTO `lc_error_log` VALUES ('55813', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Non-static method Overtrue\\Pinyin\\Pinyin::sentence() should not be called statically', '0', '44', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:03:02', '2024-04-10 14:03:02');
INSERT INTO `lc_error_log` VALUES ('55814', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Non-static method Overtrue\\Pinyin\\Pinyin::sentence() should not be called statically', '0', '44', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:03:03', '2024-04-10 14:03:03');
INSERT INTO `lc_error_log` VALUES ('55815', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Call to undefined method Overtrue\\Pinyin\\Pinyin::Pinyin()', '0', '45', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:03:29', '2024-04-10 14:03:29');
INSERT INTO `lc_error_log` VALUES ('55816', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Non-static method Overtrue\\Pinyin\\Pinyin::permalink() should not be called statically', '0', '45', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:04:31', '2024-04-10 14:04:31');
INSERT INTO `lc_error_log` VALUES ('55817', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Non-static method Overtrue\\Pinyin\\Pinyin::sentence() should not be called statically', '0', '48', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:17:16', '2024-04-10 14:17:16');
INSERT INTO `lc_error_log` VALUES ('55818', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\PDOConnection.php', 'SQLSTATE[42S02]: Base table or view not found: 1146 Table \'zy_gouziyuan.lc_terms\' doesn\'t exist', '10501', '796', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:38:59', '2024-04-10 14:38:59');
INSERT INTO `lc_error_log` VALUES ('55819', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'Call to undefined function app\\api\\controller\\v1\\decodeURIComponent()', '0', '72', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:44:02', '2024-04-10 14:44:02');
INSERT INTO `lc_error_log` VALUES ('55820', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'array_filter() expects parameter 2 to be a valid callback, no array or string given', '0', '52', '/v1/test?page=1', '127.0.0.1', '2024-04-10 14:52:37', '2024-04-10 14:52:37');
INSERT INTO `lc_error_log` VALUES ('55821', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\controller\\v1\\Test.php', 'syntax error, unexpected \'array_push\' (T_STRING)', '0', '72', '/v1/test?page=1', '127.0.0.1', '2024-04-10 16:39:55', '2024-04-10 16:39:55');
INSERT INTO `lc_error_log` VALUES ('55822', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'Too few arguments to function app\\platform\\service\\Wordpress::labelEvent(), 0 passed in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php on line 170 and exactly 1 expected', '0', '181', '/v1/test?page=1', '127.0.0.1', '2024-04-10 20:50:08', '2024-04-10 20:50:08');
INSERT INTO `lc_error_log` VALUES ('55823', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'unserialize() expects parameter 1 to be string, array given', '0', '181', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:26:41', '2024-04-10 21:26:41');
INSERT INTO `lc_error_log` VALUES ('55824', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'unserialize(): Error at offset 0 of 7 bytes', '0', '181', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:27:59', '2024-04-10 21:27:59');
INSERT INTO `lc_error_log` VALUES ('55825', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'unserialize(): Error at offset 0 of 7 bytes', '0', '185', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:28:42', '2024-04-10 21:28:42');
INSERT INTO `lc_error_log` VALUES ('55826', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'Cannot pass parameter 2 by reference', '0', '179', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:38:53', '2024-04-10 21:38:53');
INSERT INTO `lc_error_log` VALUES ('55827', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'Call to undefined function app\\platform\\service\\getimage()', '0', '180', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:42:41', '2024-04-10 21:42:41');
INSERT INTO `lc_error_log` VALUES ('55828', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'Call to undefined function app\\platform\\service\\getimageinfo()', '0', '180', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:42:48', '2024-04-10 21:42:48');
INSERT INTO `lc_error_log` VALUES ('55829', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'Call to undefined function app\\platform\\service\\getimageinfo()', '0', '180', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:46:56', '2024-04-10 21:46:56');
INSERT INTO `lc_error_log` VALUES ('55830', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'Class \'app\\platform\\service\\Imagick\' not found', '0', '185', '/v1/test?page=1', '127.0.0.1', '2024-04-10 21:47:06', '2024-04-10 21:47:06');
INSERT INTO `lc_error_log` VALUES ('55831', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->saveAll', '10500', '117', '/v1/test?page=1', '127.0.0.1', '2024-04-10 22:06:21', '2024-04-10 22:06:21');
INSERT INTO `lc_error_log` VALUES ('55832', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\db\\BaseQuery.php', 'method not exist:think\\db\\Query->saveAll', '10500', '117', '/v1/test?page=1', '127.0.0.1', '2024-04-10 22:10:41', '2024-04-10 22:10:41');
INSERT INTO `lc_error_log` VALUES ('55833', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'Undefined variable: user', '0', '194', '/v1/test?page=1', '127.0.0.1', '2024-04-10 22:11:48', '2024-04-10 22:11:48');
INSERT INTO `lc_error_log` VALUES ('55834', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php', 'Argument 1 passed to think\\Model::setAttr() must be of the type string, int given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php on line 355', '0', '367', '/v1/test?page=1', '127.0.0.1', '2024-04-10 22:13:05', '2024-04-10 22:13:05');
INSERT INTO `lc_error_log` VALUES ('55835', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php', 'Argument 1 passed to think\\Model::setAttr() must be of the type string, int given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php on line 355', '0', '367', '/v1/test?page=1', '127.0.0.1', '2024-04-10 22:14:18', '2024-04-10 22:14:18');
INSERT INTO `lc_error_log` VALUES ('55836', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php', 'Argument 1 passed to think\\Model::setAttr() must be of the type string, int given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php on line 355', '0', '367', '/v1/test?page=1', '127.0.0.1', '2024-04-10 22:14:43', '2024-04-10 22:14:43');
INSERT INTO `lc_error_log` VALUES ('55837', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php', 'Argument 1 passed to think\\Model::setAttr() must be of the type string, int given, called in D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\think-orm\\src\\model\\concern\\Attribute.php on line 355', '0', '367', '/v1/test?page=1', '127.0.0.1', '2024-04-10 22:15:28', '2024-04-10 22:15:28');
INSERT INTO `lc_error_log` VALUES ('55838', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\platform\\service\\Wordpress.php', 'explode() expects parameter 2 to be string, array given', '0', '183', '/v1/test?page=1', '127.0.0.1', '2024-04-11 08:36:06', '2024-04-11 08:36:06');
INSERT INTO `lc_error_log` VALUES ('55839', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-04-30 23:15:41', '2024-04-30 23:15:41');
INSERT INTO `lc_error_log` VALUES ('55840', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-04-30 23:15:43', '2024-04-30 23:15:43');
INSERT INTO `lc_error_log` VALUES ('55841', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-04-30 23:16:04', '2024-04-30 23:16:04');
INSERT INTO `lc_error_log` VALUES ('55842', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-04-30 23:16:07', '2024-04-30 23:16:07');
INSERT INTO `lc_error_log` VALUES ('55843', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-04-30 23:16:08', '2024-04-30 23:16:08');
INSERT INTO `lc_error_log` VALUES ('55844', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:10:02', '2024-05-01 08:10:02');
INSERT INTO `lc_error_log` VALUES ('55845', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:11:04', '2024-05-01 08:11:04');
INSERT INTO `lc_error_log` VALUES ('55846', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:12:12', '2024-05-01 08:12:12');
INSERT INTO `lc_error_log` VALUES ('55847', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:12:26', '2024-05-01 08:12:26');
INSERT INTO `lc_error_log` VALUES ('55848', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:16:10', '2024-05-01 08:16:10');
INSERT INTO `lc_error_log` VALUES ('55849', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:16:16', '2024-05-01 08:16:16');
INSERT INTO `lc_error_log` VALUES ('55850', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:17:45', '2024-05-01 08:17:45');
INSERT INTO `lc_error_log` VALUES ('55851', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:18:01', '2024-05-01 08:18:01');
INSERT INTO `lc_error_log` VALUES ('55852', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\business\\Common.php', '错误', '0', '190', '/v1/getSystemConfig', '127.0.0.1', '2024-05-01 08:18:02', '2024-05-01 08:18:02');
INSERT INTO `lc_error_log` VALUES ('55853', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getUploadPath', '127.0.0.1', '2024-05-04 10:06:23', '2024-05-04 10:06:23');
INSERT INTO `lc_error_log` VALUES ('55854', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/imgUpload11111', '127.0.0.1', '2024-05-04 11:08:44', '2024-05-04 11:08:44');
INSERT INTO `lc_error_log` VALUES ('55855', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/imgUpload11111', '127.0.0.1', '2024-05-04 11:08:44', '2024-05-04 11:08:44');
INSERT INTO `lc_error_log` VALUES ('55856', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-05-04 13:36:45', '2024-05-04 13:36:45');
INSERT INTO `lc_error_log` VALUES ('55857', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-05-04 13:42:10', '2024-05-04 13:42:10');
INSERT INTO `lc_error_log` VALUES ('55858', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-05-04 13:46:45', '2024-05-04 13:46:45');
INSERT INTO `lc_error_log` VALUES ('55859', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/', '127.0.0.1', '2024-05-04 14:22:18', '2024-05-04 14:22:18');
INSERT INTO `lc_error_log` VALUES ('55860', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\controller\\v1\\Common.php', 'Call to undefined method app\\api\\service\\v1\\Common::getAuth()', '0', '32', '/v1/getAuth?id=1', '127.0.0.1', '2024-05-04 14:26:30', '2024-05-04 14:26:30');
INSERT INTO `lc_error_log` VALUES ('55861', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', 'Too few arguments to function app\\api\\service\\v1\\Common::getAuth(), 0 passed in D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\controller\\v1\\Common.php on line 32 and exactly 1 expected', '0', '30', '/v1/getAuth', '127.0.0.1', '2024-05-04 14:27:48', '2024-05-04 14:27:48');
INSERT INTO `lc_error_log` VALUES ('55862', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', 'Too few arguments to function app\\api\\service\\v1\\Common::getAuth(), 0 passed in D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\controller\\v1\\Common.php on line 32 and exactly 1 expected', '0', '30', '/v1/getAuth?id=1', '127.0.0.1', '2024-05-04 14:28:00', '2024-05-04 14:28:00');
INSERT INTO `lc_error_log` VALUES ('55863', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '请求类型错误', '0', '32', '/v1/getAuth', '127.0.0.1', '2024-05-04 14:41:48', '2024-05-04 14:41:48');
INSERT INTO `lc_error_log` VALUES ('55864', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?id=1', '127.0.0.1', '2024-05-04 14:50:22', '2024-05-04 14:50:22');
INSERT INTO `lc_error_log` VALUES ('55865', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?id=1', '127.0.0.1', '2024-05-04 14:52:35', '2024-05-04 14:52:35');
INSERT INTO `lc_error_log` VALUES ('55866', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:55:39', '2024-05-04 14:55:39');
INSERT INTO `lc_error_log` VALUES ('55867', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:55:54', '2024-05-04 14:55:54');
INSERT INTO `lc_error_log` VALUES ('55868', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:56:02', '2024-05-04 14:56:02');
INSERT INTO `lc_error_log` VALUES ('55869', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:56:19', '2024-05-04 14:56:19');
INSERT INTO `lc_error_log` VALUES ('55870', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:57:07', '2024-05-04 14:57:07');
INSERT INTO `lc_error_log` VALUES ('55871', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:57:46', '2024-05-04 14:57:46');
INSERT INTO `lc_error_log` VALUES ('55872', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:58:18', '2024-05-04 14:58:18');
INSERT INTO `lc_error_log` VALUES ('55873', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:58:23', '2024-05-04 14:58:23');
INSERT INTO `lc_error_log` VALUES ('55874', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误111', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:59:27', '2024-05-04 14:59:27');
INSERT INTO `lc_error_log` VALUES ('55875', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '参数错误111', '0', '32', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 14:59:52', '2024-05-04 14:59:52');
INSERT INTO `lc_error_log` VALUES ('55876', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', 'Undefined index: random_string', '0', '42', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 15:02:00', '2024-05-04 15:02:00');
INSERT INTO `lc_error_log` VALUES ('55877', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', '未授权', '0', '57', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 15:03:46', '2024-05-04 15:03:46');
INSERT INTO `lc_error_log` VALUES ('55878', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', '参数错误', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 15:59:09', '2024-05-04 15:59:09');
INSERT INTO `lc_error_log` VALUES ('55879', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', '参数错误', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 15:59:23', '2024-05-04 15:59:23');
INSERT INTO `lc_error_log` VALUES ('55880', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', '参数错误1', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 16:00:12', '2024-05-04 16:00:12');
INSERT INTO `lc_error_log` VALUES ('55881', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', '参数错误2', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 16:00:46', '2024-05-04 16:00:46');
INSERT INTO `lc_error_log` VALUES ('55882', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', 'domain不是有效的域名或者IP', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 16:02:41', '2024-05-04 16:02:41');
INSERT INTO `lc_error_log` VALUES ('55883', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', 'domain不是有效的域名或者IP', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 16:03:29', '2024-05-04 16:03:29');
INSERT INTO `lc_error_log` VALUES ('55884', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', 'domain不是有效的域名或者IP', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 16:05:17', '2024-05-04 16:05:17');
INSERT INTO `lc_error_log` VALUES ('55885', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Validate.php', 'domain不是有效的域名或者IP', '0', '524', '/v1/getAuth?host=172.30.30.1', '127.0.0.1', '2024-05-04 16:05:39', '2024-05-04 16:05:39');
INSERT INTO `lc_error_log` VALUES ('55886', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\vendor\\topthink\\framework\\src\\think\\Route.php', 'Route Not Found', '0', '799', '/v1/getAuthInfo', '127.0.0.1', '2024-05-04 16:17:18', '2024-05-04 16:17:18');
INSERT INTO `lc_error_log` VALUES ('55887', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-service\\app\\api\\service\\v1\\Common.php', 'syntax error, unexpected \'{\'', '0', '52', '/v1/getAuth', '127.0.0.1', '2024-05-04 16:17:27', '2024-05-04 16:17:27');
INSERT INTO `lc_error_log` VALUES ('55888', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 20:13:38', '2024-05-04 20:13:38');
INSERT INTO `lc_error_log` VALUES ('55889', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '未登录', '-1', '47', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 20:13:38', '2024-05-04 20:13:38');
INSERT INTO `lc_error_log` VALUES ('55890', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:16:23', '2024-05-04 21:16:23');
INSERT INTO `lc_error_log` VALUES ('55891', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 21:17:36', '2024-05-04 21:17:36');
INSERT INTO `lc_error_log` VALUES ('55892', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getDownloadLog?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:17:39', '2024-05-04 21:17:39');
INSERT INTO `lc_error_log` VALUES ('55893', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:17:40', '2024-05-04 21:17:40');
INSERT INTO `lc_error_log` VALUES ('55894', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/contentData', '127.0.0.1', '2024-05-04 21:17:44', '2024-05-04 21:17:44');
INSERT INTO `lc_error_log` VALUES ('55895', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/getprofit', '127.0.0.1', '2024-05-04 21:17:50', '2024-05-04 21:17:50');
INSERT INTO `lc_error_log` VALUES ('55896', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/contentData', '127.0.0.1', '2024-05-04 21:17:58', '2024-05-04 21:17:58');
INSERT INTO `lc_error_log` VALUES ('55897', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/getprofit', '127.0.0.1', '2024-05-04 21:18:01', '2024-05-04 21:18:01');
INSERT INTO `lc_error_log` VALUES ('55898', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/contentData', '127.0.0.1', '2024-05-04 21:18:25', '2024-05-04 21:18:25');
INSERT INTO `lc_error_log` VALUES ('55899', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 21:18:28', '2024-05-04 21:18:28');
INSERT INTO `lc_error_log` VALUES ('55900', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 21:19:12', '2024-05-04 21:19:12');
INSERT INTO `lc_error_log` VALUES ('55901', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:19:16', '2024-05-04 21:19:16');
INSERT INTO `lc_error_log` VALUES ('55902', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/getprofit', '127.0.0.1', '2024-05-04 21:19:24', '2024-05-04 21:19:24');
INSERT INTO `lc_error_log` VALUES ('55903', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:19:26', '2024-05-04 21:19:26');
INSERT INTO `lc_error_log` VALUES ('55904', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getUserInfo', '127.0.0.1', '2024-05-04 21:19:30', '2024-05-04 21:19:30');
INSERT INTO `lc_error_log` VALUES ('55905', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:19:35', '2024-05-04 21:19:35');
INSERT INTO `lc_error_log` VALUES ('55906', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 21:19:44', '2024-05-04 21:19:44');
INSERT INTO `lc_error_log` VALUES ('55907', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 21:20:07', '2024-05-04 21:20:07');
INSERT INTO `lc_error_log` VALUES ('55908', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:20:15', '2024-05-04 21:20:15');
INSERT INTO `lc_error_log` VALUES ('55909', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/contentData', '127.0.0.1', '2024-05-04 21:20:28', '2024-05-04 21:20:28');
INSERT INTO `lc_error_log` VALUES ('55910', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 21:26:15', '2024-05-04 21:26:15');
INSERT INTO `lc_error_log` VALUES ('55911', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:01:27', '2024-05-04 22:01:27');
INSERT INTO `lc_error_log` VALUES ('55912', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:01:49', '2024-05-04 22:01:49');
INSERT INTO `lc_error_log` VALUES ('55913', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:02:07', '2024-05-04 22:02:07');
INSERT INTO `lc_error_log` VALUES ('55914', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:08:43', '2024-05-04 22:08:43');
INSERT INTO `lc_error_log` VALUES ('55915', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:09:18', '2024-05-04 22:09:18');
INSERT INTO `lc_error_log` VALUES ('55916', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:09:47', '2024-05-04 22:09:47');
INSERT INTO `lc_error_log` VALUES ('55917', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:20:20', '2024-05-04 22:20:20');
INSERT INTO `lc_error_log` VALUES ('55918', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:20:35', '2024-05-04 22:20:35');
INSERT INTO `lc_error_log` VALUES ('55919', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:21:12', '2024-05-04 22:21:12');
INSERT INTO `lc_error_log` VALUES ('55920', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:22:08', '2024-05-04 22:22:08');
INSERT INTO `lc_error_log` VALUES ('55921', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:22:21', '2024-05-04 22:22:21');
INSERT INTO `lc_error_log` VALUES ('55922', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:22:25', '2024-05-04 22:22:25');
INSERT INTO `lc_error_log` VALUES ('55923', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:23:10', '2024-05-04 22:23:10');
INSERT INTO `lc_error_log` VALUES ('55924', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:23:20', '2024-05-04 22:23:20');
INSERT INTO `lc_error_log` VALUES ('55925', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:23:56', '2024-05-04 22:23:56');
INSERT INTO `lc_error_log` VALUES ('55926', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:24:01', '2024-05-04 22:24:01');
INSERT INTO `lc_error_log` VALUES ('55927', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:24:13', '2024-05-04 22:24:13');
INSERT INTO `lc_error_log` VALUES ('55928', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:24:53', '2024-05-04 22:24:53');
INSERT INTO `lc_error_log` VALUES ('55929', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:24:59', '2024-05-04 22:24:59');
INSERT INTO `lc_error_log` VALUES ('55930', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 22:25:12', '2024-05-04 22:25:12');
INSERT INTO `lc_error_log` VALUES ('55931', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:25:14', '2024-05-04 22:25:14');
INSERT INTO `lc_error_log` VALUES ('55932', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:25:17', '2024-05-04 22:25:17');
INSERT INTO `lc_error_log` VALUES ('55933', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:26:07', '2024-05-04 22:26:07');
INSERT INTO `lc_error_log` VALUES ('55934', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:26:32', '2024-05-04 22:26:32');
INSERT INTO `lc_error_log` VALUES ('55935', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:51:38', '2024-05-04 22:51:38');
INSERT INTO `lc_error_log` VALUES ('55936', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:52:50', '2024-05-04 22:52:50');
INSERT INTO `lc_error_log` VALUES ('55937', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:53:23', '2024-05-04 22:53:23');
INSERT INTO `lc_error_log` VALUES ('55938', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:54:14', '2024-05-04 22:54:14');
INSERT INTO `lc_error_log` VALUES ('55939', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:54:53', '2024-05-04 22:54:53');
INSERT INTO `lc_error_log` VALUES ('55940', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:55:10', '2024-05-04 22:55:10');
INSERT INTO `lc_error_log` VALUES ('55941', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:55:38', '2024-05-04 22:55:38');
INSERT INTO `lc_error_log` VALUES ('55942', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:58:10', '2024-05-04 22:58:10');
INSERT INTO `lc_error_log` VALUES ('55943', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 22:58:14', '2024-05-04 22:58:14');
INSERT INTO `lc_error_log` VALUES ('55944', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:00:03', '2024-05-04 23:00:03');
INSERT INTO `lc_error_log` VALUES ('55945', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:02:18', '2024-05-04 23:02:18');
INSERT INTO `lc_error_log` VALUES ('55946', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:03:38', '2024-05-04 23:03:38');
INSERT INTO `lc_error_log` VALUES ('55947', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:04:00', '2024-05-04 23:04:00');
INSERT INTO `lc_error_log` VALUES ('55948', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:06:29', '2024-05-04 23:06:29');
INSERT INTO `lc_error_log` VALUES ('55949', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:06:42', '2024-05-04 23:06:42');
INSERT INTO `lc_error_log` VALUES ('55950', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:06:46', '2024-05-04 23:06:46');
INSERT INTO `lc_error_log` VALUES ('55951', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:07:24', '2024-05-04 23:07:24');
INSERT INTO `lc_error_log` VALUES ('55952', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:07:34', '2024-05-04 23:07:34');
INSERT INTO `lc_error_log` VALUES ('55953', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:07:53', '2024-05-04 23:07:53');
INSERT INTO `lc_error_log` VALUES ('55954', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:11:46', '2024-05-04 23:11:46');
INSERT INTO `lc_error_log` VALUES ('55955', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:13:22', '2024-05-04 23:13:22');
INSERT INTO `lc_error_log` VALUES ('55956', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:29:57', '2024-05-04 23:29:57');
INSERT INTO `lc_error_log` VALUES ('55957', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:31:54', '2024-05-04 23:31:54');
INSERT INTO `lc_error_log` VALUES ('55958', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:32:01', '2024-05-04 23:32:01');
INSERT INTO `lc_error_log` VALUES ('55959', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:32:03', '2024-05-04 23:32:03');
INSERT INTO `lc_error_log` VALUES ('55960', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:35:16', '2024-05-04 23:35:16');
INSERT INTO `lc_error_log` VALUES ('55961', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:36:34', '2024-05-04 23:36:34');
INSERT INTO `lc_error_log` VALUES ('55962', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:37:16', '2024-05-04 23:37:16');
INSERT INTO `lc_error_log` VALUES ('55963', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:37:28', '2024-05-04 23:37:28');
INSERT INTO `lc_error_log` VALUES ('55964', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:37:36', '2024-05-04 23:37:36');
INSERT INTO `lc_error_log` VALUES ('55965', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:38:06', '2024-05-04 23:38:06');
INSERT INTO `lc_error_log` VALUES ('55966', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:38:52', '2024-05-04 23:38:52');
INSERT INTO `lc_error_log` VALUES ('55967', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:39:44', '2024-05-04 23:39:44');
INSERT INTO `lc_error_log` VALUES ('55968', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:41:50', '2024-05-04 23:41:50');
INSERT INTO `lc_error_log` VALUES ('55969', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:41:54', '2024-05-04 23:41:54');
INSERT INTO `lc_error_log` VALUES ('55970', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:42:31', '2024-05-04 23:42:31');
INSERT INTO `lc_error_log` VALUES ('55971', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:42:36', '2024-05-04 23:42:36');
INSERT INTO `lc_error_log` VALUES ('55972', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:43:26', '2024-05-04 23:43:26');
INSERT INTO `lc_error_log` VALUES ('55973', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/getUploadPath', '127.0.0.1', '2024-05-04 23:43:28', '2024-05-04 23:43:28');
INSERT INTO `lc_error_log` VALUES ('55974', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:43:33', '2024-05-04 23:43:33');
INSERT INTO `lc_error_log` VALUES ('55975', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:43:33', '2024-05-04 23:43:33');
INSERT INTO `lc_error_log` VALUES ('55976', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:43:39', '2024-05-04 23:43:39');
INSERT INTO `lc_error_log` VALUES ('55977', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:43:39', '2024-05-04 23:43:39');
INSERT INTO `lc_error_log` VALUES ('55978', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:50:36', '2024-05-04 23:50:36');
INSERT INTO `lc_error_log` VALUES ('55979', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:50:36', '2024-05-04 23:50:36');
INSERT INTO `lc_error_log` VALUES ('55980', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:50:42', '2024-05-04 23:50:42');
INSERT INTO `lc_error_log` VALUES ('55981', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:50:42', '2024-05-04 23:50:42');
INSERT INTO `lc_error_log` VALUES ('55982', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:52:47', '2024-05-04 23:52:47');
INSERT INTO `lc_error_log` VALUES ('55983', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:52:47', '2024-05-04 23:52:47');
INSERT INTO `lc_error_log` VALUES ('55984', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:56:14', '2024-05-04 23:56:14');
INSERT INTO `lc_error_log` VALUES ('55985', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:56:14', '2024-05-04 23:56:14');
INSERT INTO `lc_error_log` VALUES ('55986', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:57:37', '2024-05-04 23:57:37');
INSERT INTO `lc_error_log` VALUES ('55987', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:57:37', '2024-05-04 23:57:37');
INSERT INTO `lc_error_log` VALUES ('55988', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:59:11', '2024-05-04 23:59:11');
INSERT INTO `lc_error_log` VALUES ('55989', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:59:11', '2024-05-04 23:59:11');
INSERT INTO `lc_error_log` VALUES ('55990', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:59:20', '2024-05-04 23:59:20');
INSERT INTO `lc_error_log` VALUES ('55991', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getKeList?page=1&pageSize=20', '127.0.0.1', '2024-05-04 23:59:23', '2024-05-04 23:59:23');
INSERT INTO `lc_error_log` VALUES ('55992', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-04 23:59:24', '2024-05-04 23:59:24');
INSERT INTO `lc_error_log` VALUES ('55993', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-04 23:59:24', '2024-05-04 23:59:24');
INSERT INTO `lc_error_log` VALUES ('55994', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 00:02:45', '2024-05-05 00:02:45');
INSERT INTO `lc_error_log` VALUES ('55995', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getDownloadLog?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:02:48', '2024-05-05 00:02:48');
INSERT INTO `lc_error_log` VALUES ('55996', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:02:50', '2024-05-05 00:02:50');
INSERT INTO `lc_error_log` VALUES ('55997', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:03:27', '2024-05-05 00:03:27');
INSERT INTO `lc_error_log` VALUES ('55998', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:08:51', '2024-05-05 00:08:51');
INSERT INTO `lc_error_log` VALUES ('55999', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:09:24', '2024-05-05 00:09:24');
INSERT INTO `lc_error_log` VALUES ('56000', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:10:34', '2024-05-05 00:10:34');
INSERT INTO `lc_error_log` VALUES ('56001', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:11:35', '2024-05-05 00:11:35');
INSERT INTO `lc_error_log` VALUES ('56002', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getOrderList?page=1&pageSize=20', '127.0.0.1', '2024-05-05 00:11:43', '2024-05-05 00:11:43');
INSERT INTO `lc_error_log` VALUES ('56003', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 00:11:47', '2024-05-05 00:11:47');
INSERT INTO `lc_error_log` VALUES ('56004', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 00:11:47', '2024-05-05 00:11:47');
INSERT INTO `lc_error_log` VALUES ('56005', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:53:43', '2024-05-05 10:53:43');
INSERT INTO `lc_error_log` VALUES ('56006', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:53:43', '2024-05-05 10:53:43');
INSERT INTO `lc_error_log` VALUES ('56007', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:55:01', '2024-05-05 10:55:01');
INSERT INTO `lc_error_log` VALUES ('56008', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:55:01', '2024-05-05 10:55:01');
INSERT INTO `lc_error_log` VALUES ('56009', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:55:18', '2024-05-05 10:55:18');
INSERT INTO `lc_error_log` VALUES ('56010', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:55:18', '2024-05-05 10:55:18');
INSERT INTO `lc_error_log` VALUES ('56011', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:55:25', '2024-05-05 10:55:25');
INSERT INTO `lc_error_log` VALUES ('56012', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:55:25', '2024-05-05 10:55:25');
INSERT INTO `lc_error_log` VALUES ('56013', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:56:17', '2024-05-05 10:56:17');
INSERT INTO `lc_error_log` VALUES ('56014', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:56:17', '2024-05-05 10:56:17');
INSERT INTO `lc_error_log` VALUES ('56015', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:56:32', '2024-05-05 10:56:32');
INSERT INTO `lc_error_log` VALUES ('56016', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:56:32', '2024-05-05 10:56:32');
INSERT INTO `lc_error_log` VALUES ('56017', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:56:45', '2024-05-05 10:56:45');
INSERT INTO `lc_error_log` VALUES ('56018', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:56:45', '2024-05-05 10:56:45');
INSERT INTO `lc_error_log` VALUES ('56019', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:58:30', '2024-05-05 10:58:30');
INSERT INTO `lc_error_log` VALUES ('56020', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:58:30', '2024-05-05 10:58:30');
INSERT INTO `lc_error_log` VALUES ('56021', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 10:59:07', '2024-05-05 10:59:07');
INSERT INTO `lc_error_log` VALUES ('56022', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 10:59:07', '2024-05-05 10:59:07');
INSERT INTO `lc_error_log` VALUES ('56023', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:09:05', '2024-05-05 11:09:05');
INSERT INTO `lc_error_log` VALUES ('56024', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:09:05', '2024-05-05 11:09:05');
INSERT INTO `lc_error_log` VALUES ('56025', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:09:17', '2024-05-05 11:09:17');
INSERT INTO `lc_error_log` VALUES ('56026', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:09:17', '2024-05-05 11:09:17');
INSERT INTO `lc_error_log` VALUES ('56027', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:09:43', '2024-05-05 11:09:43');
INSERT INTO `lc_error_log` VALUES ('56028', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:09:43', '2024-05-05 11:09:43');
INSERT INTO `lc_error_log` VALUES ('56029', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:11:56', '2024-05-05 11:11:56');
INSERT INTO `lc_error_log` VALUES ('56030', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:11:56', '2024-05-05 11:11:56');
INSERT INTO `lc_error_log` VALUES ('56031', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:33:52', '2024-05-05 11:33:52');
INSERT INTO `lc_error_log` VALUES ('56032', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:33:52', '2024-05-05 11:33:52');
INSERT INTO `lc_error_log` VALUES ('56033', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:34:08', '2024-05-05 11:34:08');
INSERT INTO `lc_error_log` VALUES ('56034', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:34:08', '2024-05-05 11:34:08');
INSERT INTO `lc_error_log` VALUES ('56035', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:35:25', '2024-05-05 11:35:25');
INSERT INTO `lc_error_log` VALUES ('56036', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:35:25', '2024-05-05 11:35:25');
INSERT INTO `lc_error_log` VALUES ('56037', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:37:13', '2024-05-05 11:37:13');
INSERT INTO `lc_error_log` VALUES ('56038', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:37:13', '2024-05-05 11:37:13');
INSERT INTO `lc_error_log` VALUES ('56039', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:43:38', '2024-05-05 11:43:38');
INSERT INTO `lc_error_log` VALUES ('56040', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:43:38', '2024-05-05 11:43:38');
INSERT INTO `lc_error_log` VALUES ('56041', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:44:13', '2024-05-05 11:44:13');
INSERT INTO `lc_error_log` VALUES ('56042', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:44:13', '2024-05-05 11:44:13');
INSERT INTO `lc_error_log` VALUES ('56043', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:44:21', '2024-05-05 11:44:21');
INSERT INTO `lc_error_log` VALUES ('56044', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:44:21', '2024-05-05 11:44:21');
INSERT INTO `lc_error_log` VALUES ('56045', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:44:52', '2024-05-05 11:44:52');
INSERT INTO `lc_error_log` VALUES ('56046', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:44:52', '2024-05-05 11:44:52');
INSERT INTO `lc_error_log` VALUES ('56047', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 11:47:24', '2024-05-05 11:47:24');
INSERT INTO `lc_error_log` VALUES ('56048', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 11:47:24', '2024-05-05 11:47:24');
INSERT INTO `lc_error_log` VALUES ('56049', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 11:51:20', '2024-05-05 11:51:20');
INSERT INTO `lc_error_log` VALUES ('56050', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 11:51:23', '2024-05-05 11:51:23');
INSERT INTO `lc_error_log` VALUES ('56051', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 11:51:47', '2024-05-05 11:51:47');
INSERT INTO `lc_error_log` VALUES ('56052', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 11:55:17', '2024-05-05 11:55:17');
INSERT INTO `lc_error_log` VALUES ('56053', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 11:55:34', '2024-05-05 11:55:34');
INSERT INTO `lc_error_log` VALUES ('56054', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 11:55:53', '2024-05-05 11:55:53');
INSERT INTO `lc_error_log` VALUES ('56055', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 12:32:50', '2024-05-05 12:32:50');
INSERT INTO `lc_error_log` VALUES ('56056', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 12:33:02', '2024-05-05 12:33:02');
INSERT INTO `lc_error_log` VALUES ('56057', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 12:33:02', '2024-05-05 12:33:02');
INSERT INTO `lc_error_log` VALUES ('56058', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/tongji/index', '127.0.0.1', '2024-05-05 12:33:23', '2024-05-05 12:33:23');
INSERT INTO `lc_error_log` VALUES ('56059', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\api\\middleware\\AuthTokenMiddleware.php', '请登录', '-1', '70', '/v1/mp/getZiyuanList?page=1&pageSize=20&category_id=&status=&start_time=&end_time=&title=', '127.0.0.1', '2024-05-05 12:33:23', '2024-05-05 12:33:23');
INSERT INTO `lc_error_log` VALUES ('56060', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 13:25:35', '2024-05-05 13:25:35');
INSERT INTO `lc_error_log` VALUES ('56061', 'api', 'D:\\phpstudy_pro\\WWW\\gouziyuan-php\\app\\common\\lib\\WeChat.php', '获取access_token失败', '0', '30', '/v1/getQrCodeLogin', '127.0.0.1', '2024-05-05 13:26:08', '2024-05-05 13:26:08');

-- ----------------------------
-- Table structure for lc_kecheng
-- ----------------------------
DROP TABLE IF EXISTS `lc_kecheng`;
CREATE TABLE `lc_kecheng` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '课程名称',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '课程描述',
  `study_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '学习人数',
  `grade` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '课程难度',
  `cover` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '封面',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `vip_dis` decimal(3,2) unsigned NOT NULL DEFAULT '1.00' COMMENT 'VIP折扣，1为不享受折扣，0.5为5折，0为vip免费',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '标签',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  `collect_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数',
  `report_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '举报数',
  `browse_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览数',
  `praise_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `kejian` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '课件',
  `tool` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '工具',
  `is_top` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否置顶',
  `is_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1显示 0仅自己可见',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0待审核 1审核通过 2审核未通过 -1删除',
  `ke_update_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0未更新完 1已更新完',
  `ke_update_time` char(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '课程更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=23600 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_kecheng
-- ----------------------------

-- ----------------------------
-- Table structure for lc_kecheng_chapters
-- ----------------------------
DROP TABLE IF EXISTS `lc_kecheng_chapters`;
CREATE TABLE `lc_kecheng_chapters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '章节名称',
  `ke_id` int(10) unsigned NOT NULL COMMENT '课程id',
  `sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `ke_id` (`ke_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_kecheng_chapters
-- ----------------------------

-- ----------------------------
-- Table structure for lc_kecheng_directory
-- ----------------------------
DROP TABLE IF EXISTS `lc_kecheng_directory`;
CREATE TABLE `lc_kecheng_directory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `ke_chapters_id` int(10) unsigned NOT NULL COMMENT '课程章节id',
  `is_free` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否支持试看， 0不支持，1支持',
  `duration_str` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '视频时长 （）',
  `duration` int(10) unsigned DEFAULT '0' COMMENT '视频时长 （单位秒）',
  `type` tinyint(3) unsigned DEFAULT NULL COMMENT '类型：1视频；2图文',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '视频地址/图文内容',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `ke_chapters_id` (`ke_chapters_id`)
) ENGINE=InnoDB AUTO_INCREMENT=858 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_kecheng_directory
-- ----------------------------

-- ----------------------------
-- Table structure for lc_kecheng_order
-- ----------------------------
DROP TABLE IF EXISTS `lc_kecheng_order`;
CREATE TABLE `lc_kecheng_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` char(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '订单号',
  `sell_user_id` int(10) unsigned NOT NULL COMMENT '卖家id',
  `user_id` int(10) unsigned NOT NULL COMMENT '买家id',
  `ke_id` int(10) unsigned NOT NULL COMMENT '课程id',
  `ke_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '课程名称',
  `ke_cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '课程封面',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格',
  `pay_type` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付方式',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0未支付 1已支付',
  `pay_time` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`,`order_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_kecheng_order
-- ----------------------------

-- ----------------------------
-- Table structure for lc_mysql
-- ----------------------------
DROP TABLE IF EXISTS `lc_mysql`;
CREATE TABLE `lc_mysql` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_mysql
-- ----------------------------

-- ----------------------------
-- Table structure for lc_order
-- ----------------------------
DROP TABLE IF EXISTS `lc_order`;
CREATE TABLE `lc_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '订单号',
  `sell_user_id` int(10) unsigned NOT NULL COMMENT '卖家id',
  `user_id` int(10) unsigned NOT NULL,
  `ziyuan_id` int(10) unsigned NOT NULL,
  `ziyuan_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '资源名称',
  `ziyuan_cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '封面',
  `download_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '下载url',
  `extract` char(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '提取码',
  `download_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `kami` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '卡密',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `pay_type` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付方式',
  `pay_time` char(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0未支付 1已支付 2取消订单',
  `vip` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是VIP订单：1是，0不是',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `sell_user_id` (`sell_user_id`),
  KEY `user_id` (`user_id`),
  KEY `ziyuan_title` (`ziyuan_title`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_order
-- ----------------------------

-- ----------------------------
-- Table structure for lc_pay_callback
-- ----------------------------
DROP TABLE IF EXISTS `lc_pay_callback`;
CREATE TABLE `lc_pay_callback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'title，如：微信支付',
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '内容',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ip地址',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_pay_callback
-- ----------------------------

-- ----------------------------
-- Table structure for lc_sms_code
-- ----------------------------
DROP TABLE IF EXISTS `lc_sms_code`;
CREATE TABLE `lc_sms_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '手机号',
  `code` int(10) unsigned DEFAULT NULL COMMENT '验证码',
  `type` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '类型',
  `status` tinyint(3) unsigned NOT NULL COMMENT '状态：0失败，1成功',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_sms_code
-- ----------------------------

-- ----------------------------
-- Table structure for lc_system_config
-- ----------------------------
DROP TABLE IF EXISTS `lc_system_config`;
CREATE TABLE `lc_system_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '字段名称：如，name',
  `is_frontend` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否在前端显示',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '值：如，购资源',
  `note` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '说明：如网站首页',
  `type` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'text' COMMENT '类型：text、单选、多选、url等',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `field` (`field`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_system_config
-- ----------------------------
INSERT INTO `lc_system_config` VALUES ('1', 'title', '1', 'NETSS系统', '网站名称', 'text', '2024-01-24 12:46:09', '2024-05-05 13:37:58');
INSERT INTO `lc_system_config` VALUES ('2', 'title_abbr', '1', 'NETSS系统', '网站名称简称', 'text', '2024-02-02 11:49:44', '2024-05-05 13:38:00');
INSERT INTO `lc_system_config` VALUES ('3', 'platform', '1', 'NETSS系统-自主研发的虚拟资源网站系统-云课堂系统', '网站标题', 'text', '2022-10-18 11:30:16', '2024-05-05 13:38:59');
INSERT INTO `lc_system_config` VALUES ('4', 'keywords', '1', 'NETSS系统,资源网站,资源站,云课堂系统,下载站系统,软件下载,资源商城,视频教程', '关键词', 'text', '2022-10-18 11:31:31', '2024-05-05 13:38:27');
INSERT INTO `lc_system_config` VALUES ('5', 'description', '1', 'NETSS虚拟资源网站系统，让你的资源变得更有价值，是一个优质的互联网资源买卖平台，更注重资源的实用性，深入研究每一类资源的使用人群，使用场景、受众、需要资源传递的信息', '描述', 'text', '2022-10-18 11:32:10', '2024-05-05 13:38:41');
INSERT INTO `lc_system_config` VALUES ('6', 'logo', '1', 'https://www.gouziyuan.cn/images/logo_zj.png', 'logo', 'text', '2024-01-24 12:53:09', '2024-02-02 11:50:03');
INSERT INTO `lc_system_config` VALUES ('7', 'platform_wechat', '1', 'https://gzy.data.gouziyuan.cn/gouziyuan/platform_qrcode.jpg', '平台微信', 'image', '2022-10-18 11:41:37', '2024-02-02 11:50:02');
INSERT INTO `lc_system_config` VALUES ('8', 'qq1', '1', '80032217', '客服QQ', 'text', '2022-10-18 11:42:46', '2024-02-02 11:50:02');
INSERT INTO `lc_system_config` VALUES ('9', 'qq2', '1', '921628873', '客服QQ', 'text', '2022-10-18 11:43:02', '2024-02-02 11:50:00');
INSERT INTO `lc_system_config` VALUES ('10', 'icp', '1', '黔ICP备88888888', 'ICP备案号', 'text', '2022-10-18 12:01:34', '2024-05-05 13:37:38');
INSERT INTO `lc_system_config` VALUES ('11', 'copyright', '1', 'NETSS系统', '版权信息', 'text', '2024-01-31 12:43:22', '2024-02-29 16:22:21');

-- ----------------------------
-- Table structure for lc_users
-- ----------------------------
DROP TABLE IF EXISTS `lc_users`;
CREATE TABLE `lc_users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '登录名',
  `openid` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '微信openid',
  `unionid` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '微信授权id',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `is_follow` tinyint(3) unsigned DEFAULT '0' COMMENT '是否关注公众号，1关注 0未关注',
  `user_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '真实姓名',
  `id_card` char(18) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '身份证号',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `qq` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'QQ号',
  `email` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱号',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地址',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_password_time` datetime DEFAULT NULL COMMENT '最后修改密码时间',
  `salt` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `disable_time` datetime DEFAULT NULL,
  `describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '简介',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1禁用',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1普通用户 2创作者 3创作者申请中',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  KEY `login_name` (`login_name`),
  KEY `user_name` (`user_name`),
  KEY `phone` (`phone`),
  KEY `status` (`type`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=15425 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_users
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_account
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_account`;
CREATE TABLE `lc_user_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `account_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '收款账号，如果是微信，这里是openid',
  `account_holder` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名/昵称',
  `opening_bank` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '开户行',
  `account_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '账号类型，alipay/wxpay/bank',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_account
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_action_log
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_action_log`;
CREATE TABLE `lc_user_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '操作名称',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '操作值',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_action_log
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_feedback
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_feedback`;
CREATE TABLE `lc_user_feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_follow
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_follow`;
CREATE TABLE `lc_user_follow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `follow_user_id` int(10) unsigned NOT NULL COMMENT '对方id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `follow_user_id` (`follow_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_follow
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_login
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_login`;
CREATE TABLE `lc_user_login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `mode` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '微信/密码/短信验证码等',
  `platform` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'web',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=610 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_login
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_online
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_online`;
CREATE TABLE `lc_user_online` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `platform` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端平台 android、ios',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_register_mp
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_register_mp`;
CREATE TABLE `lc_user_register_mp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `id_card` char(18) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '身份证号',
  `email` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '邮箱号',
  `qq` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'QQ号',
  `fail_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0待审核，1已通过，2未通过',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_register_mp
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_tixian
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_tixian`;
CREATE TABLE `lc_user_tixian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `money` decimal(10,2) unsigned NOT NULL,
  `account_name` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '账号',
  `account_holder` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '姓名',
  `opening_bank` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '开户行',
  `fee` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '手续费',
  `type` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'alipay/wxpay/bank',
  `fail` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '失败原因',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0待审核 1已通过 2未通过',
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_tixian
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_vip
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_vip`;
CREATE TABLE `lc_user_vip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vip_id` int(10) unsigned NOT NULL COMMENT 'vip ID',
  `order_id` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '订单号',
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '会员名称',
  `category_id` char(5) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源分类id，all为所有资源类型',
  `type` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'ziyuan、kecheng',
  `price` decimal(10,2) unsigned NOT NULL,
  `pay_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '支付状态，0未支付，1已支付',
  `pay_type` char(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付类型',
  `pay_time` char(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付时间',
  `end_time` datetime DEFAULT NULL COMMENT '会员到期时间',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0正常 -1已删除',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_vip
-- ----------------------------
INSERT INTO `lc_user_vip` VALUES ('1', '1', '20240323612217487227879424', '8', '全站资源会员', 'all', 'ziyuan', '128.00', '1', null, '2024-03-23 17:42:51', '2025-03-23 17:41:51', '0', '2024-03-23 17:41:51', '2024-03-24 11:33:22');
INSERT INTO `lc_user_vip` VALUES ('2', '2', '3482343242342323', '8', '网站会员', '1', 'ziyuan', '1.00', '1', null, '2024-03-24 10:00:17', '2025-03-24 10:00:19', '0', '2024-03-24 10:00:27', '2024-03-24 10:03:43');
INSERT INTO `lc_user_vip` VALUES ('3', '3', '3432432223332237', '8', '软件应用VIP', '4', 'ziyuan', '1.00', '1', null, '2024-03-24 10:01:25', '2025-03-24 10:01:28', '0', '2024-03-24 10:01:34', '2024-03-24 10:03:41');
INSERT INTO `lc_user_vip` VALUES ('4', '5', '7434343433453343436', '8', '课程专区VIP', null, 'kecheng', '1.00', '1', null, '2024-03-24 10:03:12', '2025-03-24 10:03:15', '0', '2024-03-24 10:03:21', '2024-03-26 09:33:20');

-- ----------------------------
-- Table structure for lc_user_wallet
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_wallet`;
CREATE TABLE `lc_user_wallet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '余额',
  `current_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '可提现余额',
  `retain_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '不可提现余额',
  `total_profit` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '总收益',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=434 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_wallet
-- ----------------------------

-- ----------------------------
-- Table structure for lc_user_wallet_log
-- ----------------------------
DROP TABLE IF EXISTS `lc_user_wallet_log`;
CREATE TABLE `lc_user_wallet_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `project` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '项目表',
  `project_id` char(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '项目ID',
  `money` decimal(10,2) unsigned NOT NULL COMMENT '金额',
  `after` decimal(10,2) unsigned NOT NULL COMMENT '更新后的金额',
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '类型：+/-',
  `source` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '来源',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_user_wallet_log
-- ----------------------------

-- ----------------------------
-- Table structure for lc_vip
-- ----------------------------
DROP TABLE IF EXISTS `lc_vip`;
CREATE TABLE `lc_vip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ziyuan/kecheng',
  `category_id` char(5) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源分类id，all为所有资源类型',
  `day` int(5) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启，1开启，0不开启',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_vip
-- ----------------------------
INSERT INTO `lc_vip` VALUES ('1', '全站资源会员', '标记“会员免费”的所有资源可免费下载，每天可免费下载5个资源', 'ziyuan', 'all', '365', '128.00', '1', '2024-03-22 11:47:24', '2024-03-22 11:47:24');
INSERT INTO `lc_vip` VALUES ('2', '网站源码VIP', '标记“会员免费”的【网站源码】类别下所有资源可免费下载，每天可免费下载5个资源', 'ziyuan', '1', '365', '68.00', '1', '2024-03-22 11:47:58', '2024-03-22 11:47:58');
INSERT INTO `lc_vip` VALUES ('3', '软件应用VIP', '标记“会员免费”的【软件应用】类别下所有资源可免费下载，每天可免费下载5个资源', 'ziyuan', '4', '365', '68.00', '1', '2024-03-22 11:48:32', '2024-03-22 11:48:32');
INSERT INTO `lc_vip` VALUES ('4', '视频资源VIP', '标记“会员免费”的【视频资源】类别下所有资源可免费下载，每天可免费下载5个资源', 'ziyuan', '5', '365', '58.00', '1', '2024-03-22 11:49:09', '2024-03-22 11:49:09');
INSERT INTO `lc_vip` VALUES ('5', '课程专区VIP', '课程专区内所有标记“会员免费”的课程可免费观看', 'kecheng', null, '365', '18.00', '1', '2024-03-22 11:49:27', '2024-03-22 11:49:27');

-- ----------------------------
-- Table structure for lc_wx_binding
-- ----------------------------
DROP TABLE IF EXISTS `lc_wx_binding`;
CREATE TABLE `lc_wx_binding` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `nickname` char(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '微信昵称',
  `type` tinyint(3) unsigned NOT NULL COMMENT '1绑定 0解绑',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_wx_binding
-- ----------------------------

-- ----------------------------
-- Table structure for lc_ziyuan
-- ----------------------------
DROP TABLE IF EXISTS `lc_ziyuan`;
CREATE TABLE `lc_ziyuan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `category_id` int(10) unsigned NOT NULL COMMENT '分类id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '资源名称',
  `cover` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '封面，数组格式',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '售价',
  `demo_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '演示地址',
  `download_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '下载地址',
  `extract` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '提取密码/解压密码',
  `audio` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '试听音频地址',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '标签',
  `abstract` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '摘要',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '内容详情',
  `collect_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数',
  `download_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载数',
  `report_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '举报次数',
  `read_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读数',
  `praise_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `is_top` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '1置顶 0不置顶',
  `self_visible` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '仅自己可见，0不是 1是',
  `ziyuan_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '资源类型，1资源地址，2本地上传',
  `is_kami` tinyint(3) unsigned DEFAULT '0' COMMENT '是否是卡密',
  `kami` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '卡密内容，多条卡密请换行',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0待审核 1审核通过 2审核未通过 3草稿箱 -1删除',
  `fail_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '审核失败原因',
  `audit_time` char(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源审核时间',
  `vip_dis` decimal(3,2) unsigned DEFAULT '1.00' COMMENT '1为不享受折扣，0.5为5折，0为vip免费',
  `ziyuan_update_time` char(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源更新时间',
  `biaoshi` char(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '标识：用于标识第三方平台导入的数据',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=29675 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_ziyuan
-- ----------------------------

-- ----------------------------
-- Table structure for lc_ziyuan_category
-- ----------------------------
DROP TABLE IF EXISTS `lc_ziyuan_category`;
CREATE TABLE `lc_ziyuan_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `title` char(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '分类名称',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标地址',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '跳转url',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '关键词',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1显示 0不显示',
  `home_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否在首页显示 1显示 0不显示',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_ziyuan_category
-- ----------------------------
INSERT INTO `lc_ziyuan_category` VALUES ('1', '0', '网站源码', 'http://gzy.data.gouziyuan.cn/gouziyuan/icon/code.png', '', '网站源码,,PHP源码,PHP,网站模板,软件源码,游戏源码,资源分享,网站插件,源码分享,商业源码,源码教程,免费源码', '资交网源码提供最新海量网站源码、破解软件、游戏源码、免费源码、小程序源码、模板插件、商业源码等供站长下载学习！分享最具价值的内容，是一个专注于收集精品源码的专业性网站！', '0', '1', '1', '2022-07-27 15:43:08', '2024-03-20 09:46:43');
INSERT INTO `lc_ziyuan_category` VALUES ('2', '0', '设计素材', 'http://gzy.data.gouziyuan.cn/gouziyuan/icon/design1.png', '', null, null, '0', '1', '1', '2022-07-27 15:43:44', '2022-11-10 15:37:39');
INSERT INTO `lc_ziyuan_category` VALUES ('3', '0', '办公范文', 'http://gzy.data.gouziyuan.cn/gouziyuan/icon/office1.png', '', null, null, '0', '1', '1', '2022-07-27 15:44:02', '2022-11-10 15:37:37');
INSERT INTO `lc_ziyuan_category` VALUES ('4', '0', '软件应用', 'http://gzy.data.gouziyuan.cn/gouziyuan/icon/software.png', '', null, null, '0', '1', '1', '2022-07-27 15:44:20', '2022-11-07 14:23:50');
INSERT INTO `lc_ziyuan_category` VALUES ('5', '0', '视频', 'http://gzy.data.gouziyuan.cn/gouziyuan/icon/video.png', '', null, null, '0', '1', '1', '2022-07-27 15:44:36', '2022-11-07 14:23:08');
INSERT INTO `lc_ziyuan_category` VALUES ('6', '0', '音频', 'http://gzy.data.gouziyuan.cn/gouziyuan/icon/audio.png', '', null, null, '0', '1', '1', '2022-07-27 15:44:58', '2022-11-07 14:23:36');
INSERT INTO `lc_ziyuan_category` VALUES ('7', '0', '其他资源', 'http://gzy.data.gouziyuan.cn/gouziyuan/icon/other.png', '', null, null, '0', '1', '1', '2022-07-27 15:45:10', '2022-11-07 14:23:26');
INSERT INTO `lc_ziyuan_category` VALUES ('8', '1', 'html模板', '', '', null, null, '0', '1', '1', '2022-07-27 15:46:54', '2023-01-07 12:01:28');
INSERT INTO `lc_ziyuan_category` VALUES ('9', '1', '后台模板', '', '', null, null, '0', '1', '1', '2022-07-27 15:47:04', '2022-07-27 15:47:07');
INSERT INTO `lc_ziyuan_category` VALUES ('10', '1', '手机网站', '', '', null, null, '0', '1', '1', '2022-07-27 15:47:26', '2022-07-27 15:47:28');
INSERT INTO `lc_ziyuan_category` VALUES ('11', '1', '整站源码', '', '', null, null, '0', '1', '1', '2022-07-27 15:47:35', '2022-07-27 15:47:37');
INSERT INTO `lc_ziyuan_category` VALUES ('12', '2', 'PSD', '', '', null, null, '0', '1', '1', '2022-07-27 15:48:09', '2022-07-27 15:48:11');
INSERT INTO `lc_ziyuan_category` VALUES ('13', '2', 'AI', '', '', null, null, '0', '1', '1', '2022-07-27 15:48:20', '2022-07-27 15:48:22');
INSERT INTO `lc_ziyuan_category` VALUES ('14', '2', 'JPG/PNG', '', '', null, null, '0', '1', '1', '2022-07-27 15:48:31', '2022-12-01 10:34:51');
INSERT INTO `lc_ziyuan_category` VALUES ('15', '2', 'CDR', '', '', null, null, '0', '1', '1', '2022-07-27 15:48:49', '2022-07-27 15:48:51');
INSERT INTO `lc_ziyuan_category` VALUES ('16', '2', 'CAD', '', '', null, null, '0', '1', '1', '2022-07-27 15:49:01', '2022-07-27 15:49:04');
INSERT INTO `lc_ziyuan_category` VALUES ('17', '2', 'C4D', '', '', null, null, '0', '1', '1', '2022-07-27 15:49:22', '2022-07-27 15:49:25');
INSERT INTO `lc_ziyuan_category` VALUES ('18', '2', '其他', '', '', null, null, '1', '1', '1', '2022-07-27 15:49:36', '2022-12-05 23:09:18');
INSERT INTO `lc_ziyuan_category` VALUES ('19', '3', 'Word', '', '', null, null, '0', '1', '1', '2022-07-27 15:49:54', '2022-07-27 15:49:57');
INSERT INTO `lc_ziyuan_category` VALUES ('20', '3', 'Excel', '', '', null, null, '0', '1', '1', '2022-07-27 15:50:07', '2022-07-27 15:50:36');
INSERT INTO `lc_ziyuan_category` VALUES ('21', '3', 'PPT', '', '', null, null, '0', '1', '1', '2022-07-27 15:50:17', '2022-07-27 15:50:37');
INSERT INTO `lc_ziyuan_category` VALUES ('22', '3', '其他', '', '', null, null, '0', '1', '1', '2022-07-27 15:50:27', '2022-07-27 15:50:39');
INSERT INTO `lc_ziyuan_category` VALUES ('23', '4', 'windows', '', '', null, null, '0', '1', '1', '2022-07-27 15:50:57', '2022-12-05 23:10:33');
INSERT INTO `lc_ziyuan_category` VALUES ('24', '4', 'Android', '', '', null, null, '0', '1', '1', '2022-07-27 15:51:10', '2022-12-05 23:08:06');
INSERT INTO `lc_ziyuan_category` VALUES ('25', '5', '课程视频', '', '', null, null, '0', '1', '1', '2022-07-27 15:51:25', '2022-07-27 15:51:25');
INSERT INTO `lc_ziyuan_category` VALUES ('26', '5', '视频素材', '', '', null, null, '0', '1', '1', '2022-07-27 15:51:41', '2022-07-27 15:51:41');
INSERT INTO `lc_ziyuan_category` VALUES ('42', '2', 'AE', '', '', null, null, '0', '1', '1', '2022-11-30 18:08:32', '2022-11-30 18:08:32');
INSERT INTO `lc_ziyuan_category` VALUES ('43', '2', 'PR', '', '', null, null, '0', '1', '1', '2022-11-30 18:08:39', '2022-11-30 18:08:39');
INSERT INTO `lc_ziyuan_category` VALUES ('44', '4', 'IOS', '', '', null, null, '0', '1', '1', '2022-12-05 23:08:17', '2022-12-05 23:08:17');
INSERT INTO `lc_ziyuan_category` VALUES ('45', '4', 'MAC', '', '', null, null, '0', '1', '1', '2022-12-05 23:08:50', '2022-12-05 23:08:50');
INSERT INTO `lc_ziyuan_category` VALUES ('46', '4', 'Linux', '', '', null, null, '0', '1', '1', '2022-12-05 23:09:07', '2022-12-05 23:10:21');
INSERT INTO `lc_ziyuan_category` VALUES ('47', '6', '背景音乐', null, null, null, null, '0', '1', '1', '2023-01-03 00:03:39', '2023-01-03 00:03:41');
INSERT INTO `lc_ziyuan_category` VALUES ('48', '6', '音效', null, null, null, null, '0', '1', '1', '2023-01-03 00:05:29', '2023-01-03 00:05:29');
INSERT INTO `lc_ziyuan_category` VALUES ('49', '6', '音频素材包', null, null, null, null, '0', '1', '1', '2023-07-18 22:12:23', '2023-07-18 22:12:23');
INSERT INTO `lc_ziyuan_category` VALUES ('50', '1', '插件', null, null, null, null, '0', '1', '1', '2024-02-01 19:25:13', '2024-02-01 19:25:13');

-- ----------------------------
-- Table structure for lc_ziyuan_collect
-- ----------------------------
DROP TABLE IF EXISTS `lc_ziyuan_collect`;
CREATE TABLE `lc_ziyuan_collect` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ziyuan_id` int(10) unsigned NOT NULL,
  `ziyuan_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '资源名称',
  `ziyuan_cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '封面',
  `price` decimal(10,2) unsigned DEFAULT '0.00',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `ziyuan_id` (`ziyuan_id`),
  KEY `ziyuan_title` (`ziyuan_title`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_ziyuan_collect
-- ----------------------------

-- ----------------------------
-- Table structure for lc_ziyuan_read
-- ----------------------------
DROP TABLE IF EXISTS `lc_ziyuan_read`;
CREATE TABLE `lc_ziyuan_read` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ziyuan_id` int(10) unsigned NOT NULL,
  `ziyuan_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '资源名称',
  `ziyuan_cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '封面',
  `price` decimal(10,2) unsigned DEFAULT '0.00',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `ziyuan_id` (`ziyuan_id`),
  KEY `ziyuan_title` (`ziyuan_title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lc_ziyuan_read
-- ----------------------------
