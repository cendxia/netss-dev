export default ({$axios}, inject) => {
	// 获取所有类别
    inject('zyGetCategory', (subclass=false) => $axios.get('/getCategorys?subclass='+subclass));

    // 获取资源分类详情
    inject('zyGetCategoryInfo', (category_id) => $axios.get('/getCategoryInfo?category_id='+category_id));

    // 获取资源详情
    inject('zyGetInfo', (id, info='') => $axios.get('/getZiyuanInfo?id='+id+'&info='+info));

    // 获取推荐资源
    inject('zyGetTuijian', (category_id) => $axios.get('/getTuijianZiyuan?category_id='+category_id));

    // 获取资源列表
    inject('zyGetZiyuanList', (category_id='', user_id='', page=1, pageSize=20, all='') => $axios.get('/getZiyuanList?category_id='+category_id+'&user_id='+user_id+'&page='+page+'&pageSize='+pageSize+'&all='+all));

    // 随机获取资源列表
    inject('zyGetZiyuanListRandom', (page=1, pageSize=20) => $axios.get('/getZiyuanListRandom?page='+page+'&pageSize='+pageSize));

    // 获取浏览量最高的资源列表
    inject('zyGetZiyuanListBrowse', (page=1, pageSize=20) => $axios.get('/getZiyuanListBrowse?page='+page+'&pageSize='+pageSize));

    // 获取专辑列表
    inject('zyGetZhuanjiList', (user_id) => $axios.get('/getZhuanjiList?user_id='+user_id));

    // 根据分类id获取兄弟分类信息
    inject('zyGetBrotherCategorys', (category_id) => $axios.get('/getBrotherCategorys?category_id='+category_id));

    // 收藏/取消收藏
    inject('zySetCollect', (data) => $axios.post('/setCollect', data));

    // 搜索
    inject('zySearch', (keyWord, category_id, page, pageSize) => $axios.get('/search?keyWord='+keyWord+'&category_id='+category_id+'&page='+page+'&pageSize='+pageSize));

    // 资源下载

    // 获取分类所有一级分类下所有的资源
    inject('zyGetCategoryZiyuan', (pageSize=15) => $axios.get('/getCategoryZiyuan?pageSize='+pageSize));

    // 获取Ta的资源
    inject('zyGetUserZiyuan', (user_id, ziyuan_id='') => $axios.get('/getUserZiyuan?user_id='+user_id+'&ziyuan_id='+ziyuan_id));

    // 获取专辑信息
    inject('getZhuanjiInfo', (zhuanji='') => $axios.get('/getZhuanjiInfo?zhuanji='+zhuanji));


    // 获取标签列表
    inject('getLabelList', (num='') => $axios.get('/getLabelList?num='+num));
}
