export default ({$axios}, inject) => {
    // 用户注册
    inject('userRegister', (data) => $axios.post('/register', data))
    // 用户登录
    inject('userLogin', (data) => $axios.post('/login', data))
    // 退出登录
    inject('loginOut', () => $axios.post('/loginOut'))
    // 获取用户详情
    inject('getUserInfo', () => $axios.get('/getUserInfo'))
    // 修改用户信息
    inject('setUser', (data) => $axios.post('/setUser', data))
    // 找回密码
    // 获取登录二维码
    // 微信登录回调
    // 微信扫描登录时获取登录信息
    // 获取粉丝列表
    inject('userGetFansList', (page=1, pageSize=20, user_id='') => $axios.get('/getFansList?user_id='+user_id+'&page='+page+'&pageSize='+pageSize))
    // 获取关注列表
    inject('userGetFollowList', (page=1, pageSize=20, user_id='') => $axios.get('/getFollowList?user_id='+user_id+'&page='+page+'&pageSize='+pageSize))
    // 创作者基础信息
    // 提交订单
    // 获取下载记录
    // 获取订单列表
    // 获取订单详情
    // 获取资源/课程订单详情
    // 查询订单状态
    // 获取收藏列表
    inject('userGetCollectList', (page=1, pageSize=20) => $axios.get('/getCollectList?page='+page+'&pageSize='+pageSize))
    // 关注/取消关注
    inject('userFollow', (data) => $axios.post('/follow', data))
    // 获取登录日志
    inject('userLoginLog', () => $axios.get('/loginLog'))
    // 获取用户操作日志
    inject('userActionLog', () => $axios.get('/actionLog'))
    // 获取用户最新状态
    inject('getUpUserStatus', () => $axios.get('/upUserStatus'))
    // 提交反馈
    inject('userFeedback', (data) => $axios.post('/feedback', data))
    // 获取绑定微信需要的参数
    // 获取带参数的二维码，用户绑定微信
    // 获取带参数的二维码进行登录
    // 前端通过api查询微信扫码关注登录信息


    // 获取VIP列表
    // 提交VIP订单
    // 查询VIP订单状态
    // 查询我的VIP记录

    // 获取百度网盘下载工具
}
