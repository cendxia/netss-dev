export default ({$axios}, inject) => {
    
    // 获取文章栏目
    inject('getArticleColumn', (id) => $axios.get('/getArticleColumn?id='+id));

    // 获取栏目文章
    inject('getColumnArticleList', (category_id, page=1, pageSize=20) => $axios.get('/getColumnArticleList?category_id='+category_id+'&page='+page+'&pageSize='+pageSize));

    // 获取文章详情
    inject('getArticleInfo', (id) => $axios.get('/getArticleInfo?id='+id));
    
    // 文章搜索
    inject('getArticleSearch', (page=1, pageSize=20, keyWord='') => $axios.get('/getArticleSearch?page='+page+'&pageSize='+pageSize+'&keyWord='+keyWord));

}
