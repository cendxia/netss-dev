export default ({$axios}, inject) => {
    // 获取课程列表
    inject('getKeList', (page=1, pageSize=20) => $axios.get('/getKeList?page='+page+'&pageSize='+pageSize))
    // 获取课程详情
    inject('getKeInfo', (ke_id,order_id='',chapters_id='',directory_id='') => $axios.get('/getKeInfo?ke_id='+ke_id+'&order_id='+order_id+'&chapters_id='+chapters_id+'&directory_id='+directory_id))
    // 提交订单
    inject('submitKechengOrder', (data) => $axios.post('/submitKechengOrder', data))
    // 查询订单状态
    inject('getKechengOrderStatus', (order_id='') => $axios.get('/getKechengOrderStatus?order_id='+order_id))
    // 获取课程订单列表
    inject('getKechengOrderList', (page=1,pageSize=20) => $axios.get('/getKechengOrderList?page='+page+'&pageSize='+pageSize))
    // 获取课程订单详情
    inject('getKechengOrderInfo', (order_id) => $axios.get('/getKechengOrderInfo?order_id='+order_id))
    // 获取试看课程列表
    inject('getKeTryList', (page=1,pageSize=20) => $axios.get('/getKeTryList?page='+page+'&pageSize='+pageSize))

}
