export default ({$axios}, inject) => {
    // 发送短信验证码

    // 发送邮件

    // 获取系统配置信息
    inject('getSystemConfig', () => $axios.get('/getSystemConfig'))
    // 获取七牛云上传token

    // 获取微信小程序支持平台

    // 下载

    // 获取首页平台数据，平台流量、资源总数、下载次数
    inject('getIndexTongji', () => $axios.get('/getIndexTongji'))

}
