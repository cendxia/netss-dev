export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'NETSS系统',
        htmlAttrs: {
            lang: 'en'
        },
        bodyAttrs: {
            style: 'margin:0'
        },
        meta: [{
                charset: 'utf-8'
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                name: 'format-detection',
                content: 'telephone=no'
            },
            {
                hid: 'keywords',
                name: 'keywords',
                content: ''
            },
            {
                hid: 'description',
                name: 'description',
                content: ''
            }
        ],
        link: [{
            rel: 'icon',
            type: 'image/x-icon',
            href: '/favicon.ico'
        }],
        script: [
            { src: 'https://hm.baidu.com/hm.js?40c3f73a27b964d6aa85d7b0ac073045' }
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '~/assets/css/free.css',
        '~/assets/css/animate.css'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [{
            src: '~/plugins/antd-ui.js'
        },
        {
            src: '~/plugins/baidu-tongji.js'
        },
        {
            src: '~/plugins/wangEditor.js',
            ssr: false
        },
        {
            src: '~/plugins/echarts.js'
        },
        {
            src: '~/plugins/axios.js',
            ssr: true
        },
        {
            src: '~/api/user.js'
        },
        {
            src: '~/api/ziyuan.js'
        },
        {
            src: '~/api/pay.js'
        },
        {
            src: '~/api/common.js'
        },
        {
            src: '~/api/article.js'
        },
        {
            src: '~/api/kecheng.js'
        },
        {
            src: '~/plugins/videoPlayer.js',
            ssr: false
        }
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    axios: {
        proxy: true,
        prefix: '/api'
    },
    proxy: {
        '/api': {
            target: 'http://api.gzy.com/v1', //要代理的后端地址
            pathRewrite: {
                '^/api': ''
            },
            // changeOrigin: true
        }
    },

    // 这块配置要写上
    server: {
        port: 3000, //这块要自定义,否则多个肯定有端口冲突
        host: '0.0.0.0'
    },


    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        'cookie-universal-nuxt',
        '@nuxtjs/router',
        '@nuxtjs/axios',
        '@nuxtjs/proxy'
    ],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {

    },

    loading: '~/components/common/loading-auto.vue'

}
