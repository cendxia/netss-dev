import {message} from 'ant-design-vue';

export default function ({$axios, store, $cookies}) {
    // 超时时间为8秒
    $axios.defaults.timeout = 800000

    // 请求拦截
    $axios.onRequest(config => {
        let token = store.state.token
        token && (config.headers.token = token)
        return config;
    },error => {
        return Promise.reject(error)
    })

    // 响应拦截
    $axios.onResponse(response => {
        let res = response.data;
        if (res.status == -1){
            // 未登录，请前往登录
            $cookies.set('gouziyuanUser','')
            // 获取登录前的url，用于登录后自动跳转到该url
            let url = window.location.pathname
            $cookies.set('beforeUrl', url)
            window.location.href = '/login'
        }else if(res.status == 2){
            // 未申请创作者
            message.error('当前账户不是创作者账户！')
            window.location.href = '/mp-register'
        }else if(res.status == 3){
            message.error('无权限！')
            window.location.href = '/'
        }else if (res.status != 1) {
            message.error(res.message)
        }
        return res;
    },error => {
        let { response } = error
        if(response){
            // 服务器最起码返回结果了
            message.error(response.data.message)
        }else{
            // 服务器连结果都没有返回
            if(!window.navigator.onLine){
                // 客户端断网处理
            }
            return Promise.reject(error)
        }
    })
}
